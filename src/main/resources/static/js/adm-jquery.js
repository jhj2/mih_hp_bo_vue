$(document).ready(function(){
    $('.lnb_menu.drop').on('click',function(){
        $(this).next('div.lnb_dropdown').stop().slideToggle(100);
        $(this).toggleClass('close');
    });

    //datepick
    $(".datepick").flatpickr({
        dateFormat: "Y.m.d",
        locale:'ko'
    });
});

function pop_close(){
    $(".popup").hide();
    $('.overlay').hide();
}