$.ajaxSetup ({
	// Disable caching of AJAX responses */
	cache: false,
	headers: { "x-ajax-request" : true, "Accept" : "application/json" },
	//accepts: "application/json; charset=utf-8",
	error: function(err){
		alert(err.responseJSON.message);
	},
});

// 회원정보 팝업 조회
function inqireSearch(){

	if($("select[name=indvdlInfoInqireResnCd]").val() == ""){
		alert("개인정보 조회 목적/사유 항목을 선택하세요.");
		return false;
	}

	if($("select[name=indvdlInfoInqireResnCd]").val() == "A1407"){
		if($("#indvdlInfoInqireResnRm").val() == ""){
			alert("기타 목적/사유를 입력해주세요.");
			return false;
		}
	}
	
	var param = {
		"indvdlInfoInqireResnCd" : $("select[name=indvdlInfoInqireResnCd]").val()
		, "indvdlInfoInqireResnRm" : $("input[name=indvdlInfoInqireResnRm]").val()
		, "mberIdx" : $("#mberIdx").val()
	}

	$.ajax({
		url  : "/member/com/infoDetail",
		type : "post",
		data : param,
		dataType : "JSON",
		success : function(res){
			// 변수 선언
			var result = res.resultVO;
			console.log("res====");
			console.log(res);
			
			var rdnmadrBass = ((result.rdnmadrBass == null) ? "" : result.rdnmadrBass);
			var rdnmadrDetail = ((result.rdnmadrDetail == null) ? "" : result.rdnmadrDetail);
			var mberNm = ((result.mberNm == null) ? "" : result.mberNm);
			var mberIdx = ((result.mberIdx == null) ? "" : result.mberIdx);
			var cardName = ((result.cardName == null) ? "" : result.cardName);
			var cardMaskNo = ((result.cardMaskNo == null) ? "" : result.cardMaskNo);
			var hpNo1 = ((result.hpNo1 == null) ? "" : result.hpNo1);
			var hpNo2 = ((result.hpNo2 == null) ? "" : result.hpNo2);
			var hpNo3 = ((result.hpNo3 == null) ? "" : result.hpNo3);
			var refndBankNm = ((result.refndBankNm == null) ? "" : result.refndBankNm);
			var refndAcnutno = ((result.refndAcnutno == null) ? "" : result.refndAcnutno);
			var refndDpstr = ((result.refndDpstr == null) ? "" : result.refndDpstr);
			var email = ((result.email == null) ? "" : result.email);
			
			// 주소, 이름/고유번호, 결재카드, 휴대폰, 환불계좌, 이메일 비식별 => 식별로 조회
			$("#addr").html(rdnmadrBass + " " + rdnmadrDetail);
			$("span[id=mberNm]").html(mberNm + " / " + mberIdx);
			$("#cardInfo").html(cardName + " " + cardMaskNo);
			$("#mobile").html(hpNo1 + "-" + hpNo2 + "-" + hpNo3);
			$("#refndBank").html(refndBankNm + " " + refndAcnutno + " " + refndDpstr);
			$("#email").html(email);
		},
		error: function(xhr, type){
			console.log(xhr);
		}
	});
}

$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	
$(function() {	
	goPage = function(pageNo, formName){
	console.log("pageNo======");
	console.log(pageNo);
	console.log("formName===");
	console.log(formName);
		$("#"+formName).find("#currentPageNo").val(pageNo);
		$("#"+formName).submit();
	};
	
	// 공통팝업(memberPopup.jsp)에서 사용하는 ajax 페이징처리 함수
	msgSendGoPage = function(pageNo, formName){
		
		var mberIdx = $("#"+formName).find("#mberIdx").val();
		//$("#"+formName).find("#currentPageNo").val(pageNo);
		
		var param = {
			"currentPageNo" : pageNo
			, "mberIdx" : mberIdx
		}
	
		$.ajax({
			url  : "/member/com/msgInfo",
			type : "post",
			data : param,
			dataType : "JSON",
			success : function(res){
				console.log(res);
				var result = res.resultList;
				var paging = res.paging;
				var html = "";
				
				$(".test").empty();
				$.each(result, function(i){
					
					html += '<tr>';
					html += '<td>'+ (res.totCnt-((paging.currentPageNo-1) * paging.recordCountPerPage + i)) + '</td>';
					html += '<td>'+result[i].msgSndngDt+'</td>';
					html += '<td>'+result[i].msgCoursTyNm+'</td>';
					html += '<td class="text_left"><pre>'+result[i].msgCn+'</pre></td>';
					html += '</tr>';
				});
				$(".test").append(html);
				$("#pagination").html(paging.pagination);
			},
			error: function(xhr, type){
				console.log(xhr);
			}
		});
	};
	
	formValidation = function(formName){
		//console.log(11111111);
		var formInstance = $('#'+formName).parsley();
		if(!formInstance.isValid()){
			for (var idx in formInstance.fields) {
				if(formInstance.fields[idx].validationResult != true){
					//console.log(formInstance.fields[idx]);
					formInstance.fields[idx].element.focus();
					alert(formInstance.fields[idx].domOptions.errorMessage);
					return false;
				}
			}
		}
		return true;
	};
	
	formInit = function(formName){
		$("#"+formName)[0].reset();
	};
	
	$("[id^=amodal_]").on("click", function () {
		var id = $(this).attr("href");
		$.ajax({
		    url : $(this).attr("data-modal-url"),
		    success : function(data){
		    	$(""+id).html(data);
		 	}
		});
	});
});


$valid = {
	//날짜 체크
	chkDate : function(fromStr, toStr){
		var fromArr = fromStr.split('.');
	    var toArr = toStr.split('.');

	    var fromDate = new Date(fromArr[0], fromArr[1], fromArr[2]);
	    var toDate = new Date(toArr[0], toArr[1], toArr[2]);

	
	    if (fromStr != '' && toStr == '') {
	    	alert('종료일을 입력해주세요.');
			return false;
		}

	    if (fromStr == '' && toStr != '') {
	    	alert('시작일을 입력해주세요.');
			return false;
		}
		
		if (toDate < fromDate) {
			alert('시작일은 종료일 이전에만 선택가능 합니다.');
			return false;
		}
		
		return true; 
	}
}