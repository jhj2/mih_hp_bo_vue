// 유틸.
let Util = {

    /**
     * 월
     *
     * @param obj
     */
    setMonth: function (_obj, _selectedVal) {
        let month = "";
        let selected = "";
        for (let i = 1; i <= 12; i++) {

            if (i <= 9) {
                month = "0" + i;
            } else {
                month = i;
            }


            selected = month == _selectedVal ? "selected" : "";

            _obj.append("<option value='" + month + "' " + selected + ">" + month + "</option>");
        }
    },

    /**
     * 일
     *
     * @param obj
     */
    setDay: function (_obj, _selectedVal) {
        let day = "";
        let selected = "";
        for (let i = 1; i <= 29; i++) {

            if (i <= 9) {
                day = "0" + i;
            } else {
                day = i;
            }

            selected = day == _selectedVal ? "selected" : "";

            _obj.append("<option value='" + day + "' " + selected + ">" + day + "</option>");

            if (day == 29) {
                _obj.append("<option value='" + 30 + "' " + selected + ">말일</option>");
            }

        }
    },

    /**
     * 시간
     *
     * @param obj
     */
    setHour: function (_obj, _selectedVal) {
        let time = "";
        let selected = "";
        for (let i = 0; i < 24; i++) {

            if (i <= 9) {
                time = "0" + i;
            } else {
                time = i;
            }

            selected = time == _selectedVal ? "selected" : "";

            _obj.append("<option value='" + time + "' " + selected + ">" + time + "</option>");
        }
    },

    /**
     * 분
     *
     * @param obj
     */
    setMinute: function (_obj, _selectedVal) {
        let time = "";
        let selected = "";
        for (let i = 0; i < 60; i++) {

            if (i <= 9) {
                time = "0" + i;
            } else {
                time = i;
            }

            selected = time == _selectedVal ? "selected" : "";

            _obj.append("<option value='" + time + "' " + selected + ">" + time + "</option>");
        }
    },

    /**
     * * 숫자만 입력 이벤트.
     *
     * @param objId
     * @returns {Boolean}
     */
    onlyNum: function (_objId) {
        let $obj = $("#" + _objId);
        $obj.keyup(function () {
            let inputVal = $(this).val();
            $(this).val(inputVal.replace(/[^0-9]/g, ""));
        });
    },

    /**
    * * 숫자만 입력 이벤트.
    *
    * @param objId
    * @returns {Boolean}
    */
   blurOnlyNum: function (obj) {
       let inputVal = $(obj).val();
       $(obj).val(inputVal.replace(/[^0-9]/g, ""));
   },

    /**
     * 시간 체크
     * @param _objId 대상id
     * @param _checkNum 체크 숫자
     */
    checkTime: function (_objId, _checkNum) {
        let $obj = Util.makeJqueryObj(_objId);
        $obj.on('keyup', function () {
            if (isNaN($(this).val())) {
                $(this).val("");
                $(this).focus();
            }
            if (Number($(this).val()) > _checkNum) {
                $(this).val("");
                $(this).focus();
            }
        });
    },

    getOnlyNum: function (str) {
        return str.replace(/[^0-9]/g, "");
    },

    /**
     * 이미지 여부
     *
     * @param val
     * @returns {Boolean}
     */
    isImageFile: function (_val) {
        if (_val.length > 0) {
            let regExp = /(.jpg|.jpeg|.gif|.png)$/i;

            if (regExp.test(_val)) {
                return true;
            }
        } else if (_val == '') {
            return false;
        }
        return false;
    },

    /**
     * jpg 파일 여부
     *
     * @param val
     * @returns {Boolean}
     */
    isJpegFile: function (_val) {
        if (_val.length > 0) {
            let regExp = /(.jpg|.jpeg)$/i;

            if (regExp.test(_val)) {
                return true;
            }
        } else if (_val === '') {
            return false;
        }

        return false;
    },

    /**
     * 입력 값이 유효한 이메일 형식인지를 검사한다.
     *
     * @param val
     * @returns {Boolean}
     */
    checkEmail: function (_val) {
        if (_val.length > 0) {
            let regExp = /[a-z0-9]{2,}@[a-z0-9-]{2,}\.[a-z0-9]{2,}/i;

            if (!regExp.test(_val)) {
                return false;
            }
        } else if (_val == '') {
            return false;
        }

        return true;
    },

    /**
     * 핸드폰 번호 유효성 체크 (format : '-' 없이)
     *
     * @param val
     * @returns {Boolean}
     */
    checkMobile: function (_val) {
        if (_val.length > 0) {
            let regExp = /^01[016789]\d{3,4}\d{4}$/;

            if (!regExp.test(_val)) {
                return true;
            }
        } else if (_val == '') {
            return true;
        }

        return false;
    },

    /**
     * 핸드폰 번호에 '-' 를 넣는다.
     *
     * @param str
     * @returns
     */
    toMobileFormat: function (_str) {
        return _str.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1-$2-$3");
    },

    /**
     * 일반전화 번호에 '-' 를 넣는다.
     *
     * @param str
     * @returns
     */
    toTelFormat: function (_str) {
        return _str.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/, "$1-$2-$3");
    },

    /**
     * 일반 전화 체크(format : '-' 없이)
     *
     * @param val
     * @returns {Boolean}
     */
    checkPhone: function (_val) {
        if (Util.contain(_val, "-")) {
            _val = Util.replaceAll(_val, "-", "");
        }

        if (_val.length > 0) {
            let regExp = /^\d{2,3}\d{3,4}\d{4}$/;
            if (!regExp.test(_val)) {
                return true;
            }
        } else if (_val == '') {
            return true;
        }

        return false;
    },

    /**
     * 통화 표시
     * @param num
     */
    numberWithCommas: function (num) {
        if (Util.isEmpty(num)) {
            return "";
        }
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },

    /**
     * 숫자와 . 만 입력가능. 소수점 두번째 자리까지 체크한다.
     */
    checkPoint: function (_val) {
        let flag = false;
        let regExp = /^[0-9.]*$/i;

        if (!regExp.test(_val)) {
            alert("입력글자에 숫자와 점 이외에 다른값이 있습니다.");
            flag = true;
        } else {
            let temps = _val.split(".");
            if (2 < temps.length) {
                alert("소수점이 한개 이상 존재 합니다.");
                flag = true;
            } else if (1 == temps.length) {
                return false;
            } else {
                if (2 < temps[1].length) {
                    alert("소수점이하 둘째 이상 존재 합니다.");
                    flag = true;
                }
            }
        }
        return flag;
    },

    /**
     * 특정 문자를 변환한다.
     *
     * @param _str
     * @param _orgStr
     *            변경될 문자
     * @param _changeStr
     *            변경할 문자
     * @returns
     */
    replaceAll: function (_str, _orgStr, _changeStr) {
        return _str.split(_orgStr).join(_changeStr);
    },

    /**
     * 한글 영문 바이트수
     *
     * @param val
     * @returns {Number}
     */
    byteLength: function (_val) {
        let cnt = 0;
        for (let i = 0; i < _val.length; i++) {
            let c = escape(_val.charAt(i));

            if (c.length == 1) {
                cnt++;
            } else if (c.indexOf("%u") != -1) {
                cnt += 2;
            } else if (c.indexOf("%") != -1) {
                cnt += c.length / 3;
            }
        }
        return cnt;
    },

    /**
     * 전화번호 포멧
     * @param num : 사업자 번호
     * @param type: 0 : 가운데자리 * 표시, 1 : 그냥 표시
     * @returns {*}
     */
    phoneFormatter: function (num, type) {
        let formatNum = "";

        if (Util.isEmpty(num)) {
            return "";
        }

        try {
            if (num.length == 11) {

                if (type == 0) {
                    formatNum = num.replace(/(\d{3})(\d{4})(\d{4})/, '$1-****-$3');
                } else {
                    formatNum = num.replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3');
                }

            } else if (num.length == 8) {
                formatNum = num.replace(/(\d{4})(\d{4})/, '$1-$2');
            } else {

                if (num.indexOf('02') == 0) {
                    if (type == 0) {
                        formatNum = num.replace(/(\d{2})(\d{4})(\d{4})/, '$1-****-$3');
                    } else {
                        formatNum = num.replace(/(\d{2})(\d{4})(\d{4})/, '$1-$2-$3');
                    }
                } else {
                    if (type == 0) {
                        formatNum = num.replace(/(\d{3})(\d{3})(\d{4})/, '$1-***-$3');
                    } else {
                        formatNum = num.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
                    }
                }
            }
        } catch (e) {
            formatNum = num;
            console.log("Util.phoneFormatter : " + e);
        }
        return formatNum;
    },

    /**
     * null 을 _tobe 으로 변경.
     *
     * @param _val
     * @returns
     */
    nvl: function (_val, _tobe) {
        if (_val == null || _val == undefined || _val == "") {
            return _tobe;
        } else {
            return _val;
        }
    },

    /**
     * 마지막 문제 제거
     *
     * @param val
     * @param ch
     * @returns
     */
    deleteLastChar: function (_val, _ch) {
        return _val.substring(0, _val.lastIndexOf(_ch));
    },


    /**
     * 값 null 유무
     *
     * @param val
     * @returns {Boolean}
     */
    isNullValue: function (_val) {
        if (_val != null && _val.length > 0) {
            return false;
        } else {
            return true;
        }
    }
    ,

    /**
     * 비어있는지 확인
     *
     * @param str
     * @returns {Boolean}
     */
    isEmpty: function (_str) {
        return typeof _str == 'undefined' || _str === null || _str.length === 0
            || typeof _str == 'string' && !_str.trim();
    }
    ,

    /**
     * 안비었는지 확인
     *
     * @param str
     * @returns
     */
    isNotEmpty: function (_str) {
        return !Util.isEmpty(_str);
    }
    ,

    /**
     * 쿠키값 가져온다.
     * @returns {string}
     */
    getCookie: function (name) {
        let value = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
        return value ? value[2] : null;
    }
    ,

    /**
     * 쿠키를 설정 한다.
     * @param name
     * @param val
     * @param expiredays
     */
    setCookie: function (name, val, expiredays) {
        let date = new Date();
        date.setDate(date.getDate() + expiredays);
        document.cookie = escape(name) + "=" + escape(val) + "; expires=" + date.toUTCString();
    }
    ,

    /**
     * 문자열 포함 유무.
     *
     * @param str
     * @param findStr
     * @returns {Boolean}
     */
    contain: function (_str, _findStr) {
        return _str.indexOf(_findStr) !== -1;
    }
    ,

    copyObject: function (obj) {
        return JSON.parse(JSON.stringify(obj));
    },

    /**
     * 금액 입력시 자동으로 ',' 붙여 준다.
     * format-name 로 element의 name을 지정 할수 있다.
     * format-name 없이 사용할 경우 id가 name 이된다.
     * @param $obj input element 에 적용.
     * @returns {Object}
     */
    formatNumber: function ($obj) {
        try {
            $obj = $obj.filter(function () {
                return $(this).attr("format") == "amt"
            });
        } catch (e) {
            $obj = undefined
        }

        function _commaAttach(value) {
            return (parseInt(value.replace(/\D/g, "")) + "")
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }

        /**
         * Number Format
         *
         * Jquery Object의 숫자값을 3자리 단위 콤마 형식으로 변경하여 저장한다.
         * 숫자 이외의 문자는 모두 삭제한다.
         *
         * @private
         * @param {$this}
         */
        function _formating($this, event) {
            let chag_offset = 0;
            if (event) {
                let currCommaCnt = _commaAttach($($this).val()).split(",").length - 1;
                let prevCommaCnt = _commaAttach($('#_' + $this.id).val()).split(",").length - 1;
                let curr_offset = event.target.selectionStart;
                let chag_offset = curr_offset + (currCommaCnt - prevCommaCnt);
            }
            // format number
            $($this).val(function (index, value) {
                return _commaAttach(value);
            });

            if (chag_offset > 0) {
                event.target.selectionStar = chag_offset;
                event.target.selectionEnd = chag_offset;
            }

            // comma replace
            let num = $($this).val().replace(/,/gi, "");
            $('#_' + $this.id).val(num || "0");
        }

        /**
         * Initialize
         *
         * 객체 초기화시 입력받은 Jquery Object에 Number Format 이벤트를 바인딩한다.
         *
         * @private
         * @return {Object}
         */
        function _init() {
            let $$obj = $obj || $("input[format='amt']");
            $$obj.each(function () {
                if ($("#_" + this.id).length == 0) {
                    $(this).after('<input type="hidden" name="' + ($(this).attr("format-name") || this.id) + '" id="_' + this.id + '" value="' + $(this).val() + '" >');
                }
                $(this).unbind('keyup');
                $(this).bind('keyup', function (event) {
                    // skip for arrow keys
                    if (event && event.which >= 37 && event.which <= 40 || (event.which == 16)) return;
                    _formating(this, event);
                });
                _formating(this);
            });
            return $$obj;
        }

        return _init();
    }
    ,


    /**
     * 해당 문자열의 Bytes를 리턴한다.
     *
     * @param s
     * @returns {Number}
     */
    getByte: function (_s) {
        let sum = 0;
        let len = _s.length;
        for (let i = 0; i < len; i++) {
            let ch = _s.substring(i, i + 1);
            let en = escape(ch);
            if (en.length <= 4) {
                sum++;
            } else {
                sum += 2;
            }
        }
        return sum;
    }
};




