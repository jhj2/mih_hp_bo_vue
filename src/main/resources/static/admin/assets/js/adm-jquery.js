$(document).ready(function(){
    $('.lnb_menu.drop').on('click',function(){
        $(this).next('div.lnb_dropdown').stop().slideToggle(100);
        $(this).toggleClass('close');
    });

    //datepick
    $(".datepick").flatpickr({
        dateFormat: "Y.m.d",
        //주말제외
        disable: [
            function(date) {
                return (date.getDay() === 0 || date.getDay() === 6);
            }
        ],
        locale:'ko'
    });
});

function pop_close(){
    $(".popup").hide();
    $('.overlay').hide();
}