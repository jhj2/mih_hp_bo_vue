<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form id="adminAddForm" name="adminAddForm" method="post" enctype="multipart/form-data" data-parsley-ui-enabled="false">

<p class="pop_title">관리자 등록</p>
<table cellpadding="0" cellspacing="0" class="t_form">
	<colgroup>
		<col width="150px"><col width="240px"><col width="150px"><col width="240px">
	</colgroup>
	<tbody>
		<tr>
			<td class="th">관리자ID</td>
			<td>
				<input type="text" name="mngrId" id="mngrId" class="w100" maxlength="20"
					data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.admin.mngrId"/>"/>
			</td>
			<td class="th">관리자명</td>
			<td>
				<input type="text" name="mngrNm" id="mngrNm" class="w100" maxlength="33" 
					data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.admin.mngrNm"/>"/>
			</td>
		</tr>
		<tr>
			<td class="th">이메일</td>
			<td>
				<input type="text" id="email" name="email" class="w100" maxlength="40"
					data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.encode.email"/>"/>
			</td>
			<td class="th">핸드폰번호</td>
			<td>
				<input type="text" id="hpNo" name="hpNo" class="w100"
					data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.encode.hpNo"/>"/>
			</td>
		</tr>
		<tr>
			<td class="th">비밀번호</td>
			<td>
				<input type="password" name="mngrPw" id="mngrPw" class="w100" maxlength="20"
					data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.encode.pw"/>"/>
			</td>
			<td class="th"></td>
			<td></td>
		</tr>
	</tbody>
</table>
<div class="btn_wrap">
	<a href="#close-modal" rel="modal:close" class="cancle">취소하기</a>
	<a href="javascript:fn_save();" class="submit">등록하기</a>
</div>
<a href="#close-modal" rel="modal:close" class="close-modal">Close</a>

</form>

<script>
$(function(){
	// 관리자ID 체크
	$("#adminAddForm").find("#mngrId").keyup(function(){
		$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g, ''));
	});
	
	// 이메일 체크
	$("#adminAddForm").find("#email").keyup(function(){
		$(this).val($(this).val().replace(/[^a-zA-Z0-9|._@-]/g, ''));
	});
	
	// 핸드폰 번호 하이픈
	$("#adminAddForm").find("#hpNo").keyup(function(){
		var hpNo = $(this).val().replace(/[^0-9]/g, '');
		$(this).val(phoneHyphen(hpNo));
	});

	// 유효성검사(한글 제외한 영문, 특수문자, 숫자 입력가능한 로직)
	$("#adminAddForm").find("#mngrPw").keyup(function(){
		$(this).val($(this).val().replace(/[\ㄱ-ㅎㅏ-ㅣ가-힣]/g, ""));
	});
});

// 수정하기
function fn_save(){
	var url = "/admin/add";

	// 영문(대문자), 영문(소문자), 숫자, 특수문자 패턴 정의
	var pattern = /[A-Z]/;
	var pattern2 = /[a-z]/;
	var pattern3 = /[0-9]/;
	var pattern4 = /[~!@#$%^&*()-+/]/;

	// 새 비밀번호, 새 비밀번호 확인
	var pw = $("#mngrPw").val();
	
	// validation 체크
	if(formValidation("adminAddForm")){

		// 유효성검사
		if(!emailRegEx($("#email").val()) ){
			alert("유효하지 않은 이메일입니다.");
			return false;
		}
		if(!phoneRegEx($("#hpNo").val()) ){
			alert("유효하지 않은 휴대폰번호입니다.");
			return false;
		}

		// 유효성검사
		if( (!pattern.test(pw) || !pattern2.test(pw) || !pattern3.test(pw) || pw.length<8)
			&& (!pattern.test(pw) || !pattern2.test(pw) || !pattern4.test(pw) || pw.length<8)
			&& (!pattern.test(pw) || !pattern3.test(pw) || !pattern4.test(pw) || pw.length<8)
			&& (!pattern2.test(pw) || !pattern3.test(pw) || !pattern4.test(pw) || pw.length<8) ){

			alert("비밀번호는 영문, 숫자, 특수문자 포함 8자리 이상으로 구성하여야 합니다.");
			$("#mngrPw").val("");
			$("#mngrPw").focus();
			return false;
		}
		
		var frm = $("#adminAddForm")[0];
		frm.action = url;
		frm.submit();
	}
}

// 이메일 정규식
var emailRegEx = function(email){

	var regExp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	if(!regExp.test(email)){
		return false;
	}
	return true;
}

// 핸드폰 정규식
var phoneRegEx = function(phone){

	var regExp = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;
	if(!regExp.test(phone)){
		return false;
	}
	return true;
}

// 핸드폰 하이픈 형태 입력
var phoneHyphen = function(hpNo){
	hpNo = hpNo.replace(/-/g, ''),
	hpNoLength = hpNo.length,
	hpNoStr = "";

	if(hpNoLength > 10) {
		hpNo = hpNo.substring(0, 11);
	}
	hpNoLength = hpNo.length;
	
	if(hpNoLength < 4){
		return hpNo;
	}else if(hpNoLength < 7){
		hpNoStr += hpNo.substr(0, 3);
		hpNoStr += '-';
		hpNoStr += hpNo.substr(3);
		return hpNoStr;
	}else if(hpNoLength < 11){
		hpNoStr += hpNo.substr(0, 3);
		hpNoStr += '-';
		hpNoStr += hpNo.substr(3, 3);
		hpNoStr += '-';
		hpNoStr += hpNo.substr(6);
		return hpNoStr;
	}else{
		hpNoStr += hpNo.substr(0, 3);
		hpNoStr += '-';
		hpNoStr += hpNo.substr(3, 4);
		hpNoStr += '-';
		hpNoStr += hpNo.substr(7);
		return hpNoStr;
	}
}
</script>