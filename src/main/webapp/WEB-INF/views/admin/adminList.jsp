<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
<form id="adminFrm" name="adminFrm" method="get">
	<!-- 검색조건 유지를 위한 hidden값 -->
	<input type="hidden" id="mngrId" name="mngrId" />
	
	<!-- 페이징처리 변수 -->
	<input type="hidden" id="currentPageNo" name="currentPageNo" value="${paging.currentPageNo}"/>
	<input type="hidden" id="pageSize" name="pageSize" value="${paging.pageSize}"/>
	<input type="hidden" id="recordCountPerPage" name="recordCountPerPage" value="${paging.recordCountPerPage}"/>
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 지부정보 관리 start -->
				<h3 class="page_title">관리자 관리</h3>
				<div class="border_box">
					<div class="flex">
						<p class="tit">관리자명</p>
						<div class="con">
							<input type="text" id="searchKeyword" name="searchKeyword" value="${vo.searchKeyword}">
							<button type="button" id="btnSearch" class="s_btn submit" onclick="javascript:fn_search();">검색</button>
						</div>
					</div>
				</div>
				
				<div class="clear">
					<p class="t_total mart40">총 ${totCnt}건</p>
					<a href="#cb_add" id="amodal_cabinet_price_add" data-modal-url="/admin/add/popup" rel="modal:open" class="s_btn submit right" style="margin-top:20px;">등록하기</a>
				</div>
				<table cellpadding="0" cellspacing="0" class="t_list">
					<colgroup>
						<col width="70px"><col width="12px"><col width="100px"><col width="140px"><col width="220px"><col width="130px"><col width="60px"><col width="80px">
					</colgroup>
					<thead>
					<tr>
						<th>번호</th>
						<th>관리자ID</th>
						<th>관리자명</th>
						<th>휴대폰</th>
						<th>이메일</th>
						<th>가입일</th>
						<th>관리</th>
						<th>비밀번호 변경</th>
					</tr>
					</thead>
					
					<tbody>
					<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td colspan="5"><spring:message code="no.search.data.msg"/></td>
					</tr>
					</c:if>
					<c:if test="${fn:length(resultList) > 0}">
					<c:forEach var="result" items="${resultList}" varStatus="status">
					<tr>
						<!-- 전체 조회건수-((현재페이지 번호-1) * 1페이지당 레코드수 + 현재게시물 출력순서 -->
						<td><c:out value="${totCnt-((paging.currentPageNo-1) * paging.recordCountPerPage + status.index)}"/></td>
						<td><c:out value="${result.mngrId}"/></td>
						<td>
							<c:choose>
								<c:when test="${fn:length(result.mngrNm) > 2}">
									<c:out value="${util:masking(result.mngrNm, 1, fn:length(result.mngrNm)-2)}"/>
								</c:when>
								<c:otherwise>
									<c:out value="${util:masking(result.mngrNm, 1, fn:length(result.mngrNm)-1)}"/>
								</c:otherwise>
							</c:choose>&nbsp;
						</td>
						<td>
							<c:out value="${result.hpNo1}-${util:masking(result.hpNo2, 0, fn:length(result.hpNo2))}-${result.hpNo3}"/>
						</td>
						<td><c:out value="${util:masking(result.email, 2, fn:length(fn:substringBefore(result.email, '@'))-2)}"/></td>
						<td><c:out value="${result.regDt}"/></td>
						<td><a href="#popup_add" id="amodal_cabinet_price" data-modal-url="/admin/popup?mngrId=${result.mngrId}" rel="modal:open" class="text_under color_warn">수정</a></td>
						<td>
							<!-- admin 계정은 비밀번호 변경 안되도록 처리 -->
							<c:if test="${result.mngrId ne 'admin'}">
								<a href="#popup_add" id="amodal_cabinet_price" data-modal-url="/admin/pw/popup?mngrId=${result.mngrId}" rel="modal:open" class="text_under color_warn">변경</a>
							</c:if>
							
							<%-- 로그인한 사람 본인ID만 수정가능하게끔 실행하는 로직 주석처리
							<c:if test="${result.mngrId eq sessionUser.userId}">
								<a href="#popup_add" id="amodal_cabinet_price" data-modal-url="/admin/pw/popup?mngrId=${result.mngrId}" rel="modal:open" class="text_under color_warn">변경</a>
							</c:if> --%>
						</td>
					</tr>
					</c:forEach>
					</c:if>
					</tbody>
				</table>
				${paging.pagination}
			</div>
		</div>
	</div>
	
</form>

<!-- 관리자 관리 popup start -->
<%@ include file="/WEB-INF/views/common/modal.jsp" %>

<script type="text/javascript">
$(document).ready(function(){
	// 검색조건 유지
});

// 조회
function fn_search(){
	var frm = $("#adminFrm")[0];
	frm.action = "/admin/list";
	frm.submit();
}

// 등록페이지
function fn_addView(){
	var frm = $("#adminFrm")[0];
	frm.action = "/admin/add";
	frm.submit();
}

// 수정페이지
function fn_updView(mngrId){
	$("#mngrId").val(mngrId);
	
	var frm = $("#adminFrm")[0];
	frm.action = "/admin/upd";
	frm.submit();
}
</script>
</body>
</html>