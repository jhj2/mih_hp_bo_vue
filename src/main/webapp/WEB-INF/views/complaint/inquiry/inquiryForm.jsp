<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!DOCTYPE html>
<html>
<body>
<form id="frmInqryAdd" name="frmInqryAdd" method="post" action="/complaint/inquiry/edit">
	<input type="hidden" id="inqryIdx" name="inqryIdx" value="${add.inqryIdx}">
	<input type="hidden" id="mberNm" name="mberNm" value="${add.mberNm}">
	<input type="hidden" id="answrrId" name="answrrId" value="${add.answrrId}">
	<input type="hidden" id="updtDt" name="updtDt" value="${add.updtDt}">
	<input type="hidden" id="inqrySj" name="inqrySj" value="${add.inqrySj}">
	<input type="hidden" id="inqryCn" name="inqryCn" value="${add.inqryCn}">
	<input type="hidden" id="mberIdx" name="mberIdx" value="${add.mberIdx}">
    <div id="wrap">
        <!-- header start -->
        <header>
            <%@ include file="/WEB-INF/views/common/top.jsp"%>
            <!-- <div id="gnb">
                gnb
            </div> -->
            <%@ include file="/WEB-INF/views/common/lnb.jsp"%>
        </header>
        <!-- header end -->
        <div id="container">
            <div class="contents">
                <!-- 1:1 문의 답변 start -->
                <h3 class="page_title">1:1 문의 답변</h3>
                <table cellpadding="0" cellspacing="0" class="t_form">
                    <colgroup>
                        <col width="150px"><col width="150px"><col width="110px"><col width="150px"><col width="110px"><col width="150px"><col width="110px"><col width="150px">
                    </colgroup>
                    <tbody>
                        <tr>
                            <td class="th">회원</td>
                            <td>
                            	<c:set var="maskingLen" value="2"/>
	                           	<c:if test="${fn:length(add.mberNm) < 3}">
	                           		<c:set var="maskingLen" value="1"/>
	                           	</c:if>
	                           	<c:out value="${util:masking(add.mberNm, 1, fn:length(add.mberNm) - maskingLen)}"/> &nbsp;<a href="#mb_info" id="amodal_member" data-modal-url="/member/com/info?mberIdx=${add.mberIdx}" rel="modal:open" class="text_under color03">${add.mberIdx}</a>
                            </td>
                            <td class="th">접수일</td>
                            <td class="color6"><c:out value="${add.registDt}"/></td>
                            <td class="th">답변자</td>
                            <td class="color6"><c:out value="${add.answrrId}"/></td>
                            <td class="th">답변일</td>
                            <td class="color6"><c:out value="${add.updtDt}"/></td>
                        </tr>
                        <tr>
                            <td class="th">제목</td>
                            <td colspan="7"><c:out value="${add.inqrySj}" escapeXml="false"/></td>
                        </tr>
                        <tr>
                            <td class="th">내용</td>
                            <td colspan="7">
                                <p class="text"><c:out value="${add.inqryCn}" escapeXml="false"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="th">첨부 이미지</td>
                            <td colspan="7">
                                <div class="view_img">
                                <c:forEach var="item" items="${url}" varStatus="status">
                                    <p class="showImage"><img src="<c:out value="${config:value('file.attach.web.path')}"/><c:out value="${item.attPath}"/>/<c:out value="${item.fileSysNm}"/>" alt=""></p>
                                </c:forEach>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="th">답변</td>
                            <td colspan="7">
                                <textarea name="answerCn" id="answerCn" rows="6" maxlength="1000"><c:out value="${add.answerCn}"/></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="btn_wrap">
                    <a href="/complaint/inquiry/list" class="cancle">취소하기</a>
                    <a href="#;" class="submit" id="btnAnswer">답변하기</a>
                </div>
                <!-- 1:1 문의 답변 end -->
            </div>
        </div>
    </div>
    <div class="popup_img popup"></div>
    <div class="overlay" onclick="javascript:pop_close()"></div>
    <script>
        $(document).ready(function(){
            //image popup
            $(document).on("click",".showImage",function(){
                var path = $(this).find('img').attr('src')
                showImage(path);
            });
            function showImage(fileCallPath){
                $(".popup_img").html("<img src='"+fileCallPath+"' ><span class='pop_close'></span>");
                $(".popup_img").show();
                $('.overlay').show();
            }
            $(document).on("click",".pop_close",function(){
                $(".popup").hide();
                $('.overlay').hide();
            });
        });
        //docu end
    </script>
</form>
</body>
<%@ include file="/WEB-INF/views/common/modal.jsp" %>
<script type="text/javascript">
$(function(){
	// 답변하기 클릭이벤트
	$("#btnAnswer").click(function(){
		// 답변 공백여부  체크
        var saveCont = $("#answerCn").val();
        saveCont = saveCont.replace(/&nbsp;/gi, "");
        saveCont = saveCont.replace(/<br>/gi, "");
        saveCont = saveCont.replace(/ /gi, "");
        if (saveCont == "<p><\/p>" || saveCont == "") {
        	alert("답변이 입력되지 않았습니다.");
            return false;
        }

		// 답변 등록
		if (confirm("답변을 등록 하시겠습니까?")) {
			// 답변 등록 시 줄바꿈, 띄어쓰기 br 태그로 변환
			var str = $('#answerCn').val();
        	str = str.replace(/(?:\r\n|\r|\n)/g, '<br/>');
        	$('#answerCn').val(str);
        	
			// form submit
			$("#frmInqryAdd").submit();
		}
	});
});
</script>
</html>