<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!DOCTYPE html>
<html>
<body>
<form id="frmInqry" name="frmInqry" method="post" enctype="multipart/form-data">
	<input type="hidden" id="inqryIdx" name="inqryIdx" value="${edit.inqryIdx}">
	<input type="hidden" id="mberIdx" name="mberIdx" value="${edit.mberIdx}">
	<input type="hidden" id="inqryCn" name="inqryCn" value="${edit.inqryCn}">
	<input type="hidden" id="answerCn" name="answerCn" value="${edit.answerCn}">
    <div id="wrap">
        <!-- header start -->
        <header>
            <%@ include file="/WEB-INF/views/common/top.jsp"%>
            <!-- <div id="gnb">
                gnb
            </div> -->
            <%@ include file="/WEB-INF/views/common/lnb.jsp"%>
        </header>
        <!-- header end -->
        <div id="container">
            <div class="contents">
                <!-- 1:1 문의 조회 start -->
                <h3 class="page_title">1:1 문의 조회</h3>
                <table cellpadding="0" cellspacing="0" class="t_form">
                    <colgroup>
                        <col width="150px"><col width="150px"><col width="110px"><col width="150px"><col width="110px"><col width="150px"><col width="110px"><col width="150px">
                    </colgroup>
                    <tbody>
                        <tr>
                            <td class="th">회원</td>
                            <td>
	                            <c:set var="maskingLen" value="2"/>
	                           	<c:if test="${fn:length(edit.mberNm) < 3}">
	                           		<c:set var="maskingLen" value="1"/>
	                           	</c:if>
	                           	<c:out value="${util:masking(edit.mberNm, 1, fn:length(edit.mberNm) - maskingLen)}"/> &nbsp;<a href="#mb_info" id="amodal_member" data-modal-url="/member/com/info?mberIdx=${edit.mberIdx}" rel="modal:open" class="text_under color03">${edit.mberIdx}</a>                            
                            </td>
                            <td class="th">접수일</td>
                            <td class="color6"><c:out value="${edit.registDt}"/></td>
                            <td class="th">답변자</td>
                            <td class="color6"><c:out value="${edit.answrrId}"/></td>
                            <td class="th">답변일</td>
                            <td class="color6"><c:out value="${edit.updtDt}"/></td>
                        </tr>
                        <tr>
                            <td class="th">제목</td>
                            <td colspan="7"><c:out value="${edit.inqrySj}"/></td>
                        </tr>
                        <tr>
                            <td class="th">내용</td>
                            <td colspan="7">
                                <p class="text"><c:out value="${edit.inqryCn}" escapeXml="false"/></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="th">첨부 이미지</td>
                            <td colspan="7">
                                <div class="view_img">
                                	<c:forEach var="item" items="${url}" varStatus="status">
                                    	<p class="showImage"><img src="<c:out value="${config:value('file.attach.web.path')}"/><c:out value="${item.attPath}"/>/<c:out value="${item.fileSysNm}"/>" alt=""></p>
                                    </c:forEach>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="th">답변</td>
                            <td colspan="7">
                                <p class="text"><c:out value="${edit.answerCn}" escapeXml="false"/></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="btn_wrap">
                    <a href="/complaint/inquiry/list" class="submit" id="btnCnfirm">확인</a>
                </div>
                <!-- 1:1 문의 조회 end -->
            </div>
        </div>
    </div>
    <div class="popup_img popup"></div>
    <div class="overlay" onclick="javascript:pop_close()"></div>
    <script>
        $(document).ready(function(){
            //image popup
            $(document).on("click",".showImage",function(){
                var path = $(this).find('img').attr('src');
                showImage(path);
            });
            function showImage(fileCallPath){
                $(".popup_img").html("<img src='"+fileCallPath+"' ><span class='pop_close'></span>");
                $(".popup_img").show();
                $('.overlay').show();
            }
            $(document).on("click",".pop_close",function(){
                $(".popup").hide();
                $('.overlay').hide();
            });
        });
        //docu end
    </script>
</form>
</body>
<%@ include file="/WEB-INF/views/common/modal.jsp" %>
<script type="text/javascript">
$(function(){
	// 본문내용 띄어쓰기, 줄바꿈 변환
	var strInqryCn = $('#inqryCn').val();
	strInqryCn = strInqryCn.split('<br/>').join("\r\n");
	$('#inqryCn').val(strInqryCn);

	// 답변내용 띄어쓰기, 줄바꿈 변환
	var strAnswerCn = $('#answerCn').val();
	strAnswerCn = strAnswerCn.split('<br/>').join("\r\n");
	$('#answerCn').val(strAnswerCn);
});
</script>
</html>