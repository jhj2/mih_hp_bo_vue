<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="util" uri="/WEB-INF/tlds/utilTag.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!doctype html>
<html>
<body>
	
    <div id="wrap">
        <!-- header start -->
        <header>
        </header>
        <!-- header end -->
        <div id="container">
            <div class="contents">
                <!-- 1:1 문의 start -->
                <h3 class="page_title">1:1 문의</h3>
                <table cellpadding="0" cellspacing="0"  border="1" style='border-collapse:collapse;table-layout:fixed;'>
                    <colgroup>
                        <col width="70px"><col width="130px"><col width="490px"><col width="120px"><col width="80px"><col width="120px"><col width="110px">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>번호</th>
                            <th>회원</th>
                            <th>제목</th>
                            <th>접수일</th>
                            <th>답변자</th>
                            <th>답변일</th>
                            <th>답변</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_in">
                    <c:forEach var="item" items="${list}" varStatus="status">           
                        <tr>
                            <td><c:out value="${paging.pageMaxNo - status.index}"/></td>
                            <td>
                            	<c:set var="maskingLen" value="2"/>
                            	<c:if test="${fn:length(item.mberNm) < 3}">
                            		<c:set var="maskingLen" value="1"/>
                            	</c:if>
                            	<c:out value="${util:masking(item.mberNm, 1, fn:length(item.mberNm) - maskingLen)}"/> &nbsp;
                            	<c:out value="${item.mberIdx}"/>
                            	
                            </td>
                            <td><c:out value="${item.inqrySj}"/></td>
                            <td style="text-align: center;"><c:out value="${item.registDt}"/></td>
                            <td style="text-align: center;"><c:out value="${item.answrrId}"/></td>
                            <td style="text-align: center;"><c:out value="${item.updtDt}"/></td>
                            <c:choose>
								<c:when test="${item.answerYn eq 'N'}">
									<td style="text-align: center;">대기</td>
								</c:when>
								<c:otherwise>
									<td style="text-align: center;">완료</td>
								</c:otherwise>
							</c:choose>
                        </tr>
                    </c:forEach>
                    <c:if test="${total eq 0}">
	                   	<tr>
	                   		<td colspan="7"><spring:message code="no.search.data.msg"/></td>
	                   	</tr>
                    </c:if>
                    </tbody>
                </table>
                <!-- 1:1 문의 end -->
            </div>
        </div>
    </div>
</body>
</html>