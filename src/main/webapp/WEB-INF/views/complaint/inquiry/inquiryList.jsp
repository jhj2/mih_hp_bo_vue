<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
    <div id="wrap">
        <!-- header start -->
        <header>
            <%@ include file="/WEB-INF/views/common/top.jsp"%>
            <%@ include file="/WEB-INF/views/common/lnb.jsp"%>
        </header>
        <!-- header end -->
        <div id="container">
            <div class="contents">
                <!-- 1:1 문의 start -->
                <h3 class="page_title">1:1 문의</h3>
                <form id="frmInqry" name="frmInqry" action="/complaint/inquiry/list">
				<input type="hidden" id="inqryIdx" name="inqryIdx" value="${inquiry.inqryIdx}"/>
				<input type="hidden" id="currentPageNo" name="currentPageNo" value="${paging.currentPageNo}"/>
    			<input type="hidden" id="pageSize" name="pageSize" value="${paging.pageSize}"/>
    			<input type="hidden" id="recordCountPerPage" name="recordCountPerPage" value="${paging.recordCountPerPage}"/>
                <div class="border_box">
                    <div class="flex">
                        <p class="tit">답변상태</p>
                        <div class="con">
                            <p class="checkbox"><input type="radio" id="stat_all" name="answerYn" value="" checked><label for="stat_all"><i></i>전체</label></p>
                            <p class="checkbox"><input type="radio" id="stat_02" name="answerYn" value="Y" <c:if test="${inquiry.answerYn eq 'Y'}">checked</c:if>><label for="stat_02"><i></i>완료</label></p>
                            <p class="checkbox"><input type="radio" id="stat_01" name="answerYn" value="N" <c:if test="${inquiry.answerYn eq 'N'}">checked</c:if>><label for="stat_01"><i></i>대기</label></p>
                        </div>
                    </div>
                    <div class="flex">
                        <p class="tit">회원</p>
                        <div class="con">
                            <select name="selMber" id="selMber">
                                <option value="nm">이름</option>
                            </select>
                            <input type="text" id="mberNm" name="mberNm" value="${inquiry.mberNm}">
                        </div>
                    </div>
                    <div class="flex">
                        <p class="tit">키워드</p>
                        <div class="con">
                            <select name="keyword" id="keyword">
                                <option value="0" <c:if test="${inquiry.keyword eq '0'}">selected</c:if>>전체</option>
                                <option value="1" <c:if test="${inquiry.keyword eq '1'}">selected</c:if>>제목</option>
                                <option value="2" <c:if test="${inquiry.keyword eq '2'}">selected</c:if>>문의</option>
                                <option value="3" <c:if test="${inquiry.keyword eq '3'}">selected</c:if>>답변</option>
                            </select>
                            <input type="text" id="txtKeyword" name="txtKeyword" value="${inquiry.txtKeyword}">
                            <button type="button" id="btnSearch" class="s_btn submit">검색</button>
                        </div>
                    </div>
                </div>
                </form>
                <form id="excelForm" method="post" role="form" >
                <input type="hidden" id="inqryIdx" name="inqryIdx" value="${inquiry.inqryIdx}"/>
                <input type="hidden" id="answerYn" name="answerYn" value="${inquiry.answerYn}"/>
                <input type="hidden" id="mberNm" name="mberNm" value="${inquiry.mberNm}"/>
                <input type="hidden" id="keyword" name="keyword" value="${inquiry.keyword}"/>
                <input type="hidden" id="txtKeyword" name="txtKeyword" value="${inquiry.txtKeyword}"/>
    			</form>
                <div class="clear">
					<p class="t_total mart40" id="total">총 ${total} 건</p>
                  	<a href="javascript:fn_excelDownLod();" class="s_btn submit right mart30">엑셀다운로드</a>
                </div>
                
                <table cellpadding="0" cellspacing="0" class="t_list full mart10" style="table-layout:fixed">
                    <colgroup>
                        <col width="70px"><col width="130px"><col width="490px"><col width="120px"><col width="80px"><col width="120px"><col width="110px">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>번호</th>
                            <th>회원</th>
                            <th>제목</th>
                            <th>접수일</th>
                            <th>답변자</th>
                            <th>답변일</th>
                            <th>답변</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_in">
                    <c:forEach var="item" items="${list}" varStatus="status">           
                        <tr>
                            <td><c:out value="${total-((paging.currentPageNo-1) * paging.recordCountPerPage + status.index)}"/></td>
                            <td>
                            	<c:set var="maskingLen" value="2"/>
                            	<c:if test="${fn:length(item.mberNm) < 3}">
                            		<c:set var="maskingLen" value="1"/>
                            	</c:if>
                            	<c:out value="${util:masking(item.mberNm, 1, fn:length(item.mberNm) - maskingLen)}"/> &nbsp;
                            	<!-- 탈퇴한 회원이면 번호에 링크 제외하도록 처리 -->
								<c:if test="${item.mberNm eq null}">
                            		<c:out value="${item.mberIdx}"/>
                            	</c:if>
								<c:if test="${item.mberNm ne null}">
                            		<a href="#mb_info" id="amodal_member" data-modal-url="/member/com/info?mberIdx=${item.mberIdx}" rel="modal:open" class="text_under color03">${item.mberIdx}</a>
                            	</c:if>
                            </td>
                            <td><a href="/complaint/inquiry/view?inqryIdx=${item.inqryIdx}" class="p_link"><c:out value="${item.inqrySj}"/></a></td>
                            <td><c:out value="${item.registDt}"/></td>
                            <td><c:out value="${item.answrrId}"/></td>
                            <td><c:out value="${item.updtDt}"/></td>
                            <c:choose>
							<c:when test="${item.answerYn eq 'N'}">
								<td>대기<a class="color_warn text_under marl10" href="/complaint/inquiry/form?inqryIdx=${item.inqryIdx}">답변하기</a></td>
							</c:when>
							<c:otherwise>
								<td>완료</td>
							</c:otherwise>
							</c:choose>
                        </tr>
                    </c:forEach>
                    <c:if test="${total eq 0}">
	                   	<tr>
	                   		<td colspan="7"><spring:message code="no.search.data.msg"/></td>
	                   	</tr>
                    </c:if>
                    </tbody>
                </table>
                ${paging.pagination}
                <!-- 1:1 문의 end -->
            </div>
        </div>
    </div>
</body>
<%@ include file="/WEB-INF/views/common/modal.jsp" %>
<script type="text/javascript">
$(document).ready(function(){
	// 검색
	$("#btnSearch").click(function(){
		$("#currentPageNo").val(1);
	 	$("#frmInqry").submit();
	});

	// 검색 내용 입력 후 엔터키 이벤트
	$("#txtKeyword").keyup(function(event) {
	    if (event.keyCode === 13) {
	        $("#btnSearch").click();
	    }
	});

	// 고객이름 입력 후 엔터키 이벤트
	$("#mberNm").keyup(function(event) {
	    if (event.keyCode === 13) {
	        $("#btnSearch").click();
	    }
	});
});

function fn_excelDownLod(){
	var excelForm = document.getElementById('excelForm');
	excelForm.action = "/complaint/inquiry/excel";
	excelForm.submit();
}
</script>
</html>