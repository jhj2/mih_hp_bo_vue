<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
	<form id="frmNotice" name="frmNotice" action="/complaint/notice/list">
	<input type="hidden" id="noticeIdx" name="noticeIdx" value="${notice.noticeIdx}"/>
	<input type="hidden" id="currentPageNo" name="currentPageNo" value="${paging.currentPageNo}"/>
    <input type="hidden" id="pageSize" name="pageSize" value="${paging.pageSize}"/>
    <input type="hidden" id="recordCountPerPage" name="recordCountPerPage" value="${paging.recordCountPerPage}"/>
    <div id="wrap">
        <!-- header start -->
        <header>
            <%@ include file="/WEB-INF/views/common/top.jsp"%>
            <%@ include file="/WEB-INF/views/common/lnb.jsp"%>
        </header>
        <!-- header end -->
        <div id="container">
            <div class="contents">
                <!-- 공지사항 관리 start -->
                <h3 class="page_title">공지사항 관리</h3>
                <div class="border_box">
                    <div class="flex">
                        <p class="tit">키워드</p>
                        <div class="con">
                            <select name="bhfIdx" id="bhfIdx">
                            	<option value="0">전체공지</option>
                                <c:forEach var="branchList" items="${noticeBranchList}">
                                	<option value="${branchList.bhfIdx}" <c:if test="${notice.bhfIdx == branchList.bhfIdx}"> selected</c:if>><c:out value="${branchList.bhfNm}"/></option>
                                </c:forEach>
                            </select>
                            <select name="searchKeywordType" id="searchKeywordType">
                                <option value="all" <c:if test="${notice.searchKeywordType eq 'all'}">selected</c:if>>전체</option>
                                <option value="sj" <c:if test="${notice.searchKeywordType eq 'sj'}">selected</c:if>>제목</option>
                                <option value="cn" <c:if test="${notice.searchKeywordType eq 'cn'}">selected</c:if>>내용</option>
                            </select>
                            <input type="text" id="searchKeyword" name="searchKeyword" value="${notice.searchKeyword}">
                            <button type="button" id="btnSearch" class="s_btn submit">검색</button>
                        </div>
                    </div>
                </div>
                <div class="clear mart40">
                    <p class="t_total" id="total">총 ${total} 건</p>
                    <a href="/complaint/notice/form" class="s_btn submit right" id="btnRegist">등록하기</a>
                </div>
                <table cellpadding="0" cellspacing="0" class="t_list mart10" style="table-layout:fixed">
                    <colgroup>
                        <col width="70px"><col width="130px"><col width="530px"><col width="120px"><col width="130px"><col width="100px"><col width="100px">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>번호</th>
                            <th>지부</th>
                            <th>제목</th>
                            <th>등록자</th>
                            <th>등록일</th>
                            <th>노출여부</th>
                            <th>수정</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_in">
                    <c:forEach var="item" items="${list}" varStatus="status">
                        <tr>
                            <td><c:out value="${total-((paging.currentPageNo-1) * paging.recordCountPerPage + status.index)}"/></td>
                            <td><c:out value="${item.noticeBhfNm}"/></td>
                            <td><a onclick="javascript:fn_openWindow(${item.noticeIdx})" class="p_link"><c:out value="${item.noticeSj}"/></a></td>
                            <td><c:out value="${item.registId}"/></td>
                            <td><c:out value="${item.registDt}"/></td>
                            <td><c:out value="${item.useYn eq 'Y' ? '노출':'미노출'}"/></td>
                            <td><a href="/complaint/notice/view?noticeIdx=${item.noticeIdx}" class="color_warn text_under">수정</a></td>
                        </tr>
                    </c:forEach>
                    <c:if test="${total eq 0}">
	                   	<tr>
	                   		<td colspan="6"><spring:message code="no.search.data.msg"/></td>
	                   	</tr>
                    </c:if>
                    </tbody>
                </table>
                ${paging.pagination}
                <!-- 공지사항 관리 end -->
            </div>
        </div>
    </div>
</form>
</body>
<script type="text/javascript">
$(document).ready(function(){
	// 검색
	$("#btnSearch").click(function(){
		$("#currentPageNo").val(1);
		$("#frmNotice").submit();
	});
});

// Front 공지사항 페이지로 이동
function fn_openWindow(param) {  
	var url = "${config:value('server.protocol.https')}"+"://"+"${config:value('server.bgbgb.domain')}"+"/complaint/notice/view?noticeIdx="+param;
    window.open(url, "보관복지부");
}
</script>
</html>