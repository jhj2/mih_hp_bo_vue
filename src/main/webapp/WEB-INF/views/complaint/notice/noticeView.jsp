<%@page import="java.io.Console"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
	<form id='regiForm' name="regiForm" method="post" role="form" enctype="multipart/form-data" action="/complaint/notice/upload">
    <input type="hidden" id="noticeIdx" name="noticeIdx" value="${edit.noticeIdx}">
    <div id="wrap">
        <!-- header start -->
        <header>
            <%@ include file="/WEB-INF/views/common/top.jsp"%>
            <!-- <div id="gnb">
                gnb
            </div> -->
            <%@ include file="/WEB-INF/views/common/lnb.jsp"%>
        </header>
        <!-- header end -->
        <div id="container">
            <div class="contents">
                <!-- 공지사항 등록 start -->
                <h3 class="page_title">공지사항 상세</h3>
                <table cellpadding="0" cellspacing="0" class="t_form">
                    <colgroup>
                        <col width="150px"><col width="390px"><col width="150px"><col width="390px">
                    </colgroup>
                    <tbody>
                        <tr>
                            <td class="th">지부</td>
                            <td>
                                <select name="bhfIdx" id="bhfIdx">
                                	<option value="0">전체공지</option>
	                                <c:forEach var="branchList" items="${noticeBranchList}">
	                                	<option value="${branchList.bhfIdx}"<c:if test="${edit.noticeBhfIdx == branchList.bhfIdx}">selected</c:if>><c:out value="${branchList.bhfNm}"/></option>
	                                </c:forEach>
	                            </select>
                            </td>
                            <td class="th">등록자</td>
                            <td><c:out value="${edit.registId}"/></td>
                        </tr>
                        <tr>
                            <td class="th">제목</td>
                            <td colspan="3">
                                <input type="text" id="noticeSj" name="noticeSj" class="w70" value="<c:out value="${edit.noticeSj}"/>" maxlength="50">
                                <p class="txt_count">최대 50자</p>
                            </td>
                        </tr>
                        <tr>
							<td class="th">노출여부</td>
                            <td colspan="3">
                                <p class="checkbox"><input type="radio" id="stat_01" name="useYn" value="Y"  <c:if test="${edit.useYn eq 'Y'}">checked</c:if>><label for="stat_01"><i></i>노출</label></p>
                            	<p class="checkbox"><input type="radio" id="stat_02" name="useYn" value="N"  <c:if test="${edit.useYn eq 'N'}">checked</c:if>><label for="stat_02"><i></i>미노출</label></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="th">내용</td>
                            <td colspan="3">
                                <textarea name="noticeCn" id="noticeCn" style="display:none;"><c:out value="${edit.noticeCn}"/></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input type="hidden" name="editImageValues" id="editImageValues"/>
                <div class="btn_wrap">
                    <a href="#;" class="cancle">취소하기</a>
                    <a href="#;" class="submit" id="saveBtn">수정하기</a>
                </div>
                <!-- 공지사항 등록 end -->
            </div>
        </div>
    </div>
    <div class="popup_img popup"></div>
    <div class="overlay" onclick="javascript:pop_close()"></div>
    <script>
        $(document).ready(function(){
            //image popup
            $(document).on("click",".showImage",function(){
                var path = $(this).find('img').attr('src')
                showImage(path);
            });
            function showImage(fileCallPath){
                $(".popup_img").html("<img src='"+fileCallPath+"' ><span class='pop_close'></span>");
                $(".popup_img").show();
                $('.overlay').show();
            }
            $(document).on("click",".pop_close",function(){
                $(".popup").hide();
                $('.overlay').hide();
            });
        });
        //docu end
    </script>
    </form>
</body>
<script type="text/javascript" src="/resources/smarteditor2/js/HuskyEZCreator.js" charset="utf-8"></script>
<script type="text/javascript">
$(function(){
	// 지부 셋팅
	$("#noticeBhfIdx").val(${edit.noticeBhfIdx}).attr("selected", "selected");
	
	// 전역변수 선언
	var editor_object = [];

	nhn.husky.EZCreator.createInIFrame({
		oAppRef: editor_object,
		elPlaceHolder: "noticeCn",
		sSkinURI: "/resources/smarteditor2/SmartEditor2Skin.html",
		htParams : {
			// 툴바 사용 여부
			bUseToolbar : true,
			// 입력창 크기 조절바 사용 여부
			bUseVerticalResizer : true,
			// 모드 탭(Editor | HTML | TEXT) 사용 여부
			bUseModeChanger : true,
		}
	});

	// 등록하기 버튼 클릭이벤트
	$("#saveBtn").click(function(){
		// validation 체크
		chkFrm();
	});

	// 엔터키 사용시 validation 체크
	$('#noticeSj').keydown(function(e) {
		if (e.keyCode == 13) {
			return false;
	    }
	});

	// 저장전 validation 체크
	function chkFrm() {
		// id가 smarteditor인 textarea에 에디터에서 대입
		editor_object.getById["noticeCn"].exec("UPDATE_CONTENTS_FIELD", []);

		// 제목 글자수 체크
		if ($("#noticeSj").val().length > 50) {
			alert("제목은 최대 50자까지 입력 가능합니다.");
			return false;
		}

		// 제목 입력여부 체크
		if ($("#noticeSj").val().length == 0) {
			alert("제목을 입력되지 않았습니다.");
			return false;
		}
		
		// 제목 스페이스(공백) 체크
		var blank_pattern = /^\s+|\s+$/g;
		if ($("#noticeSj").val().replace(blank_pattern, "") == "") {
			alert("제목을 입력되지 않았습니다.");
			$("#noticeSj").focus();
			return false;
		}
		
		// 내용 입력여부 체크
        var saveCont = $("#noticeCn").val();
        saveCont = saveCont.replace(/&nbsp;/gi, "");
        saveCont = saveCont.replace(/<br>/gi, "");
        saveCont = saveCont.replace(/ /gi, "");
        if (saveCont == "<p><\/p>" || saveCont == "") {
        	alert("내용이 입력되지 않았습니다.");
            return false;
        }

        if (confirm("저장하시겠습니까?")) {	
			// form submit
			$("#regiForm").submit();
		}
    }

	// 취소하기 클릭이벤트
	$(".cancle").click(function(){
		location.href="/complaint/notice/list";
	});
});
</script>
</html>