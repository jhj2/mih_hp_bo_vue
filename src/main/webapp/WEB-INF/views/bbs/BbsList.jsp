<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
<form id="bbsFrm" name="bbsFrm" method="get">

	<input type="hidden" id="bbsIdx" name="bbsIdx" value=0 />
	<input type="hidden" id="ctgryIdx" name="ctgryIdx" value=0 />
	
	<!-- 페이징처리 변수 -->
	<input type="hidden" id="currentPageNo" name="currentPageNo" value="${paging.currentPageNo}"/>
	<input type="hidden" id="pageSize" name="pageSize" value="${paging.pageSize}"/>
	<input type="hidden" id="recordCountPerPage" name="recordCountPerPage" value="${paging.recordCountPerPage}"/>
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 게시판 카테고리 관리 start -->
				<h3 class="page_title">게시판 관리</h3>
				<div class="border_box">
					<div class="flex">
						<p class="tit">카테고리</p>
						<div class="con">
							<select name="searchCtgryIdx" id="searchCtgryIdx">
								<c:forEach var="result" items="${ctgryList}" varStatus="status">
									<option value="${result.commCd}" <c:if test="${search.searchCtgryIdx eq result.commCd}">selected</c:if>><c:out value="${result.commCdNm}"/></option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="flex">
						<p class="tit">키워드</p>
						<div class="con">
							<select name="searchCondition" id="searchCondition">
								<option value="1" <c:if test="${search.searchCondition eq '1'}">selected</c:if>>제목</option>
								<option value="2" <c:if test="${search.searchCondition eq '2'}">selected</c:if>>내용</option>
								<option value="3" <c:if test="${search.searchCondition eq '3'}">selected</c:if>>작성자</option>
							</select>
							<input type="text" id="searchData" name="searchData" value="${search.searchData}">
							<button type="button" id="btnSearch" class="s_btn submit" onclick="javascript:fn_search()">검색</button>
						</div>
					</div>
					
				</div>
				<div class="clear">
					<p class="t_total">총 ${totCnt}건</p>
<%--					<a href="/bbs/addView" class="s_btn submit right">등록하기</a>--%>
					<a onclick="javascript:fn_add()" class="s_btn submit right">등록하기</a>
				</div>
				<!-- 리스트 start -->
				<table id="ctgryTb" cellpadding="0" cellspacing="0" class="t_list mart10">
					<colgroup>
						<col width="80px"><col width="150px"><col width="100px"><col width="100px"><col width="100px"><col width="70px">
					</colgroup>
					<thead>
					<tr>
						<th>번호</th>
						<th>제목</th>
						<th>사용여부</th>
						<th>등록자</th>
						<th>등록일자</th>
						<th>관리</th>
					</tr>
					</thead>
					
					<tbody>
					<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td colspan="6"><spring:message code="no.search.data.msg"/></td>
					</tr>
					</c:if>
					
					<c:if test="${fn:length(resultList) > 0}">
					<c:forEach var="result" items="${resultList}" varStatus="status">
						<c:if test="${result.depth == '0'}">
							<tr name="1" class="${result.ref}" value="${result.depth}" style="display: table-row;">
						</c:if>
						<c:if test="${result.depth != '0'}">
							<tr name="0" class="${result.ref}" value="${result.depth}" style="display: none;">
						</c:if>
							<td>${result.bbsIdx}</td>
<%--							<td>${totCnt-((paging.currentPageNo-1) * paging.recordCountPerPage + status.index)}</td>--%>
							<td style="text-align:left;">
								<c:if test="${result.depth ne '0'}">
									<c:set var="nPx" value="40"/>	<%-- 게시글 답글 영역 아이콘 크기 --%>
<%--									<span style="margin-left: ${nPx * ((result.depth % 4) > 0 ? (result.depth % 4): 1) }px;"/>--%>
									<span style="margin-left: ${nPx * result.depth}px;"/>
									<c:out value=" >"/>
								</c:if>
								<c:if test="${result.reBbsFlag}"><button type="button" id="reBbsBtn" class="s_btn submit" onclick="javascript:setup_reBbsList(${result.bbsIdx},${result.depth},this);">+</button></c:if>
								<a onclick="fn_enter(${result.bbsIdx})"><c:out value="${result.bbsSj}"/></a></td>
							<td><c:out value="${result.useYnNm}"/></td>
							<td><c:out value="${result.regId}"/></td>
							<td><c:out value="${result.regDt}"/></td>
							<td>
								<a href="/bbs/updView?bbsIdx=${result.bbsIdx}" class="color_warn text_under">수정</a>&nbsp;
								<a href="javascript:fn_delete('<c:out value="${result.bbsIdx}"/>');" class="color_warn text_under">삭제</a>
							</td>
						</tr>
					</c:forEach>
					</c:if>
					</tbody>
				</table>
				<!-- 리스트 end -->
				${paging.pagination}
				<!-- 지부정보 관리 end -->
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
$(function() {
	// 카테고리 선택했을때
	$(document).on("change", '#searchCtgryIdx', function() {
		if($("#searchCtgryIdx").val() != "0"){
			$("#currentPageNo").val(1);	// 검색했을때 1페이지 유지
			var frm = $("#bbsFrm")[0];
			frm.action = "/bbs/list";
			frm.submit();
		}
	});
	setup_reBbsTable();
});
// 게시글 답글 하이라키 구조 설정
function setup_reBbsTable(){
	var temp = $("#ctgryTb tbody").find('tr');
	var strDepth = [];
	var nDepth = 0;
	for(var i=0;i<temp.length;i++) {
		var listBbsIdx = "";
		if (i > 0){
			listBbsIdx = temp.eq(i-1).find('td').eq(0).text();
		}
		var listDepth = Number(temp.eq(i).attr('value'));
		if (nDepth < listDepth){
			nDepth = listDepth;
			strDepth.push(listBbsIdx);
		} else if (nDepth > listDepth){
			var depthCnt = nDepth - listDepth;
			for (var j=0; j< depthCnt;j++){
				strDepth.pop();
			}
			nDepth = listDepth;
		}
		temp.eq(i).attr('class', strDepth.join());
	}
}
// 게시글 답글 활성/비활성
function setup_reBbsList(bbsIdx, nDepth, obj){
	var bFlag = 0;
	if ($(obj).text() == '+'){
		bFlag = 1;
		$(obj).text("-");
	} else {
		bFlag = 0;
		$(obj).text("+");
	}
	var noneDisplayList = [];
	var temp = $("#ctgryTb tbody").find('tr[class*='+bbsIdx+']');
	for(var i=0;i<temp.length;i++) {
		var tempDepth = Number(temp.eq(i).attr("value"));
		var subDepthFlag = false;
		if (nDepth == (tempDepth-1)){
			temp.eq(i).attr("name", bFlag);
			subDepthFlag = true;
		}
		if ( subDepthFlag ){	// 대상
			if( bFlag == 1 ){
				temp.eq(i)[0].style.display = "table-row";
			} else {
				temp.eq(i)[0].style.display = "none";
			}
		} else {	// 하위
			var bDisplay = Number(temp.eq(i).attr("name"));
			var strDisplay = temp.eq(i).attr("class");
			for(var j=0;j<noneDisplayList.length;j++){
				if (strDisplay.indexOf(noneDisplayList[j]) !== -1){
					bDisplay = 0;
					break;
				}
			}
			if (bFlag == 1 && bDisplay == 1){
				temp.eq(i)[0].style.display = "table-row";
			} else {
				temp.eq(i)[0].style.display = "none";
				noneDisplayList.push(temp.eq(i).find('td').eq(0).text());
			}
		}
	}
}
// 조회
function fn_search(){
	$("#currentPageNo").val(1);	// 검색했을때 1페이지 유지
	
	var frm = $("#bbsFrm")[0];
	frm.action = "/bbs/list";
	frm.submit();
}

// 삭제
function fn_delete(bbsIdx){

	if(!confirm('<spring:message text="게시물을 삭제하시겠습니까?"/>')){
		return;
	}else{
		$("#bbsFrm").find("#bbsIdx").val(bbsIdx);
		var frm = $("#bbsFrm")[0];
		frm.action = "/bbs/del";
		frm.method = "post";
		frm.submit();
	}
}

// 내용보기
function fn_enter(bbsIdx){
	$("#bbsFrm").find("#bbsIdx").val(bbsIdx);
	$("#bbsFrm").find("#ctgryIdx").val($("#searchCtgryIdx").val());
	var frm = $("#bbsFrm")[0];
	frm.action = "/bbs/detail";
	frm.method = "get";
	frm.submit();
}

// 등록하기
function fn_add(){
	$("#bbsFrm").find("#ctgryIdx").val($("#searchCtgryIdx").val());
	var frm = $("#bbsFrm")[0];
	frm.action = "/bbs/addView";
	frm.method = "get";
	frm.submit();
}
</script>
</body>
</html>