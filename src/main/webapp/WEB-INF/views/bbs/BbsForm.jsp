<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
<form id="bbsForm" name="bbsForm" method="post" enctype="multipart/form-data">

	<!-- 등록일 경우 hidden값 세팅 -->
	<c:if test="${STS_FLAG eq 'add'}">
		<input type="hidden" id="bCtgryIdx" name="bCtgryIdx" value="${resultGrant.ctgryIdx}">
		<input type="hidden" id="bfileUseYn" name="bfileUseYn" value="${resultGrant.fileUseYn}">
		<input type="hidden" id="bfilePermCnt" name="bfilePermCnt" value="${resultGrant.filePermCnt}">
		<input type="hidden" id="bfilePermBt" name="bfilePermBt" value="${resultGrant.filePermBt}">
		<input type="hidden" id="bEdiUseYn" value="${resultGrant.ediUseYn}">
		<input type="hidden" id="ref" name="ref" value="0" />
		<input type="hidden" id="depth" name="depth" value="0" />
	</c:if>
	<!-- 수정일 경우 hidden값 세팅 -->
	<c:if test="${STS_FLAG eq 'upd'}">
		<input type="hidden" id="bbsIdx" name="bbsIdx" value="${resultVO.bbsIdx}">
		
		<input type="hidden" id="bCtgryIdx" name="bCtgryIdx" value="">
		<input type="hidden" id="bfileUseYn" name="bfileUseYn" value="${resultGrant.fileUseYn}">
		<input type="hidden" id="bfilePermCnt" name="bfilePermCnt" value="${resultGrant.filePermCnt}">
		<input type="hidden" id="bfilePermBt" name="bfilePermBt" value="${resultGrant.filePermBt}">
		<input type="hidden" id="bEdiUseYn" value="${resultGrant.ediUseYn}">
		
		<input type="hidden" name="delFileIdx" id="delFileIdx">
		<input type="hidden" name="delFileTableIdx" id="delFileTableIdx">/bbs/addView
		
		<input type="hidden" id="bbsCmtIdx" name="bbsCmtIdx" value="0" />
	</c:if>
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 지부정보 등록 start -->
				<h3 class="page_title">
					게시글
					<c:if test="${STS_FLAG eq 'add'}">
						등록
					</c:if>
					<c:if test="${STS_FLAG eq 'upd'}">
						수정
					</c:if>
				</h3>
				<table cellpadding="0" cellspacing="0" class="t_form">
					<colgroup>
						<col width="150px"><col width="230px"><col width="150px"><col width="390px">
					</colgroup>
					<tbody>
						<tr>
							<td class="th" >제목</td>
							<td colspan="3">
								<input type="text" id="bbsSj" name="bbsSj" class="w70"  maxlength="50" value="${resultVO.bbsSj}" style="text-align:left;" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.notice.subject.write"/>"/>
								<p class="txt_count">최대 50자</p>
							</td>
						</tr>
						<tr>
							<td class="th">카테고리</td>
							<td>
								<select name="ctgryIdx" id="ctgryIdx" disabled="disabled" value="${resultGrant.ctgryIdx}" onchange="javascript:fn_get_ctgry_conf()" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.selCtgryNm"/>">
									<c:forEach var="result" items="${ctgryList}" varStatus="status">
										<option value="${result.commCd}" <c:if test="${resultVO.ctgryIdx eq result.commCd}">selected</c:if>><c:out value="${result.commCdNm}"/></option>
									</c:forEach>
								</select>
							</td>
							<td class="th">사용여부</td>
							<td>
								<div class="con">
									<c:forEach var="result" items="${useYnRadio}" varStatus="status">
										<p class="checkbox"><input type="radio" id="stat_0${status.index+1}" name="useYn" value="${result.val1}" <c:if test="${resultVO.useYn eq result.val1}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.useYn"/>"><label for="stat_0${status.index+1}"><i></i>${result.cdNm}</label></p>
									</c:forEach>
									<%-- <p class="checkbox"><input type="radio" id="stat_01" name="useYn" value="Y" <c:if test="${resultVO.useYn eq 'Y'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.useYn"/>"><label for="stat_01"><i></i>사용</label></p>
									<p class="checkbox"><input type="radio" id="stat_02" name="useYn" value="N" <c:if test="${resultVO.useYn eq 'N'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.useYn"/>"><label for="stat_02"><i></i>미사용</label></p> --%>
								</div>
							</td>
						</tr>
						<tr>
							<td class="th">내용</td>
							<td colspan="3">
								<textarea name="bbsCn" id="bbsCn" rows="3" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.notice.content.write"/>">${resultVO.bbsCn}</textarea>
							</td>
						</tr>
					</tbody>
				</table>
				
				<!-- 첨부파일 시작 -->
				<input type="file" id="tempFile" name="tempFile" class="tempFile" style="display:none;"/>
				<p class="stit mart30" style="display:none;">첨부파일</p>
				<div class="clear" style="margin-top:-45px; display:none;">
					<a onclick="javascript:add_file();" class="s_btn submit right">추가</a>
				</div>
				
				<table id="fileTbl" cellpadding="0" cellspacing="0" class="t_list mart10" style="display:none;">
					<colgroup>
						<col width="365px"><col width="100px">
					</colgroup>
					<thead>
					<tr>
						<th>첨부파일</th>
						<th>관리</th>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="result" items="${fileList}" varStatus="status">
						<tr class="fileTr">
							<td style="text-align:left;">
								<input type="hidden" name="dbData" class="dbData" value="Y"/>
								<input type="hidden" name="fileTableNm" id="fileTableNm" value="<c:out value="${result.fileTableNm}" />" />
								<input type="hidden" name="fileTableIdx" id="fileTableIdx" value="<c:out value="${result.fileTableIdx}" />" />
								<input type="hidden" name="fileIdx" id="fileIdx" value="<c:out value="${result.fileIdx}" />" />
								<input type="hidden" name="fileSort" id="fileSort" value="${result.fileSort}">
								<input type="file" name="attachFile" class="attachFile" style="display:none;"/>
								<input type="text" name="fileOrgNm" class="w90" readonly="readonly" value="<c:out value="${result.fileOrgNm}" />" style="cursor:pointer; border:0px;"
									onclick="javascript:fileDown('<c:out value="${result.sysPath}" />', '<c:out value="${fn:replace(result.attPath, '\\\\', '/')}" />', '<c:out value="${result.fileSysNm}" />', '<c:out value="${result.fileOrgNm}" />');event.cancelBubble=true;">
							</td>
							<td>
								<a onclick="javascript:del_file('<c:out value="${result.fileIdx}"/>', this);" class="color_warn text_under">삭제</a>
							</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
				
				<!-- 게시판 댓글 등록 start -->
				<div id="cmtTbl" style="display:none">
					<p class="stit mart30">댓글</p>
					<div class="clear">
						<!-- <p class="t_total">총 ${totCnt}건</p>
						<a href="/bbs/addView" class="s_btn submit right">댓글달기</a> -->
					</div>
					<table cellpadding="0" cellspacing="0" class="t_list mart10">
						<colgroup>
							<col width="365px"><col width="100px"><col width="100px"><col width="70px">
						</colgroup>
						<thead>
						<tr>
							<th>댓글 내용</th>
							<th>등록자</th>
							<th>등록일자</th>
							<th>관리</th>
						</tr>
						</thead>

						<tbody>
						<c:if test="${fn:length(bbsCmtList) == 0}">
							<tr>
								<td colspan="4"><spring:message text="댓글이 없습니다."/></td>
							</tr>
						</c:if>
						<c:if test="${fn:length(bbsCmtList) > 0}">
							<c:forEach var="bbsCmt" items="${bbsCmtList}" varStatus="status">
								<tr>
									<td style="text-align:left;">
										<c:out value="${bbsCmt.bbsCmtCn}"/>
									</td>
									<td><c:out value="${bbsCmt.regId}"/></td>
									<td>
										<c:out value="${bbsCmt.regDt}"/>
									</td>
									<td>
										<a href="javascript:fn_delete('<c:out value="${bbsCmt.bbsCmtIdx}"/>');" class="color_warn text_under">삭제</a>
									</td>
								</tr>
							</c:forEach>
						</c:if>
						</tbody>
					</table>
				</div>
				
				<div class="btn_wrap">
					<a href="javascript:fn_cancel();" class="cancle">취소하기</a>
					<c:if test="${STS_FLAG eq 'add'}">
						<a href="javascript:fn_add();" class="submit">등록하기</a>
					</c:if>
					<c:if test="${STS_FLAG eq 'upd'}">
						<a href="javascript:fn_upd();" class="submit">수정하기</a>
					</c:if>
				</div>
				<!-- 게시판 카테고리 등록 end -->
			</div>
		</div>
	</div>
</form>
<script type="text/javascript" src="/resources/smarteditor2/js/HuskyEZCreator.js" charset="utf-8"></script>
<script type="text/javascript">
// 스마트에디터 전역변수 선언
var editorObj = [];

window.onload = function(){
	if(${STS_FLAG eq 'add'}){
		if("${bbsResult.ctgryIdx}" != "0"){
			$("select[name=ctgryIdx]").val("${resultGrant.ctgryIdx}");
		}
	}
	if(${STS_FLAG eq 'upd'}){	// 게시글 수정 카테고리 변경 불가
		$("#ctgryIdx").attr("disabled", true);
	}
	display_update()
}
$(document).ready(function(){
	// 정렬순서, 파일 허용개수, 파일당 허용 업로드 용량(MB) 유효성검사 추가(숫자를 제외한 나머지 문자 입력 안되도록 수정)
	$("#sort").keyup(function(){
		$("#sort").val($("#sort").val().replace(/[^0-9]*$/, ''));
	});
	$("#filePermCnt").keyup(function(){
		$("#filePermCnt").val($("#filePermCnt").val().replace(/[^0-9]*$/, ''));
	});
	$("#filePermBt").keyup(function(){
		$("#filePermBt").val($("#filePermBt").val().replace(/[^0-9]*$/, ''));
	});
	
	// 첨부파일 선택시 용량 유효성검사
	$(document).on("change", '.tempFile', function(){
		if($(this)[0].files[0].size >= Number($("#bfilePermBt").val() * 1024 * 1024)){	// 첨부파일 제한용량 단위는 MB
			msg = "<spring:message code="valid.attach.file.size.msg" arguments="_###_"/>";
			msg = msg.replace("_###_", $("#bfilePermBt").val());
			alert(msg);
			// $(this).parent().parent().remove();
			return false;
		}

		var html = "";
		var dbDataLength = $(document).find(".dbData").length;
		var dbFlag = "N";	// dbFlag값
		html += '<tr class="fileTr">';
		html += '<td style="text-align:left;">';
		html += '<input type="hidden" name="dbData" id="dbData" class="dbData" value="N"/>';
		html += '<input type="hidden" name="fileSort" id="fileSort" value="'+Number(dbDataLength+1)+'">';
		html += '<input type="file" name="attachFile" class="attachFile" style="display:none;"/>';
		html += '<input type="text" name="fileOrgNm" readonly="readonly" class="w90" style="border:0px;">';
		html += '</td>';
		html += '<td>';
		html += '<a onclick="javascript:del_file(\''+dbFlag+'\', this);" class="color_warn text_under">삭제</a>';
		html += '</td>';
		html += '</tr>';
		$("#fileTbl tbody").append(html);

		var temp = $(document).find(".tempFile").eq(0).clone(true);
		$(document).find(".attachFile").eq(dbDataLength).prop("files", temp.eq(0).prop("files"))
		$(document).find(".attachFile").eq(dbDataLength).closest("tr").css("display", "");
		$(document).find(".attachFile").eq(dbDataLength).closest("tr").find("input[name=fileOrgNm]").val(temp.val().replace(/C:\\fakepath\\/i, ''));

		// $(document).find(".attachFile").eq(dbDataLength).on("change", function(){
		// 	$(document).find(".attachFile").eq(dbDataLength).closest("tr").css("display", "");
		// 	$(document).find(".attachFile").eq(dbDataLength).closest("tr").find("input[name=fileOrgNm]").val($(document).find(".attachFile").eq(dbDataLength).val().replace(/C:\\fakepath\\/i, ''));
		// });
	});

	// 내용에 들어가는 값 안보이게 처리
	if("${resultGrant.ediUseYn}" == "Y"){	// 에디터 사용여부가 Y일 경우
		setEditor();
		$("#bbsCn").css("display", "none");
		$("#bbsCn").next().css({"display":"block", "height":"400px"});	// , "height":"360px"
		$("#bbsCn").next().contents().find(".se2_input_area").children().eq(0).css("height", "400px");	// .find(".se2_input_area husky_seditor_editing_area_container")

	}else{	// 에디터 사용여부가 N일 경우
		$("textArea[id=bbsCn]").css("display", "block");
		$("#bbsCn").next().css({"display":"none"});
	}
});

// 스마트에디터 세팅함수
function setEditor(){
	nhn.husky.EZCreator.createInIFrame({
		oAppRef: editorObj,
		elPlaceHolder: "bbsCn",
		sSkinURI: "/resources/smarteditor2/SmartEditor2Skin.html",
		htParams : {
			// 툴바(속성변경 상단툴바) 사용 여부
			bUseToolbar : true,
			// 입력창 크기 조절바 사용 여부
			bUseVerticalResizer : true,
			// 모드 탭(Editor|HTML|TEXT) 사용 여부
			bUseModeChanger : true
		}
	});
}

// 화면 유효성 확인
function display_update(){
	// 첨부파일 display 설정
	if($("#ctgryIdx").val() != "" && $("#bfileUseYn").val() == "Y"){
		$("#fileTbl").css("display", "table");
		$("#fileTbl").prev().css("display", "block");
		$("#fileTbl").prev().prev().css("display", "block");
	}
	else {
		$("#fileTbl").css("display", "none");
		$("#fileTbl").prev().css("display", "none");
		$("#fileTbl").prev().prev().css("display", "none");
	}

	// 댓글 display 설정
	if((${STS_FLAG eq 'upd'})&&("${resultGrant.cmtUseYn}" == "Y")){
		$("#cmtTbl").css("display", "block")
	}
	else {
		$("#cmtTbl").css("display", "none")
	}
}

// 내용 -> 스마트에디터 유효성검사 체크
function valiEditor(){
	// ID가 스마트에디터인 textArea에 에디터에서 대입(editorObj : 스마트에디터 전역변수)
	editorObj.getById["bbsCn"].exec("UPDATE_CONTENTS_FIELD", []);

	// &nbsp, <br>, 공백 값 공백으로 치환
	var bbsCn = $("#bbsCn").val();
	bbsCn = bbsCn.replace(/&nbsp;/gi, "");
	bbsCn = bbsCn.replace(/<br>/gi, "");
	bbsCn = bbsCn.replace(/ /gi, "");

	// 내용이 스마트에디터일 경우 유효성검사
	if(bbsCn == "<p><\/p>" || bbsCn == ""){
		alert("내용을 입력하세요.");
		return false;
	}else{
		return true;
	}
}

// 등록하기
function fn_add(){

	if($("#bEdiUseYn").val() == "Y"){
		$("#bbsCn").removeAttr("data-parsley-required").removeAttr("data-parsley-error-message");
	}else if($("#bEdiUseYn").val() == "N"){
		$("#bbsCn").attr("data-parsley-required", "true").attr("data-parsley-error-message", "내용을 입력하세요.");
	}

	// validation 체크
	if(formValidation("bbsForm")){

		// 내용 스마트에디터 유효성검사 체크
		if($("#bEdiUseYn").val() == "Y"){
			if(!valiEditor()) return;
		}
		$("#bbsForm").find("#ctgryIdx").removeAttr("disabled");
		$("#bbsForm").find("#ref").val(${resultGrant.ref != null ? resultGrant.ref : 0});
		$("#bbsForm").find("#depth").val(${resultGrant.depth != null ? resultGrant.depth : 0});
		var frm = $("#bbsForm")[0];
		frm.action = "/bbs/add";
		frm.submit();
	}
}

// 수정하기
function fn_upd(){

	// validation 체크
	if(formValidation("bbsForm")){

		// 내용 스마트에디터 유효성검사 체크
		if($("#bEdiUseYn").val() == "Y"){
			if(!valiEditor()) return;
		}
		$("#bbsForm").find("#ctgryIdx").removeAttr("disabled");
		var frm = $("#bbsForm")[0];
		frm.action = "/bbs/upd";
		frm.submit();
	}
}

// 취소하기
function fn_cancel(){
	location.href = "/bbs/list"
}

// 카테고리 선택하기 
function fn_get_ctgry_conf(){
	
	if($("#ctgryIdx").val() == ""){	// 카테고리가 선택이 안된 경우
		$("#bbsCn").css("display", "none");
		$("#bbsCn").next().css("display", "none");
	}else if($("#ctgryIdx").val() != ""){	// 카테고리가 선택된 경우
		
		if ($("#ctgryIdx").val() != $("#bCtgryIdx").val()){	// 카테고리 선택이 변경된 경우
			$("#bCtgryIdx").val($("#ctgryIdx").val());

			var param = {
				ctgryIdx : $("#ctgryIdx").val()
			};
			
			$.ajax({
				type: "post",
				url: "/bbs/ctgry/grant",
				async: false,
				data: param,
				dataType: "json",
				success: function (res) {
					$("#bfileUseYn").val(res.resultGrant.fileUseYn);
					$("#bfilePermCnt").val(res.resultGrant.filePermCnt);
					$("#bfilePermBt").val(res.resultGrant.filePermBt);

					$("#bEdiUseYn").val(res.resultGrant.ediUseYn);

					// 에디터 사용여부가 Y이면 에디터 활성화, N이면 에디터 비활성화 및 textarea 활성화
					if(res.resultGrant.ediUseYn == "Y"){
						$("#bbsCn").css("display", "none");
						$("#bbsCn").next().css({"display":"block", "height":"400px"});	// , "height":"360px"
						$("#bbsCn").next().contents().find(".se2_input_area").children().eq(0).css("height", "400px");	// .find(".se2_input_area husky_seditor_editing_area_container")
					}else{
						$("#bbsCn").css("display", "block");
						$("#bbsCn").next().css("display", "none");
					}
				},
				error: function(err){
					alert(err.responseJSON.message);	// 코드값 확인 : err.responseJSON.code
					location.reload();
				}
			});
		}
		display_update();
	}
}

// 첨부파일 추가
function add_file(){
	var dbDataLength = $(document).find(".dbData").length;
 	if ($("#bfilePermCnt").val() <= dbDataLength){	// 첨부파일 개수 제한 확인
		alert("<spring:message code="valid.attach.file.count.max.msg"/>")
		return false;
	}
	// 임시파일을 초기화
	$(document).find(".tempFile").eq(0).val("");
	$(document).find(".tempFile").eq(0).click();
}

// 첨부파일 삭제
function del_file(fileIdx, curLoc){

	if(fileIdx == "N"){	// DB에서 가져온값이 아닐경우 화면상으로만 삭제
		$(curLoc).parent().parent().remove();
	}else{
		if(!confirm('<spring:message text="첨부파일을 삭제하시겠습니까?"/>')){
			return;
		}else{
			var param = {
				"fileIdx" : fileIdx
			}
			
			$.ajax({
				type: "post",
				url: "/bbs/file/del",
				data : param,
				dataType: "json",
				success: function(res){	
					alert("첨부파일이 정상적으로 삭제되었습니다.");
					location.reload();
				},
				error: function(e){
					alert(err.responseJSON.message);	// 코드값 확인 : err.responseJSON.code
					location.reload();
				}
			});

			/* $("#bbsForm").find("#delFileIdx").val(fileIdx);
			$("#bbsForm").find("#delFileTableNm").val($(curLoc).parent().parent().find("#fileTableNm").val());
			$("#bbsForm").find("#delFileTableIdx").val($(curLoc).parent().parent().find("#fileTableIdx").val());
			
			var frm = $("#bbsForm")[0];
			frm.action = "/bbs/file/del";
			frm.method = "post";
			frm.submit(); */
		}
	}
}

// 첨부파일 다운로드
function fileDown(sysPath, attPath, svf, ogf){

	var saveFileNm = svf;	// saveFileNm (DB에 저장된 임시 파일명)
	var orgFileNm = ogf;	// orgFileNm (진짜 파일명)
	location.href = "/com/fileDown?sysPath="+sysPath+"&attPath="+attPath+"&orgFileNm="+encodeURIComponent(orgFileNm)+"&saveFileNm="+saveFileNm;
}

// 댓글 삭제
function fn_delete(bbsCmtIdx){

	if(!confirm('<spring:message text="댓글을 삭제하시겠습니까?"/>')){
		return;
	}else{
		$("#bbsForm").find("#bbsCmtIdx").val(bbsCmtIdx);
		var frm = $("#bbsForm")[0];
		frm.action = "/bbs/bbsCmt/del";
		frm.method = "post";
		frm.submit();
	}
}
</script>
</body>
</html>