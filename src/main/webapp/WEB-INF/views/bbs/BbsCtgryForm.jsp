<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
<form id="bbsCtgryForm" name="bbsCtgryForm" method="post">
	
	<!-- 수정일 경우 hidden값 세팅 -->
	<c:if test="${STS_FLAG eq 'upd'}">
		<input type="hidden" id="ctgryIdx" name="ctgryIdx" value="${resultVO.ctgryIdx}">
	</c:if>
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 지부정보 등록 start -->
				<h3 class="page_title">
					게시판 카테고리
					<c:if test="${STS_FLAG eq 'add'}">
						등록
					</c:if>
					<c:if test="${STS_FLAG eq 'upd'}">
						수정
					</c:if>
				</h3>
				<table cellpadding="0" cellspacing="0" class="t_form">
					<colgroup>
						<col width="100px"><col width="130px"><col width="100px"><col width="130px"><col width="100px"><col width="130px">
					</colgroup>
					<tbody>
					<tr>
						<td class="th">카테고리</td>
						<td>
							<input type="text" id="ctgryNm" name="ctgryNm" class="w100" maxlength="10" value="${resultVO.ctgryNm}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.ctgryNm"/>">
						</td>
						<td class="th">정렬순서</td>
						<td>
							<input type="text" id="sort" name="sort" class="w100" maxlength="3" value="${resultVO.sort}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.sort"/>">
						</td>
						<td class="th">사용여부</td>
						<td>
							<div class="con">
								<c:forEach var="result" items="${useYnRadio}" varStatus="status">
									<p class="checkbox"><input type="radio" id="use_0${status.index+1}" name="useYn" value="${result.val1}" <c:if test="${resultVO.useYn eq result.val1}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.useYn"/>"><label for="use_0${status.index+1}"><i></i>${result.cdNm}</label></p>
								</c:forEach>
								<%-- <p class="checkbox"><input type="radio" id="use_01" name="useYn" value="Y" <c:if test="${resultVO.useYn eq 'Y'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.useYn"/>"><label for="use_01"><i></i>사용</label></p>
								<p class="checkbox"><input type="radio" id="use_02" name="useYn" value="N" <c:if test="${resultVO.useYn eq 'N'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.useYn"/>"><label for="use_02"><i></i>미사용</label></p> --%>
							</div>
						</td>
					</tr>
					<tr>
						<td class="th">첨부파일<br/>사용여부</td>
						<td>
							<div class="con">
								<c:forEach var="result" items="${useYnRadio}" varStatus="status">
									<p class="checkbox"><input type="radio" id="file_use_0${status.index+1}" name="fileUseYn" value="${result.val1}" <c:if test="${resultVO.fileUseYn eq result.val1}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.fileUseYn"/>"><label for="file_use_0${status.index+1}"><i></i>${result.cdNm}</label></p>
								</c:forEach>
								<%-- <p class="checkbox"><input type="radio" id="file_use_01" name="fileUseYn" value="Y" <c:if test="${resultVO.fileUseYn eq 'Y'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.fileUseYn"/>"><label for="file_use_01"><i></i>사용</label></p>
								<p class="checkbox"><input type="radio" id="file_use_02" name="fileUseYn" value="N" <c:if test="${resultVO.fileUseYn eq 'N'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.fileUseYn"/>"><label for="file_use_02"><i></i>미사용</label></p> --%>
							</div>
						</td>
						<td class="th">답글<br/>사용여부</td>
						<td>
							<div class="con">
								<c:forEach var="result" items="${useYnRadio}" varStatus="status">
									<p class="checkbox"><input type="radio" id="ans_use_0${status.index+1}" name="ansUseYn" value="${result.val1}" <c:if test="${resultVO.ansUseYn eq result.val1}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.ansUseYn"/>"><label for="ans_use_0${status.index+1}"><i></i>${result.cdNm}</label></p>
								</c:forEach>
								<%-- <p class="checkbox"><input type="radio" id="ans_use_01" name="ansUseYn" value="Y" <c:if test="${resultVO.ansUseYn eq 'Y'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.ansUseYn"/>"><label for="ans_use_01"><i></i>사용</label></p>
								<p class="checkbox"><input type="radio" id="ans_use_02" name="ansUseYn" value="N" <c:if test="${resultVO.ansUseYn eq 'N'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.ansUseYn"/>"><label for="ans_use_02"><i></i>미사용</label></p> --%>
							</div>
						</td>
						<td class="th">댓글<br/>사용여부</td>
						<td>
							<div class="con">
								<c:forEach var="result" items="${useYnRadio}" varStatus="status">
									<p class="checkbox"><input type="radio" id="cmt_use_0${status.index+1}" name="cmtUseYn" value="${result.val1}" <c:if test="${resultVO.cmtUseYn eq result.val1}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.cmtUseYn"/>"><label for="cmt_use_0${status.index+1}"><i></i>${result.cdNm}</label></p>
								</c:forEach>
								<%-- <p class="checkbox"><input type="radio" id="cmt_use_01" name="cmtUseYn" value="Y" <c:if test="${resultVO.cmtUseYn eq 'Y'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.cmtUseYn"/>"><label for="cmt_use_01"><i></i>사용</label></p>
								<p class="checkbox"><input type="radio" id="cmt_use_02" name="cmtUseYn" value="N" <c:if test="${resultVO.cmtUseYn eq 'N'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.cmtUseYn"/>"><label for="cmt_use_02"><i></i>미사용</label></p> --%>
							</div>
						</td>
					</tr>
					<tr>
						<td class="th">에디터<br/>사용여부</td>
						<td>
							<div class="con">
								<c:forEach var="result" items="${useYnRadio}" varStatus="status">
									<p class="checkbox"><input type="radio" id="edi_use_0${status.index+1}" name="ediUseYn" value="${result.val1}" <c:if test="${resultVO.ediUseYn eq result.val1}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.ediUseYn"/>"><label for="edi_use_0${status.index+1}"><i></i>${result.cdNm}</label></p>
								</c:forEach>
								<%-- <p class="checkbox"><input type="radio" id="edi_use_01" name="ediUseYn" value="Y" <c:if test="${resultVO.ediUseYn eq 'Y'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.ediUseYn"/>"><label for="edi_use_01"><i></i>사용</label></p>
								<p class="checkbox"><input type="radio" id="edi_use_02" name="ediUseYn" value="N" <c:if test="${resultVO.ediUseYn eq 'N'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.ediUseYn"/>"><label for="edi_use_02"><i></i>미사용</label></p> --%>
							</div>
						</td>
						<td class="th">파일<br/>허용개수</td>
						<td>
							<input type="text" id="filePermCnt" name="filePermCnt" class="w100" maxlength="2" value="${resultVO.filePermCnt}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.filePermCnt"/>">
						</td>
						<td class="th">파일당 허용<br/>업로드 용량(MB)</td>
						<td>
							<input type="text" id="filePermBt" name="filePermBt" class="w100" maxlength="4" value="${resultVO.filePermBt}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.filePermBt"/>">
						</td>
					</tr>
					<tr>
						<td class="th">댓글<br/>정렬방식</td>
						<td>
							<div class="con">
								<c:forEach var="result" items="${cmtOrdWayRadio}" varStatus="status">
									<p class="checkbox"><input type="radio" id="cmt_ord_0${status.index+1}" name="cmtOrdWay" value="${result.val1}" <c:if test="${resultVO.cmtOrdWay eq result.val1}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.cmtOrdWay"/>"><label for="cmt_ord_0${status.index+1}"><i></i>${result.cdNm}</label></p>
								</c:forEach>
								<%-- <p class="checkbox"><input type="radio" id="cmt_ord_01" name="cmtOrdWay" value="A" <c:if test="${resultVO.cmtOrdWay eq 'A'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.cmtOrdWay"/>"><label for="cmt_ord_01"><i></i>오름차순</label></p>
								<p class="checkbox"><input type="radio" id="cmt_ord_02" name="cmtOrdWay" value="D" <c:if test="${resultVO.cmtOrdWay eq 'D'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.cmtOrdWay"/>"><label for="cmt_ord_02"><i></i>내림차순</label></p> --%>
							</div>
						</td>
						<td class="th"></td>
						<td>
							
						</td>
						<td class="th"></td>
						<td>
							
						</td>
					</tr>
					</tbody>
				</table>
				
				<p class="stit mart40">권한 설정</p>
				<table cellpadding="0" cellspacing="0" class="t_form">
					<colgroup>
						<col width="200px"><col width="350px"><col width="200px"><col width="350px">
					</colgroup>
					<tbody>
						<tr>
							<td class="th">읽기 권한</td>
							<td>
								<div class="con">
									<c:forEach var="result" items="${rdAuthRadio}" varStatus="status">
										<p class="checkbox"><input type="radio" id="rd_auth_0${status.index+1}" name="rdAuth" value="${result.val1}" <c:if test="${resultVO.rdAuth eq result.val1}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.rdAuth"/>"><label for="rd_auth_0${status.index+1}"><i></i>${result.cdNm}</label></p>
									</c:forEach>
									<%-- <p class="checkbox"><input type="radio" id="rd_auth_01" name="rdAuth" value="M" <c:if test="${resultVO.rdAuth eq 'M'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.rdAuth"/>"><label for="rd_auth_01"><i></i>회원</label></p>
									<p class="checkbox"><input type="radio" id="rd_auth_02" name="rdAuth" value="A" <c:if test="${resultVO.rdAuth eq 'A'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.rdAuth"/>"><label for="rd_auth_02"><i></i>회원+비회원</label></p> --%>
								</div>
							</td>
							<td class="th">쓰기 권한</td>
							<td>
								<div class="con">
									<c:forEach var="result" items="${wtAuthRadio}" varStatus="status">
										<p class="checkbox"><input type="radio" id="wt_auth_0${status.index+1}" name="wtAuth" value="${result.val1}" <c:if test="${resultVO.wtAuth eq result.val1}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.wtAuth"/>"><label for="wt_auth_0${status.index+1}"><i></i>${result.cdNm}</label></p>
									</c:forEach>
									<%-- <p class="checkbox"><input type="radio" id="wt_auth_01" name="wtAuth" value="C" <c:if test="${resultVO.wtAuth eq 'C'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.wtAuth"/>"><label for="wt_auth_01"><i></i>담당자</label></p>
									<p class="checkbox"><input type="radio" id="wt_auth_02" name="wtAuth" value="A" <c:if test="${resultVO.wtAuth eq 'A'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.wtAuth"/>"><label for="wt_auth_02"><i></i>익명 쓰기</label></p>
									<p class="checkbox"><input type="radio" id="wt_auth_03" name="wtAuth" value="N" <c:if test="${resultVO.wtAuth eq 'N'}">checked</c:if> data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.wtAuth"/>"><label for="wt_auth_03"><i></i>사용안함</label></p> --%>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				
				<div class="btn_wrap">
					<a href="javascript:fn_cancel();" class="cancle">취소하기</a>
					<c:if test="${STS_FLAG eq 'add'}">
						<a href="javascript:fn_add();" class="submit">등록하기</a>
					</c:if>
					<c:if test="${STS_FLAG eq 'upd'}">
						<a href="javascript:fn_upd();" class="submit">수정하기</a>
					</c:if>
				</div>
				<!-- 게시판 카테고리 등록 end -->
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
$(document).ready(function(){
	// 정렬순서, 파일 허용개수, 파일당 허용 업로드 용량(MB) 유효성검사 추가(숫자를 제외한 나머지 문자 입력 안되도록 수정)
	$("#sort").keyup(function(){
		$("#sort").val($("#sort").val().replace(/[^0-9]*$/, ''));
	});
	$("#filePermCnt").keyup(function(){
		$("#filePermCnt").val($("#filePermCnt").val().replace(/[^0-9]*$/, ''));
	});
	$("#filePermBt").keyup(function(){
		$("#filePermBt").val($("#filePermBt").val().replace(/[^0-9]*$/, ''));
	});
});

// 등록하기
function fn_add(){

	// validation 체크
	if(formValidation("bbsCtgryForm")){

		var frm = $("#bbsCtgryForm")[0];
		frm.action = "/bbs/ctgry/add";
		frm.submit();
	}
}

// 수정하기
function fn_upd(){

	// validation 체크
	if(formValidation("bbsCtgryForm")){

		var frm = $("#bbsCtgryForm")[0];
		frm.action = "/bbs/ctgry/upd";
		frm.submit();
	}
}

// 취소하기
function fn_cancel(){
	location.href = "/bbs/ctgry/list"
}
</script>
</body>
</html>