<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
<form id="bbsCtgryFrm" name="bbsCtgryFrm" method="get">

	<input type="hidden" id="ctgryIdx" name="ctgryIdx" value=0 />
	
	<!-- 페이징처리 변수 -->
	<input type="hidden" id="currentPageNo" name="currentPageNo" value="${paging.currentPageNo}"/>
	<input type="hidden" id="pageSize" name="pageSize" value="${paging.pageSize}"/>
	<input type="hidden" id="recordCountPerPage" name="recordCountPerPage" value="${paging.recordCountPerPage}"/>
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 게시판 카테고리 관리 start -->
				<h3 class="page_title">게시판 카테고리 관리</h3>
				<div class="border_box">
					<div class="flex">
						<p class="tit">카테고리</p>
						<div class="con">
							<input type="text" id="searchCtgryNm" name="searchCtgryNm" value="${vo.searchCtgryNm}" placeholder="">
						</div>
						<button type="button" id="btnSearch" class="s_btn submit" onclick="javascript:fn_search();">검색</button>
					</div>
				</div>
				
				<div class="clear">
					<p class="t_total">총 ${totCnt}건</p>
					<a href="/bbs/ctgry/addView" class="s_btn submit right">등록하기</a>
				</div>
				<table cellpadding="0" cellspacing="0" class="t_list mart10">
					<colgroup>
						<col width="65px"><col width="150px"><col width="100px"><col width="100px"><col width="100px"><col width="100px"><col width="100px"><col width="100px"><col width="70px">
					</colgroup>
					<thead>
					<tr>
						<th>번호</th>
						<th>카테고리</th>
						<th>사용여부</th>
						<th>파일허용개수</th>
						<th>파일당 허용 업로드 용량</th>
						<th>댓글 정렬방식</th>
						<th>등록자</th>
						<th>등록일자</th>
						<th>관리</th>
					</tr>
					</thead>
					
					<tbody>
					<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td colspan="9"><spring:message code="no.search.data.msg"/></td>
					</tr>
					</c:if>
					<c:if test="${fn:length(resultList) > 0}">
					<c:forEach var="result" items="${resultList}" varStatus="status">
					<tr>
						<!-- 전체 조회건수-((현재페이지 번호-1) * 1페이지당 레코드수 + 현재게시물 출력순서 -->
						<td>${totCnt-((paging.currentPageNo-1) * paging.recordCountPerPage + status.index)}</td>
						<td><c:out value="${result.ctgryNm}"/></td>
						<td><c:out value="${result.useYnNm}"/></td>
						<td><c:out value="${result.filePermCnt}"/></td>
						<td><c:out value="${result.viewFilePermBt}"/></td>
						<td><c:out value="${result.cmtOrdWayNm}"/></p></td>
						<td><c:out value="${result.regNm}"/></td>
						<td><c:out value="${result.regDt}"/></td>
						<td>
							<a href="/bbs/ctgry/updView?ctgryIdx=${result.ctgryIdx}" class="color_warn text_under">수정</a>&nbsp;
							<c:if test="${result.viewDelFlag eq 'Y'}">
								<a href="javascript:fn_delete('<c:out value="${result.ctgryIdx}"/>');" class="color_warn text_under">삭제</a>
							</c:if>
						</td>
					</tr>
					</c:forEach>
					</c:if>
					</tbody>
				</table>
				${paging.pagination}
				<!-- 지부정보 관리 end -->
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
// 조회
function fn_search(){
	$("#currentPageNo").val(1);	// 검색했을때 1페이지 유지
	
	var frm = $("#bbsCtgryFrm")[0];
	frm.action = "/bbs/ctgry/list";
	frm.submit();
}

// 삭제
function fn_delete(ctgryIdx){

	if(!confirm('<spring:message text="카테고리를 삭제하시겠습니까?"/>')){
		return;
	}else{
		$("#bbsCtgryFrm").find("#ctgryIdx").val(ctgryIdx);
		var frm = $("#bbsCtgryFrm")[0];
		frm.action = "/bbs/ctgry/del";
		frm.method = "post";
		frm.submit();
	}
}
</script>
</body>
</html>