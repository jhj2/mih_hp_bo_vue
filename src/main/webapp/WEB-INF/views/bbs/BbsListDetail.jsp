<%@ page import="com.mih.bo.task.bbs.vo.Bbs" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
<form id="bbsCmtFrm" name="bbsCmtFrm" method="get" enctype="multipart/form-data">

	<input type="hidden" id="bbsIdx" name="bbsIdx" value="${bbsResult.bbsIdx}" />
	<input type="hidden" id="bbsCmtIdx" name="bbsCmtIdx" value="0" />
	<input type="hidden" id="ref" name="ref" value="0" />
	<input type="hidden" id="depth" name="depth" value="0" />
	<input type="hidden" id="ctgryIdx" name="ctgryIdx" value="${resultGrant.ctgryIdx}">
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 지부정보 등록 start -->
				<h3 class="page_title">
					게시글 상세조회
				</h3>
				<c:if test="${resultGrant.ansUseYn eq 'Y'}">
				<div class="clear" style="margin-top:-45px;">
					<a onclick="javascript:fn_reBbs();" class="s_btn submit right">답글 등록</a>
				</div>
				</c:if>
				<table cellpadding="0" cellspacing="0" class="t_form">
					<colgroup>
						<col width="150px"><col width="230px"><col width="150px"><col width="390px">
					</colgroup>
					<tbody>
						<tr>
							<td class="th" >제목</td>
							<td colspan="3">
								<c:out value="${bbsResult.bbsSj}"/>
							</td>
						</tr>
						<tr>
							<td class="th">카테고리</td>
							<td>
								<c:out value="${bbsResult.ctgryNm}"/>
							</td>
							<td class="th">등록일</td>
							<td >
								<c:out value="${bbsResult.regDt}"/>
							</td>
						</tr>
						<tr>
							<td class="th">내용</td>
							<td colspan="3">
								<c:out value="${bbsResult.viewBbsCn}" escapeXml="false"/>
							</td>
						</tr>
					</tbody>
				</table>
				<!-- 게시판 카테고리 등록 end -->
			</div>
			
			<div class="contents" id="fileTbl" style="display:none">
				<!-- 첨부파일 시작 -->
				<p class="stit mart30">첨부파일</p>
				
				<table cellpadding="0" cellspacing="0" class="t_list mart10">
					<colgroup>
						<col width="365px">
					</colgroup>
					<thead>
					<tr>
						<th>첨부파일</th>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="result" items="${fileList}" varStatus="status">
						<tr class="fileTr">
							<td style="text-align:left;">
								<input type="hidden" name="dbData" class="dbData" value="Y"/>
								<input type="hidden" name="fileTableNm" id="fileTableNm" value="<c:out value="${result.fileTableNm}" />" />
								<input type="hidden" name="fileTableIdx" id="fileTableIdx" value="<c:out value="${result.fileTableIdx}" />" />
								<input type="hidden" name="fileIdx" id="fileIdx" value="<c:out value="${result.fileIdx}" />" />
								<input type="hidden" name="fileSort" id="fileSort" value="${result.fileSort}">
								<input type="file" name="attachFile" class="attachFile" style="display:none;"/>
								<input type="text" name="fileOrgNm" class="w90" readonly="readonly" value="<c:out value="${result.fileOrgNm}" />" style="cursor:pointer; border:0px;"
									onclick="javascript:fileDown('<c:out value="${result.sysPath}" />', '<c:out value="${fn:replace(result.attPath, '\\\\', '/')}" />', '<c:out value="${result.fileSysNm}" />', '<c:out value="${result.fileOrgNm}" />');event.cancelBubble=true;">
							</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>

			<!-- 게시판 댓글 등록 start -->
			<div class="contents" id="cmtTbl" style="display:none">
				<p class="stit mart30">댓글</p>
				<div class="clear">
					<!-- <p class="t_total">총 ${totCnt}건</p>
					<a href="/bbs/addView" class="s_btn submit right">댓글달기</a> -->
				</div>
				<table cellpadding="0" cellspacing="0" class="t_list mart10">
					<colgroup>
<%--						<col width="365px"><col width="50px"><col width="50px">--%>
						<col width="80%"><col width="10%"><col width="10%">
					</colgroup>
					<thead>
					<tr>
						<th>댓글 내용</th>
						<th>등록자</th>
						<th>등록일자</th>
					</tr>
					</thead>
					
					<tbody>
					<c:if test="${fn:length(bbsCmtList) == 0}">
					<tr>
						<td colspan="3"><spring:message text="댓글이 없습니다."/></td>
					</tr>
					</c:if>
					<c:if test="${fn:length(bbsCmtList) > 0}">
						<c:forEach var="bbsCmt" items="${bbsCmtList}" varStatus="status">
						<tr name="1" class="${bbsCmt.ref}" value="${bbsCmt.depth}" style="display: table-row;">
							<td style="text-align:left;">
								<c:if test="${bbsCmt.depth ne '0'}">
									<c:set var="nPx" value="40"/>	<%-- 게시글 답글 영역 아이콘 크기 --%>
									<span style="margin-left: ${nPx * bbsCmt.depth}px;"/>
									<c:out value="[RE]"/>&nbsp;&nbsp;
								</c:if>
								<c:choose>
									<c:when test="${bbsCmt.reBbsFlag}">
										<button type="button" id="reCmtBtn" class="s_btn submit" style="display:inline-block;" onclick="javascript:setup_reCmtList(${bbsCmt.bbsCmtIdx},${bbsCmt.depth},this);">-</button>
									</c:when>
									<c:otherwise>
										<button type="button" id="reCmtBtn" class="s_btn submit" style="display:none;" onclick="javascript:setup_reCmtList(${bbsCmt.bbsCmtIdx},${bbsCmt.depth},this);">-</button>
									</c:otherwise>
								</c:choose>
<%--								<c:if test="${bbsCmt.reBbsFlag}">--%>
<%--									<button type="button" id="reCmtBtn" class="s_btn submit" onclick="javascript:setup_reCmtList(${bbsCmt.bbsCmtIdx},${bbsCmt.depth},this);">-</button></c:if>--%>
								<c:out value="${bbsCmt.bbsCmtCn}"/>
                                <c:if test="${resultGrant.repUseYn && bbsCmt.depth == 0}">
                                    <button type="button" id="btnSearch" class="s_btn submit" style="float: right;" onclick="javascript:fn_rep(${bbsCmt.bbsIdx}, ${bbsCmt.bbsCmtIdx}, ${bbsCmt.depth}, this)">답글</button>
                                </c:if>
							</td>
							<td>
								<c:out value="${bbsCmt.regId}"/>
							</td>
							<td>
								<c:out value="${bbsCmt.regDt}"/>
							</td>
						</tr>
						</c:forEach>
					</c:if>
					</tbody>
				</table>
				<!-- <table cellpadding="0" cellspacing="0" class="t_form">
					<colgroup>
						<col width="150px"><col width="670px"><col width="100px">
					</colgroup>
					<tbody>
						<tr>
							<td class="th" >댓글 적는 부분</td>
							<td>
								<input type="text" id="bbsCmtCn" name="bbsCmtCn" value="">
							</td>
						</tr>
					</tbody>
				</table> -->
			</div>
			
			<!-- 게시판 댓글 end -->
			<div class="btn_wrap">
				<a href="/bbs/list" class="submit">목록</a>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
window.onload = function(){
	if("${resultGrant.cmtUseYn}" == "Y"){	//댓글 사용여부
		$("#cmtTbl").css("display", "block");
	}
	if("${resultGrant.fileUseYn}" == "Y"){	//첨부파일 사용여부
		$("#fileTbl").css("display", "block");
	}
}

// 조회
function fn_search(){
	$("#currentPageNo").val(1);	// 검색했을때 1페이지 유지
	
	var frm = $("#bbsCmtFrm")[0];
	frm.action = "/bbs/list";
	frm.submit();
}

// 삭제
function fn_delete(bbsIdx){

	if(!confirm('<spring:message text="댓글을 삭제하시겠습니까?"/>')){
		return;
	}else{
		$("#bbsCmtFrm").find("#bbsCmtIdx").val(bbsIdx);
		var frm = $("#bbsCmtFrm")[0];
		frm.action = "/bbs/bbsCmt/del";
		frm.method = "post";
		frm.submit();
	}
}

// 첨부파일 다운로드
function fileDown(sysPath, attPath, svf, ogf){

	var saveFileNm = svf; // saveFileNm (DB에 저장된 임시 파일명)
	var orgFileNm = ogf;	// orgFileNm (진짜 파일명)
	location.href = "/com/fileDown?sysPath="+sysPath+"&attPath="+attPath+"&orgFileNm="+encodeURIComponent(orgFileNm)+"&saveFileNm="+saveFileNm;
}
// 답글
function fn_reBbs(){
	$("#bbsCmtFrm").find("#ref").val(${bbsResult.bbsIdx});
	$("#bbsCmtFrm").find("#depth").val(${bbsResult.depth}+1);
	var frm = $("#bbsCmtFrm")[0];
	frm.action = "/bbs/addView";
	frm.method = "get";
	frm.submit();
}
// 리플 대댓글 활성/비활성
function setup_reCmtList(bbsCmtIdx, nDepth, obj){
	var bFlag = 0;
	if ($(obj).text() == '+'){
		bFlag = 1;
		$(obj).text("-");
	} else {
		bFlag = 0;
		$(obj).text("+");
	}
	var noneDisplayList = [];
	var temp = $("#cmtTbl tbody").find('tr[class*='+bbsCmtIdx+']');
	for(var i=0;i<temp.length;i++) {
		var tempDepth = Number(temp.eq(i).attr("value"));
		var subDepthFlag = false;
		if (nDepth == (tempDepth-1)){
			temp.eq(i).attr("name", bFlag);
			subDepthFlag = true;
		}
		if ( subDepthFlag ){	// 대상
			if( bFlag == 1 ){
				temp.eq(i)[0].style.display = "table-row";
			} else {
				temp.eq(i)[0].style.display = "none";
			}
		} else {	// 하위
			var bDisplay = Number(temp.eq(i).attr("name"));
			var bValue = (temp.eq(i).attr("value"));
			var strDisplay = temp.eq(i).attr("class");
			for (var j = 0; j < noneDisplayList.length; j++) {
				if (strDisplay.indexOf(noneDisplayList[j]) !== -1) {
					bDisplay = 0;
					break;
				}
			}
			if (!(bValue >= 0)) {	<!-- 생성된 답글창인 경우 변경하지 않는다. continue -->
				continue;
			}
			if (bFlag == 1 && bDisplay == 1){
				temp.eq(i)[0].style.display = "table-row";
			} else {
				temp.eq(i)[0].style.display = "none";
				noneDisplayList.push(temp.eq(i).find('td').eq(0).text());
			}
		}
	}
}
// 리플
function fn_rep(bbsIdx, bbsCmtIdx, depth, obj){
	if( $(obj).parent().parent().parent().find("#rep_input") ){
		$(obj).parent().parent().parent().find("#rep_input").remove();
	}
	var html = "";
	html += '<tr id="rep_input" class="'+bbsCmtIdx+'">'; //!important
	html += '<td colspan="3" style="text-align: left;">';
	html += '<input type="text" id="rep_text" style="width: 70%;" placeholder="리플(대댓글)을 입력해주세요."/>';
	html += '<button type="button" id="btnRep" class="s_btn submit" onclick="javascript:fn_rep_enter('+bbsIdx+','+bbsCmtIdx+','+depth+',this)">등록</button>';
	html += '</td>';
	html += '</tr>';
	$(obj).parent().parent().after(html);
}
function fn_rep_enter(bbsIdx, bbsCmtIdx, depth, obj){
	<%
		Bbs bbs = new Bbs();
	%>
	var bbs = '<%= bbs %>';
	var tempBbsCmtCn = $(obj).parent().find('#rep_text').val();
	if (tempBbsCmtCn == ""){
		alert('<spring:message code="valid.notice.content.write"/>');
		return;
	}
	var param = {
		bbsIdx : bbsIdx,
		bbsCmtIdx : bbsCmtIdx,
		depth : (depth+1),
		bbsCmtCn : tempBbsCmtCn
	};

	$.ajax({
		type: "get",
		url: "/bbs/detail/addReCmt",
		async: false,
		data: param,
		dataType: "json",
		success: function (res) {
			console.log(res);
			var bbs = res.bbs;

			var html = "";
			html += '<tr name="1" class="'+bbs.ref+'" value="'+bbs.depth+'" style="display: table-row;">';
			html += '<td style="text-align:left;">';
			var nPx = 40;
			html += '<span style="margin-left:'+(nPx*bbs.depth)+'px;"/>';
			html +=	'[RE]&nbsp;&nbsp;';
			html += bbs.bbsCmtCn;
			html +=	'</td>'
			html += '<td>'+bbs.regId+'</td>';
			html +=	'<td>'+bbs.regDt+'</td>';
			html +=	'</tr>';

			$(obj).parent().parent().parent().find("tr[class="+bbsCmtIdx+"]").eq(-1).after(html);	<!-- 리플의 마지막 -->
			var temp = $(obj).parent().parent().parent().find('tr');
			for(var i=0;i<temp.size();i++){
				if(i > 0 && temp.eq(i).attr('class') == bbsCmtIdx){
					temp.eq(i-1).find('#reCmtBtn').eq(0)[0].style.display = "inline-block";			<!-- 부모 리플 버튼 -->
					break;
				}
			}
			$(obj).parent().parent().remove();														<!-- 답글 등록창 삭제 -->
		},
		error: function (e) {
			alert(e.responseJSON.message);	// 코드값 확인 : err.responseJSON.code
			location.reload();
		}
	});
}
</script>
</body>
</html>