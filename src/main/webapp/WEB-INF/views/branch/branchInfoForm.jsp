<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
<form id="branchFrm" name="branchFrm" method="post" enctype="multipart/form-data" data-parsley-ui-enabled="false">
	
	<!-- 위도, 경도 -->
	<input type="hidden" id="bhfLa" name="bhfLa" value="${resultVO.bhfLa}">
	<input type="hidden" id="bhfLo" name="bhfLo" value="${resultVO.bhfLo}">
	<input type="hidden" id="delFileIdx" name="delFileIdx" value="">
	
	<c:if test="${STS_FLAG eq 'upd'}">
		<input type="hidden" id="bhfIdx" name="bhfIdx" value="${resultVO.bhfIdx}">
	</c:if>
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 지부정보 등록 start -->
				<h3 class="page_title">
					지부정보
					<c:if test="${STS_FLAG eq 'add'}">
						등록
					</c:if>
					<c:if test="${STS_FLAG eq 'upd'}">
						수정
					</c:if>
				</h3>
				<table cellpadding="0" cellspacing="0" class="t_form">
					<colgroup>
						<col width="125px"><col width="135px"><col width="125px"><col width="250px"><col width="125px"><col width="340px">
					</colgroup>
					<tbody>
					<tr>
						<td class="th">지부</td>
						<td>
							<input type="text" id="bhfNm" name="bhfNm" class="w100" maxlength="10" value="${resultVO.bhfNm}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.bhfNm"/>">
						</td>
						<td class="th">사업자등록번호</td>
						<td>
							<div class="flex">
								<input type="text" id="bizrno" name="bizrno" class="w60" maxlength="12" value="${resultVO.bizrno}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.bizrno"/>">	<!-- onblur="javascript:bizNoFormat(this, 1, 'blur');" onkeyup="javascript:bizNoFormat(this, 1, 'keyup');" -->
								<p class="txt_count">‘-’ 포함 입력</p>
							</div>
						</td>
						<td class="th">지부 슬로건</td>
						<td>
							<div class="flex">
								<input type="text" id="slogan" name="slogan" class="w60" value="${resultVO.slogan}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.slogan"/>" maxlength="15">
								<p class="txt_count">10자 내외, 최대 15자</p>
							</div>
						</td>
					</tr>
					<tr>
						<td class="th">내용</td>
						<td colspan="5">
							<c:if test="${STS_FLAG eq 'add'}">
								<div class="file_wrap">
									<p class="thumbnail"><img src="" alt=""><span class="img_delete">이미지 삭제</span></p>
									<input id="upload01" type="file" name="attachFile" class="uploadBtn" accept="image/*">
									<label for="upload01" class="add_file">이미지 찾기</label>
								</div>
								<div class="file_wrap">
									<p class="thumbnail"><img src="" alt=""><span class="img_delete">이미지 삭제</span></p>
									<input id="upload02" type="file" name="attachFile" class="uploadBtn" accept="image/*">
									<label for="upload02" class="add_file">이미지 찾기</label>
								</div>
								<div class="file_wrap">
									<p class="thumbnail"><img src="" alt=""><span class="img_delete">이미지 삭제</span></p>
									<input id="upload03" type="file" name="attachFile" class="uploadBtn" accept="image/*">
									<label for="upload03" class="add_file">이미지 찾기</label>
								</div>
								<div class="file_wrap">
									<p class="thumbnail"><img src="" alt=""><span class="img_delete">이미지 삭제</span></p>
									<input id="upload04" type="file" name="attachFile" class="uploadBtn" accept="image/*">
									<label for="upload04" class="add_file">이미지 찾기</label>
								</div>
								<div class="file_wrap">
									<p class="thumbnail"><img src="" alt=""><span class="img_delete">이미지 삭제</span></p>
									<input id="upload05" type="file" name="attachFile" class="uploadBtn" accept="image/*">
									<label for="upload05" class="add_file">이미지 찾기</label>
								</div>
								<p class="txt_count">용량 5MB 이내</p>
							</c:if>
							
							<c:if test="${STS_FLAG eq 'upd'}">
								<c:forEach var="i" begin="0" end="4" varStatus="status">
									<c:set var="loop_flag" value="false" />	<!-- flag값 -->
									<c:forEach var="result" items="${imgList}" varStatus="status2">
										<c:if test="${status.index+1 eq result.fileSort}">
											<div class="file_wrap">	<%-- ${status.index+1}/${result.fileSort} --%>
												<p class="thumbnail"><img src="<c:out value="${config:value('file.attach.web.path')}"/><c:out value="${result.attPath}"/>/<c:out value="${result.fileSysNm}"/>" alt=""><span class="img_delete" id="db_img_delete">이미지 삭제</span></p>
												<input id="upload0${status.index+1}" type="file" name="attachFile" class="uploadBtn" accept="image/*">
												<label for="upload0${status.index+1}" class="add_file">이미지 찾기</label>
												
												<input type="hidden" id="fileIdx" name="fileIdx" value="${result.fileIdx}">
												<input type="hidden" id="fileTableIdx" name="fileTableIdx" value="${result.fileTableIdx}">
												<input type="hidden" id="fileSort" name="fileSort" value="${result.fileSort}">
											</div>
											<c:set var="loop_flag" value="true" />
										</c:if>
									</c:forEach>
									
									<c:if test="${not loop_flag}">
										<div class="file_wrap">	<%-- ${status.index+1}-${result.fileSort} --%>
											<p class="thumbnail"><img src="" alt=""><span class="img_delete">이미지 삭제</span></p>
											<input id="upload0${status.index+1}" type="file" name="newAttachFile" class="uploadBtn" accept="image/*">
											<label for="upload0${status.index+1}" class="add_file">이미지 찾기</label>
											
											<input type="hidden" id="fileIdx" name="fileIdx" value="${result.fileIdx}">
											<input type="hidden" id="fileTableIdx" name="fileTableIdx" value="${result.fileTableIdx}">
											<input type="hidden" id="newFileSort" name="newFileSort" value="${status.index+1}">
										</div>
									</c:if>
								</c:forEach>
								<p class="txt_count">용량 5MB 이내</p>
							</c:if>
						</td>
					</tr>
					
					<tr>
						<td class="th">주소</td>
						<td colspan="5">
							<!-- 0820 수정 주소입력 -->
							<div class="flex address">
								<p class="find_add">
									<input type="text" id="zip" name="zip" readonly class="w100" value="${resultVO.zip}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.zip"/>">
									<a href="javascript:fn_addr('#zip', '#rdnmadrBass', '#rdnmadrDetail');">주소 찾기</a>
								</p>
								<input type="text" id="rdnmadrBass" name="rdnmadrBass" readonly value="${resultVO.rdnmadrBass}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.zip"/>">
								<input type="text" id="rdnmadrDetail" name="rdnmadrDetail" value="${resultVO.rdnmadrDetail}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.rdnmadrDetail"/>" placeholder="상세 주소" maxlength="50">
							</div>
						</td>
					</tr>
					<tr>
						<td class="th">이용시간</td>
						<td colspan="5">
							<input type="text" id="useTime" name="useTime" class="w60" value="${resultVO.useTime}" maxlength="50" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.useTime"/>">
							<p class="txt_count">30자 내외, 최대 50자</p>
						</td>
					</tr>
					<tr>
						<td class="th">이용안내</td>
						<td colspan="5">	<!-- ${fn:replace(resultVO.useGuidance, crcn, br)} -->
							<textarea name="useGuidance" id="useGuidance" rows="6" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.useGuidance"/>">${resultVO.useGuidance}</textarea>
							<p class="txt_count">200자 내외, 최대400자</p>
						</td>
					</tr>
					<tr>
						<td class="th">오시는 길</td>
						<td colspan="5">
							<textarea name="visitStret" id="visitStret" rows="6" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.visitStret"/>">${resultVO.visitStret}</textarea>
							<p class="txt_count">200자 내외, 최대400자</p>
						</td>
					</tr>
					</tbody>
				</table>
				
				<p class="stit mart40">결제 정보</p>
				<table cellpadding="0" cellspacing="0" class="t_form">
					<colgroup>
						<col width="170px"><col width="370px"><col width="170px"><col width="370px">
					</colgroup>
					<tbody>
						<tr>
							<td class="th">사이트 Key</td>
							<td>
								<input type="text" id="siteKey" name="siteKey" class="w100" value="${resultVO.siteKey}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.siteKey"/>" maxlength="25">
							</td>
							<td class="th">사이트 코드</td>
							<td>
								<input type="text" id="siteCd" name="siteCd" class="w100" value="${resultVO.siteCd}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.siteCd"/>" maxlength="5">
							</td>
						</tr>
						<%-- <tr>
							<td class="th">사이트명</td>
							<td>
								<input type="text" id="siteNm" name="siteNm" class="w100" value="${resultVO.siteNm}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.siteNm"/>" maxlength="20" placeholder="영어(알파벳)만 입력 가능합니다.">
							</td>
							<td class="th">그룹 아이디</td>
							<td>
								<input type="text" id="groupId" name="groupId" class="w100" value="${resultVO.groupId}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.groupId"/>" maxlength="20">
							</td>
						</tr> --%>
					</tbody>
				</table>
				
				<p class="stit mart40">기타정보</p>
				<table cellpadding="0" cellspacing="0" class="t_form">
					<colgroup>
						<col width="170px"><col width="370px"><col width="170px"><col width="370px">
					</colgroup>
					<tbody>
						<tr>
							<td class="th">오픈일</td>
							<td colspan="3">
								<input type="text" id="opnDt" name="opnDt" class="datepick" value="${resultVO.opnDt}" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.opnDt"/>">
								<select name="opnSi" id="opnSi" class="w130" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.opnDt"/>">
									<option value="">선택</option>
									<c:forEach var="i" begin="0" end="23" step="1">
										<c:choose>
											<c:when test="${i < 10}">
												<option value="0${i}" <c:if test="${fn:substring(resultVO.opnSi, 1, 2) eq i}">selected</c:if>>0${i}</option>
											</c:when>
											<c:otherwise>
												<option value="${i}" <c:if test="${resultVO.opnSi eq i}">selected</c:if>>${i}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>&nbsp;시&nbsp;
								<select name="opnMin" id="opnMin" class="w130" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.opnDt"/>">
									<option value="">선택</option>
									<c:forEach var="i" begin="0" end="59" step="1">
										<c:choose>
											<c:when test="${i < 10}">
												<option value="0${i}" <c:if test="${fn:substring(resultVO.opnMin, 1, 2) eq i}">selected</c:if>>0${i}</option>
											</c:when>
											<c:otherwise>
												<option value="${i}" <c:if test="${resultVO.opnMin eq i}">selected</c:if>>${i}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>&nbsp;분&nbsp;
								<%-- <select name="opnSec" id="opnSec" class="w130" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.branch.opnDt"/>">
									<option value="">선택</option>
									<c:forEach var="i" begin="0" end="59" step="1">
										<c:choose>
											<c:when test="${i < 10}">
												<option value="0${i}" <c:if test="${fn:substring(resultVO.opnSec, 1, 2) eq i}">selected</c:if>>0${i}</option>
											</c:when>
											<c:otherwise>
												<option value="${i}" <c:if test="${resultVO.opnSec eq i}">selected</c:if>>${i}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>&nbsp;초 --%>
							</td>
						</tr>
						<tr>
							<td class="th">알림톡링크 URL</td>
							<td colspan="3">
								<input type="text" id="linkUrl" name="linkUrl" class="w60" value="${resultVO.linkUrl}" maxlength="50">
							</td>
						</tr>
					</tbody>
				</table>
				
				<div class="btn_wrap">
					<a href="javascript:fn_cancel();" class="cancle">취소하기</a>
					<c:if test="${STS_FLAG eq 'add'}">
						<a href="javascript:fn_add();" class="submit">등록하기</a>
					</c:if>
					<c:if test="${STS_FLAG eq 'upd'}">
						<a href="javascript:fn_upd();" class="submit">수정하기</a>
					</c:if>
				</div>
				<!-- 지부정보 등록 end -->
			</div>
		</div>
	</div>
	<div id="modalID" class="modal">
		<p>modal example <br>modal example <br>modal example</p>
	</div>
</form>
<%-- 다음 주소 API --%>
<!-- https로 사용시 -->
<script src="https://spi.maps.daum.net/imap/map_js_init/postcode.v2.js"></script>

<!-- 다음주소 API -> 위도, 경도 뽑는 API -->
<script src="//dapi.kakao.com/v2/maps/sdk.js?appkey=${kakaoMapKey}&libraries=services"></script>
<script type="text/javascript">
$(document).ready(function(){

	$(".file_wrap .uploadBtn").change(loadPreviews_click);

	// 첨부파일 이미지 초기화
	$(".img_delete").on("click", function(){

		if($(this).attr("id") == "db_img_delete"){	// DB삭제
			$("#delFileIdx").val($("#delFileIdx").val() + $(this).parent().parent().find("#fileIdx").val() + "|");
			$(this).parents(".file_wrap").removeClass("add");
			$(this).siblings("img").attr("src", "");
			$(this).parent().siblings(".uploadBtn").val("");

			// 닫기버튼 비활성화
			$(this).css("display", "none");
		}else{
			$(this).parents(".file_wrap").removeClass("add");
			$(this).siblings("img").attr("src", "");
			$(this).parent().siblings(".uploadBtn").val("");

			// 닫기버튼 활성화
			$(this).css("display", "none");
		}
		$(this).parent().parent().find(".add_file").css("opacity", "1");
	});

	// 수정화면일때 DB에서 불러온 이미지 삭제버튼 활성화
	if("${STS_FLAG}" == "upd"){
		var fileTableIdx = $(".file_wrap input[name=fileTableIdx]");
		for(var i=0; i<fileTableIdx.length; i++){
			if(fileTableIdx.eq(i).val() != ""){
				fileTableIdx.eq(i).prev().prev().prev().prev().find("span").css("display", "block");
				fileTableIdx.eq(i).prev().prev().css("opacity", "0");
			}
		}

		// [수정화면] 배너에 이미지가 있을경우 file_wrap 클래스 선택자에 add 클래스 추가
		for(var i=0; i<$(".file_wrap img").length; i++){
			if($(".file_wrap img").eq(i).attr("src") != ""){
				$(".file_wrap").eq(i).addClass("add");
			}
		}
	}

	// 사업자번호 입력할때마다 자동 '-'(하이픈) 추가
	$("#bizrno").keyup(function(){
		console.log(this.value);
		this.value = addAutoHyphen(this.value);
	});

	// 사이트명, 사이트 코드, 사이트Key, 그룹 아이디, 알림톡 링크 URL 유효성검사 추가(영문을 제외한 나머지 문자 입력 안되도록 수정)
	$("#siteNm").keyup(function(){
		$("#siteNm").val($("#siteNm").val().replace(/[^a-zA-Z]*$/, ''));
	});

	$("#linkUrl").keyup(function(){
		$("#linkUrl").val($("#linkUrl").val().replace(/[^a-zA-Z0-9:/.]*$/, ''));
	});

	/*$("#siteCd").keyup(function(){
		$("#siteCd").val($("#siteCd").val().replace(/[^a-zA-Z]*$/, ''));
	});
	$("#siteKey").keyup(function(){
		$("#siteKey").val($("#siteKey").val().replace(/[^a-zA-Z]*$/, ''));
	});
	$("#groupId").keyup(function(){
		$("#groupId").val($("#groupId").val().replace(/[^a-zA-Z]*$/, ''));
	}); */

	// 이용안내, 오시는 길 글자체크(textArea)
	$("#useGuidance").keyup(function(e){
		var content = $(this).val();
		//$('#counter').html("("+content.length+" / 최대 200자)");	// 글자수 실시간 카운팅
		
		if(content.length > 400){
			alert("최대 400자까지 입력 가능합니다.");
			$(this).val(content.substring(0, 400));
			//$('#counter').html("(200 / 최대 200자)");
		}
	});
	
	$("#visitStret").keyup(function(e){
		var content = $(this).val();
		//$('#counter').html("("+content.length+" / 최대 200자)");	// 글자수 실시간 카운팅
		
		if(content.length > 400){
			alert("최대 400자까지 입력 가능합니다.");
			$(this).val(content.substring(0, 400));
			//$('#counter').html("(200 / 최대 200자)");
		}
	});

	// 지부정보관리 - 수정화면에서 오픈일이 한참 지났을경우 오픈일 선택 안되도록 처리
	<c:if test="${STS_FLAG eq 'upd'}">
		if("${resultVO.disableYn}" == "Y"){
			$("#opnDt").prop("disabled", true).addClass("readonly");
			$("#opnSi").prop("disabled", true).addClass("readonly").css("color", "black");
			$("#opnMin").prop("disabled", true).addClass("readonly").css("color", "black");
			$("#opnSec").prop("disabled", true).addClass("readonly").css("color", "black");
		}
	</c:if>
});

// 사업자번호 형식 '-'(하이픈) 추가
function addAutoHyphen(str){
	str = str.replace(/[^0-9]/g, '');
	var tmp = '';
	if( str.length < 4){
		return str;
	}else if(str.length < 6){
		tmp += str.substr(0, 3);
		tmp += '-'; tmp += str.substr(3);
		return tmp;
	}else if(str.length < 11){
		tmp += str.substr(0, 3);
		tmp += '-';
		tmp += str.substr(3, 2);
		tmp += '-';
		tmp += str.substr(5);
		return tmp;
	}
	return str;
}

// 이미지 미리보기
function loadPreviews_click(e) {
	$(".file_wrap .uploadBtn").each(function(){
		var $input = $(this);
		var inputFiles = this.files;
		if(inputFiles == undefined || inputFiles.length == 0) return;
		var inputFile = inputFiles[0];
		
		var reader = new FileReader();
		reader.onload = function(event) {
			$input.parent(".file_wrap").addClass("add");
			$input.siblings(".thumbnail").find("img").attr("src", event.target.result);

			// 닫기버튼 활성화(등록/수정 둘다 활성화되도록 수정)
			//if("${STS_FLAG}" == "upd"){
				$input.prev().find("span").css("display", "block");
			//}
		};
		reader.readAsDataURL(inputFile);
	});
}

// 등록하기
function fn_add(){

	// validation 체크
	if(formValidation("branchFrm")){

		var bizrno = $("#bizrno").val();
		if(bizrno.length < 12){
			alert("<spring:message code='valid.branch.bizrno'/>");
			return false;
		}
		
		var frm = $("#branchFrm")[0];
		frm.action = "/branch/info/add";
		frm.submit();
	}
}

// 수정하기
function fn_upd(){

	// validation 체크
	if(formValidation("branchFrm")){

		var bizrno = $("#bizrno").val();
		if(bizrno.length < 12){
			alert("<spring:message code='valid.branch.bizrno'/>");
			return false;
		}
		
		var frm = $("#branchFrm")[0];
		frm.action = "/branch/info/upd";
		frm.submit();
	}
}

// 취소하기
function fn_cancel(){
	location.href = "/branch/info/list"
}

// 다음 API 주소 검색(210802 추가)
//@param : zipCode(우편번호), addr(구주소), nAddr(신주소), nAddrDtl(신주소상세), fullAddr(전체주소)
function fn_addr(zipCode, nAddr, nAddrDtl){
	new daum.Postcode({
		animation:true,
		oncomplete: function(data){
			// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분

			// 각 주소의 노출 규칙에 따라 주소를 조합한다.
			// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
			var roadAddr = '';		// 도로명 주소 변수 ((신)주소)
			var jibunAddr = '';		// 지번주소 변수 ((구)주소)
			var extraAddr = '';		// 조합형 주소 변수

			// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
			//if(data.userSelectedType === 'R'){	// 사용자가 도로명 주소를 선택했을 경우
			//	fullAddr = data.roadAddress;
			//}else{	// 사용자가 지번 주소를 선택했을 경우(J)
			//	fullAddr = data.jibunAddress;
			//}
			
			roadAddr = data.roadAddress;
			jibunAddr = data.jibunAddress;

			// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
			//if(data.userSelectedType === 'R'){
				//법정동명이 있을 경우 추가한다.
				if(data.bname !== ''){
					extraAddr += data.bname;
				}
				// 건물명이 있을 경우 추가한다.
				if(data.buildingName !== ''){
					extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
				}
				// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
				roadAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
			//}

			// 우편번호와 주소 정보를 해당 필드에 넣는다.
			//$("#brhZipCode").val(data.postcode1+''+data.postcode2);
			//$(sAddr).val(jibunAddr);	// (구)주소
			
			$(zipCode).val(data.zonecode);	// 우편번호
			$(nAddr).val(roadAddr);	// (신)주소
			//$(fullAddr).val("["+data.zonecode+"] "+roadAddr);	// 전체주소 형식 : [우편번호] (신)주소

			// 커서를 상세주소 필드로 이동한다.
			$(nAddrDtl).focus();

			// 카카오 지도 API(위도(lat), 경도(lon))
			Promise.resolve(data).then(o => {
				const { address } = data;
				return new Promise((resolve, reject) => {
					const geocoder = new daum.maps.services.Geocoder();
					geocoder.addressSearch(address, (result, status) =>{
						if(status === daum.maps.services.Status.OK){
							const { x, y } = result[0];
							resolve({ lat: y, lon: x })
						}else{
							reject();
						}
					});
				})
			}).then(result => {
				// 위도(lat), 경도(lon) 결과 값
				console.log(result);
				$("#bhfLa").val(result.lat);
				$("#bhfLo").val(result.lon);
			});
		}
	}).open();
}

// 사업자번호 형식
function bizNoFormat(num, type, eventFlag){
	var numFormat = "";
	var regType = /^[0-9-]*$/;
	var flag = "";
	
	try{
		// 숫자만 허용하는 정규표현식 함수(한글 입력 불가처리)
		if(!regType.test($(num).val())){
			$(num).val("");
			flag = "N";
		}

		// 형식 유효성검사 주석처리
		if(flag != "N"){
			if($(num).val().length == 10){
				if(type == 0){
					numFormat = $(num).val().replace(/(\d{3})(\d{2})(\d{5})/, '$1-$2-*****');
				}else{
					numFormat = $(num).val().replace(/(\d{3})(\d{2})(\d{5})/, '$1-$2-$3');
				}
			}else{
				//alert("사업자번호 형식과 일치하지 않습니다.");
			}
		}
		
	}catch(e){
		numFormat = $(num).val();
		console.log(e);
	}

	if(eventFlag == "blur"){
		$("#regNum").val(numFormat);
	}
}

// 사업자번호 maxlength 정하는 함수
function fn_maxLength(obj){
	if($(obj).val().indexOf("-") > -1){
		$(obj).attr("maxlength", "12");
	}else{
		$(obj).attr("maxlength", "10");
	}
}
</script>
</body>
</html>