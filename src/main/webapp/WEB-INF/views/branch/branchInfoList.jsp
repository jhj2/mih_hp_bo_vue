<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
<form id="branchFrm" name="branchFrm" method="get">
	
	<!-- 페이징처리 변수 -->
	<input type="hidden" id="currentPageNo" name="currentPageNo" value="${paging.currentPageNo}"/>
	<input type="hidden" id="pageSize" name="pageSize" value="${paging.pageSize}"/>
	<input type="hidden" id="recordCountPerPage" name="recordCountPerPage" value="${paging.recordCountPerPage}"/>
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 지부정보 관리 start -->
				<h3 class="page_title">지부정보 관리</h3>
				<div class="clear">
					<p class="t_total">총 ${totCnt}건</p>
					<a href="/branch/info/addView" class="s_btn submit right">등록하기</a>
				</div>
				<table cellpadding="0" cellspacing="0" class="t_list mart10">
					<colgroup>
						<col width="65px"><col width="95px"><col width="100px"><col width="110px"><col width="240px"><col width="290px"><col width="110px"><col width="70px">
					</colgroup>
					<thead>
					<tr>
						<th>번호</th>
						<th>지부</th>
						<th>지부 아이디</th>
						<th>사업자등록번호</th>
						<th>지부 슬로건</th>
						<th>주소</th>
						<th>등록일</th>
						<th>관리</th>
					</tr>
					</thead>
					
					<tbody>
					<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td colspan="8"><spring:message code="no.search.data.msg"/></td>
					</tr>
					</c:if>
					<c:if test="${fn:length(resultList) > 0}">
					<c:forEach var="result" items="${resultList}" varStatus="status">
					<tr>
						<!-- 전체 조회건수-((현재페이지 번호-1) * 1페이지당 레코드수 + 현재게시물 출력순서 -->
						<td>${totCnt-((paging.currentPageNo-1) * paging.recordCountPerPage + status.index)}</td>
						<td><c:out value="${result.bhfNm}"/></td>
						<td><c:out value="${result.bhfIdx}"/></td>
						<td><c:out value="${result.bizrno}"/></td>
						<td><c:out value="${result.slogan}"/></td>
						<td><p style="width:280px; white-space:nowrap;overflow:hidden;text-overflow:ellipsis;"><c:out value="${result.addr}"/></p></td>
						<td><c:out value="${result.registDt}"/></td>
						<td><a href="/branch/info/updView?bhfIdx=${result.bhfIdx}" class="color_warn text_under">수정</a></td>
					</tr>
					</c:forEach>
					</c:if>
					</tbody>
				</table>
				${paging.pagination}
				<!-- 지부정보 관리 end -->
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
</script>
</body>
</html>