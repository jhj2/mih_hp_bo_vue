<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>

<form id="branchFrm" name="branchFrm" method="get">
	
	<!-- 검색조건 유지를 위한 hidden값 -->
	<input type="hidden" id="bhfIdx" name="bhfIdx" value="${vo.bhfIdx}"/>
	<input type="hidden" id="tourHopeTimeTyCd" name="tourHopeTimeTyCd" value="${vo.tourHopeTimeTyCd}"/>
	<input type="hidden" id="mberGbn" name="mberGbn" value="${vo.mberGbn}"/>
	<input type="hidden" id="itemGbn" name="itemGbn" value="${vo.itemGbn}"/>
	
	<!-- 페이징처리 변수 -->
	<input type="hidden" id="currentPageNo" name="currentPageNo" value="${paging.currentPageNo}"/>
	<input type="hidden" id="pageSize" name="pageSize" value="${paging.pageSize}"/>
	<input type="hidden" id="recordCountPerPage" name="recordCountPerPage" value="${paging.recordCountPerPage}"/>
	
	<!-- 종료하기 변수 -->
	<input type="hidden" name="tourReqstIdx" id="tourReqstIdx" />
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 지부투어 관리 start -->
				<h3 class="page_title">지부투어 관리</h3>
				<div class="border_box">
					<div class="flex">
						<p class="tit">투어상태</p>
						<div class="con">
							<div class="con">
								<p class="checkbox"><input type="checkbox" id="stat_all" name="tourSttus" value="" checked><label for="stat_all"><i></i>전체</label></p>
								<p class="checkbox"><input type="checkbox" id="stat_01" name="tourSttus" value="P"><label for="stat_01"><i></i>진행</label></p>
								<p class="checkbox"><input type="checkbox" id="stat_02" name="tourSttus" value="E"><label for="stat_02"><i></i>종료</label></p>
							</div>
						</div>
					</div>
					<div class="flex">
						<p class="tit">투어신청</p>
						<div class="con">
							<select name="selBhfIdx" id="selBhfIdx">
								<c:forEach var="result" items="${brBhfList}" varStatus="status">
									<option value="${result.commCd}">${result.commCdNm}</option>
								</c:forEach>
							</select>
							<input type="text" class="datepick" id="tourHopeDe" name="tourHopeDe" placeholder="" value="${vo.tourHopeDe}">
							<select name="selTourHopeTimeTy" id="selTourHopeTimeTy">
								<option value="">시간대</option>
								<c:forEach var="result" items="${codeList}" varStatus="status">
									<option value="${result.cmmnCd}"><c:out value="${result.cdKorNm}"/></option>
								</c:forEach>
								<!-- <option value="A">오전 10:00 ~ 오후 12:00</option>
								<option value="P">오후 2:00 ~ 오후 4:00</option>
								<option value="D">오후 4:00 ~ 오후 6:00</option> -->
							</select>
						</div>
					</div>
					<div class="flex">
						<p class="tit">신청자</p>
						<div class="con">
							<select name="selMberGbn" id="selMberGbn">
								<option value="">전체</option>
								<option value="01">회원</option>
								<option value="02">비회원</option>
							</select>
							<select name="selItemGbn" id="selItemGbn">
								<option value="">선택</option>
								<option value="01">이름</option>
								<option value="02">휴대폰</option>
							</select>
							<input type="text" id="keyword" name="keyword" value="${vo.keyword}">
							<button type="button" id="btnSearch" class="s_btn submit" onclick="javascript:fn_search();">검색</button>
						</div>
					</div>
				</div>
				
				<p class="t_total mart40">총 ${totCnt}건</p>
				<table cellpadding="0" cellspacing="0" class="t_list">
					<colgroup>
						<col width="70px"><col width="90px"><col width="70px"><col width="130px"><col width="90px"><col width="120px"><col width="190px"><col width="80px"><col width="120px"><col width="110px">
					</colgroup>
					<thead>
					<tr>
						<th>번호</th>
						<th>이름</th>
						<th>회원 여부</th>
						<th>휴대폰</th>
						<th>지부</th>
						<th>투어 희망일</th>
						<th>시간대</th>
						<th>개인정보 조회</th>
						<th>투어 상태</th>
						<th>신청일</th>
					</tr>
					</thead>
					
					<tbody>
					<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td colspan="10"><spring:message code="no.search.data.msg"/></td>
					</tr>
					</c:if>
					<c:if test="${fn:length(resultList) > 0}">
					<c:forEach var="result" items="${resultList}" varStatus="status">
					<tr>
						<!-- 전체 조회건수-((현재페이지 번호-1) * 1페이지당 레코드수 + 현재게시물 출력순서 -->
						<td>
							${totCnt-((paging.currentPageNo-1) * paging.recordCountPerPage + status.index)}
						</td>
						<td>
							<span id="spanVisitrNm">
								<c:choose>
									<c:when test="${fn:length(result.visitrNm) > 2}">
										<c:out value="${util:masking(result.visitrNm, 1, fn:length(result.visitrNm)-2)}"/>
									</c:when>
									<c:otherwise>
										<c:out value="${util:masking(result.visitrNm, 1, fn:length(result.visitrNm)-1)}"/>
									</c:otherwise>
								</c:choose>
							</span>
							<!-- 탈퇴한 회원이면 번호에 링크 제외하도록 처리 -->
							<c:if test="${result.secsnYn eq 'Y'}">
								<c:out value="${result.mberIdx}"/>
							</c:if>
							<c:if test="${result.secsnYn ne 'Y'}">
								<a href="#mb_info" id="amodal_member" data-modal-url="/member/com/info?mberIdx=${result.mberIdx}" rel="modal:open" class="text_under color03"><c:out value="${result.mberIdx}"/></a>
							</c:if>
							<%-- <a href="javascript:fn_memberInfo('${result.mberIdx}');" class="text_under color03">${result.mberIdx}</a> --%>	<!-- rel="modal:open" -->
						</td>
						<td><c:out value="${result.mberFlag}"/></td>
						<td>
							<span id="spanHp">${result.hpNo1}-${util:masking(result.hpNo2, 0, fn:length(result.hpNo2))}-${result.hpNo3}</span>
						</td>
						<td><c:out value="${result.bhfNm}"/></td>
						<td><c:out value="${result.tourHopeDe}"/></td>
						<td><c:out value="${result.tourHopeTimeTyNm}"/></td>
						<td><a onclick="javascript:fn_infoSearch(this, '${result.tourReqstIdx}');" class="t_btn">조회</a></td>
						<td>
							<c:out value="${result.tourSttusNm}"/>
							<c:if test="${result.tourSttus eq 'P'}">
								<a href="javascript:fn_quit('${result.tourReqstIdx}');" class="color_warn text_under marl10">종료하기</a>
							</c:if>
						</td>
						<td>${result.tourRqstdt}</td>
					</tr>
					</c:forEach>
					</c:if>
					</tbody>
				</table>
				${paging.pagination}
			</div>
		</div>
	</div>
</form>

<!-- 회원 정보 popup start -->
<%@ include file="/WEB-INF/views/common/modal.jsp" %>

<script type="text/javascript">
$(document).ready(function(){

	$("select[name=selBhfIdx] option:eq(0)").attr("selected", true);

	// 지부투어-지부 selectbox
	$("#selBhfIdx").change(function(){
		$("#bhfIdx").val($(this).val());
	});
	// 지부투어-시간대 selectbox
	$("#selTourHopeTimeTy").change(function(){
		$("#tourHopeTimeTyCd").val($(this).val());
	});
	// 지부투어-회원구분 selectbox
	$("#selMberGbn").change(function(){
		$("#mberGbn").val($(this).val());
	});
	// 지부투어-검색조건(이름, 휴대폰)
	$("#selItemGbn").change(function(){
		$("#itemGbn").val($(this).val());
	});

	// 검색조건 유지
	if("${vo.bhfIdx}" != ""){
		$("#selBhfIdx").val("${vo.bhfIdx}");
	}
	if("${vo.tourHopeTimeTyCd}" != ""){
		$("#selTourHopeTimeTy").val("${vo.tourHopeTimeTyCd}");
	}
	if("${vo.mberGbn}" != ""){
		$("#selMberGbn").val("${vo.mberGbn}");
	}
	if("${vo.itemGbn}" != ""){
		$("#selItemGbn").val("${vo.itemGbn}");
	}

	// 투어상태 체크박스 유지
	if("${vo.tourSttus}" != ""){
		if("${vo.tourSttus}" == "P"){
			$("input[name=tourSttus]").attr("checked", false);
			$("input[name=tourSttus]").eq(1).attr("checked", true);
		}else if("${vo.tourSttus}" == "E"){
			$("input[name=tourSttus]").attr("checked", false);
			$("input[name=tourSttus]").eq(2).attr("checked", true);
		}
	}

	// 투어상태 -> 단일선택 체크박스
	$("input[name=tourSttus]").click(function(){
		if($(this).prop('checked')){
			$("input[name=tourSttus]").prop("checked", false);
			$(this).prop("checked", true);

			// 선택한 체크박스 값에 따라 값 변경
			if($(this).attr("id") == "stat_01"){
				$(this).val("P");
			}else if($(this).attr("id") == "stat_02"){
				$(this).val("E");
			}else{
				$(this).val("");
			}
		}
	});
});

// 등록페이지
function fn_addView(){
	var frm = $("#branchFrm")[0];
	frm.action = "/branch/info/addView";
	frm.submit();
}

// 수정페이지
function fn_updView(bhfIdx){
	$("#bhfIdx").val(bhfIdx);
	
	var frm = $("#branchFrm")[0];
	frm.action = "/branch/info/updView";
	frm.submit();
}

// 검색
function fn_search(){

	$("#currentPageNo").val(1);
	
	var frm = $("#branchFrm")[0];
	frm.action = "/branch/tour/list";
	frm.submit();
}

// 종료하기
function fn_quit(tourReqstIdx){

	if(!confirm('<spring:message text="종료하시겠습니까?"/>')){
		return;
	}else{
		var frm = $("#branchFrm")[0];
		$("#tourReqstIdx").val(tourReqstIdx);
		
		frm.action = "/branch/tour/upd";
		frm.submit();
	}
}

// 개인정보 조회
function fn_infoSearch(obj, tourReqstIdx){

	if(confirm("지부 투어 신청자 정보를 조회합니다. 맞습니까?")) {
		$.ajax({
			type: "get",
			url: "/branch/tour/requester",
			data : {"tourReqstIdx" : tourReqstIdx},
			dataType: "json",
			success: function(res){	
				$(obj).parent().parent().find("span[id=spanHp]").html(res.requester.hp);
				$(obj).parent().parent().find("span[id=spanVisitrNm]").html(res.requester.visitrNm);
			},
			error: function(e){
				alert("오류가 발생하였습니다.");
			}
		});
	}
}

// 회원정보 조회
function fn_memberInfo(mberIdx){

	var param = {
		"mberIdx" : mberIdx
	}
	console.log("param===");
	console.log(param);

	// 회원정보 팝업 호출 URL 세팅
	var url = "/member/com/info";

	// 모달 팝업 호출(모달 팝업창 class명으로 로드)
	$(".jquery-modal").load(url, param, function(response, status, xhr){
		if(status == "success"){
			$(".jquery-modal").show();
			$(this).css("background", "none");
			$("#mb_info").show();
		}
	});
}
</script>
</body>
</html>