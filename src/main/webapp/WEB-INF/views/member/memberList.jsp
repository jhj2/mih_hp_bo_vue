<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
<form id="memberFrm" name="memberFrm" method="get">
	<!-- 검색조건 유지를 위한 hidden값 -->
	<input type="hidden" id="snsSeCd" name="snsSeCd" value="${vo.snsSeCd}"/>
	<input type="hidden" id="mberGbn" name="mberGbn" value="${vo.mberGbn}"/>
	
	<!-- 페이징처리 변수 -->
	<input type="hidden" id="currentPageNo" name="currentPageNo" value="${paging.currentPageNo}"/>
	<input type="hidden" id="pageSize" name="pageSize" value="${paging.pageSize}"/>
	<input type="hidden" id="recordCountPerPage" name="recordCountPerPage" value="${paging.recordCountPerPage}"/>
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 지부정보 관리 start -->
				<h3 class="page_title">회원 관리</h3>
				<div class="border_box">
					<div class="flex">
						<p class="tit">가입기간</p>
						<div class="con">
							<p class="radio_btn">
								<input type="radio" id="period_all" name="searchPeriod" value="0" <c:if test="${vo.searchPeriod == '0'}">checked</c:if> ><label for="period_all"><i></i>전체</label>
							</p>
							<p class="radio_btn">
								<input type="radio" id="period_01" name="searchPeriod" value="-12" <c:if test="${vo.searchPeriod == '-12'}">checked</c:if> ><label for="period_01"><i></i>1년</label>
							</p>
							<p class="radio_btn">
								<input type="radio" id="period_02" name="searchPeriod" value="-6" <c:if test="${vo.searchPeriod == '-6'}">checked</c:if> ><label for="period_02"><i></i>6개월</label>
							</p>
							<p class="radio_btn">
								<input type="radio" id="period_03" name="searchPeriod" value="-3" <c:if test="${vo.searchPeriod == '-3'}">checked</c:if> ><label for="period_03"><i></i>3개월</label>
							</p>
							<p class="radio_btn">
								<input type="radio" id="period_04" name="searchPeriod" value="-1" <c:if test="${vo.searchPeriod == '-1'}">checked</c:if> ><label for="period_04"><i></i>1개월</label>
							</p>
							<input type="text" class="datepick marl10" id="searchStartDt" name="searchStartDt" value="${vo.searchStartDt}" placeholder="">
							<span class="color6">부터</span>
							<input type="text" class="datepick marl10" id="searchEndDt" name="searchEndDt" value="${vo.searchEndDt}" placeholder="">
							<span class="color6">까지</span>
						</div>
					</div>
					
					<div class="flex">
						<p class="tit">인증 경로</p>
						<div class="con">
							<select name="selSnsSeCd" id="selSnsSeCd">
								<option value="">전체</option>
								<option value="A0401">네이버</option>
								<option value="A0402">카카오</option>
							</select>
						</div>
					</div>
					<div class="flex">
						<p class="tit">쿠폰 코드</p>
						<div class="con">
							<input type="text" id="searchRecCd" name="searchRecCd" value="${vo.searchRecCd}" placeholder="">
						</div>
					</div>
					
					<div class="flex">
						<p class="tit">회원 정보</p>
						<div class="con">
							<select name="selMberGbn" id="selMberGbn">
								<option value="01" <c:if test="${vo.mberGbn eq '01'}">selected</c:if>>이름</option>
								<option value="02" <c:if test="${vo.mberGbn eq '02'}">selected</c:if>>휴대폰번호</option>
								<option value="03" <c:if test="${vo.mberGbn eq '03'}">selected</c:if>>이메일</option>
							</select>
							<input type="text" id="keyword" name="keyword" value="${vo.keyword}" placeholder="">
							<button type="button" id="btnSearch" class="s_btn submit" onclick="javascript:fn_search();">검색</button>
						</div>
					</div>
				</div>
				
				<p class="t_total mart40">총 ${totCnt}건</p>
				<table cellpadding="0" cellspacing="0" class="t_list">
					<colgroup>
						<col width="70px"><col width="100px"><col width="130px"><col width="130px"><col width="200px"><col width="120px"><col width="120px"><col width="100px"><col width="130px">
					</colgroup>
					<thead>
					<tr>
						<th>번호</th>
						<th>가입/인증</th>
						<th>회원</th>
						<th>휴대폰</th>
						<th>이메일</th>
						<th>생년월일</th>
						<th>가입일</th>
						<th>쿠폰 코드</th>
						<th>최종 로그인</th>
					</tr>
					</thead>
					
					<tbody>
					<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td colspan="9"><spring:message code="no.search.data.msg"/></td>
					</tr>
					</c:if>
					<c:if test="${fn:length(resultList) > 0}">
					<c:forEach var="result" items="${resultList}" varStatus="status">
					<tr>
						<!-- 전체 조회건수-((현재페이지 번호-1) * 1페이지당 레코드수 + 현재게시물 출력순서 -->
						<td><c:out value="${totCnt-((paging.currentPageNo-1) * paging.recordCountPerPage + status.index)}"/></td>
						<td><c:out value="${result.snsSeNm}"/></td>
						<td>
							<c:choose>
								<c:when test="${fn:length(result.mberNm) > 2}">
									<c:out value="${util:masking(result.mberNm, 1, fn:length(result.mberNm)-2)}"/>
								</c:when>
								<c:otherwise>
									<c:out value="${util:masking(result.mberNm, 1, fn:length(result.mberNm)-1)}"/>
								</c:otherwise>
							</c:choose>&nbsp;
							<!-- 탈퇴한 회원이면 번호에 링크 제외하도록 처리 -->
							<c:if test="${result.secsnYn eq 'Y'}">
								<c:out value="${result.mberIdx}"/>
							</c:if>
							<c:if test="${result.secsnYn eq 'N'}">
								<a href="#mb_info" id="amodal_member" data-modal-url="/member/com/info?mberIdx=${result.mberIdx}" rel="modal:open" class="color03 text_under"><c:out value="${result.mberIdx}"/></a>
							</c:if>
						</td>
						<td>
							<c:out value="${result.hpNo1}-${util:masking(result.hpNo2, 0, fn:length(result.hpNo2))}-${result.hpNo3}"/>
						</td>
						<td><c:out value="${util:masking(result.email, 2, fn:length(fn:substringBefore(result.email, '@'))-2)}"/></td>
						<td><c:out value="${fn:replace(result.brthdy,'-', '.') }"/></td>
						<td><c:out value="${result.registDt}"/></td>
						<td><c:out value="${result.recCd}"/></td>
						<td><c:out value="${result.lastConectDt}"/></td>
					</tr>
					</c:forEach>
					</c:if>
					</tbody>
				</table>
				${paging.pagination}
			</div>
		</div>
	</div>
	
</form>
	<!-- 회원 정보 popup start -->
	<%@ include file="/WEB-INF/views/common/modal.jsp" %>
<script type="text/javascript">
$(document).ready(function(){

	// 인증 경로 selectbox
	$("#selSnsSeCd").change(function(){
		$("#snsSeCd").val($(this).val());
	});
	// 회원 정보 selectbox
	$("#selMberGbn").change(function(){
		$("#mberGbn").val($(this).val());
	});

	// 검색조건 유지
	if("${vo.snsSeCd}" != ""){
		$("#selSnsSeCd").val("${vo.snsSeCd}");
	}
	
	if("${vo.mberGbn}" != ""){
		$("#selMberGbn").val("${vo.mberGbn}");
	}else if("${vo.mberGbn}" == ""){
		$("#selMberGbn").val("01");
		$("#mberGbn").val("01");
	}

	// 가입기간 선택(전체, 1년, 6개월, 3개월, 1개월)
	$("[name=searchPeriod]").on("click", function(){
		var n = Number($(this).val());
		var startDt = "";
		var endDt = "";
		if(n < 0){
			startDt = n.months().fromNow().toString("yyyy.MM.dd");
			endDt = new Date().toString("yyyy.MM.dd")
		}
		$("#searchStartDt").val(startDt);
		$("#searchEndDt").val(endDt);
	});
});

// 조회
function fn_search(){
	$("#currentPageNo").val(1);	// 검색했을때 1페이지 유지
	
	var frm = $("#memberFrm")[0];
	frm.action = "/member/list";
	frm.submit();
}

// 등록페이지
function fn_addView(){
	var frm = $("#memberFrm")[0];
	frm.action = "/branch/info/addView";
	frm.submit();
}

// 수정페이지
function fn_updView(bhfIdx){
	$("#bhfIdx").val(bhfIdx);
	
	var frm = $("#branchFrm")[0];
	frm.action = "/branch/info/updView";
	frm.submit();
}

</script>
</body>
</html>