<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>

<!doctype html>
<html lang="ko">
<head>
    <%@ include file="/WEB-INF/views/common/common.jsp" %>
</head>
<body>
    <div id="wrap">
        <h1 class="logo"><a href="/main">보관복지부 logo</a></h1>
        <!-- container start -->
        <div id="error">
            <div class="icon">
                <h5>서비스를 일시적으로 이용하실 수 없습니다.</h5>
                <p>잠시 후 다시 시도해주시기 바랍니다.</p>
            </div>
            <div class="btn_wrap">
                <a href="/main" class="submit w200">홈페이지로 이동</a>
            </div>
        </div>
        <!-- container end -->
    </div>
</body>
</html>