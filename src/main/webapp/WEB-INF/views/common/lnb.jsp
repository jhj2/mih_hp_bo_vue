<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="config" uri="/WEB-INF/tlds/configTag.tld"%>

<div id="lnb">
	<ul>
		<li id="menu_idx_1">
			<a href="#;" class="lnb_menu drop close">통합게시판 관리</a>
			<div class="lnb_dropdown" style="display: none;">
				<a href="/bbs/ctgry/list" class="dropdown_menu">게시판 카테고리 관리</a>
				<a href="/bbs/list" class="dropdown_menu">게시물 관리</a>
			</div>
		</li>
		<li id="menu_idx_2"><a href="/org/list" class="lnb_menu">조직도 관리</a></li>
		<li id="menu_idx_3"><a href="/admin/list" class="lnb_menu">관리자 관리</a></li>
		<li id="menu_idx_4"><a href="/com/cd/list" class="lnb_menu">공통코드관리</a></li>
		
		<!-- <li id="menu_idx_2"><a href="/member/list" class="lnb_menu">회원관리</a></li>
		<li id="menu_idx_3"><a href="/cabinet/order/list" class="lnb_menu">캐비닛 서비스 관리</a></li>
		<li id="menu_idx_4">
			<a href="#;" class="lnb_menu drop close">결제 관리</a>
			<div class="lnb_dropdown" style="display: none;">
				<a href="/payment/list" class="dropdown_menu">결제내역 관리</a>
				<a href="/payment/expect/list" class="dropdown_menu">결제 예정금액 관리</a>
				<a href="/payment/add/list" class="dropdown_menu">운영자 추가 결제</a>
			</div>
		</li>
		
		<li id="menu_idx_5">
			<a href="#;" class="lnb_menu drop close">캐비닛 관리</a>
			<div class="lnb_dropdown" style="display: none;">
				<a href="/cabinet/asset/list" class="dropdown_menu">캐비닛 자산 관리</a>
				<a href="/cabinet/price/list" class="dropdown_menu">캐비닛 월 이용료 관리</a>
			</div>
		</li>
		<li id="menu_idx_6">
			<a href="#;" class="lnb_menu drop close">지부관리</a>
			<div class="lnb_dropdown" style="display: none;">
				<a href="/branch/info/list" class="dropdown_menu">지부정보 관리</a>
				<a href="/branch/tour/list" class="dropdown_menu">지부 투어 관리</a>
			</div>
		</li>
		<li id="menu_idx_7">
			<a href="#;" class="lnb_menu drop close">종합민원실 관리</a>
			<div class="lnb_dropdown" style="display: none;">
				<a href="/complaint/notice/list" class="dropdown_menu">공지 관리</a>
				<a href="/complaint/inquiry/list" class="dropdown_menu">1:1 문의 관리</a>
			</div>
		</li>
		<li id="menu_idx_8">
			<a href="#;" class="lnb_menu drop close">사이트 관리</a>
			<div class="lnb_dropdown" style="display: none;">
				<a href="/site/policy/list" class="dropdown_menu">보관정책 관리</a>
				<a href="/site/banner/list" class="dropdown_menu">배너 관리</a>
				<a href="/site/popup/list" class="dropdown_menu">팝업 관리</a>
			</div>
		</li>
		<li id="menu_idx_9">
			<a href="#;" class="lnb_menu drop close">약관 관리</a>
			<div class="lnb_dropdown" style="display: none;">
				<a href="/stplat/stplat/list" class="dropdown_menu">이용약관 관리</a>
				<a href="/stplat/policy/list" class="dropdown_menu">개인정보처리방침 관리</a>
			</div>
		</li> -->
	</ul>
</div>

<script type="text/javascript">
$(function() {
	$("#menu_idx_${menuIdx} .lnb_menu").toggleClass("active");
	$("#menu_idx_${menuIdx} .lnb_menu.drop").removeClass("close")
	$("#menu_idx_${menuIdx} .lnb_menu.drop").next('div.lnb_dropdown').stop().slideToggle(100);
});
</script>