<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="config" uri="/WEB-INF/tlds/configTag.tld"%>
<%@ taglib prefix="util" uri="/WEB-INF/tlds/utilTag.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="popup_img popup">
	<img id="popupImg" src="">	<!-- popup img -->
	<span class="pop_close" onclick="javascript:pop_close();"></span>
</div>