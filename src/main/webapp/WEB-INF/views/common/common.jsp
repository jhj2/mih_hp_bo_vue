<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="config" uri="/WEB-INF/tlds/configTag.tld"%>
<%@ taglib prefix="util" uri="/WEB-INF/tlds/utilTag.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=2.0,user-scalable=no">
<meta name="format-detection" content="telephone=no" />
<title>메이드인헤븐 관리자</title>
<link href="/resources/assets/css/reset.css?ver=${ver}" rel="stylesheet" type="text/css">
<link href="/resources/assets/css/fonts.css?ver=${ver}" rel="stylesheet" type="text/css">
<link href="/resources/admin/assets/css/admin.css?ver=${ver}" rel="stylesheet" type="text/css">
<link href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css" rel="stylesheet">
<script src="/resources/js/jquery-3.6.0.min.js" type="text/javascript"></script>
<script src="/resources/js/jquery-migrate-3.3.2.min.js" type="text/javascript"></script>
<script src="/resources/js/adm-jquery.js?ver=${ver}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/ko.js"></script>
<script src="/resources/js/jquery.modal.js"></script>
<script src="/resources/js/jquery.blockUI.js"></script>
<script src="/resources/js/parsley.min.js"></script>
<script src="/resources/js/date.js"></script>
<script src="/resources/js/util.js?ver=${ver}"></script>
<script src="/resources/js/common.js?ver=${ver}"></script>
<script src="/resources/js/commonUtil.js?ver=${ver}"></script>

<!-- 로그인 관리자 정보 -->
<sec:authentication property="principal" var="principal"/>
<c:if test="${principal ne 'anonymousUser'}">
	<sec:authentication property="principal.user" var="sessionUser"/>
</c:if>