<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="config" uri="/WEB-INF/tlds/configTag.tld"%>
<%@ taglib prefix="util" uri="/WEB-INF/tlds/utilTag.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<div id="cb_select" class="modal modal_view"></div>

<div id="end_date" class="modal"></div>

<div id="reset" class="modal min_height"></div>

<div id="end" class="modal"></div>

<div id="refund" class="modal"></div>

<div id="refund_view" class="modal"></div>

<div id="order" class="modal min_height"></div>

<div id="mb_info" class="modal"></div>

<div id="popup_add" class="modal min_height"></div>

<div id="popup_mod" class="modal min_height"></div>

<!-- 이용희망일 변경 -->
<div id="userequest" class="modal min_height"></div>

<div id="cb_add" class="modal"></div>

<div id="cb_view" class="modal"></div>

<div id="pay_change" class="modal"></div>

<div id="select_order" class="modal"></div>

<div id="pay_add" class="modal"></div>