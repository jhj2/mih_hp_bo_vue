<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="config" uri="/WEB-INF/tlds/configTag.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<div class="header_top">
	<h1><a href="/main">보관복지부 logo</a></h1>
	<div class="util">
		<p class="user"><c:out value="${sessionUser.userName}" /><span class="id"><c:out value="${sessionUser.userId}" /></span></p>
		<a href="/user/logout" class="logout">로그아웃</a>
	</div>
</div>