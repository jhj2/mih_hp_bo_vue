<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="config" uri="/WEB-INF/tlds/configTag.tld"%>
<%@ taglib prefix="util" uri="/WEB-INF/tlds/utilTag.tld"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form id="memberPopupFrm" name="memberPopupFrm" method="post">

	<!-- 페이징처리 변수 -->
	<input type="hidden" id="currentPageNo" name="currentPageNo" value="<c:out value="${paging.currentPageNo}"/>"/>
	<input type="hidden" id="pageSize" name="pageSize" value="<c:out value="${paging.pageSize}"/>"/>
	<input type="hidden" id="recordCountPerPage" name="recordCountPerPage" value="<c:out value="${paging.recordCountPerPage}"/>"/>
	<input type="hidden" id="mberIdx" name="mberIdx" value="<c:out value="${resultVO.mberIdx}"/>"/>

	<p class="pop_title">회원 정보</p>
	<div class="border_box">
		<div class="flex">
			<p class="tit">개인정보 조회</p>
			<div class="con flex">
				<select name="indvdlInfoInqireResnCd" id="indvdlInfoInqireResnCd" class="w60">
					<option value="">목적/사유를 선택하세요.</option>
					<c:forEach var="code" items="${reasonCodeList}">
						<option value="${code.cmmnCd}"><c:out value="${code.cdKorNm}"/></option>
					</c:forEach>
				</select>
				<!-- 기타 옵션 선택 시 사유 직접 입력 input -->
				<input type="text" id="indvdlInfoInqireResnRm" name="indvdlInfoInqireResnRm" style="display:none;" placeholder="기타/목적 사유">
				<button type="button" id="" class="s_btn submit" onclick="javascript:inqireSearch();">조회</button>
			</div>
		</div>
	</div>
	
	<table cellpadding="0" cellspacing="0" class="t_form mart30">
	<colgroup>
		<col width="16%"><col width="33%"><col width="16%"><col width="35%">
	</colgroup>
	<tbody>
		<tr>
			<td class="th">가입/인증</td>
			<td class="color6" id="snsSeNm"><c:out value="${resultVO.snsSeNm}"/></td>
			<td class="th">주소</td>
			<td class="color6" id="addr">
				<c:out value="${resultVO.rdnmadrBass}"/> <c:out value="${util:masking(resultVO.rdnmadrDetail, 0, fn:length(resultVO.rdnmadrDetail))}"/>
			</td>
		</tr>
		<tr>
			<td class="th">이름/고유번호</td>
			<td class="color6" id="nmMberIdx">
				<c:if test="${resultVO.mberIdx ne null}">
					<input type="hidden" name="mberIdx" id="mberIdx" value="<c:out value="${resultVO.mberIdx}"/>"/>
				</c:if>
				<span id="mberNm">
				<c:choose>
					<c:when test="${fn:length(resultVO.mberNm) > 2}">
						<c:out value="${util:masking(resultVO.mberNm, 1, fn:length(resultVO.mberNm)-2)}"/>
					</c:when>
					<c:otherwise>
						<c:out value="${util:masking(resultVO.mberNm, 1, fn:length(resultVO.mberNm)-1)}"/>
					</c:otherwise>
				</c:choose> / ${resultVO.mberIdx}
				</span>
			</td>
			<td class="th">결제카드</td>
			<td class="color6" id="cardInfo">
				<c:out value="${resultVO.cardName}"/>&nbsp;<c:out value="${resultVO.cardMaskNo}"/>
				<!-- 삼성카드 1534-5678-****-8774 -->
			</td>
		</tr>
		<tr>
			<td class="th">휴대폰</td>
			<td class="color6" id="mobile">
				<c:out value="${resultVO.hpNo1}-${util:masking(resultVO.hpNo2, 0, fn:length(resultVO.hpNo2))}-${resultVO.hpNo3}"/>
			</td>
			<td class="th">환불계좌</td>
			<td class="color6" id="refndBank">
				<c:out value="${resultVO.refndBankNm}"/>&nbsp;&nbsp;<c:out value="${util:masking(resultVO.refndAcnutno, fn:length(resultVO.refndAcnutno)-4, fn:length(resultVO.refndAcnutno))}"/>&nbsp;
				<c:choose>
					<c:when test="${fn:length(resultVO.refndDpstr) > 2}">
						<c:out value="${util:masking(resultVO.refndDpstr, 1, fn:length(resultVO.refndDpstr)-2)}"/>
					</c:when>
					<c:otherwise>
						<c:out value="${util:masking(resultVO.refndDpstr, 1, fn:length(resultVO.refndDpstr)-1)}"/>
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr>
			<td class="th">이메일</td>
			<td class="color6" id="email"><c:out value="${util:masking(resultVO.email, 2, fn:length(fn:substringBefore(resultVO.email, '@'))-2)}"/></td>
			<td class="th">마케팅 수신 동의</td>
			<td>
				<c:forEach var="result" items="${mrUseStplat}" varStatus="status">
					<c:if test="${result.stplatAgreYn eq 'Y'}">
						<span class="marketing active">
							<c:if test="${status.index eq 0}">
								알림톡
							</c:if>
							<c:if test="${status.index eq 1}">
								이메일
							</c:if>
							<c:if test="${status.index eq 2}">
								전화
							</c:if>
						</span>
					</c:if>
					<c:if test="${result.stplatAgreYn eq 'N'}">
						<span class="marketing">
							<c:if test="${status.index eq 0}">
								알림톡
							</c:if>
							<c:if test="${status.index eq 1}">
								이메일
							</c:if>
							<c:if test="${status.index eq 2}">
								전화
							</c:if>
						</span>
					</c:if>
				</c:forEach>
				<!-- <span class="marketing active">알림톡</span>
				<span class="marketing ">이메일</span>
				<span class="marketing">전화</span> -->
			</td>
		</tr>
		<tr>
			<td class="th">생년월일</td>
			<td class="color6" id="brthdy"><c:out value="${fn:replace(resultVO.brthdy,'-', '.') }"/></td>
			<td class="th">가입/최종로그인</td>
			<td class="color6" id="joinLastLogin">
				<c:out value="${resultVO.registDt}"/> / <c:out value="${resultVO.lastConectDt}"/>	<!--  2021.08.24 12:13 --> <!--&#91;휴면&#93;-->
			</td>
		</tr>
		<!-- 회원 정보 항목에 추천인 코드 항목 추가 => 입력되지 않은 경우에는 미출력(211129 추가) -->
		<c:if test="${resultVO.recCd ne null}">
			<tr>
				<td class="th">쿠폰 코드</td>
				<td class="color6" colspan="3">
					<c:out value="${resultVO.recCd}" />
				</td>
			</tr>
		</c:if>
	</tbody>
	</table>
	
	<div class="text_right mart10">
		<a href="javascript:void(0);" class="s_btn submit" onclick="javascript:pageMove('/cabinet/order/list?searchKeywordType=M&searchKeyword=<c:out value="${resultVO.mberNm}"/>');">이용내역</a>
		<a href="javascript:void(0);" class="s_btn submit" onclick="javascript:pageMove('/payment/list?keywordTy=M&keyword=<c:out value="${resultVO.mberNm}"/>');">결제내역</a>
		<a href="javascript:void(0);" class="s_btn submit" onclick="javascript:pageMove('/complaint/inquiry/list?mberNm=<c:out value="${resultVO.mberNm}"/>');">1:1 문의</a>
	</div>
	
	<!-- 메세지 발송 내역 start -->
	<p class="stit">메세지 발송 내역</p>
	<table cellpadding="0" cellspacing="0" class="t_list">
		<colgroup>
			<col width="8%"><col width="17%"><col width="12%"><col width="63%">
		</colgroup>
		<thead>
		<tr>
			<th>번호</th>
			<th>일시</th>
			<th>경로</th>
			<th>메시지 내용</th>
		</tr>
		</thead>
		
		<tbody class="test">
		<c:if test="${fn:length(resultList) == 0}">
		<tr>
			<td colspan="4"><spring:message code="no.search.data.msg"/></td>
		</tr>
		</c:if>
		<c:if test="${fn:length(resultList) > 0}">
		<c:forEach var="result" items="${resultList}" varStatus="status">
		<tr>
			<!-- 전체 조회건수-((현재페이지 번호-1) * 1페이지당 레코드수 + 현재게시물 출력순서 -->
			<td>
				<c:out value="${totCnt-((paging.currentPageNo-1) * paging.recordCountPerPage + status.index)}"/>
			</td>
			<td><c:out value="${result.msgSndngDt}"/></td>
			<td><c:out value="${result.msgCoursTyNm}"/></td>
			<td class="text_left">
				<pre><c:out value="${result.msgCn}"/></pre>
			</td>
		</tr>
		</c:forEach>
		</c:if>
		</tbody>
	</table>
	<span id="pagination">${paging.pagination}</span>
	<!-- 메세지 발송 내역 end -->
	
	<div class="btn_wrap">
		<a href="#close-modal" rel="modal:close" class="submit">확인</a>
	</div>
	<a href="#close-modal" rel="modal:close" class="close-modal">Close</a>
</form>
<!-- 회원 정보 popup end -->
<script>

$(function(){

})

// 개인정보 select box에 따른 변경
$("select[name=indvdlInfoInqireResnCd]").change(function(){
	if($(this).val() == "A1407"){
		$(this).next().css("display", "block");
		$(this).next().val("");
	}else{
		$(this).next().css("display", "none");
		$(this).next().val("");
	}
});

// 페이지 이동 함수
function pageMove(url){
	location.href = url;
}
</script>