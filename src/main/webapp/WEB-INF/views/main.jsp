<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>

<!doctype html>
<html lang="ko">
<head>
	<%@ include file="/WEB-INF/views/common/common.jsp"%>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
</head>
<body>
    <div id="wrap">
        <!-- header start -->
        <header>
            <%@ include file="/WEB-INF/views/common/top.jsp"%>
            <!-- <div id="gnb">
                gnb
            </div> -->
            <%@ include file="/WEB-INF/views/common/lnb.jsp"%>
        </header>
        <!-- header end -->
        <div id="container">
            <div class="contents dashboard">
                <div class="flex">
                    <div class="status">
                        <div class="title"><p>캐비닛 서비스 현황</p></div>
                        <div class="flex">
                            <div class="box">
                                <p class="sub">신규 주문</p>
                                <div>
                                    <dl>
                                        <dt>오늘 주문완료</dt>
                                        <dd>
                                        	<a href="/cabinet/order/list?searchStartDate=${today}&searchEndDate=${today}&arrOrdSttus=C&arrUseSttusCd=A0601,A0602" class="link"><c:out value="${orderStat.todayCount}"/></a>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt>어제 주문완료</dt>
                                        <dd>
                                        	<a href="/cabinet/order/list?searchStartDate=${yesterday}&searchEndDate=${yesterday}&arrOrdSttus=C&arrUseSttusCd=A0601,A0602" class="link_yester"><c:out value="${orderStat.yesterdayCount}"/></a>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="box">
                                <p class="sub">결제 결과&#40;<c:out value="${paymentStat.batchDt}"/> 정기결제 기준&#41;</p>
                                <div>
                                    <dl>
                                        <dt>승인</dt>
                                        <dd><a href="/payment/list?resTy=S&searchStartDt=${paymentStat.batchDt}&searchEndDt=${paymentStat.batchDt}" class="link"><fmt:formatNumber type="number" maxFractionDigits="3" value="${paymentStat.successCount}" /></a></dd>
                                    </dl>
                                    <dl>
                                        <dt class="color_warn">승인오류</dt>
                                        <dd><a href="/payment/list?resTy=F&searchStartDt=${paymentStat.batchDt}&searchEndDt=${paymentStat.batchDt}" class="link_error"><fmt:formatNumber type="number" maxFractionDigits="3" value="${paymentStat.failCount}" /></a></dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rate">
                        <div class="title">
                            <p>지부별 캐비닛 가동율</p>
                        </div>
                        <div class="rate_slide">
                        	<c:forEach var="item" items="${branchStat}">
                            <div class="li">
                                <p class="branch"><c:out value="${item.bhfNm}"/></p>
                                <p class="rate_num">
                                    <strong><fmt:formatNumber type="percent" value="${item.percent}" pattern="0%"/></strong>
                                    <a href="/cabinet/order/list?bhfIdx=${item.bhfIdx}&arrOrdSttus=C&arrUseSttusCd=A0601,A0602,A0603,A0604">
                                    <i class="text_under">&#40;<c:out value="${item.useCount}"/>/<c:out value="${item.totalCount}"/>&#41;</i>
                                    </a>
                                </p>
                            </div>
                            </c:forEach>
                        </div>
                        <div class="slider_counter">1/2</div>
                    </div>
                </div>
                <div class="flex mart60">
                    <div class="board">
                        <div class="title">
                            <p>종료 신청</p>
                            <a href="/cabinet/order/list?arrUseSttusCd=A0604" class="more">더보기</a>
                        </div>
                        <table cellpadding="0" cellspacing="0">
                            <colgroup>
                                <col width="76px">
                                <col width="76px">
                                <col width="92px">
                                <col width="69px">
                                <col width="15px">
                            </colgroup>
                            <tbody>
                            	<c:forEach var="item" items="${endList}">
                                <tr>
                                    <td>
                                    <c:choose>
									<c:when test="${fn:length(item.mberNm) > 2}">
										<c:out value="${util:masking(item.mberNm, 1, fn:length(item.mberNm)-2)}"/>
									</c:when>
									<c:otherwise>
										<c:out value="${util:masking(item.mberNm, 1, fn:length(item.mberNm)-1)}"/>
									</c:otherwise>
									</c:choose>
                                    &nbsp;<c:out value="${item.mberIdx}"/></td>
                                    <td><c:out value="${item.bhfNm}"/></td>
                                    <td><c:out value="${item.goodsKorNm}"/></td>
                                    <td><c:out value="${item.goodsLbl}"/></td>
                                    <c:choose>
                                    	<c:when test="${item.newYn eq 'Y'}">
                                    		<td><i class="new">N</i></td>
                                    	</c:when>
                                    	<c:otherwise>
                                    		<td></td>
                                    	</c:otherwise>
                                    </c:choose>
                                </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="board">
                        <div class="title">
                            <p>지부투어 신청</p>
                            <a href="/branch/tour/list" class="more">더보기</a>
                        </div>
                        <table cellpadding="0" cellspacing="0">
                            <colgroup>
                                <col width="74px">
                                <col width="68px">
                                <col width="82px">
                                <col width="89px">
                                <col width="15px">
                            </colgroup>
                            <tbody>
                            	<c:forEach var="item" items="${tourList}">
                                <tr>
                                    <td>
                                    <c:choose>
									<c:when test="${fn:length(item.visitrNm) > 2}">
										<c:out value="${util:masking(item.visitrNm, 1, fn:length(item.visitrNm)-2)}"/>
									</c:when>
									<c:otherwise>
										<c:out value="${util:masking(item.visitrNm, 1, fn:length(item.visitrNm)-1)}"/>
									</c:otherwise>
									</c:choose>
                                    &nbsp;<c:out value="${item.mberIdx}"/>
                                    </td>
                                    <td><c:out value="${item.bhfNm}"/></td>
                                    <td><c:out value="${item.tourHopeDe}"/></td>
                                    <td>
                                    	<c:out value="${item.tourHopeTimeTyNm}"/>
                                    	<%-- <c:choose>
                                    		<c:when test="${item.tourHopeTimeTy eq 'A'}">오전 10:00 ~ 오후 12:00</c:when>
                                    		<c:when test="${item.tourHopeTimeTy eq 'P'}">오후 14:00 ~ 오후 16:00</c:when>
                                    		<c:when test="${item.tourHopeTimeTy eq 'D'}">오후 16:00 ~ 오후 18:00</c:when>
                                    		<c:otherwise></c:otherwise>
                                    	</c:choose> --%>
                                    </td>
                                    <c:choose>
                                    	<c:when test="${item.newYn eq 'Y'}">
                                    		<td><i class="new">N</i></td>
                                    	</c:when>
                                    	<c:otherwise>
                                    		<td></td>
                                    	</c:otherwise>
                                    </c:choose>
                                </tr>
                                </c:forEach>
                            </tbody>
                        </table>
					</div>
					<div class="board">
						<div class="title">
							<p>1:1 문의</p>
							<!-- 211108 1:1 문의 카운팅 추가 -->
							<div class="tit_info">
								<div class="total">
									<p>전체 <a href="/complaint/inquiry/list"><c:out value="${inqryCnt.totCnt}"/></a></p>
									<p>완료 <a href="/complaint/inquiry/list?answerYn=Y"><c:out value="${inqryCnt.complCnt}"/></a></p>
									<p class="color_warn">대기 <a href="/complaint/inquiry/list?answerYn=N" class="color_warn"><c:out value="${inqryCnt.waitCnt}"/></a></p>
								</div>
								<a href="/complaint/inquiry/list" class="more">더보기</a>
							</div>
						</div>
						<table cellpadding="0" cellspacing="0" class="inquiry">
                            <colgroup>
                                <col width="100px">
                                <col width="213px">
                                <col width="15px">
                            </colgroup>
                            <tbody>
                            	<c:forEach var="item" items="${inquiryList}">
	                                <tr>
	                                    <td>
	                                    <c:choose>
										<c:when test="${fn:length(item.mberNm) > 2}">
											<c:out value="${util:masking(item.mberNm, 1, fn:length(item.mberNm)-2)}"/>
										</c:when>
										<c:otherwise>
											<c:out value="${util:masking(item.mberNm, 1, fn:length(item.mberNm)-1)}"/>
										</c:otherwise>
										</c:choose>
	                                    &nbsp;<c:out value="${item.mberIdx}"/></td>
	                                    <td><p><a href="javascript:fn_inqryView('${item.inqryIdx}', '${item.inqrySts}')" style="color:#000000;"><c:out value="${item.inqrySj}"/></a></p></td>
	                                    <c:choose>
	                                    	<c:when test="${item.newYn eq 'Y'}">
	                                    		<td><i class="new">N</i></td>
	                                    	</c:when>
	                                    	<c:otherwise>
	                                    		<td></td>
	                                    	</c:otherwise>
	                                    </c:choose>
	                                </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
	// 지부별 캐비닛 가동율
	var $slider = $('.rate_slide');
	if ($slider.length) {
		var currentSlide;
		var slidesCount;
		var sliderCounter = document.getElementsByClassName('slider_counter');
		var updateSliderCounter = function(slick, currentIndex) {
			currentSlide = slick.slickCurrentSlide() + 1;
			slidesCount = slick.slideCount;
			$(sliderCounter).text(currentSlide + '/' +slidesCount)
		};
		$slider.on('init', function(event, slick) {
			$slider.append(sliderCounter);
			updateSliderCounter(slick);
		});
		$slider.on('afterChange', function(event, slick, currentSlide) {
			updateSliderCounter(slick, currentSlide);
		});
		$slider.slick({
			rows:5,
			swipe:false,
		});
	}
});

// 1:1 문의 제목 클릭하면 답변상태에 따른 View페이지 혹은 답변페이지로 이동(220106 추가)
function fn_inqryView(inqryIdx, inqrySts){

	if(inqrySts == 'Y'){	// 답변완료
		location.href = "/complaint/inquiry/view?inqryIdx="+inqryIdx;
	}else{	// 답변대기
		location.href = "/complaint/inquiry/form?inqryIdx="+inqryIdx;
	}
}
</script>
</body>
</html>