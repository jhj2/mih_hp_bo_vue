<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>

<!doctype html>
<html lang="ko">
<head>
	<%@ include file="/WEB-INF/views/common/common.jsp"%>
</head>
<body>

<input type="hidden" id="code" value="<c:out value='${errorCode}'/>"/>
<div id="login_wrap">
	<div class="login">
		<h1 class="logo"><img src="/resources/assets/images/main_logo.png" alt="MIH 로고"></h1>
		<form method="post" action="/user/authen">
			<fieldset>
				<legend></legend>
				<input type="text" id="username" name="username" placeholder="아이디">
				<input type="password" id="password" name="password" placeholder="비밀번호">
				<p>비밀번호는 영문, 숫자 및 특수문자 포함 8자 이상</p>
				<button>로그인</button>
			</fieldset>
		</form>
	</div>
</div>

<script type="text/javascript">
if($("#code").val() == "101" || $("#code").val() == "102" || $("#code").val() == "103"){
	alert('<spring:message code="login.fail.msg.101"/>');
}
</script>
</body>
</html>