<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
<form id="orgFrm" name="orgFrm" method="post">

	<!-- <input type="hidden" id="bbsIdx" name="bbsIdx" value=0 /> -->
	<input type="hidden" id="resultJson" name="resultJson" />
	
	<!-- 페이징처리 변수 -->
	<input type="hidden" id="currentPageNo" name="currentPageNo" value="${paging.currentPageNo}"/>
	<input type="hidden" id="pageSize" name="pageSize" value="${paging.pageSize}"/>
	<input type="hidden" id="recordCountPerPage" name="recordCountPerPage" value="${paging.recordCountPerPage}"/>
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 게시판 카테고리 관리 start -->
				<h3 class="page_title">조직도 관리</h3>
				<div class="clear">
					<%-- <p class="t_total">총 ${totCnt}건</p> --%>
					<a onclick="javascript:fn_save();" class="s_btn submit right">저장하기</a>
				</div>
				<!-- 리스트 start -->
				<div class="orgList">
					<div class="ceoList">
						대표이사
					</div>
					<div class="headList">
						<div id="ito">ITO사업본부(김동기)</div>
						<div id="si">SI사업본부<br/>(윤정규)</div>
						<div id="busin">경영지원본부(윤정규)</div>
						<div id="tech">기술연구소(전민우)</div>
					</div>
					<div class="thirdList">
						<div id="thirdIto">ITO사업부(김상봉)</div>
						<div id="hro">HRO사업부<br/>(이승훈)</div>
						<div id="thirdSi">SI사업1부<br/>(황태진)</div>
						<div id="plan">기획부<br/>(조용우)</div>
						<div id="thirdBusin">경영지원부<br/>(배은경)</div>
						<div id="rnd">R&D<br/>(전민우)</div>
					</div>
				</div>
				<!-- 리스트 end -->
				<!-- <div id="result"></div> -->
			</div>
		</div>
	</div>
</form>
<style>
	.ceoList{	width:98.4%; height:50px; line-height:45px; float:left; text-align:center; border:2px solid #000000; background:#b4c6e7; font-size:20px; overflow-y:auto; margin-top:20px; margin-left:0.5%; margin-bottom:10px;	}
	
	.headList{	width:99%; height:50px; line-height:45px; float:left; text-align:center; font-size:16px; margin-bottom:10px;	}
	.headList #ito{	width:66.1%; float:left; margin-left:0.5%; border:2px solid #000000; background:#d9e1f2; 	}
	.headList #si{	width:6.3%; float:left; margin-left:0.5%; border:2px solid #000000; background:#d9e1f2; line-height:22.5px;	}
	.headList #busin{	width:19.4%; float:left; margin-left:0.5%; border:2px solid #000000; background:#d9e1f2; 	}
	.headList #tech{	width:6.1%; float:left; margin-left:0.5%; border:2px solid #000000; background:#d9e1f2; line-height:22px;	}
	
	.thirdList{	width:99%; height:50px; line-height:45px; float:left; text-align:center; font-size:16px; margin-bottom:10px;	}
	.thirdList #thirdIto{	width:46.2%; float:left; margin-left:0.5%; border:2px solid #000000; background:#d9e1f2; 	}
	.thirdList #hro{	width:19.4%; float:left; margin-left:0.5%; border:2px solid #000000; background:#d9e1f2; line-height:22.5px; 	}
	.thirdList #thirdSi{	width:6.3%; float:left; margin-left:0.5%; border:2px solid #000000; background:#d9e1f2; line-height:22.5px; 	}
	.thirdList #plan{	width:6%; float:left; margin-left:0.5%; border:2px solid #000000; background:#d9e1f2; line-height:22.5px; 	}
	.thirdList #thirdBusin{	width:12.9%; float:left; margin-left:0.5%; border:2px solid #000000; background:#d9e1f2; line-height:22.5px; 	}
	.thirdList #rnd{	width:6.1%; float:left; margin-left:0.5%; border:2px solid #000000; background:#d9e1f2; line-height:22.5px;	}
	
	.teamList{	width:6.1%; height:600px; float:left; border:1px solid #d4d4d4; font-size:14px; overflow-y:auto; margin-left:0.5%;	}
	.teamList span{	width:90%; height:25px; text-align:center; line-height:25px; display:inline-block; margin-top:5px; margin-bottom:5px; margin-left:5px; padding-left:5px; background:#201c18; color:#ffffff;	}
	
	.sortable{	width:100%; list-style-type:none; margin:0; padding:0;	}
	.sortable li{	height:25px; line-height:25px; font-size:14px; text-align:center; margin-top:5px;	}
	.sortable li span{	display:inline-block;	}
	
	.sortable2 {	width:440px; list-style-type:none; margin:0; padding:0;	}
	.sortable2 li {	height:25px; padding-left:5px; font-size:14px;	}
	.sortable2 li span{	display:inline-block;	}
	
	#yel{	background:yellow;	}
	
	/* 조직원 이동 타켓 CSS */
	.orgMove {
		/* border: 1px dotted black; */
		margin: 0 1em 1em 0;
		height: 50px;
		margin-left:auto;
		margin-right:auto;
		/* 파랑색으로 표신되는 것이 주요 포인트 */
		/* background-color: #0f77be; */
	}
</style>
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
<script type="text/javascript">
$(function(){
	if("${orgList.size() > 0}")
	{
		var html = "";
		var gbCd = null;
		var cnt = 0;
		var maxCnt = Number(${orgList.size()});
		var emt_flg = true;
		<c:forEach var="result" items="${orgList}" varStatus="status">
			if (Number("${result.lvl}") >= 4) {
				if (gbCd == null) {
					gbCd = "${result.orgCd}";
					html += '<div class="teamList" id="'+"${result.orgCd}"+'">';
					html += '<span>';
					html +=	"${result.orgNm}";
					html += '</span>';
					html += '<ul class="sortable">';
					html += '<li value="'+"${result.orgCd}"+'" id="yel">'+"${result.orgUserNm}"+'</li>';
				} else if (gbCd == "${result.orgCd}") {
					html += '<li value="'+"${result.orgCd}"+'">'+"${result.orgUserNm}"+'</li>';
				} else {
					html += '</ul>';
					html += '</div>';
					gbCd = "${result.orgCd}";
					if (cnt < maxCnt) {
						html += '<div class="teamList" id="'+"${result.orgCd}"+'">';
						html += '<span>';
						html +=	"${result.orgNm}";
						html += '</span>';
						html += '<ul class="sortable">';
						// debugger;
						if ((${result.sort == '1'})
								&& (Number("${result.orgCd}") <= 16)
								&& (Number("${result.porgCd}") < 7)){
							html += '<li value="'+"${result.orgCd}"+'" id="yel">'+"${result.orgUserNm}"+'</li>';
						} else {
							html += '<li value="'+"${result.orgCd}"+'">'+"${result.orgUserNm}"+'</li>';
						}
					}
				}
				cnt++;
			}
		</c:forEach>
		html += '</ul>';
		html += '</div>';
		$(".thirdList").after(html);
	}

	$(".teamList .sortable").sortable({	// ID가 "sortable"인 태그의 내부에 포함된 태그를 사용해서 드래그 가능한 리스트 생성
		// 드래그 앤 드롭 단위 css 선택자
		connectWith: ".teamList .sortable",

		// 이동하려는 location에 추가 되는 클래스
		placeholder: "orgMove",
		update: function(event, ui){	// 요소가 변할경우 실행
			$(ui.item).css("background", "#0f77be");
		},
		stop: function(event, ui){	// 모든 이동이 끝난 후 마지막으로 실행
			// 드래그한 이름에 background 색상 보이게 추가
			//$(ui.item).css("background", "#0f77be");
		}
	});
	$(".teamList .sortable li").disableSelection();	// 아이템 내부의 글자를 드래그해서 선택하지 못하도록 하는 기능(반드시 필요한 부분은 아님)
	return;

	if("${orgListLength}" == "0"){
		var html = "";
		html += '<div class="teamList">';
		html += '<span>K쇼핑제휴</span>';
		html += '<ul class="sortable">';
		html += '<li id="yel">손호진</li>';
		html += '<li>강치성</li>';
		html += '<li>정광일</li>';
		html += '<li>이예린</li>';
		html += '<li>지향난</li>';
		html += '</ul>';
		html += '</div>';

		html += '<div class="teamList">';
		html += '<span>K쇼핑EMC</span>';
		html += '<ul class="sortable">';
		html += '<li id="yel">허민회</li>';
		html += '<li>변미해</li>';
		html += '<li>박승현</li>';
		html += '<li>김지혜</li>';
		html += '<li>이원웅</li>';
		html += '<li>원인정</li>';
		html += '</ul>';
		html += '</div>';

		html += '<div class="teamList">';
		html += '<span>홈IoT</span>';
		html += '<ul class="sortable">';
		html += '<li id="yel">이창기</li>';
		html += '<li>강동헌</li>';
		html += '<li>김초롱</li>';
		html += '<li>이봉한</li>';
		html += '<li>이윤경</li>';
		html += '<li>신혜인</li>';
		html += '<li>손일영</li>';
		html += '</ul>';
		html += '</div>';
		
		html += '<div class="teamList">';
		html += '<span>신원</span>';
		html += '<ul class="sortable">';
		html += '<li id="yel">김승겸</li>';
		html += '<li>신소라</li>';
		html += '<li>윤상아</li>';
		html += '<li>남경희</li>';
		html += '</ul>';
		html += '</div>';

		html += '<div class="teamList">';
		html += '<span>잡코리아</span>';
		html += '<ul class="sortable">';
		html += '<li id="yel">이대규</li>';
		html += '<li>이재흠</li>';
		html += '<li>이혜진</li>';
		html += '<li>김성욱</li>';
		html += '</ul>';
		html += '</div>';
		
		html += '<div class="teamList">';
		html += '<span>신한mizi</span>';
		html += '<ul class="sortable">';
		html += '<li>이보엽</li>';
		html += '<li>김창환</li>';
		html += '<li>양교훈</li>';
		html += '</ul>';
		html += '</div>';
		
		html += '<div class="teamList">';
		html += '<span>개발</span>';
		html += '<ul class="sortable">';
		html += '<li>김인성</li>';
		html += '<li>곽성식</li>';
		html += '<li>이기훈</li>';
		html += '<li>박재용</li>';
		html += '<li>이민하</li>';
		html += '<li>전현준</li>';
		html += '<li>강하림</li>';
		html += '<li>김지헌</li>';
		html += '<li>김진수</li>';
		html += '<li>김광수</li>';
		html += '</ul>';
		html += '</div>';
		
		html += '<div class="teamList">';
		html += '<span>1팀</span>';
		html += '<ul class="sortable">';
		html += '<li>이학만</li>';
		html += '</ul>';
		html += '</div>';

		html += '<div class="teamList">';
		html += '<span>2팀</span>';
		html += '<ul class="sortable">';
		html += '<li>이동기</li>';
		html += '</ul>';
		html += '</div>';
		
		html += '<div class="teamList">';
		html += '<span>영업지원</span>';
		html += '<ul class="sortable">';
		html += '<li>이호정</li>';
		html += '</ul>';
		html += '</div>';
		
		html += '<div class="teamList">';
		html += '<span>&nbsp;</span>';
		html += '<ul class="sortable">';
		html += '<li>&nbsp;</li>';
		html += '</ul>';
		html += '</div>';
		
		html += '<div class="teamList">';
		html += '<span>&nbsp;</span>';
		html += '<ul class="sortable">';
		html += '<li>&nbsp;</li>';
		html += '</ul>';
		html += '</div>';
		
		html += '<div class="teamList">';
		html += '<span>인사/총무</span>';
		html += '<ul class="sortable">';
		html += '<li>김수연</li>';
		html += '</ul>';
		html += '</div>';
		
		html += '<div class="teamList">';
		html += '<span>재무/회계</span>';
		html += '<ul class="sortable">';
		html += '<li>배은경</li>';
		html += '</ul>';
		html += '</div>';
		
		html += '<div class="teamList">';
		html += '<span>R&D</span>';
		html += '<ul class="sortable">';
		html += '<li>김민기</li>';
		html += '<li>황기선</li>';
		html += '<li>경민기</li>';
		html += '<li>박지호</li>';
		html += '<li>김재우</li>';
		html += '<li>기경욱</li>';
		html += '<li>이지영</li>';
		html += '</ul>';
		html += '</div>';
		
		$(".thirdList").after(html);
	}else{
		var resultJson = JSON.parse('${orgList}');
		var html = "";
		$(".teamList").remove();
		
		$.each(resultJson, function(key, value){
			var name = value.name.split("|");
			html += '<div class="teamList">';
			html += '<span>'+value.team+'</span>';
			html += '<ul class="sortable">';

			for(var i=0; i<name.length-1; i++){
				if(value.team == "K쇼핑제휴" || value.team == "K쇼핑EMC" || value.team == "홈IoT" || value.team == "신원" || value.team == "잡코리아"){
					if(i == 0){
						html += '<li id="yel">'+name[i]+'</li>';
					}else{
						html += '<li>'+name[i]+'</li>';
					}
				}else{
					html += '<li>'+name[i]+'</li>';
				}
			}
			html += '</ul>';
			html += '</div>';
		});
		$(".thirdList").after(html);
	}

	// ID가 "sortable"인 태그의 내부에 포함된 태그를 사용해서 드래그 가능한 리스트 생성
	$(".teamList .sortable").sortable({
		// 드래그 앤 드롭 단위 css 선택자
		connectWith: ".teamList .sortable",
		
		// 이동하려는 location에 추가 되는 클래스
		placeholder: "orgMove",
		update: function(event, ui){	// 요소가 변할경우 실행
			$(ui.item).css("background", "#0f77be");
		},
		stop: function(event, ui){	// 모든 이동이 끝난 후 마지막으로 실행
			// 드래그한 이름에 background 색상 보이게 추가
			//$(ui.item).css("background", "#0f77be");
		}
	});

	// 아이템 내부의 글자를 드래그해서 선택하지 못하도록 하는 기능(반드시 필요한 부분은 아님)
	$(".teamList .sortable li").disableSelection();
	
	/* $(".teamList .sortable").sortable({
		// 드래그 앤 드롭 단위 css 선택자
		connectWith: ".teamList .sortable li",
		// 움직이는 css 선택자
		handle: ".sortable li",
		// 움직이지 못하는 css 선택자
		//cancel: ".no-move",
		// 이동하려는 location에 추가 되는 클래스
		placeholder: "orgMove"
	}); */
});

// 저장
function fn_save(){
	// (".teamList") 불러와서 데이터 전달
	var arr = new Array();
	for(var i=0; i<$(".teamList").length; i++) {
		for(var j=0; j<$(".teamList").eq(i).find(".sortable li").length; j++) {
			var obj = new Object();
			obj.desOrgCd = $(".teamList").eq(i).attr("id"); //바뀔조직코드
			obj.desSort = j+1; // SORT
			obj.ordCd = $(".teamList").eq(i).find("li").eq(j).attr("value");	//기존 조직코드
			obj.name = $(".teamList").eq(i).find("li").eq(j).text();	//대상 이름
			arr.push(obj);
		}
	}
	var jsonData = JSON.stringify(arr);

	$.ajax({
		type: "post",
		url: "/org/json/save",
		// data : arr,
		data :  {"jsonData" : jsonData},
		dataType: "json",
		success: function(res){
			if(res.orgList.length > 0)
			{
				$(".teamList").remove();
				var html = "";
				var gbCd = null;
				var cnt = 0;
				var maxCnt = Number(res.orgList.length);
				var emt_flg = true;
				$.each(res.orgList, function(key, result){
					if (Number(result.lvl) >= 4) {
						if (gbCd == null) {
							gbCd = result.orgCd;
							html += '<div class="teamList" id="'+result.orgCd+'">';
							html += '<span>';
							html +=	result.orgNm;
							html += '</span>';
							html += '<ul class="sortable">';
							html += '<li value="'+result.orgCd+'" id="yel">'+result.orgUserNm+'</li>';
						} else if (gbCd == result.orgCd) {
							html += '<li value="'+result.orgCd+'">'+result.orgUserNm+'</li>';
						} else {
							html += '</ul>';
							html += '</div>';
							gbCd = result.orgCd;
							if (cnt < maxCnt) {
								html += '<div class="teamList" id="'+result.orgCd+'">';
								html += '<span>';
								html +=	result.orgNm;
								html += '</span>';
								html += '<ul class="sortable">';
								// debugger;
								if ((result.sort == '1')
										&& (Number(result.orgCd) <= 16)
										&& (Number(result.porgCd) < 7)){
									html += '<li value="'+result.orgCd+'" id="yel">'+result.orgUserNm+'</li>';
								} else {
									html += '<li value="'+result.orgCd+'">'+result.orgUserNm+'</li>';
								}
							}
						}
						cnt++;
					}
				});
				html += '</ul>';
				html += '</div>';
				$(".thirdList").after(html);
			}
			// ID가 "sortable"인 태그의 내부에 포함된 태그를 사용해서 드래그 가능한 리스트 생성
			$(".teamList .sortable").sortable({
				// 드래그 앤 드롭 단위 css 선택자
				connectWith: ".teamList .sortable",

				// 이동하려는 location에 추가 되는 클래스
				placeholder: "orgMove",
				update: function(event, ui){	// 요소가 변할경우 실행
					$(ui.item).css("background", "#0f77be");
				},
				stop: function(event, ui){	// 모든 이동이 끝난 후 마지막으로 실행
					// 드래그한 이름에 background 색상 보이게 추가
					//$(ui.item).css("background", "#0f77be");
				}
			});

			// 아이템 내부의 글자를 드래그해서 선택하지 못하도록 하는 기능(반드시 필요한 부분은 아님)
			$(".teamList .sortable li").disableSelection();
		},
		error: function(err){
			alert(err.responseJSON.message);	// 코드값 확인 : err.responseJSON.code
			location.reload();
		}
	});
	return;

	// 배열 선언
	var arr = new Array();
	for(var i=0; i<$(".teamList").length; i++){
		var obj = new Object();
		obj.team = $(".teamList").eq(i).find("span").text();
		obj.name = "";

		for(var j=0; j<$(".teamList").eq(i).find(".sortable li").length; j++){
			//console.log($(".teamList").eq(i).find(".sortable li").eq(j).text());
			obj.name += $(".teamList").eq(i).find(".sortable li").eq(j).text() + "|";
		}
		arr.push(obj);
	}
	//console.log(arr);
	
	// json 형태의 문자열로 생성
	var jsonData = JSON.stringify(arr);
	$("#resultJson").val(jsonData);	// $("#result").html(jsonData);
	
	// java단 json 파일 저장
	//var frm = $("#orgFrm")[0];
	//frm.action = "/org/json/save";
	//frm.method = "post";
	//frm.submit();

	// parameter 넘기기
	var param = {
		"resultJson" : $("#resultJson").val()
	}
	
	$.ajax({
		type: "post",
		url: "/org/json/save",
		data : param,
		dataType: "json",
		success: function(res){
			// JSON tree 형태로 할 수 있도록 구조화
			var resultJson = JSON.parse(res.org.resultJson);
			var html = "";
			$(".teamList").remove();
			
			$.each(resultJson, function(key, value){
				var name = value.name.split("|");
				html += '<div class="teamList">';
				html += '<span>'+value.team+'</span>';
				html += '<ul class="sortable">';

				for(var i=0; i<name.length-1; i++){
					if(value.team == "K쇼핑제휴" || value.team == "K쇼핑EMC" || value.team == "홈IoT" || value.team == "신원" || value.team == "잡코리아"){
						if(i == 0){
							html += '<li id="yel">'+name[i]+'</li>';
						}else{
							html += '<li>'+name[i]+'</li>';
						}
					}else{
						html += '<li>'+name[i]+'</li>';
					}
				}
				
				html += '</ul>';
				html += '</div>';
			});
			$(".thirdList").after(html);

			// Ajax 이후 스크립트 실행되도록 수정
			// ID가 "sortable"인 태그의 내부에 포함된 태그를 사용해서 드래그 가능한 리스트 생성
			$(".teamList .sortable").sortable({
				// 드래그 앤 드롭 단위 css 선택자
				connectWith: ".teamList .sortable",

				// 이동하려는 location에 추가 되는 클래스
				placeholder: "orgMove",
				update: function(event, ui){	// 요소가 변할경우 실행
					$(ui.item).css("background", "#0f77be");
				},
				stop: function(event, ui){	// 모든 이동이 끝난 후 마지막으로 실행
					// 드래그한 이름에 background 색상 보이게 추가
					//$(ui.item).css("background", "#0f77be");
				}
			});
			// 아이템 내부의 글자를 드래그해서 선택하지 못하도록 하는 기능(반드시 필요한 부분은 아님)
			$(".teamList .sortable li").disableSelection();
		},
		error: function(e){
			alert(err.responseJSON.message);	// 코드값 확인 : err.responseJSON.code
			location.reload();
		}
	});

	// json 파일 저장
	//jsonDown(jsonData, "D:/STS/workspace/mih_hp_bo/src/main/resources/json/org.json", "text/plain");
}

// [Javascript] json 파일 저장 함수
function jsonDown(jsonData, fileNm, contentType){
	var a = document.createElement("a");
	var file = new Blob([jsonData], {type: contentType});
	a.href = URL.createObjectURL(file);
	a.download = fileNm;
	a.click();
}
</script>
</body>
</html>