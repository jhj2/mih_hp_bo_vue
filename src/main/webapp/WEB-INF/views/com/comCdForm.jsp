<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>
<form id="comCdForm" name="comCdForm" method="post">
	
	<!-- 수정일 경우 hidden값 세팅 -->
	<c:if test="${STS_FLAG eq 'upd'}">
		<input type="hidden" id="comCd" name="comCd" class="w100" value="${resultVO.comCd}">
	</c:if>
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 지부정보 등록 start -->
				<h3 class="page_title">
					공통코드관리
					<c:if test="${STS_FLAG eq 'add'}">
						등록
					</c:if>
					<c:if test="${STS_FLAG eq 'upd'}">
						수정
					</c:if>
				</h3>
				<table cellpadding="0" cellspacing="0" class="t_form">
					<colgroup>
						<col width="100px"><col width="130px"><col width="100px"><col width="130px"><col width="100px"><col width="130px">
					</colgroup>
					<tbody>
					<tr>
						<td class="th">공통코드</td>
						<td>
							<c:if test="${STS_FLAG eq 'add'}">
								<input type="text" id="comCd" name="comCd" class="w100" value="자동세팅" readonly="readonly" style="background:#efefef;">
							</c:if>
							<c:if test="${STS_FLAG eq 'upd'}">
								<c:out value="${resultVO.comCd}"/>
							</c:if>
						</td>
						<td class="th">공통코드명</td>
						<td>
							<input type="text" id="cdNm" name="cdNm" class="w100" value="${resultVO.cdNm}" maxlength="150" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.cd.cdNm"/>">
						</td>
						<td class="th">코드영문명</td>
						<td>
							<input type="text" id="cdEngNm" name="cdEngNm" class="w100" value="${resultVO.cdEngNm}" maxlength="100">
						</td>
					</tr>
					<tr>
						<td class="th">상위공통코드</td>
						<td>
							<select name="upperComCd" id="upperComCd" class="w100" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.cd.upperComCd"/>">
								<c:forEach var="result" items="${cdList}" varStatus="status">
									<option value="${result.comCd}" <c:if test="${resultVO.upperComCd eq result.comCd}">selected</c:if>><c:out value="${result.cdNm}"/></option>
								</c:forEach>
							</select>
						</td>
						<td class="th">코드레벨</td>
						<td>
							<input type="text" id="lvl" name="lvl" class="w100" value="${resultVO.lvl}" readonly="readonly" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.cd.lvl"/>">
						</td>
						<td class="th">정렬순서</td>
						<td>
							<input type="text" id="cdSort" name="cdSort" class="w100" value="${resultVO.cdSort}" maxlength="9" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.cd.cdSort"/>">
						</td>
					</tr>
					<tr>
						<td class="th">값1</td>
						<td>
							<input type="text" id="val1" name="val1" class="w100" value="${resultVO.val1}" maxlength="15">
						</td>
						<td class="th">값2</td>
						<td>
							<input type="text" id="val2" name="val2" class="w100" value="${resultVO.val2}" maxlength="15">
						</td>
						<td class="th">값3</td>
						<td>
							<input type="text" id="val3" name="val3" class="w100" value="${resultVO.val3}" maxlength="15">
						</td>
					</tr>
					<tr>
						<td class="th">사용여부</td>
						<td>
							<select name="useYn" id="useYn" class="w100" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.useYn"/>">
								<c:forEach var="result" items="${useYnSelect}" varStatus="status">
									<option value="${result.val1}" <c:if test="${resultVO.useYn eq result.val1}">selected</c:if>>${result.cdNm}</option>
								</c:forEach>
							</select>
							<%-- <select name="useYn" id="useYn" class="w100" data-parsley-required="true" data-parsley-error-message="<spring:message code="valid.bbs.useYn"/>">
								<option value="">선택</option>
								<option value="Y" <c:if test="${resultVO.useYn eq 'Y'}">selected</c:if>>사용</option>
								<option value="N" <c:if test="${resultVO.useYn eq 'N'}">selected</c:if>>미사용</option>
							</select> --%>
						</td>
						<td class="th">코드설명</td>
						<td colspan="3">
							<input type="text" id="cdDc" name="cdDc" class="w100" value="${resultVO.cdDc}" maxlength="200">
						</td>
					</tr>
					</tbody>
				</table>
				
				<div class="btn_wrap">
					<a href="javascript:fn_cancel();" class="cancle">취소하기</a>
					<c:if test="${STS_FLAG eq 'add'}">
						<a href="javascript:fn_add();" class="submit">등록하기</a>
					</c:if>
					<c:if test="${STS_FLAG eq 'upd'}">
						<a href="javascript:fn_upd();" class="submit">수정하기</a>
					</c:if>
				</div>
				<!-- 게시판 카테고리 등록 end -->
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
$(document).ready(function(){
	// 코드영문명 유효성검사 추가(영문, 숫자를 제외한 나머지 문자 입력 안되도록 구현) 
	$("#cdEngNm").keyup(function(){
		$("#cdEngNm").val($("#cdEngNm").val().replace(/[^a-zA-Z_]*$/, ''));
	});

	// 정렬순서 유효성검사 추가(숫자를 제외한 나머지 문자 입력 안되도록 구현)
	$("#cdSort").keyup(function(){
		$("#cdSort").val($("#cdSort").val().replace(/[^0-9]*$/, ''));
	});

	// 상위공통코드 선택하면 코드레벨값 세팅
	$("#upperComCd").change(function(){
		if($(this).val() == ""){
			$("#lvl").val("");	// 값 초기화
		}else if($(this).val() == "-1"){
			$("#lvl").val("1");
		}else{
			$("#lvl").val("2");
		}
	});

	// 수정화면에서 상위공통코드 combo box disabled 처리
	<c:if test="${STS_FLAG eq 'upd'}">
		$("#upperComCd").prop("disabled", true);
	</c:if>
});

// 등록하기
function fn_add(){

	// validation 체크
	if(formValidation("comCdForm")){

		var frm = $("#comCdForm")[0];
		frm.action = "/com/cd/add";
		frm.submit();
	}
}

// 수정하기
function fn_upd(){

	// validation 체크
	if(formValidation("comCdForm")){

		var frm = $("#comCdForm")[0];
		frm.action = "/com/cd/upd";
		frm.submit();
	}
}

// 취소하기
function fn_cancel(){
	location.href = "/com/cd/list"
}
</script>
</body>
</html>