<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<!doctype html>
<html>
<body>

<!-- 엑셀다운로드 Form 세팅 -->
<form id="excelFrm" id="excelFrm" method="post">
	<!-- 엑셀다운로드 변수 -->
	<input type="hidden" name="exFileNm" id="exFileNm" value="" />
	<input type="hidden" name="exHeaderId" id="exHeaderId" value="" />
	<input type="hidden" name="exHeaderNm" id="exHeaderNm" value="" />
	
	<input type="hidden" name="searchComCd" id="searchComCd" value="" />
	<input type="hidden" name="searchUseYn" id="searchUseYn" value="" />
</form>

<form id="comCdFrm" name="comCdFrm" method="get">

	<input type="hidden" id="comCd" name="comCd" value=""/>
	
	<!-- 페이징처리 변수 -->
	<input type="hidden" id="currentPageNo" name="currentPageNo" value="${paging.currentPageNo}"/>
	<input type="hidden" id="pageSize" name="pageSize" value="${paging.pageSize}"/>
	<input type="hidden" id="recordCountPerPage" name="recordCountPerPage" value="${paging.recordCountPerPage}"/>
	
	<div id="wrap">
		<!-- header start -->
		<header>
			<%@ include file="/WEB-INF/views/common/top.jsp"%>
			<%@ include file="/WEB-INF/views/common/lnb.jsp"%>
		</header>
		<!-- header end -->
		
		<div id="container">
			<div class="contents">
				<!-- 게시판 카테고리 관리 start -->
				<h3 class="page_title">공통코드관리</h3>
				<div class="border_box">
					<div class="flex">
						<p class="tit">공통코드</p>
						<div class="con" style="margin-right:50px;">
							<input type="text" id="searchComCd" name="searchComCd" value="${vo.searchComCd}" placeholder="">
						</div>
						
						<p class="tit">사용여부</p>
						<div class="con">
							<select name="searchUseYn" id="searchUseYn">
								<c:forEach var="result" items="${useYnSelect}" varStatus="status">
									<option value="${result.val1}" <c:if test="${vo.searchUseYn eq result.val1}">selected</c:if>>${result.cdNm}</option>
								</c:forEach>
							</select>
							<%-- <select name="searchUseYn" id="searchUseYn">
								<option value="">선택</option>
								<option value="Y" <c:if test="${vo.searchUseYn eq 'Y'}">selected</c:if>>사용</option>
								<option value="N" <c:if test="${vo.searchUseYn eq 'N'}">selected</c:if>>미사용</option>
							</select> --%>
						</div>
						<button type="button" id="btnSearch" class="s_btn submit" onclick="javascript:fn_search();">검색</button>
					</div>
				</div>
				
				<div class="clear">
					<p class="t_total">총 ${totCnt}건</p>
					<a onclick="javascript:fn_excel();" class="s_btn submit right" style="margin-left:3px;">엑셀다운로드</a>
					<a href="/com/cd/addView" class="s_btn submit right">등록하기</a>
				</div>
				<table cellpadding="0" cellspacing="0" class="t_list mart10">
					<colgroup>
						<col width="150px"><col width="100px"><col width="100px"><col width="100px"><col width="100px"><col width="100px"><col width="100px"><col width="100px"><col width="70px">
					</colgroup>
					<thead>
					<tr>
						<th>공통코드</th>
						<th>공통코드명</th>
						<th>코드영문명</th>
						<th>사용여부</th>
						<th>레벨</th>
						<th>값1</th>
						<th>값2</th>
						<th>값3</th>
						<th>관리</th>
					</tr>
					</thead>
					
					<tbody>
					<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td colspan="9"><spring:message code="no.search.data.msg"/></td>
					</tr>
					</c:if>
					<c:if test="${fn:length(resultList) > 0}">
					<c:forEach var="result" items="${resultList}" varStatus="status">
					<tr>
						<td><c:out value="${result.comCd}"/></td>
						<td><c:out value="${result.cdNm}"/></td>
						<td><c:out value="${result.cdEngNm}"/></td>
						<td><c:out value="${result.useYnNm}"/></td>
						<td><c:out value="${result.lvl}"/></p></td>
						<td><c:out value="${result.val1}"/></td>
						<td><c:out value="${result.val2}"/></td>
						<td><c:out value="${result.val3}"/></td>
						<td>
							<a href="/com/cd/updView?comCd=${result.comCd}" class="color_warn text_under">수정</a>&nbsp;
							<c:if test="${result.useYn eq 'N'}">
								<a href="javascript:fn_delete('<c:out value="${result.comCd}"/>');" class="color_warn text_under">삭제</a>
							</c:if>
						</td>
					</tr>
					</c:forEach>
					</c:if>
					</tbody>
				</table>
				${paging.pagination}
				<!-- 지부정보 관리 end -->
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
// 조회
function fn_search(){
	$("#currentPageNo").val(1);	// 검색했을때 1페이지 유지
	
	var frm = $("#comCdFrm")[0];
	frm.action = "/com/cd/list";
	frm.submit();
}

// 삭제
function fn_delete(comCd){

	if(!confirm('<spring:message text="공통코드를 삭제하시겠습니까?"/>')){
		return;
	}else{
		$("#comCdFrm").find("#comCd").val(comCd);
		var frm = $("#comCdFrm")[0];
		frm.action = "/com/cd/del";
		frm.method = "post";
		frm.submit();
	}
}

// 엑셀 다운로드
function fn_excel(){
	// 1. 엑셀다운로드하기 위한 필수값 세팅(파일명, 헤더ID, 헤더명)
	$("#exFileNm").val("공통코드관리");
	$("#exHeaderId").val("com_cd:cd_nm:cd_eng_nm:use_yn_nm:lvl:val1:val2:val3");
	$("#exHeaderNm").val("공통코드:공통코드명:코드영문명:사용여부:레벨:값1:값2:값3");

	// 2. [공통코드관리] 메뉴에 있는 겁색값을 엑셀다운로드하는 Form에 값 세팅
	$("#excelFrm").find("#searchComCd").val($("#comCdFrm").find("#searchComCd").val());
	$("#excelFrm").find("#searchUseYn").val($("#comCdFrm").find("#searchUseYn").val());

	var frm = $("#excelFrm")[0];
	frm.target = '_self';
	frm.action = "/com/cd/excel";
	frm.method = "post";
	frm.submit();
}
</script>
</body>
</html>