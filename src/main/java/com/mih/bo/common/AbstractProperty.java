package com.mih.bo.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.mih.bo.common.yaml.AppInfoProperty;

import lombok.extern.slf4j.Slf4j;

/**
 * AbstractProperty
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Slf4j
@Component
public abstract class AbstractProperty {

	@Autowired
	private AppInfoProperty appInfoProperty;

	protected StopWatch stopWatch;

	protected AppInfoProperty getAppInfo() {
		return appInfoProperty;
	}

	/**
	 * 설명
	 *
	 * @param message
	*/
	protected void createStopWatch(String message) {
		if (log.isDebugEnabled()) {
			if (this.stopWatch == null) {
				this.stopWatch = new StopWatch(message);
			} else {
				addStopWatch(message);
			}
		}
	}

	/**
	 * 설명
	 *
	 * @param message
	*/
	protected void addStopWatch(String message) {
		if (log.isDebugEnabled()) {
			if (this.stopWatch == null) {
				return;
			}
			if (this.stopWatch.isRunning()) {
				this.stopWatch.stop();
			}
			this.stopWatch.start(message);
		}
	}

	/**
	 * 설명
	*/
	protected void printStopWatch() {
		if (this.stopWatch == null) {
			return;
		}
		if (log.isDebugEnabled() && this.stopWatch.isRunning()) {
			this.stopWatch.stop();
			log.debug("\n{}", this.stopWatch.prettyPrint());
		}
		stopWatch = null;
	}
}