package com.mih.bo.common.web.interceptor;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.Constants;
import com.mih.bo.common.util.RequestUtils;
import com.mih.bo.common.yaml.AppInfoProperty;

/**
 * 공통 인터셉터
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Component
public class CommonInterceptor implements HandlerInterceptor {

	private static final Logger log = LoggerFactory.getLogger(CommonInterceptor.class);

	@Autowired
	private AppInfoProperty appInfoProperty;

	/*
	 * @see org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
	*/
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		// 로그 추적 sessionId, traceId
		String sessionId = request.getSession().getId();
		String traceId = UUID.randomUUID().toString();
		log.debug("sessionId[{}]", sessionId);
		log.debug("traceId[{}]", traceId);
		MDC.put("sessionId", sessionId);
		MDC.put("traceId", traceId);

		String device = Constants.PC;
		String ieYn = Constants.NO;
		if(RequestUtils.isMobile(request)) {
			device = Constants.MOBILE;
		};

		if(RequestUtils.isIE(request)) {
			ieYn = Constants.YES;
		};

		request.setAttribute("device", device);
		request.setAttribute("ieYn", ieYn);

		// 버전 정보
		request.setAttribute("ver", appInfoProperty.getVersion());

		return true;
	}

	/*
	 * @see org.springframework.web.servlet.HandlerInterceptor#afterCompletion(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
	*/
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {

	}

	/*
	 * @see org.springframework.web.servlet.HandlerInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)
	*/
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
	}

}