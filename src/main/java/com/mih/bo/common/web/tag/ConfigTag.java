package com.mih.bo.common.web.tag;

import com.mih.bo.common.config.Property;

/**
 * config.yml Tag
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public class ConfigTag {
	
	/**
	 * config 조회
	 *
	 * @param key
	 * @return String
	*/
	public static String value(String key){
		return Property.get(key);
	}
}