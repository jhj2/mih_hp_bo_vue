package com.mih.bo.common.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * 공통 에러 컨트롤러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Controller
public class CommonErrorController {

	/**
	 * 공통 에러 페이지 설정.
	 *
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @param errorCode Error Code
	 * @param redirectUrl Redirect URL
	 * @param timeout 페이지 이동 Timeout (ms)
	 * @return ModelAndView
	 */
	@GetMapping(path = "/common/error")
	public ModelAndView commonsError(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Integer errorCode, @RequestParam(name = "redirect-url", required = false) String redirectUrl) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("url", redirectUrl);
		mav.setViewName("error");
		return mav;
	}

}
