
package com.mih.bo.common.web.handler;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Global Controller 예외 처리.
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler extends AbstractExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

	/*
	 * @see com.bgbgb.bo.commons.web.handler.AbstractExceptionHandler#handleRuntimeException(javax.servlet.http.HttpServletRequest, java.lang.RuntimeException)
	*/
	@Override
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	protected Object handleRuntimeException(HttpServletRequest request, RuntimeException ex) {
		if (log.isErrorEnabled()) {
			log.error(HttpStatus.INTERNAL_SERVER_ERROR.name(), ex);
		}
		return result(request, "error", ex);
	}

}
