package com.mih.bo.common.web.tag;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.servlet.jsp.tagext.TagSupport;

import com.mih.bo.common.util.CommonCodeUtil;

import lombok.Setter;

/**
 * @Project : MEGA
 * @Class : SelectBoxTag
 * @Description : Custom SelectBoxTag
 * @Author : kis1005
 * @Since : 2021.11.03
 */
public class SelectBoxTag extends TagSupport implements Serializable {

	@Setter
	private String name      = "";           // select box tag id
	@Setter
	private String event     = "";          // select box event
	@Setter
	private String groupCode = "";      // select box master code
	@Setter
	private String selCode   = "";        // select box select code
	@Setter
	private String allType   = "";        // all type : ALL, SEL, NON
	@Setter
	private String className = "";      // select box class name
	
	@Override
	public int doEndTag(){
		StringBuilder selectTag = new StringBuilder();
		selectTag.append("<select id=\"")
				.append(name).append("\" name=\"").append(name)
				.append("\" class=\"").append(className)
				.append("\" onchange=\"").append(event)
				.append(">");
		if (allType.equals(SELECTBOX_TYPE.SEL.toString())) {
			selectTag.append("<option value=\"\"> -- 선택 --  </option>");
		} else if (allType.equals(SELECTBOX_TYPE.ALL.toString())) {
			selectTag.append("<option value=\"\"> -- 전체 --  </option>");
		}
		
		for (Map<String, Object> info : CommonCodeUtil.getComCode(groupCode)) {
			String strSel = "";
			if (selCode.equals(info.get(CommonCodeUtil.COMMON_CODE_ID))) {
				strSel = "selected";
			}
			
			selectTag.append("<option value=\"").append(info.get(CommonCodeUtil.COMMON_CODE_ID)).append("\" ").append(strSel).append(">").append(info.get(CommonCodeUtil.COMMON_CODE_NAME))
					.append("</option>");
		}
		selectTag.append("</select>");
		
		try {
			pageContext.getOut().write(selectTag.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return EVAL_PAGE;
	}
	
	public enum SELECTBOX_TYPE {
		ALL, SEL, NON
	}
}