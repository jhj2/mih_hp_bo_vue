package com.mih.bo.common.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.mih.bo.common.AbstractProperty;

/**
 * 공통 상속 컨트롤러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Component
public abstract class AbstractController extends AbstractProperty {

	private static final Logger log = LoggerFactory.getLogger(AbstractController.class);

}