package com.mih.bo.common.web.handler;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.mih.bo.common.exception.BgbgbServiceException;
import com.mih.bo.common.util.MessageUtils;
import com.mih.bo.common.util.ResponseUtils;
import com.mih.bo.common.util.StringUtils;
import com.mih.bo.common.vo.JsonResponse;

/**
 *  추상화된 기본 예외 처리 핸들러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public abstract class AbstractExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(AbstractExceptionHandler.class);

	/**
	 * Error View page 설정.
	 *
	 * @author
	 * @version 1.0.0
	 * @since 7.0
	 */
	public static class ViewPage {

		public static final String ERROR = "error";

		public static final String ERROR_400 = "error/400";

		public static final String ERROR_401 = "error/401";

		public static final String ERROR_403 = "error/403";

		public static final String ERROR_404 = "error/404";

		public static final String ERROR_405 = "error/405";

		private ViewPage() {
			// ignore...
		}

	}

	/**
	 * HTTP Status 400 에러 처리.
	 *
	 * @param request HttpServletRequest
	 * @param error Throwable
	 * @return Object (ResponseEntity or ModelAndView)
	 */
	@ExceptionHandler({ MissingServletRequestParameterException.class, ServletRequestBindingException.class,
			TypeMismatchException.class, HttpMessageNotReadableException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@SuppressWarnings("all")
	protected Object handleBadRequestException(final HttpServletRequest request, final Throwable error) {
		log.error("{} (URL={}, METHOD={})", error.getMessage(), request.getRequestURL(), request.getMethod());
		return result(request, HttpStatus.BAD_REQUEST, error, ViewPage.ERROR_400);
	}

	/**
	 * HTTP Status 404 에러 처리.
	 *
	 * @param request HttpServletRequest
	 * @param error NoHandlerFoundException
	 * @return Object (ResponseEntity or ModelAndView)
	 */
	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	protected Object handleNotFoundException(final HttpServletRequest request, final NoHandlerFoundException error) {
		log.error("{} (URL={}, METHOD={})", error.getMessage(), request.getRequestURL(), request.getMethod());
		return result(request, HttpStatus.NOT_FOUND, error, ViewPage.ERROR_404);
	}

	/**
	 * HTTP Status 405 에러 처리.
	 *
	 * @param request HttpServletRequest
	 * @param error HttpRequestMethodNotSupportedException
	 * @return Object (ResponseEntity or ModelAndView)
	 */
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	protected Object handleNethodNotAllowedException(final HttpServletRequest request,
			final HttpRequestMethodNotSupportedException error) {
		log.error("{} (URL={}, METHOD={})", error.getMessage(), request.getRequestURL(), request.getMethod());
		// 지원되는 메소드를 보여준다.

		return result(request, HttpStatus.METHOD_NOT_ALLOWED, error, ViewPage.ERROR_405);
	}

	/**
	 * Internal Server Error
	 *
	 * @param request HttpServletRequest
	 * @param error Throwable
	 * @return Object (ResponseEntity or ModelAndView)
	 */
	@ExceptionHandler(Throwable.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	protected Object handleException(final HttpServletRequest request, final Throwable error) {
		log.error(error.getMessage(), error);
		return result(request, error);
	}

	/**
	 * Illegal Exception
	 *
	 * @param request HttpServletRequest
	 * @param error RuntimeException
	 * @return ResponseEntity
	 */
	@ExceptionHandler({ IllegalStateException.class, IllegalArgumentException.class, NullPointerException.class })
	protected abstract Object handleRuntimeException(HttpServletRequest request, RuntimeException error);

	/**
	 * 리턴.
	 *
	 * @param request HttpServletRequest
	 * @param error Throwable
	 * @return Object
	 */
	protected Object result(final HttpServletRequest request, final Throwable error) {
		return result(request, HttpStatus.INTERNAL_SERVER_ERROR, error, null, ViewPage.ERROR);
	}

	/**
	 * 리턴.
	 *
	 * @param request HttpServletRequest
	 * @param error Throwable
	 * @param viewName View Name
	 * @return Object
	 */
	protected Object result(final HttpServletRequest request, final String viewName, final Throwable error) {
		return result(request, HttpStatus.INTERNAL_SERVER_ERROR, error, null, viewName);
	}

	/**
	 * 리턴.
	 *
	 * @param request HttpServletRequest
	 * @param status HttpStatus
	 * @param error Throwable
	 * @param viewName View Name
	 * @return Object
	 */
	protected Object result(final HttpServletRequest request, final HttpStatus status, final Throwable error,
			final String viewName) {
		return result(request, status, error, null, viewName);
	}

	/**
	 * 리턴.
	 *
	 * @param request HttpServletRequest
	 * @param status HttpStatus
	 * @param error Throwable
	 * @param object Object
	 * @param viewName View Name
	 * @return Object
	 */
	protected Object result(final HttpServletRequest request, final HttpStatus status, final Throwable error,
			final Object object, final String viewName) {
		String accept = request.getHeader(HttpHeaders.ACCEPT);
		if (StringUtils.startsWithIgnoreCase(accept, MediaType.APPLICATION_JSON_VALUE)) {
			String message = MessageUtils.getMessage("response.error.msg.".concat(String.valueOf(status.value())));
			if(error instanceof BgbgbServiceException) {
				message = error.getMessage();
			}
			JsonResponse res = new JsonResponse(status.value(), message);
			return ResponseUtils.resultJson(request, res, status);
		} else {
			return ResponseUtils.resultModelAndView(request, viewName, error);
		}
	}

}
