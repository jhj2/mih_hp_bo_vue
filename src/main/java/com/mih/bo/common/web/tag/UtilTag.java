package com.mih.bo.common.web.tag;

import org.springframework.stereotype.Component;
import com.mih.bo.common.util.StringUtils;

/**
 * Util Tag
 * 
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021.09.10.
 * @Description Thymeleaf에서는 JSP에서의 .tld처럼 별도의 설정 파일은 필요치 않고<br/>
 * 사용할 유틸클래스(UtilTag.java)를 빈으로 설정(@Component만 붙여주면 됨) 하고, 아래와 같이 사용하면 된다.<br/>
 * (Ex) ＜th:block th:text="${@utilTag.masking(result.mngrNm, 1, 1)}"/>
 */
@Component
public class UtilTag {
	
	/**
	 * 마스킹 처리
	 *
	 * @param value
	 * @param beginIndex
	 * @param length
	 * @return String
	*/
	public static String masking(String value, int beginIndex, int length){
		return StringUtils.masking(value, beginIndex, length);
	}
}