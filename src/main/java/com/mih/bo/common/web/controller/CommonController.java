package com.mih.bo.common.web.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.Constants;
import com.mih.bo.common.config.Property;
import com.mih.bo.common.enums.FileAttachTypes;
import com.mih.bo.common.service.CommonService;
import com.mih.bo.common.util.DateUtils;
import com.mih.bo.common.util.StringUtils;
import com.mih.bo.common.util.UUIDUtils;
import com.mih.bo.common.vo.FileUpload;

/**
 * 공통 컨트롤러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Controller
public class CommonController extends AbstractController {
	
	private static final Logger log = LoggerFactory.getLogger(AbstractController.class);
	
	@Autowired
	private CommonService commonService;
	
	/**
	 * 사진 멀티파일 업로드
	 *
	 * @param request
	 * @param response
	 * @return String
	*/
	@RequestMapping(value="/common/editor/upload/fileupload")
	public String multiFileUpload(HttpServletRequest request, HttpServletResponse response){
		
		String separator = System.getProperty("file.separator");
		
		// 스마트 에디터 이미지 파일 경로(Write)
		String writeImageUrl = Property.get("file.upload.path") + FileAttachTypes.EDITOR.path() + separator + DateUtils.toDateString("yyyy"+separator+"MM"+separator+"dd");
		
		// 스마트 에디터 파일 보여주는 경로(View)
		String viewImageUrl = Property.get("server.protocol.https") + "://"+ Property.get("server.bgbgb.domain") + Property.get("file.attach.web.path") + Property.get("file.attach.editor.path") + separator + DateUtils.toDateString("yyyy"+separator+"MM"+separator+"dd");
		log.info("===== smartEditer 멀티파일 업로드 =====");
		log.info("스마트 에디터 이미지 생성 파일경로(Write)======="+writeImageUrl);
		log.info("스마트 에디터 파일 보여주는 경로(View)======="+viewImageUrl);

		try {
			// 파일정보
			String fileInfo = "";
			
			// 파일명 - 원본파일명
			String orgFileName = request.getHeader("file-name");
			
			// 파일확장자(확장자를 소문자로 변경)
			String fileNameExt = orgFileName.substring(orgFileName.lastIndexOf(".") + 1).toLowerCase();
			
			String fileName = UUIDUtils.md5() + Constants.DOT + fileNameExt;

			// 파일 기본경로 - 상세경로
			String filePath = writeImageUrl;
			log.info("원본파일명(orgFileName)============"+orgFileName);
			log.info("파일확장자(fileNameExt)============"+fileNameExt);
			log.info("파일명============"+fileName);
			
			File file = new File(filePath);
			if(!file.exists()){
				file.mkdirs();
			}

			String rlFileNm = filePath + separator + fileName;	// 파일 생성 경로 + 파일명
			String viewFileNm = viewImageUrl + separator + fileName;	// 파일 보여주는 경로 + 파일명
			log.info("파일생성경로+파일명============"+rlFileNm);
			log.info("view파일경로+파일명============"+viewFileNm);
			
			// 서버에 파일쓰기
			InputStream is = request.getInputStream();
			OutputStream os = new FileOutputStream(rlFileNm);
			
			int numRead;
			byte b[] = new byte[Integer.parseInt(request.getHeader("file-size"))];
			while((numRead = is.read(b, 0, b.length)) != -1){
				os.write(b, 0, numRead);
			}
			
			if(is != null) {
				is.close();
			}
			os.flush();
			os.close();
			
			// 정보 출력
			fileInfo += "&bNewLine=true";
			
			// img 태그의 title 속성을 원본파일명으로 적용시켜주기 위함
			fileInfo += "&sFileName=" + fileName;
			fileInfo += "&sFileURL=" + viewFileNm;	// 잘못된 파일명 : rlFileNm;
			
			PrintWriter pw = response.getWriter();
			pw.print(fileInfo);
			pw.flush();
			pw.close();
			
		}catch(Exception e) {
			log.error(StringUtils.printStack(e));
		}
		return null;
		//return "redirect:/complaint/notice/view";
	}

	/**
	 * [B/O] 공통 이미지 팝업
	 *
	 * @param vo
	 * @return ModelAndView
	*/
	@PostMapping(value="/common/img/popup")
	public ModelAndView commonImgPopup(FileUpload vo){
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		// [B/O] 공통 이미지 팝업조회
		mav.addObject("commonImg", commonService.commonImg(vo));
		
		return mav;
	}
}