package com.mih.bo.common.api.sns;

import com.mih.bo.common.AbstractModel;
import lombok.Data;

/**
 * 회원정보 관련 상속클래스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021.08.25
 */
@Data
public class Auth extends AbstractModel{
	
	private static final long serialVersionUID = 1371114671052798964L;

	/** DB암호화키값 */
	private String authKey;
	
	/** WEB 에서 받아온 SNS 연동 코드 */
	private String code;
	
	/** SNS구분코드 */
	private String snsSeCd;
}