package com.mih.bo.common.util;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * 비즈메시지 생성
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public class BizMsgFactory {
	/**
	 * 알림톡 메시지 생성
	 *
	 * @param templateCode(bizp_000 형식) 메시지 템플릿 코드
	 * @param paramMap            메시지 정보
	 */
	public static String getMessageInstance(String templateCode, Map<String, Object> paramMap) throws Exception {
		String methodName = "get" + templateCode.toUpperCase().replace("BIZP_", "Bizp") + "Template";
		Class<?> cls = Class.forName("com.mih.bo.common.util.BizMsgFactory");
		Method method = cls.getDeclaredMethod(methodName, Map.class);
		return (String) method.invoke(null, paramMap);
	}

	/**
	 * 고정 부가정보
	 *
	 * @return
	 */
	private static String getSuffixMsg() {
		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("\r\n");
		sbMsg.append("■ 종합민원실").append("\r\n");
		sbMsg.append("유선문의 : 1566-8246").append("\r\n");
		sbMsg.append("운영시간 : 10:00~18:30(월~토)").append("\r\n");
		sbMsg.append("(점심시간 : 12:00~13:00)").append("\r\n");
		return sbMsg.toString();
	}

	/**
	 * 메시지 템플릿 샘플
	 *
	 * @param paramMap 메시지 정보
	 * @return 메시지
	 */
	private static String getBizp000Template(Map<String, Object> paramMap) {

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("알림톡 예제 시작\r\n");
		sbMsg.append("이름: ").append(StringUtils.defaultString(paramMap.get("name"))).append("\r\n");
		sbMsg.append("1234 ABCD abcd 가나다라\r\n");
		sbMsg.append("알림톡 예제 종료");
		return sbMsg.toString();
	}

	/**
	 * 회원가입 템플릿
	 *
	 * @param paramMap
	 * @return
	 */
	private static String getBizp2021092817170105367208007Template(Map<String, Object> paramMap) {

		// #{고객명}
		String mberNm = StringUtils.defaultString(paramMap.get("mberNm"));

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("[가입완료]").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append(mberNm).append(" 주민님!").append("\r\n");
		sbMsg.append("회원가입이 완료되었습니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("보관복지부 주민이 되신 것을 진심으로 환영합니다!").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("보관복지부 앱에서는 ").append(mberNm).append(" 주민님의 보관내역 및 진행상태를 바로 확인하실 수 있습니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("많은 이용 부탁드립니다.").append("\r\n");
		sbMsg.append("감사합니다.");
		return sbMsg.toString();
	}

	/**
	 * 인증번호 템플릿
	 *
	 * @param paramMap
	 * @return
	 */
	private static String getBizp2021091714125926143498192Template(Map<String, Object> paramMap) {

		// #{인증번호}
		String certificationNo = StringUtils.defaultString(paramMap.get("certificationNo"));

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("[휴대폰 인증]").append("\r\n");
		sbMsg.append("인증번호[").append(certificationNo).append("]를 입력해주세요.");
		return sbMsg.toString();
	}

	/**
	 * 지부투어예약 템플릿
	 *
	 * @param paramMap
	 * @return
	 */
	private static String getBizp2021110815390426514574276Template(Map<String, Object> paramMap) {

		// #{고객명}
		String mberNm = StringUtils.defaultString(paramMap.get("mberNm"));
		// #{방문일}
		String tourHopeDe = StringUtils.defaultString(paramMap.get("tourHopeDe"));
		// #{지부명}
		String bhfNm = StringUtils.defaultString(paramMap.get("bhfNm"));
		// #{URL}
		String linkUrl = StringUtils.defaultString(paramMap.get("linkUrl"));

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("[방문 예약 안내]").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append(mberNm).append(" 주민님의 방문 예약이 완료 되었습니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 예약 정보").append("\r\n");
		sbMsg.append("성함 : ").append(mberNm).append("\r\n");
		sbMsg.append("방문일 : ").append(tourHopeDe).append("\r\n");
		sbMsg.append("방문지부 : ").append(bhfNm).append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 알림").append("\r\n");
		sbMsg.append("보관복지부의 모든 지부는 무인으로 운영 되고있습니다. 지부에 도착하시면 출입을 위해 보관복지부 종합민원실로 연락 부탁드립니다. (예약시간 변경 및 취소를 원하실 경우 1시간 전까지 연락 부탁드립니다.)").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 오시는 길").append("\r\n");
		sbMsg.append(bhfNm).append(" 오시는 길 안내입니다. 아래 URL을 눌러서 확인해주세요.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append(linkUrl);
		return sbMsg.toString();
	}

	/**
	 * 주문완료 템플릿
	 *
	 * @param paramMap
	 * @return
	 */
	private static String getBizp2021091714502726143559222Template(Map<String, Object> paramMap) {

		// #{고객명}
		String mberNm = StringUtils.defaultString(paramMap.get("mberNm"));
		// #{주문일}
		String ordDt = StringUtils.defaultString(paramMap.get("ordDt"));
		// #{시작일}
		String useBgnde = StringUtils.defaultString(paramMap.get("useBgnde"));
		// #{지부명}
		String bhfNm = StringUtils.defaultString(paramMap.get("bhfNm"));
		// #{상품명}
		String goodsNm = StringUtils.defaultString(paramMap.get("goodsNm"));
		// #{청구금액1} - 정기결제 예정 금액
		String fixdPayAmt = StringUtils.defaultString(paramMap.get("fixdPayAmt"));
		// #{청구금액2} - 예치금
		String deposit = StringUtils.defaultString(paramMap.get("deposit"));

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("[주문 완료 안내]").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append(mberNm).append(" 주민님의 주문이 완료 되었습니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 주문 정보").append("\r\n");
		sbMsg.append("성함 : ").append(mberNm).append("\r\n");
		sbMsg.append("주문일 : ").append(ordDt).append("\r\n");
		sbMsg.append("이용 시작일 : ").append(useBgnde).append("\r\n");
		sbMsg.append("이용지부 : ").append(bhfNm).append("\r\n");
		sbMsg.append("이용 사이즈 : ").append(goodsNm).append("\r\n");
		sbMsg.append("월 정기결제 예정 금액 : ").append(fixdPayAmt).append("원\r\n");
		sbMsg.append("예치금 : ").append(deposit).append("원\r\n");
		sbMsg.append("(1개월분의 예치금 별도 결제 예정)").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 알림").append("\r\n");
		sbMsg.append("보관복지부의 모든 지부는 무인으로 운영 되고있습니다. 출입 및 사용자 등록을 위해 오후 6시까지 ").append(bhfNm).append("에 도착하시어 종합민원실로 연락 바랍니다.(실결제는 이용 시작일에 진행 될 예정입니다.)");
		return sbMsg.toString();
	}

	/**
	 * 이용 당일 서비스 안내 템플릿
	 *
	 * @param paramMap
	 * @return
	 */
	private static String getBizp2021113015590805367196461Template(Map<String, Object> paramMap) {

		// #{고객명}
		String mberNm = StringUtils.defaultString(paramMap.get("mberNm"));
		// #{주문일}
		String ordDt = StringUtils.defaultString(paramMap.get("ordDt"));
		// #{시작일}
		String useBgnde = StringUtils.defaultString(paramMap.get("useBgnde"));
		// #{지부명}
		String bhfNm = StringUtils.defaultString(paramMap.get("bhfNm"));
		// #{상품명}
		String goodsNm = StringUtils.defaultString(paramMap.get("goodsNm"));
		// #{상품관리번호}
		String goodsLbl = StringUtils.defaultString(paramMap.get("goodsLbl"));
		// #{청구금액1} - 정기결제 예정 금액
		String fixdPayAmt = StringUtils.defaultString(paramMap.get("fixdPayAmt"));
		// #{청구금액2} - 예치금
		String deposit = StringUtils.defaultString(paramMap.get("deposit"));
		// #{URL}
		String linkUrl = StringUtils.defaultString(paramMap.get("linkUrl"));

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("[이용 안내]").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append(mberNm).append(" 주민님의 이용 시작일입니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 이용 정보").append("\r\n");
		sbMsg.append("성함 : ").append(mberNm).append("\r\n");
		sbMsg.append("주문일 : ").append(ordDt).append("\r\n");
		sbMsg.append("이용 시작일 : ").append(useBgnde).append("\r\n");
		sbMsg.append("이용지부 : ").append(bhfNm).append("\r\n");
		sbMsg.append("사이즈 및 캐비닛 번호 : ").append(goodsNm).append(" / ").append(goodsLbl).append("\r\n");
		sbMsg.append("익월 정기결제 예정 금액 : ").append(fixdPayAmt).append("원\r\n");
		sbMsg.append("예치금 : ").append(deposit).append("원\r\n");
		sbMsg.append("(1개월분의 예치금 별도 결제 예정)").append("\r\n");
		sbMsg.append("캐비닛 초기비밀번호 : 1111*").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 알림").append("\r\n");
		sbMsg.append("보관복지부의 모든 지부는 무인으로 운영 되고있습니다. 출입 및 사용자 등록을 위해 오후 6시까지 ").append(bhfNm).append("에 도착하시어 종합민원실로 연락 바랍니다. (실결제는 오늘 진행될 예정입니다.)").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 오시는 길").append("\r\n");
		sbMsg.append(bhfNm).append(" 오시는 길 안내입니다. 아래 URL을 눌러서 확인해주세요.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append(linkUrl);

		return sbMsg.toString();
	}

	/**
	 * 정기 결제 및 계약 만료 안내 템플릿
	 *
	 * @param paramMap
	 * @return
	 */
	private static String getBizp2021110811451005367058175Template(Map<String, Object> paramMap) {

		// #{고객명}
		String mberNm = StringUtils.defaultString(paramMap.get("mberNm"));
		// #{시작일}
		String useBgnde = StringUtils.defaultString(paramMap.get("useBgnde"));
		// #{종료일}
		String useEndde = StringUtils.defaultString(paramMap.get("useEndde"));
		// #{지부명}
		String bhfNm = StringUtils.defaultString(paramMap.get("bhfNm"));
		// #{상품명}
		String goodsNm = StringUtils.defaultString(paramMap.get("goodsNm"));
		// #{상품관리번호}
		String goodsLbl = StringUtils.defaultString(paramMap.get("goodsLbl"));
		// #{결제일}
		String fixdPayPrarnde = StringUtils.defaultString(paramMap.get("fixdPayPrarnde"));
		// #{청구금액1} - 정기결제 예정 금액
		String fixdPayAmt = StringUtils.defaultString(paramMap.get("fixdPayAmt"));

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("[정기 결제 안내]").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append(mberNm).append("주민님의 정기 결제 안내입니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 이용 정보").append("\r\n");
		sbMsg.append("성함 : ").append(mberNm).append("\r\n");
		sbMsg.append("이용 시작일 : ").append(useBgnde).append("\r\n");
		sbMsg.append("이용지부 : ").append(bhfNm).append("\r\n");
		sbMsg.append("사이즈 및 캐비닛 번호 : ").append(goodsNm).append(" / ").append(goodsLbl).append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("다음 정기 결제일 : ").append(fixdPayPrarnde).append("\r\n");
		sbMsg.append("월 정기결제 예정 금액 : ").append(fixdPayAmt).append("원\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 알림").append("\r\n");
		sbMsg.append("가장 가까운 결제 예정일 전일 오후 5시까지 서비스 해지 절차 완료 시 계약 해지가 가능하며, 해당 절차가 완료되지 않으면 본 서비스는 월(30일)단위로 자동 갱신되고 이용요금은 전월요금으로 청구됩니다.");
		return sbMsg.toString();
	}

	/**
	 * 휴면 계정 전환 안내 템플릿
	 *
	 * @param paramMap
	 * @return
	 */
	private static String getBizp2021091714552610216387208Template(Map<String, Object> paramMap) {

		// #{날짜}
		String drmncyCnvrsDe = StringUtils.defaultString(paramMap.get("drmncyCnvrsDe"));

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("[휴면 계정 전환 안내]").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("개인정보 보호에 대한 관계법령에 따라 고객 여러분의 개인정보 보호를 위하여 보관복지부를 1년 이상 이용하지 않으신 고객님의 개인정보를 별도 분리 보관하고 있습니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("▶ 휴면 전환 예정일: ").append(drmncyCnvrsDe).append("\r\n");
		sbMsg.append("▶ 분리보관항목: 회원 가입 시 입력한 모든 정보").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("휴면 계정으로 전환 되는 것을 원치 않으실 경우 휴면 전환일 전까지 보관복지부 로그인을 해주시기 바라며, 이후 동일하게 서비스를 이용하실 수 있습니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("만약, ").append(drmncyCnvrsDe).append("까지 별도 이용이 없으실 경우 휴면 처리 되며 휴면 전환 이후 1년 동안 보관복지부 미 이용 시 자동 탈퇴 처리 및 개인 정보는 파기됩니다.");
		return sbMsg.toString();
	}

	/**
	 * 1:1 문의 답변 템플릿
	 *
	 * @param paramMap
	 * @return
	 */
	private static String getBizp2021091714583826143377226Template(Map<String, Object> paramMap) {

		// #{고객명}
		String mberNm = StringUtils.defaultString(paramMap.get("mberNm"));

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("[문의답변 완료]").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("안녕하세요. ").append(mberNm).append(" 주민님!").append("\r\n");
		sbMsg.append("문의하신 글의 답변이 등록되었습니다.");
		return sbMsg.toString();
	}

	/**
	 * 서비스 해지 신청 안내 템플릿
	 *
	 * @param paramMap
	 * @return
	 */
	private static String getBizp2021091715003210216678209Template(Map<String, Object> paramMap) {

		// #{고객명}
		String mberNm = StringUtils.defaultString(paramMap.get("mberNm"));
		// #{시작일}
		String useBgnde = StringUtils.defaultString(paramMap.get("useBgnde"));
		// #{종료일}
		String useEndde = StringUtils.defaultString(paramMap.get("useEndde"));
		// #{지부명}
		String bhfNm = StringUtils.defaultString(paramMap.get("bhfNm"));
		// #{상품명}
		String goodsNm = StringUtils.defaultString(paramMap.get("goodsNm"));
		// #{상품관리번호}
		String goodsLbl = StringUtils.defaultString(paramMap.get("goodsLbl"));
		// #{해지일} - 종료희망일
		String endHopeDe = StringUtils.defaultString(paramMap.get("endHopeDe"));

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("[서비스 해지 신청 안내]").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append(mberNm).append(" 주민님의 서비스 해지 신청이 접수 되었습니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 이용 정보").append("\r\n");
		sbMsg.append("성함 : ").append(mberNm).append("\r\n");
		sbMsg.append("이용 시작일 : ").append(useBgnde).append("\r\n");
		sbMsg.append("계약 만료일 : ").append(useEndde).append("\r\n");
		sbMsg.append("이용지부 : ").append(bhfNm).append("\r\n");
		sbMsg.append("사이즈 및 캐비닛 번호 : ").append(goodsNm).append(" / ").append(goodsLbl).append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 서비스 해지").append("\r\n");
		sbMsg.append("서비스 해지일 : ").append(endHopeDe).append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 알림").append("\r\n");
		sbMsg.append("서비스 해지일 오후 5시까지 보관물품을 비운 캐비닛의 내부 사진을 '나의 보관'→'이용해지 신청'에 등록해주셔야 서비스 해지 절차가 완료됩니다. (운송서비스 신청시 해당 절차는 생략됩니다.)");
		return sbMsg.toString();
	}

	/**
	 * 서비스 해지 절차 완료 안내 템플릿
	 *
	 * @param paramMap
	 * @return
	 */
	private static String getBizp2021091715015710216752210Template(Map<String, Object> paramMap) {

		// #{고객명}
		String mberNm = StringUtils.defaultString(paramMap.get("mberNm"));
		// #{시작일}
		String useBgnde = StringUtils.defaultString(paramMap.get("useBgnde"));
		// #{종료일}
		String useEndde = StringUtils.defaultString(paramMap.get("useEndde"));
		// #{지부명}
		String bhfNm = StringUtils.defaultString(paramMap.get("bhfNm"));
		// #{상품명}
		String goodsNm = StringUtils.defaultString(paramMap.get("goodsNm"));
		// #{상품관리번호}
		String goodsLbl = StringUtils.defaultString(paramMap.get("goodsLbl"));
		// #{해지일} - 종료완료일
		String endComptDt = StringUtils.defaultString(paramMap.get("endComptDt"));

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("[서비스 해지 절차 완료 안내]").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append(mberNm).append(" 주민님의 서비스 해지 절차가 완료 되었습니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("요청하신 서비스 해지일에 해당 상품의 서비스가 해지 됨을 알려드립니다.").append("\r\n");
		sbMsg.append("보관복지부를 이용해 주셔서 진심으로 감사드립니다. 더 나은 보관복지부로 찾아뵙겠습니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 이용 정보").append("\r\n");
		sbMsg.append("성함 : ").append(mberNm).append("\r\n");
		sbMsg.append("이용 시작일 : ").append(useBgnde).append("\r\n");
		sbMsg.append("계약 만료일 : ").append(useEndde).append("\r\n");
		sbMsg.append("이용지부 : ").append(bhfNm).append("\r\n");
		sbMsg.append("사이즈 및 캐비닛 번호 : ").append(goodsNm).append(" / ").append(goodsLbl).append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 서비스 해지").append("\r\n");
		sbMsg.append("서비스 해지일 : ").append(endComptDt).append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 알림").append("\r\n");
		sbMsg.append("잔여금 및 예치금의 환불은 카드사 사정에 따라 영업일 기준 2-3일 정도 소요될 수 있는 점 양해부탁드립니다.");
		return sbMsg.toString();
	}

	/**
	 * 미결제 안내 템플릿
	 *
	 * @param paramMap
	 * @return
	 */
	private static String getBizp2021091715031510216962211Template(Map<String, Object> paramMap) {

		// #{고객명}
		String mberNm = StringUtils.defaultString(paramMap.get("mberNm"));
		// #{미납금액}
		String npyAmt = StringUtils.defaultString(paramMap.get("npyAmt"));
		// #{미납일수}
		String npyDayCnt = StringUtils.defaultString(paramMap.get("npyDayCnt"));

		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append("[미결제 안내]").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append(mberNm).append(" 주민님의 정기 결제가 실패하였습니다.").append("\r\n");
		sbMsg.append("등록된 카드를 확인하여 주시기 바랍니다.").append("\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 미결제 내역").append("\r\n");
		sbMsg.append("총 미납 금액 : ").append(npyAmt).append("원\r\n");
		sbMsg.append("미납 일수 : ").append(npyDayCnt).append("일\r\n");
		sbMsg.append("\r\n");
		sbMsg.append("■ 알림").append("\r\n");
		sbMsg.append("카드 한도 초과, 잔액부족, 유효기간 만료 등 회원의 귀책으로 월 이용요금이 해당 결제일에 결제되지 않아 미납요금이 예치금을 초과할 경우 연체료가 발생됩니다.").append("\r\n");
		sbMsg.append("(미납요금은 예치금으로 선처리되며 전액 납부시 연체료 미발생)");
		return sbMsg.toString();
	}
}
