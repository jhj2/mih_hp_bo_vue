package com.mih.bo.common.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.FileLock;
import java.util.Arrays;
import java.util.List;

import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.mih.bo.common.config.Property;

/**
 * Java 디렉토리 생성, 파일 복사 / 이동 / 삭제 등을 처리하는 유틸리티
 * Spring의 FileCopyUtils을 상속 받아서 파일 복사도 같이 사용 가능.
 *
 * @author
 * @version 1.0.0
 * @see FileCopyUtils
 * @since 7.0
 */
@SuppressWarnings("all")
public class FileUtils extends FileCopyUtils {

	private static final Logger log = LoggerFactory.getLogger(FileUtils.class);

	/**
	 * FIN File Extension
	 */
	public static final String EXT_PPT = "ppt";

	/**
	 * FIN File Extension
	 */
	public static final String EXT_PPTX = "pptx";

	/**
	 * FIN File Extension
	 */
	public static final String EXT_XLS = "xls";

	/**
	 * FIN File Extension
	 */
	public static final String EXT_XLSX = "xlsx";

	/**
	 * FIN File Extension
	 */
	public static final String EXT_FIN = "fin";

	/**
	 * DAT File Extension
	 */
	public static final String EXT_DAT = "dat";

	/**
	 * CSV File Extension
	 */
	public static final String EXT_CSV = "cvs";

	/**
	 * 파일 경로가 없습니다.
	 */
	private static final String NOT_FILE_PATH = "파일 경로가 없습니다.";

	/**
	 * 파일 경로가 없습니다.
	 */
	private static final String NOT_FILE_NAME = "파일명이 없습니다.";

	/**
	 * Excel File Allow Mime Type
	 */
	public static final String[] EXCEL_MIMETYPE = Property.get("file.upload.allow.excel.mimetype").split(",");
	
	/**
	 * IMAGE File Allow Mime Type
	 */
	public static final String[] IMG_MIMETYPE = Property.get("file.upload.allow.image.mimetype").split(",");
	
	/**
	 * String을 File Object로 변환.
	 *
	 * @param path 파일 경로.
	 * @return File
	 */
	public static File getFile(final String path) {
		AssertUtils.notBlank(path, NOT_FILE_PATH);
		return new File(path);
	}

	/**
	 * String을 File Object로 변환.
	 *
	 * @param url java.net.URL
	 * @return File
	 * @throws URISyntaxException
	 */
	public static File getFile(final URL url) {
		AssertUtils.notNull(url, NOT_FILE_PATH);
		try {
			return new File(url.toURI());
		} catch (URISyntaxException e) {
			throw new IllegalArgumentException(NOT_FILE_PATH);
		}
	}

	/**
	 * String을 File Object로 변환.
	 *
	 * @param path 파일 경로.
	 * @param name 파일명.
	 * @return File
	 */
	public static File getFile(final String path, final String name) {
		AssertUtils.notBlank(path, NOT_FILE_PATH);
		AssertUtils.notBlank(name, NOT_FILE_NAME);

		return new File(path, name);
	}

	/**
	 * String을 File Object로 변환.
	 *
	 * @param path 파일 경로.
	 * @param name 파일명.
	 * @return File
	 */
	public static File getFile(final File path, final String name) {
		AssertUtils.notNull(path, NOT_FILE_PATH);
		AssertUtils.notBlank(name, NOT_FILE_NAME);

		return new File(path, name);
	}

	/**
	 * 현재 날짜를 이용한 디렉토리 조회.
	 *
	 * @param basePath 기본 Path.
	 * @return 디렉토리 경로.
	 */
	public static File getCachePath(final String basePath) {
		return getCachePath(getFile(basePath));
	}

	/**
	 * Cache 디렉토리 생성 및 리턴
	 *
	 * @param basePath 기본 Path.
	 * @return 디렉토리 경로.
	 */
	public static File getCachePath(final File basePath) {
		String md5 = UUIDUtils.md5();

		// MD5에서 뒤에서 2자리씩 잘라온다.
		int poi = md5.length();
		String subPath = "";
		for (int i = 0; i < 3; i++) {
			subPath += StringUtils.substring(md5, (poi - 2), poi) + File.separator;
			poi = poi - 2;
		}

		File path = getFile(basePath, subPath);
		if (!exists(path)) { // 디렉토리가 있는지 확인한다.
			mkdirs(path);
		}
		return path;
	}

	/**
	 * Creates the directory named by this abstract pathname, including any
	 * necessary but nonexistent parent directories. Note that if this
	 * operation fails it may have succeeded in creating some of the necessary
	 * parent directories.
	 *
	 * @param path 디렉토리 경로.
	 * @return 생성 여부.
	 */
	public static boolean mkdirs(final String path) {
		return mkdirs(getFile(path));
	}

	/**
	 * Creates the directory named by this abstract pathname, including any
	 * necessary but nonexistent parent directories. Note that if this
	 * operation fails it may have succeeded in creating some of the necessary
	 * parent directories.
	 *
	 * @param path 디렉토리 경로.
	 * @return 생성 여부.
	 */
	public static boolean mkdirs(final File path) {
		AssertUtils.notNull(path, NOT_FILE_PATH);
		return path.mkdirs();
	}

	/**
	 * Renames the file denoted by this abstract pathname.
	 *
	 * @param src 원본 파일.
	 * @param dest 변경 파일.
	 * @return Rename 성공 여부.
	 */
	public static boolean rename(final String src, final String dest) {
		return rename(getFile(src), getFile(dest));
	}

	/**
	 * 파일의 디렉토리를 이동한다.
	 *
	 * @param source 원본 파일
	 * @param descPath 변경될 파일 경로
	 * @param descName 변경될 파일 이름
	 * @return Rename 성공 여부.
	 */
	public static boolean rename(final String source, final String descPath, final String descName) {
		mkdirs(descPath);
		return FileUtils.rename(new File(source), new File(descPath, descName));
	}

	/**
	 * 파일의 디렉토리를 이동한다.
	 *
	 * @param sourcePath 원본 파일 경로
	 * @param sourceName 원본 파일 이름
	 * @param descPath 변경될 파일 경로
	 * @param descName 변경될 파일 이름
	 * @return Rename 성공 여부.
	 */
	public static boolean rename(final String sourcePath, final String sourceName, final String descPath,
			final String descName) {
		mkdirs(descPath);
		return FileUtils.rename(new File(sourcePath, sourceName), new File(descPath, descName));
	}

	/**
	 * 파일의 디렉토리를 이동한다.
	 *
	 * @param source 원본 파일
	 * @param descPath 변경될 파일 경로
	 * @param descName 변경될 파일 이름
	 * @return Rename 성공 여부.
	 */
	public static boolean rename(final File source, final String descPath, final String descName) {
		mkdirs(descPath);
		return FileUtils.rename(source, new File(descPath, descName));
	}

	/**
	 * Renames the file denoted by this abstract pathname.
	 *
	 * @param src 원본 파일.
	 * @param dest 변경 파일.
	 * @return Rename 성공 여부.
	 */
	public static boolean rename(final File src, final File dest) {
		AssertUtils.notNull(src, "원본파일 - " + NOT_FILE_PATH);
		AssertUtils.notNull(dest, "대상파일 - " + NOT_FILE_PATH);
		return src.renameTo(dest);
	}

	/**
	 * 파일을 이동한다.
	 *
	 * @param src 원본 파일.
	 * @param dest 변경 파일.
	 * @return 파일 이동 성공 여부.
	 */
	public static boolean move(final String src, final String dest) {
		return move(getFile(src), getFile(dest));
	}

	/**
	 * 파일을 이동한다.
	 *
	 * @param src 원본 파일.
	 * @param dest 변경 파일.
	 * @return 파일 이동 성공 여부.
	 */
	public static boolean move(final File src, final File dest) {
		// desc 파일이 존재하면 삭제한다.
		if (dest.exists()) {
			// 파일 locak을 먼저 생성한다.
			if (getLock(dest) == null) {
				return false;
			}
			if (!dest.delete()) {
				log.warn("file delete failed; take appropriate action");
			}
		}

		// 디렉토리가 아니면 생성한다.
		if (!dest.getParentFile().exists()) {
			mkdirs(dest.getParentFile());
		}
		return rename(src, dest);
	}

	/**
	 * 파일을 삭제한다.
	 *
	 * @param src 파일 경로
	 * @return 삭제 성공 여부.
	 * @throws IOException
	 */
	public static boolean delete(final String src) throws IOException {
		return delete(getFile(src));
	}

	/**
	 * 파일을 삭제한다.
	 *
	 * @param src 파일 경로
	 * @return 삭제 성공 여부.
	 * @throws IOException
	 */
	public static boolean delete(final File src) throws IOException {
		return src.delete();
	}

	/**
	 * 디렉토리 / 파일을 삭제한다.
	 *
	 * @param src 디렉토리 / 파일 경로
	 * @return 삭제 성공 여부.
	 * @throws IOException
	 */
	public static boolean deleteAll(final String src) throws IOException {
		return deleteAll(getFile(src));
	}

	/**
	 * 디렉토리 / 파일을 삭제한다.
	 *
	 * @param src 디렉토리 / 파일 경로
	 * @return 삭제 성공 여부.
	 * @throws IOException
	 */
	public static boolean deleteAll(final File src) throws IOException {
		File[] listFiles = src.listFiles();
		if (listFiles != null && listFiles.length > 0) {
			for (File path : listFiles) {
				if (path.isFile()) {
					delete(path);
				} else {
					deleteAll(path.getPath());
				}
				delete(path);
			}
		}
		delete(src);
		return false;
	}

	/**
	 * Tests whether the file or directory denoted by this abstract pathname exists.
	 *
	 * @param path 디렉토리 / 파일 경로.
	 * @return 디렉토리 / 파일이 있으면 "true", 없으면 "false"를 리턴한다.
	 */
	public static boolean exists(final String path) {
		return exists(getFile(path));
	}

	/**
	 * Tests whether the file or directory denoted by this abstract pathname exists.
	 *
	 * @param path 디렉토리 / 파일 경로.
	 * @return 디렉토리 / 파일이 있으면 "true", 없으면 "false"를 리턴한다.
	 */
	public static boolean exists(final File path) {
		AssertUtils.notNull(path, NOT_FILE_PATH);
		return path.exists();
	}

	/**
	 * Tests whether the file denoted by this abstract pathname is a directory.
	 *
	 * @param path 디렉토리 / 파일 경로.
	 * @return 디렉토리 / 파일이 있으면 "true", 없으면 "false"를 리턴한다.
	 */
	public static boolean isDirectory(final String path) {
		return isDirectory(getFile(path));
	}

	/**
	 * Tests whether the file denoted by this abstract pathname is a directory.
	 *
	 * @param path 디렉토리 / 파일 경로.
	 * @return 디렉토리 / 파일이 있으면 "true", 없으면 "false"를 리턴한다.
	 */
	public static boolean isDirectory(final File path) {
		AssertUtils.notNull(path, NOT_FILE_PATH);
		return path.isDirectory();
	}

	/**
	 * Tests whether the file denoted by this abstract pathname is a normal
	 * file. A file is <em>normal</em> if it is not a directory and, in
	 * addition, satisfies other system-dependent criteria. Any non-directory
	 * file created by a Java application is guaranteed to be a normal file.
	 *
	 * @param path 디렉토리 / 파일 경로.
	 * @return 디렉토리 / 파일이 있으면 "true", 없으면 "false"를 리턴한다.
	 */
	public static boolean isFile(final String path) {
		return isFile(getFile(path));
	}

	/**
	 * Tests whether the file denoted by this abstract pathname is a normal
	 * file. A file is <em>normal</em> if it is not a directory and, in
	 * addition, satisfies other system-dependent criteria. Any non-directory
	 * file created by a Java application is guaranteed to be a normal file.
	 *
	 * @param path 디렉토리 / 파일 경로.
	 * @return 디렉토리 / 파일이 있으면 "true", 없으면 "false"를 리턴한다.
	 */
	public static boolean isFile(final File path) {
		AssertUtils.notNull(path, NOT_FILE_PATH);
		return path.isFile();
	}

	/**
	 * Converts this abstract pathname into a pathname string. The resulting
	 * string uses the to separate the names in the name sequence.
	 *
	 * @param path 디렉토리 / 파일 경로.
	 * @return 디렉토리 / 파일의 경로를 리턴한다.
	 */
	public static String getPath(final String path) {
		return getPath(getFile(path));
	}

	/**
	 * Converts this abstract pathname into a pathname string. The resulting
	 * string uses the to separate the names in the name sequence.
	 *
	 * @param path 디렉토리 경로.
	 * @param name 파일명.
	 * @return 디렉토리 / 파일의 경로를 리턴한다.
	 */
	public static String getPath(final String path, final String name) {
		return getPath(getFile(path, name));
	}

	/**
	 * Converts this abstract pathname into a pathname string. The resulting
	 * string uses the to separate the names in the name sequence.
	 *
	 * @param path 디렉토리 경로.
	 * @param name 파일명.
	 * @return 디렉토리 / 파일의 경로를 리턴한다.
	 */
	public static String getPath(final File path, final String name) {
		return getPath(getFile(path, name));
	}

	/**
	 * Converts this abstract pathname into a pathname string. The resulting
	 * string uses the to separate the names in the name sequence.
	 *
	 * @param path 디렉토리 / 파일 경로.
	 * @return 디렉토리 / 파일의 경로를 리턴한다.
	 */
	public static String getPath(final File path) {
		AssertUtils.notNull(path, NOT_FILE_PATH);
		return path.getPath();
	}

	/**
	 * 파일을 신규로 생성한다.
	 *
	 * @param path 파일 경로.
	 * @return 파일 생성 여부.
	 * @throws IOException
	 */
	public static boolean createNewFile(final String path) throws IOException {
		return createNewFile(getFile(path), false, false);
	}

	/**
	 * 파일을 신규로 생성한다.
	 *
	 * @param path 파일 경로.
	 * @param writable 일기 권한을 모두에게 줄 것인지 설정.
	 * @return 파일 생성 여부.
	 * @throws IOException
	 */
	public static boolean createNewFile(final String path, final boolean writable) throws IOException {
		return createNewFile(getFile(path), writable, false);
	}

	/**
	 * 파일을 신규로 생성한다.
	 *
	 * @param path 파일 경로.
	 * @param readable 일기 권한을 모두에게 줄 것인지 설정.
	 * @param writable 쓰기 권한을 모두에게 줄 것인지 설정.
	 * @return 파일 생성 여부.
	 * @throws IOException
	 */
	public static boolean createNewFile(final String path, final boolean readable, final boolean writable)
			throws IOException {
		return createNewFile(getFile(path), readable, writable);
	}

	/**
	 * 파일을 신규로 생성한다.
	 *
	 * @param path 파일 경로.
	 * @return 파일 생성 여부.
	 * @throws IOException
	 */
	public static boolean createNewFile(final File path) throws IOException {
		return createNewFile(path, false, false);
	}

	/**
	 * 파일을 신규로 생성한다.
	 *
	 * @param path 파일 경로.
	 * @param writable 일기 권한을 모두에게 줄 것인지 설정.
	 * @return 파일 생성 여부.
	 * @throws IOException
	 */
	public static boolean createNewFile(final File path, final boolean writable) throws IOException {
		return createNewFile(path, writable, false);
	}

	/**
	 * 파일을 신규로 생성한다.
	 *
	 * @param path 파일 경로.
	 * @param readable 읽기 권한을 모두에게 줄 것인지 설정.
	 * @param writable 쓰기 권한을 모두에게 줄 것인지 설정.
	 * @return 파일 생성 여부.
	 * @throws IOException
	 */
	public static boolean createNewFile(final File path, final boolean readable, final boolean writable)
			throws IOException {
		FileLock lock = getLock(path);
		if (lock == null) {
			return false;
		}
		if (path.setReadable(readable)) {
			log.info("\"{}\" 파일에 읽기 권한 - {}", path, readable);
		}
		if (path.setWritable(writable)) {
			log.info("\"{}\" 파일에 쓰기 권한 - {}", path, readable);
		}
		// Lock을 풀어준다.
		releaseLock(lock);
		return path.createNewFile();
	}

	/**
	 * 텍스트를 파일에 저장
	 *
	 * @param file 텍스트를 저장할 파일.
	 * @param text 파일에 저장할 텍스트.
	 * @throws IOException
	 */
	public static void write(final File file, final String text) throws IOException {
		write(file, text, false, false, false);
	}

	/**
	 * 텍스트를 파일에 저장
	 *
	 * @param file 텍스트를 저장할 파일.
	 * @param text 파일에 저장할 텍스트.
	 * @param isAppend if <code>true</code>, then bytes will be written to the end of the file rather than the beginning
	 * @throws IOException
	 */
	public static void write(final File file, final String text, final boolean isAppend) throws IOException {
		write(file, text, isAppend, false, false);
	}

	/**
	 * 텍스트를 파일에 저장
	 *
	 * @param file 텍스트를 저장할 파일.
	 * @param text 파일에 저장할 텍스트.
	 * @param isAppend if <code>true</code>, then bytes will be written to the end of the file rather than the beginning
	 * @param isReadable 일기 권한을 모두에게 줄 것인지 설정.
	 * @throws IOException
	 */
	public static void write(final File file, final String text, final boolean isAppend, boolean isReadable)
			throws IOException {
		write(file, text, isAppend, isReadable, false);
	}

	/**
	 * 텍스트를 파일에 저장
	 *
	 * @param file 텍스트를 저장할 파일.
	 * @param text 파일에 저장할 텍스트.
	 * @param append if <code>true</code>, then bytes will be written to the end of the file rather than the beginning
	 * @param readable 일기 권한을 모두에게 줄 것인지 설정.
	 * @param writable 쓰기 권한을 모두에게 줄 것인지 설정.
	 * @throws IOException
	 */
	public static void write(final File file, final String text, final boolean append, boolean readable,
			boolean writable) throws IOException {
		FileLock lock = getLock(file);
		if (lock == null) {
			throw new IOException("file lock setting fail..");
		}
		if (readable && file.setReadable(readable, false)) {
			log.info("\"{}\" 파일에 읽기 권한 - {}", file, readable);
		}
		if (writable && file.setWritable(writable, false)) {
			log.info("\"{}\" 파일에 쓰기 권한 - {}", file, readable);
		}
		try (FileWriter fw = new FileWriter(file, append)) {
			fw.write(text);
		}
		// Lock을 풀어준다.
		releaseLock(lock);
	}

	/**
	 * 파일의 Lock를 설정한다. null을 리턴하는 경우 파일이 없거나 다른 파일에서 Lock를 설정한 것으로 인지한다.
	 *
	 * @param file File
	 * @return FileLock
	 */
	public static FileLock getLock(final File file) {
		try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw")) {
			return randomAccessFile.getChannel().tryLock();
		} catch (FileNotFoundException e) {
			log.error("File Not Found Exception", e);
		} catch (IOException e) {
			log.error("IO Exception", e);
		}
		return null;
	}

	/**
	 * 파일의 Lock를 Release한다.
	 *
	 * @param locks FileLock List
	 */
	public static void releaseLocks(final List<FileLock> locks) {
		for (FileLock lock : locks) {
			releaseLock(lock);
		}
	}

	/**
	 * 파일의 Lock를 Release한다.
	 *
	 * @param lock FileLock
	 */
	public static void releaseLock(final FileLock lock) {
		if (lock != null && lock.isValid()) {
			try {
				lock.release();
				log.info("Released lock on {}", lock.toString());
			} catch (IOException e) {
				log.error("Cant release lock on " + lock.toString(), e);
			}
		}
	}
	
	/**
	 * 파일의 Mime Type을 체크한다.
	 *
	 * @param file MultipartFile
	 */
	public static boolean isAllowExcelMimeType(MultipartFile file) {
		try {
			return isAllowMimeType(file.getInputStream(), EXCEL_MIMETYPE);
		} catch (IOException e) {
			return false;
		}
	}
	
	/**
	 * 파일의 Mime Type을 체크한다.
	 *
	 * @param file MultipartFile
	 */
	public static boolean isAllowImageMimeType(MultipartFile file) {
		try {
			return isAllowMimeType(file.getInputStream(), IMG_MIMETYPE);
		} catch (IOException e) {
			return false;
		}
	}
	
	/**
	 * 파일의 Mime Type을 체크한다.
	 *
	 * @param allowMimeType String[]
	 */
	public static boolean isAllowMimeType(InputStream file, String[] allowMimeType) {
		try {
			String mimeType = new Tika().detect(file);
			if(!Arrays.asList(allowMimeType).contains(mimeType)) {
				return false;
			}
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
}
