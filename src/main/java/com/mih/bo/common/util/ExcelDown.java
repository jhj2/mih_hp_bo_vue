package com.mih.bo.common.util;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

/**
 * [공통모듈] 엑셀 다운로드
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.03.14.
 */
@Component("excelDown")
public class ExcelDown extends AbstractXlsView{
	
	private static final Logger log = LoggerFactory.getLogger(ExcelDown.class);
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook wb, HttpServletRequest request,
		HttpServletResponse response) throws Exception{
		
		// 0. 엑셀 데이터 가져오기
		HashMap<String, Object> excelMap = (HashMap<String, Object>) model.get("excelMap");
		
		// 1. 엑셀파일이름 세팅 및 시트를 만들어주는 함수 호출(시트의 이름과 사이즈를 지정)
		String excelFileNm = excelMap.get("exFileNm").toString() + ".xls";
		response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(excelFileNm, "UTF-8")+";charset=\"UTF-8\"");
		response.setHeader("X-Download-Options", "noopen");
		Sheet sheet = createFirstSheet(wb, excelMap.get("exFileNm").toString());
		
		// 2. 헤더값 및 헤더 별칭 가져오기
		String[] arrExHeaderId = StringUtils.convertToCamelCase(StringUtils.isNullToString(excelMap.get("exHeaderId")), '_').split(":");
		String[] arrExHeaderNm = StringUtils.convertToCamelCase(StringUtils.isNullToString(excelMap.get("exHeaderNm")), '_').split(":");
		
		// 3. 엑셀 내용 세팅
		// => i=0일 경우 헤더값을 조회하기위해서 resultList.size()+1해서 전체데이터를 조회하도록 +1함
		int num = 0;
		List<?> resultList = (List<?>) excelMap.get("resultList");
		
		for(int i=0; i<resultList.size()+1; i++){
			
			if(i == 0){
				num = i;
			}else if(i > 0){
				num = (i-1);
			}
			EgovMap egovMap = (EgovMap) resultList.get(num);
			
			// 3-1. row 생성
			Row row = sheet.createRow(i);
			
			// 3-2. cell header 및 값 생성
			for(int j=0; j<arrExHeaderId.length; j++){
				if(i == 0){
					createHeader(arrExHeaderNm, row, j);
				}else if(i > 0){
					createCell(egovMap, arrExHeaderId, row, j);
				}
			}
		}
	}

	/**
	 * Sheet 생성
	 *
	 * @param wb
	 * @param fileNm
	 * @return Sheet
	*/
	private Sheet createFirstSheet(Workbook wb, String fileNm) {
		Sheet sheet = wb.createSheet();
		wb.setSheetName(0, fileNm);
		sheet.setColumnWidth(1, 256 * 20);
		return sheet;
	}
	
	/**
	 * Cell header 생성
	 *
	 * @param arrExHeaderNm
	 * @param row
	 * @param cellNum void
	*/
	private void createHeader(String[] arrExHeaderNm, Row row, int cellNum){
		Cell cell = row.createCell(cellNum);
		cell.setCellValue(arrExHeaderNm[cellNum]);
	}
	
	/**
	 * Cell 생성
	 *
	 * @param egovMap
	 * @param arrExHeaderId
	 * @param row
	 * @param cellNum void
	*/
	private void createCell(EgovMap egovMap, String[] arrExHeaderId, Row row, int cellNum) {
		log.info("exHeaderId======"+arrExHeaderId[cellNum]);
		log.info("exHeaderId[cellNum]======"+egovMap.get(arrExHeaderId[cellNum]));
		
		// 엑셀다운로드 실행하기 전 null값 처리
		String value = null;
		if(StringUtils.isEmpty(egovMap.get(arrExHeaderId[cellNum]))){
			value = "";
		}else{
			value = egovMap.get(arrExHeaderId[cellNum]).toString();
		}
		
		Cell cell = row.createCell(cellNum);
		cell.setCellValue(value);
	}
}