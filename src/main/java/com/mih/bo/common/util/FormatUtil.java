package com.mih.bo.common.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.mih.bo.common.Constants;


/**
 * @Project : MEGA
 * @Class : FormatUtil.java
 * @Description : FormatUtil.
 * @Author : kis1005
 * @Since : 2021.11.03
 */
public class FormatUtil {

    /**
     * 파일 사이즈 변환.
     *
     * @param size
     *
     * @return
     */
    public static String fileSize( long size ){
        if (size <= 0)
            return "0";
        final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    /**
     * matchFormat
     *
     * @param str    String Value
     * @param format Format
     *
     * @return String
     */
    public static String matchFormat( String str, String format ){
        if (str == null || str.length() == 0)
            return str;
        int len = format.length();
        char[] result = new char[len];
        for (int i = 0, j = 0; i < len; i++, j++) {
            if (format.charAt(i) == '#') {
                try {
                    result[i] = str.charAt(j);
                } catch (StringIndexOutOfBoundsException e) {
                    result[i] = '\u0000';
                }
            } else {
                result[i] = format.charAt(i);
                j--;
            }
        }
        return new String(result);
    }

    /**
     * releaseFormat
     *
     * @param str    String Value
     * @param format Format
     *
     * @return String
     */
    public static String releaseFormat( String str, String format ){
        if (str == null || str.length() == 0)
            return str;
        int len = format.length();
        char[] result = new char[len];
        for (int i = 0, j = 0; i < len; i++, j++) {
            if (format.charAt(i) == '#') {
                try {
                    result[j] = str.charAt(i);
                } catch (StringIndexOutOfBoundsException e) {
                    result[j] = '\u0000';
                }
            } else {
                j--;
            }
        }
        return (new String(result)).trim();
    }

    /**
     * matchFormat
     *
     * @param dt     날짜
     * @param format Format
     *
     * @return String
     */
    public static String matchFormat( Date dt, String format ){
        if (dt == null)
            return "";
        try {
            return new SimpleDateFormat(format).format(dt);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "invalid format";
    }

    /**
     * 금액 xxx,xxx 포맷
     *
     * @param arg String value
     *
     * @return String
     */
    public static String toCurFormat(String arg) {
   		if (StringUtils.isEmpty(arg)) {
   			return "0";
   		}
   		return toCurFormat(new Double(arg.trim()));
   	}

    /**
     * 금액 xxx,xxx 포맷
     *
     * @param arg String value
     *
     * @return String
     */
    public static String toCurFormat( double arg ){
        return new DecimalFormat("###,##0").format(arg);
    }

    /**
     * PointFormat
     *
     * @param arg
     * @param pointDigit
     *
     * @return String
     */
    public static String toPointFormat( String arg, int pointDigit ){
        return toPointFormat(new Double(arg.trim()), pointDigit);
    }

    /**
     * PointFormat
     *
     * @param arg
     * @param pointDigit
     *
     * @return String
     */
    public static String toPointFormat( double arg, int pointDigit ){
        if (pointDigit <= 0) {
            return new DecimalFormat("##0").format(arg);
        } else {
            String formatStr = "##0.";
            formatStr += pointDigit;
            return new DecimalFormat(formatStr).format(arg);
        }
    }

    /**
     * DateFormat
     *
     * @param str String Value
     *
     * @return Date
     */
    public static Date toDate( String str ){
        if (str == null || str.length() == 0)
            return null;
        String format = (str.length() == 8) ? Constants.YYYYMMDD : (str.length() == 12 ? Constants.YYYYMMDDHHMM : ((str.contains("-")) ? Constants.DEFAULT_DATE_FORMAT : "yyyy/MM/dd"));
        try {
            return new SimpleDateFormat(format).parse(str);
        } catch (ParseException ex) {
            return null;
        }
    }

    /**
     * DateFormat
     *
     * @param obj
     *
     * @return Date
     */
    public static String toDate( Object obj, String format ){
        SimpleDateFormat sdfDate = new SimpleDateFormat(format);
        Date date;

        if (obj instanceof Date) {
            date = (Date) obj;
        } else {
            String str = (String) obj;
            if (str == null || str.length() == 0)
                return null;
            date = toDate(str);
        }

        return sdfDate.format(date);
    }

    /**
     * <pre>
     * 전화 번호 형식으로 포맷을 변경 한다.
     * </pre>
     *
     * @param number
     *
     * @return
     */
    public static String toPhoneFormat( String number ){
        if (number == null) {
            return "";
        }
        if (number.length() == 8) {
            return number.replaceFirst("^([0-9]{4})([0-9]{4})$", "$1-$2");
        }
        return number.replaceFirst("(^02|[0-9]{3})([0-9]{3,4})([0-9]{4})$", "$1-$2-$3");
    }

}