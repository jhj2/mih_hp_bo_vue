package com.mih.bo.common.util;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtils {

	public static final String DEFAULE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static final String YYYYMMDD_DASH = "yyyy-MM-dd";

	public static final String YYYYMMDD_DOT = "yyyy.MM.dd";

	public static final String YYYYMMDDHHMMSS_DOT = "yyyy.MM.dd HH:mm:ss";

	public static final String YYYYMMDD = "yyyyMMdd";

	public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	
	public static final String YYYYMMDD_SLASH = "yyyy/MM/dd";

	private DateUtils() {
		// ignore..
	}

	/**
	 * 어제
	 *
	 * @return LocalDateTime
	 */
	public static LocalDateTime yesterday() {
		return LocalDateTime.of(year(), monthOfYear(), dayOfMonth(), 0, 0).minusDays(1);
	}

	/**
	 *
	 *
	 * @return long
	 */
	public static long yesterdayMillis() {
		return LocalDateTime.of(year(), monthOfYear(), dayOfMonth(), 0, 0).minusDays(1).atZone(ZoneId.systemDefault())
				.toInstant().toEpochMilli();
	}

	/**
	 * 오늘
	 *
	 * @return LocalDateTime
	 */
	public static LocalDateTime today() {
		return LocalDateTime.of(year(), monthOfYear(), dayOfMonth(), 0, 0);
	}

	/**
	 *
	 *
	 * @return long
	 */
	public static long todayMillis() {
		return LocalDateTime.of(year(), monthOfYear(), dayOfMonth(), 0, 0).atZone(ZoneId.systemDefault()).toInstant()
				.toEpochMilli();
	}

	/**
	 * 내일
	 *
	 * @return LocalDateTime
	 */
	public static LocalDateTime tomorrow() {
		return LocalDateTime.of(year(), monthOfYear(), dayOfMonth(), 0, 0).plusDays(1);
	}

	/**
	 *
	 *
	 * @return long
	 */
	public static long tomorrowMillis() {
		return LocalDateTime.of(year(), monthOfYear(), dayOfMonth(), 0, 0).plusDays(1).atZone(ZoneId.systemDefault())
				.toInstant().toEpochMilli();
	}

	/**
	 * 이전날짜
	 *
	 * @return LocalDateTime
	 */
	public static LocalDateTime beforeDay(int day) {
		return LocalDateTime.of(year(), monthOfYear(), dayOfMonth(), hour(), minute(), second()).minusDays(day);
	}

	/**
	 * 다음날짜
	 *
	 * @return LocalDateTime
	 */
	public static LocalDateTime afterDay(int day) {
		return LocalDateTime.of(year(), monthOfYear(), dayOfMonth(), hour(), minute(), second()).plusDays(day);
	}
	
	/**
	 * 이전날짜
	 *
	 * @return LocalDateTime
	 */
	public static LocalDateTime beforeMonth(int month) {
		return LocalDateTime.of(year(), monthOfYear(), dayOfMonth(), hour(), minute(), second()).minusMonths(month);
	}

	/**
	 * 다음날짜
	 *
	 * @return LocalDateTime
	 */
	public static LocalDateTime afterMonth(int month) {
		return LocalDateTime.of(year(), monthOfYear(), dayOfMonth(), hour(), minute(), second()).plusMonths(month);
	}

	/**
	 * 년도
	 *
	 * @return Year
	 */
	public static int year() {
		return LocalDate.now().getYear();
	}

	/**
	 * 월
	 *
	 * @return Month
	 */
	public static int monthOfYear() {
		return LocalDate.now().getMonthValue();
	}

	/**
	 * 일
	 *
	 * @return Day
	 */
	public static int dayOfMonth() {
		return LocalDate.now().getDayOfMonth();
	}

	/**
	 * 시간
	 *
	 * @return Hour
	 */
	public static int hour() {
		return LocalDateTime.now().getHour();
	}

	/**
	 * 분
	 *
	 * @return Minute
	 */
	public static int minute() {
		return LocalDateTime.now().getMinute();
	}

	/**
	 * 초
	 *
	 * @return Second
	 */
	public static int second() {
		return LocalDateTime.now().getSecond();
	}

	/**
	 *
	 * <p>
	 *
	 * <pre>
	 * DateUtil.toDateString(str);
	 * </pre>
	 *
	 * @return String.
	 */
	public static String toDateString() {
		return toDateString(DEFAULE_FORMAT);
	}

	/**
	 *
	 * <p>
	 *
	 * <pre>
	 * DateUtil.toDateString(&quot;yyyy-MM-dd HH:mm:ss&quot;, time);
	 * </pre>
	 *
	 * @param fmt Format.
	 * @return String.
	 */
	public static String toDateString(final String fmt) {
		return toDateString(LocalDateTime.now(), fmt);
	}

	/**
	 *
	 * <p>
	 *
	 * <pre>
	 *
	 * </pre>
	 *
	 * @param fmt Format.
	 * @param date LocalDateTime
	 * @return String
	 */
	public static String toDateString(LocalDateTime date, String fmt) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(fmt);
        return date.format(formatter);
	}

	/**
	 *
	 * <p>
	 *
	 * <pre>
	 * DateUtil.toDate(date);
	 * </pre>
	 *
	 * @param date String
	 * @return Date
	 */
	public static LocalDateTime toDate(final String dateStr) throws ParseException {
		return toDate(dateStr, DEFAULE_FORMAT);
	}

	/**
	 *
	 * <p>
	 *
	 * <pre>
	 *
	 * </pre>
	 *
	 * @param fmt Format.
	 * @param dateStr String
	 * @return Date
	 */
	public static LocalDateTime toDate(final String dateStr, final String fmt) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(fmt);
		return LocalDateTime.parse(dateStr, formatter);
	}

	/**
	 *
	 * <p>
	 *
	 * <pre>
	 *
	 * </pre>
	 *
	 * @param fmt Format.
	 * @param dateStr String
	 * @return Date
	 */
	public static Date toDate(final LocalDateTime localDateTime) {
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

}
