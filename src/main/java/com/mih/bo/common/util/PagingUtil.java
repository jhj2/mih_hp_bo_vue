package com.mih.bo.common.util;

import com.mih.bo.common.vo.Paging;

public class PagingUtil {
	
	private static String DEFAULT_FUNCTION_NAME = "goPage";
	
	private static String DEFAULT_FORM_NAME = "searchForm";
	
	private static int DEFAULT_CURRENT_PAGE = 1;
	
	private static int DEFAULT_RECORD_COUNT_PER_PAGET = 2;
	
	private static int DEFAULT_PAGE_SIZE = 10;
	
	public static void setPagination(Paging paging) {
		 
        String pagination = ""; // 페이징 결과 값
        String functionName = paging.getFunctionName() == null ? DEFAULT_FUNCTION_NAME : paging.getFunctionName();
        String formName = paging.getFormName() == null ? DEFAULT_FORM_NAME : paging.getFormName();
        int currentPage = paging.getCurrentPageNo() == 0 ? DEFAULT_CURRENT_PAGE : paging.getCurrentPageNo();
        int countPerList = paging.getRecordCountPerPage() == 0 ? DEFAULT_RECORD_COUNT_PER_PAGET : paging.getRecordCountPerPage();
        int countPerPage = paging.getPageSize() == 0 ? DEFAULT_PAGE_SIZE : paging.getPageSize();
        int totalListCount = paging.getTotalRecordCount(); // 총 게시물 수
        int totalPageCount = totalListCount / countPerList; // 총 페이지 수
        
        if (totalListCount % countPerList > 0) { // 총 페이수를 구할 때 int형으로 계산하면 나머지가 있는 경우 게시물이 존재하기 때문에 총 페이지의 수를 수정
            totalPageCount = totalPageCount + 1;
        }
 
        int viewFirstPage = (((currentPage - 1) / countPerPage) * countPerPage) + 1; // 한 화면에 첫 페이지 번호
        int viewLastPage = viewFirstPage + countPerPage - 1; // 한 화면에 마지막 페이지 번호
        if (viewLastPage > totalPageCount) { // 마지막 페이지의 수가 총 페이지의 수보다 큰 경우는 게시물이 존재하지 않기 때문에 마지막 페이지의 수를 수정
        	viewLastPage = totalPageCount;
        }
 
        int totalFirstPage = 1; // 전체 페이지 중에 처음 페이지
        int totalLastPage = totalPageCount; // 전체 페이지 중에 마지막 페이지
        int prePerPage = 0; // 이전 화면에 첫번째 번호
        if (viewFirstPage - countPerPage > 0) {
            prePerPage = viewFirstPage - countPerPage;
        } else {
            prePerPage = totalFirstPage;
        }
        int nextPerPage = 0; // 이후 화면에 첫번째 번호
        if (viewFirstPage + countPerPage < totalPageCount) {
            nextPerPage = viewFirstPage + countPerPage;
        } else {
            nextPerPage = totalPageCount;
        }
 
        // 페이지 네이게이션 설정
        pagination += "<div class='paging'>";
        pagination += "<a href='javascript:" + functionName + "(" + totalFirstPage + ", \"" + formName  +  "\"" + ");' class=\"arrow first\"></a>";
        pagination += "<a href='javascript:" + functionName + "(" + prePerPage + ", \"" + formName  +  "\"" + ");' class=\"arrow prev\"></a>";
        for (int i = viewFirstPage; i <= viewLastPage; i++) {
            if (i == currentPage) {
                pagination += "<a href='javascript:" + functionName + "(" + i + ", \"" + formName  +  "\"" + ");' class='num active'>" + i + "</a>";
            } else {
            	pagination += "<a href='javascript:" + functionName + "(" + i + ", \"" + formName  +  "\"" + ");' class='num'>" + i + "</a>";	
            }
        }
        pagination += "<a href='javascript:" + functionName + "(" + nextPerPage + ", \"" + formName  +  "\"" + ");' class=\"arrow next\"></a>";
        pagination += "<a href='javascript:" + functionName + "(" + totalLastPage + ", \"" + formName  +  "\"" + ");' class=\"arrow last\"></a>";
        pagination += "</div>";
 
        int offset = ((currentPage - 1) * countPerList); // 한 화면의 표출되는 게시물의 시작 번호 (쿼리 조건절)
 
        // LIMIT는 가져올 row의 수, OFFSET은 몇 번째 row부터 가져올지를 결정
        paging.setLimit(countPerList);
        paging.setOffset(offset);
        paging.setTotalPageCount(totalPageCount);
        paging.setPagination(pagination);
        paging.setPageMaxNo(totalListCount - ((currentPage - 1) * countPerList));
    }

}
