package com.mih.bo.common.util;

import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;

public class UUIDUtils {

	private static final String DEFAULE_DATE_FORMAT = "yyyyMMddHHmmssmmmm";

	private static final int MIN_COUNT = 100001;

	private static final int MAX_COUNT = 999999;

	private static int count = MIN_COUNT;

	public static String md5() {
		synchronized (UUIDUtils.class) {
			String dateTime = DateTime.now().toString(DEFAULE_DATE_FORMAT) + count++;
			if (count == MAX_COUNT) {
				count = MIN_COUNT;
			}
			int poi = 8;
			StringBuilder sb = new StringBuilder(DigestUtils.md5Hex(dateTime));
			for (int i = 0; i < 3; i++) {
				sb.insert(poi, "-");
				poi += 1 + 4;
			}
			return sb.toString();
		}
	}

	public static String md5(final String key) {
		int poi = 8;
		StringBuilder sb = new StringBuilder(DigestUtils.md5Hex(key));
		for (int i = 0; i < 3; i++) {
			sb.insert(poi, "-");
			poi += 1 + 4;
		}
		return sb.toString();
	}

	public static String md5(final String key, final Date date) {
		synchronized (UUIDUtils.class) {
			int poi = 8;
			StringBuilder sb = new StringBuilder(DigestUtils.md5Hex(key + "-" + date));
			for (int i = 0; i < 3; i++) {
				sb.insert(poi, "-");
				poi += 1 + 4;
			}
			return sb.toString();
		}
	}

	public static String sha1() {
		synchronized (UUIDUtils.class) {
			String dateTime = DateTime.now().toString(DEFAULE_DATE_FORMAT) + count++;
			if (count == MAX_COUNT) {
				count = MIN_COUNT;
			}
			int poi = 8;
			StringBuilder sb = new StringBuilder(DigestUtils.sha1Hex(dateTime));
			for (int i = 0; i < 5; i++) {
				sb.insert(poi, "-");
				poi += 1 + 4;
			}
			return sb.toString();
		}
	}

	public static String sha1(final String key) {
		int poi = 8;
		StringBuilder sb = new StringBuilder(DigestUtils.sha1Hex(key));
		for (int i = 0; i < 5; i++) {
			sb.insert(poi, "-");
			poi += 1 + 4;
		}
		return sb.toString();
	}

	public static String sha1(final String key, final Date date) {
		synchronized (UUIDUtils.class) {
			int poi = 8;
			StringBuilder sb = new StringBuilder(DigestUtils.sha1Hex(key + "-" + date));
			for (int i = 0; i < 5; i++) {
				sb.insert(poi, "-");
				poi += 1 + 4;
			}
			return sb.toString();
		}
	}

	public static String sha256() {
		synchronized (UUIDUtils.class) {
			String dateTime = DateTime.now().toString(DEFAULE_DATE_FORMAT) + count++;
			if (count == MAX_COUNT) {
				count = MIN_COUNT;
			}
			int poi = 8;
			StringBuilder sb = new StringBuilder(DigestUtils.sha256Hex(dateTime));
			for (int i = 0; i < 10; i++) {
				sb.insert(poi, "-");
				poi += 1 + 4;
			}
			return sb.toString();
		}
	}

	public static String sha256(final String key) {
		int poi = 8;
		StringBuilder sb = new StringBuilder(DigestUtils.sha256Hex(key));
		for (int i = 0; i < 10; i++) {
			sb.insert(poi, "-");
			poi += 1 + 4;
		}
		return sb.toString();
	}

	public static String sha256(final String key, final Date date) {
		synchronized (UUIDUtils.class) {
			int poi = 8;
			StringBuilder sb = new StringBuilder(DigestUtils.sha256Hex(key + "-" + date));
			for (int i = 0; i < 10; i++) {
				sb.insert(poi, "-");
				poi += 1 + 4;
			}
			return sb.toString();
		}
	}

}
