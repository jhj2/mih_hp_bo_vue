package com.mih.bo.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("resource")
public class ExcelUtils {

	private static final Logger log = LoggerFactory.getLogger(ExcelUtils.class);

//	public static final List<GoodsPricePolicyHistoryDetail> makeGoodsPricePolicy(MultipartFile excelFile) {
//		List<GoodsPricePolicyHistoryDetail> result = new ArrayList<GoodsPricePolicyHistoryDetail>();
//		try {
//			OPCPackage opcPackage = OPCPackage.open(excelFile.getInputStream());
//			XSSFWorkbook workbook = new XSSFWorkbook(opcPackage);
//			
//			// 첫번째 시트 불러오기
//			XSSFSheet sheet = workbook.getSheetAt(0);
//			
//			for(int i=1; i<sheet.getLastRowNum() + 1; i++) {
//				GoodsPricePolicyHistoryDetail goodsPricePolicy = new GoodsPricePolicyHistoryDetail();
//				XSSFRow row = sheet.getRow(i);
//				
//				// 행이 존재하기 않으면 패스
//				if(null == row) {
//					continue;
//				}
//				
//				XSSFCell cell = row.getCell(0);
//				if(null != cell) goodsPricePolicy.setBhfIdx((int)cell.getNumericCellValue());
//				cell = row.getCell(1);
//				if(null != cell) goodsPricePolicy.setGoodsIdx((int)cell.getNumericCellValue());
//				cell = row.getCell(2);
//				if(null != cell) goodsPricePolicy.setSalePrc((int)cell.getNumericCellValue());
//				cell = row.getCell(3);
//				if(null != cell) goodsPricePolicy.setMnt6DcPrc((int)cell.getNumericCellValue());
//				cell = row.getCell(4);
//				if(null != cell) goodsPricePolicy.setMnt12DcPrc((int)cell.getNumericCellValue());
//				
//				log.info(goodsPricePolicy.toJson());
//				
//				result.add(goodsPricePolicy);
//			}
//		} catch (Exception e) {
//			log.error(StringUtils.printStack(e));
//			return null;
//		}
//		return result;
//	}
}