package com.mih.bo.common.util;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.ehcache.EhCacheCacheManager;

import com.mih.bo.common.exception.BgbgbServiceException;

/**
 * @Project : MEGA
 * @Class : CommonCodeUtil.java
 * @Description : CommonCodeUtil.
 * @Author : kis1005
 * @Since : 2021.11.03
 */
public class CommonCodeUtil {

    /**
     * The constant COMMON_CODE_CACHE_NAME.
     */
    public static final String COMMON_CODE_CACHE_NAME = "commonCodeCache";
    /**
     * The constant COMMON_CODE_ID.
     */
    public static final String COMMON_CODE_ID         = "cmmncdId";
    /**
     * The constant COMMON_CODE_NAME.
     */
    public static final String COMMON_CODE_NAME       = "cmmncdNm";
    /**
     * The constant COMMON_CODE_SN.
     */
    public static final String COMMON_CODE_SN         = "cmmncdSn";
    /**
     * The constant COMMON_PARENT_SN.
     */
    public static final String COMMON_PARENT_SN       = "parntsCdSn";

    private CommonCodeUtil() {}

    /**
     * <pre>
     * parentCodeId 에 맞는 공통코드를 가져온다.
     * </pre>
     *
     * @param parentCodeId the parent code id
     * @return com code
     */
    public static List<Map<String, Object>> getComCode( String parentCodeId ){
        List<Map<String, Object>> codeCacheList = getComCodeCacheList();
        String codeSn = "";
        int chkCnt = 0;
        for (Map<String, Object> map : codeCacheList) {
            if (map.get(COMMON_CODE_ID).equals(parentCodeId)) {
                codeSn = map.get(COMMON_CODE_SN).toString();
                chkCnt++;
            }
        }

        if (chkCnt > 1) {
            throw new BgbgbServiceException("그룹코드가 다건이 검색 되었습니다.");
        }
        if (chkCnt == 0) {
            throw new BgbgbServiceException("[" + parentCodeId + "]존재 하지 않는 그룹 코드 입니다.");
        }

        final String finalCodeSn = codeSn;
        return codeCacheList.stream().filter(m -> finalCodeSn.equals(m.get(COMMON_PARENT_SN))).collect(Collectors.toList());
    }

    /**
     * <pre>
     * parentCodeId 에 맞는 공통코드를 가져온다.
     * </pre>
     *
     * @param parentCodeId the parent code id
     * @param codeValue    the code value
     * @return com code
     */
    public static List<Map<String, Object>> getComCode( String parentCodeId, String codeValue ){
        List<Map<String, Object>> codeCacheList = getComCodeCacheList();
        String codeSn = "";
        int chkCnt = 0;
        for (Map<String, Object> map : codeCacheList) {
            if (map.get(COMMON_CODE_ID).equals(parentCodeId)) {
                codeSn = map.get(COMMON_CODE_SN).toString();
                chkCnt++;
            }
        }

        if (chkCnt > 1) {
            throw new BgbgbServiceException("그룹코드가 다건이 검색 되었습니다.");
        }
        if (chkCnt == 0) {
            throw new BgbgbServiceException("존재 하지 않는 그룹 코드 입니다.");
        }

        final String finalCodeSn = codeSn;
        return codeCacheList.stream().filter(m -> finalCodeSn.equals(m.get(COMMON_PARENT_SN))).collect(Collectors.toList());
    }

    /**
     * <pre>
     * 공통 코드 명을 가져온다.
     * </pre>
     *
     * @param cmmncdSn the cmmncd sn
     * @return com code name
     */
    public static String getComCodeName( String cmmncdSn ){
        String codeNm = "";

        for (Map<String, Object> map : getComCodeCacheList()) {
            if (map.get(COMMON_CODE_SN).equals(cmmncdSn)) {
                codeNm = (String) map.get(COMMON_CODE_NAME);
                break;
            }
        }
        return codeNm;
    }

    /**
     * Gets com code name.
     *
     * @param groupCode the group code
     * @param code      the code
     * @return the com code name
     */
    public static String getComCodeName( String groupCode, String code ){
        String codeNm = "";
        for (Map<String, Object> m : CommonCodeUtil.getComCode(groupCode)) {
            if (code.equals(m.get(COMMON_CODE_ID))) {
                codeNm = (String) m.get(COMMON_CODE_NAME);
            }
        }
        return codeNm;
    }

    /**
     * <pre>
     * 공통코드 list 를 리턴한다.
     * </pre>
     *
     * @return com code cache list
     */
    public static List<Map<String, Object>> getComCodeCacheList(){
        EhCacheCacheManager cacheManager = (EhCacheCacheManager) BeanUtil.getBean("cacheManager");
        Cache cache = cacheManager.getCache(COMMON_CODE_CACHE_NAME);
        ValueWrapper vw = cache.get(COMMON_CODE_CACHE_NAME);
        List<Map<String, Object>> cacheList = null;
        if (vw != null) {
            cacheList = (List<Map<String, Object>>) vw.get();
        }
        return cacheList;
    }

}
