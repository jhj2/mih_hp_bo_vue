package com.mih.bo.common.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringUtils extends org.apache.commons.lang3.StringUtils {
	
	private static final Logger log = LoggerFactory.getLogger(StringUtils.class);

	public static final String DEFAULT_DECIMAL_FORMAT = "###,###";
	public static final char DEFAULT_REPLACE = '*';

	public static boolean isEmpty(Object arg) {
		return null == arg || "".equals(arg) || "null".equalsIgnoreCase(String.valueOf(arg));
	}

	public static boolean isEmpty(String str) {
		return (str == null || str.trim().length() < 1 || "null".equalsIgnoreCase(str));
	}

	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	public static boolean isNotEmpty(Object arg) {
		return !isEmpty(arg);
	}

	public static String defaultString(String str, String def) {
		if (isEmpty(str)) {
			return def;
		} else {
			return str;
		}
	}

	public static String defaultString(Object arg, String def) {
		if (arg instanceof String) {
			return defaultString(String.valueOf(arg), def);
		} else {
			return isEmpty(arg) ? def : String.valueOf(arg);
		}
	}

	public static String defaultString(String str) {
		return defaultString(str, "");
	}

	public static String defaultString(Object arg) {
		return defaultString(arg, "");
	}

	public static String padding(String target, String paddingType, int length, String paddingChar, String charset) {
		String targ = defaultString(target);
		byte[] targetArr = null;
		try {
			targetArr = targ.getBytes(charset);
		} catch (UnsupportedEncodingException e) {
			log.error(StringUtils.printStack(e));
		}
		if (targetArr.length > length) {
			return targ;
		}

		String result = "";

		int padLength = length - targetArr.length;
		for ( int i = 0 ; i < padLength ; i++ ) {
			result = result.concat(paddingChar);
		}

		if ("L".equals(paddingType)) {
			result = result.concat(targ);
		} else {
			result = targ.concat(result);
		}

		return result;
	}

	public static String paddingLeft(String target, String paddingChar, int length) {
		return padding(target, "L", length, paddingChar, "UTF-8");
	}

	public static String paddingRight(String target, String paddingChar, int length) {
		return padding(target, "R", length, paddingChar, "UTF-8");
	}

	public static String paddingNumber(String target, int length) {
		return padding(target, "L", length, "0", "UTF-8");
	}

	public static String paddingString(String target, int length) {
		return padding(target, "R", length, " ", "UTF-8");
	}

	public static String trim(String target, String trimType, char trimChar) {
		//target = convNull(target);

		String result = "";

		if ( "L".equals(trimType) ) {
			int targetIndex = 0;
			for ( int i = targetIndex ; i < target.length() ; i++ ) {
				if (trimChar == target.charAt(i)) {
					targetIndex++;
				} else {
					result = target.substring(targetIndex, target.length());
					break;
				}
			}
		} else {
			int targetIndex = target.length()-1;
			for ( int i = targetIndex ; i >= 0 ; i-- ) {
				if ( trimChar == target.charAt(i) ) {
					targetIndex--;
				} else {
					result = target.substring(0, targetIndex+1);
					break;
				}
			}
		}

		return result;
	}

	public static String trimLeft(String target, char trimChar) {
		return trim(target, "L", trimChar);
	}

	public static String trimRight(String target, char trimChar) {
		return trim(target, "R", trimChar);
	}

	public static String trimNumber(String target) {
		return trim(target, "L", '0');
	}

	public static String trimString(String target) {
		return trim(target, "R", ' ');
	}

	public static Map<String, Object> trimMap(Map<String, Object> map) {
		Map<String, Object> resultMap = new HashMap<String, Object>();

		Iterator<String> keys = map.keySet().iterator();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			if (map.get(key) instanceof String && !isEmpty(map.get(key))) {
				String value = (String) map.get(key);
				resultMap.put(key, value.trim());
			} else {
				resultMap.put(key, map.get(key));
			}
		}

		return resultMap;
	}

	public static String escapeString(String src) {
		if (isEmpty(src)) {
			return "";
		}

		return src.replaceAll("#", "&#35;")
				.replaceAll("&", "&amp;")
				.replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;")
				.replaceAll("\"", "&quot;")
				.replaceAll("\'", "&apos;");
	}

	public static String escapeString2(String src) {
		if (isEmpty(src)) {
			return "";
		}

		return src.replaceAll(";", "&#59;")
				.replaceAll("#", "&#35;")
				.replaceAll("&", "&amp;")
				.replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;")
				.replaceAll("\\(", "&#40;")
				.replaceAll("\\)", "&#41;")
				.replaceAll("\\{", "&#123;")
				.replaceAll("\\}", "&#125;")
				.replaceAll("\"", "&quot;")
				.replaceAll("\'", "&apos;");
	}


	public static String nl2br(String src) {
		if (isEmpty(src)) {
			return "";
		}

		return src.replaceAll("\r", "<br />")
				.replaceAll("\n", "<br />")
				.replaceAll("\r\n", "<br />");
	}

	public static List<String> stringToList(String str, int splitIndex) {
		List<String> result = new ArrayList<String>();
		int strLength = str.length();

		if(strLength < splitIndex){
			result.add(str);
		}else{
			int beginIndex = 0;
			int endIndex = 0;
			while (endIndex < strLength) {
				beginIndex = endIndex;
				endIndex = endIndex + splitIndex;
				if(endIndex > strLength){
					endIndex = strLength;
				}
				result.add(str.substring(beginIndex, endIndex));
			}
		}

		return result;
	}

	public static String stringByteCut(String szText, String szKey, int nLength, int nPrev, boolean isNotag, boolean isAdddot){  // 문자열 자르기

		String rVal = szText;
		int oF = 0, oL = 0, rF = 0, rL = 0;
		int nLengthPrev = 0;
		Pattern p = Pattern.compile("<(/?)([^<>]*)?>", Pattern.CASE_INSENSITIVE);  // 태그제거 패턴

		if(isNotag) {rVal = p.matcher(rVal).replaceAll("");}  // 태그 제거
		rVal = rVal.replaceAll("&", "&");
		rVal = rVal.replaceAll("(!/|\r|\n| )", "");  // 공백제거

		try {
			byte[] bytes = rVal.getBytes("UTF-8");     // 바이트로 보관

			if(szKey != null && !szKey.equals("")) {
				nLengthPrev = (rVal.indexOf(szKey) == -1)? 0: rVal.indexOf(szKey);  // 일단 위치찾고
				nLengthPrev = rVal.substring(0, nLengthPrev).getBytes("MS949").length;  // 위치까지길이를 byte로 다시 구한다
				nLengthPrev = (nLengthPrev-nPrev >= 0)? nLengthPrev-nPrev:0;    // 좀 앞부분부터 가져오도록한다.
			}

			// x부터 y길이만큼 잘라낸다. 한글안깨지게.
			int j = 0;

			if(nLengthPrev > 0) while(j < bytes.length) {
				if((bytes[j] & 0x80) != 0) {
					oF+=2; rF+=3; if(oF+2 > nLengthPrev) {break;} j+=3;
				} else {if(oF+1 > nLengthPrev) {break;} ++oF; ++rF; ++j;}
			}

			j = rF;

			while(j < bytes.length) {
				if((bytes[j] & 0x80) != 0) {
					if(oL+2 > nLength) {break;} oL+=2; rL+=3; j+=3;
				} else {if(oL+1 > nLength) {break;} ++oL; ++rL; ++j;}
			}

			rVal = new String(bytes, rF, rL, "UTF-8");  // charset 옵션

			/*if (isAdddot && ((rF + rL + 3) <= bytes.length)) {
				rVal.concat("...");
			} */ // ...을 붙일지말지 옵션
		} catch(UnsupportedEncodingException e){
			log.error(StringUtils.printStack(e));
		}

		return rVal;
	}

	public static String escapeXml(String text) {
		if (null == text || text.isEmpty()) {
		    return text;
		}
		final int len = text.length();
		char current = 0;
		int codePoint = 0;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < len; i++) {
		    current = text.charAt(i);
		    boolean surrogate = false;
		    if (Character.isHighSurrogate(current)
		            && i + 1 < len && Character.isLowSurrogate(text.charAt(i + 1))) {
		        surrogate = true;
		        codePoint = text.codePointAt(i++);
		    } else {
		        codePoint = current;
		    }
		    if ((codePoint == 0x9) || (codePoint == 0xA) || (codePoint == 0xD)
		            || ((codePoint >= 0x20) && (codePoint <= 0xD7FF))
		            || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD))
		            || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF))) {
		        sb.append(current);
		        if (surrogate) {
		            sb.append(text.charAt(i));
		        }
		    }
		}

		return sb.toString();
	}

	public static String replaceIframe(String text) {
		String regex = "<iframe[^>]*?([^>\\\"']+)[\\\"']?[^>]*>";
		String regex2 = "<IFRAME[^>]*?([^>\\\"']+)[\\\"']?[^>]*>";

		text = text.replaceAll(regex, "").replaceAll("</iframe>", "");
		text = text.replaceAll(regex2, "").replaceAll("</IFRAME>", "");

		return text;
	}

	public static String replaceSpecialCharacter(String str) {
		String str_imsi	=	null;
		String []filter_word = {"","\\.","\\?","\\/","\\~","\\!","\\@","\\#","\\$","\\%","\\^","\\&","\\*","\\(","\\)","\\_","\\+","\\=","\\|","\\\\","\\}","\\]","\\{","\\[","\\\"","\\'","\\:","\\;","\\<","\\>","\\.","\\?","\\/"};

		for(int i=0;i<filter_word.length;i++){
		str_imsi = str.replaceAll(filter_word[i],"");
		str = str_imsi;
		}

		return str;
	}

	public static final boolean notEquals(final CharSequence cs1, final CharSequence cs2) {
		return !equals(cs1, cs2);
	}

	public static final boolean notEqualsIgnoreCase(final CharSequence cs1, final CharSequence cs2) {
		return !equalsIgnoreCase(cs1, cs2);
	}

	public static final boolean notEqualsAny(final CharSequence cs1, final CharSequence... searchs) {
		return !equalsAny(cs1, searchs);
	}

	public static final boolean notEqualsAnyIgnoreCase(final CharSequence cs1, final CharSequence... searchs) {
		return !equalsAnyIgnoreCase(cs1, searchs);
	}

	public static final String decimalFormat(final long value) {
		return decimalFormat(DEFAULT_DECIMAL_FORMAT, value);
	}

	public static final String decimalFormat(final String pattern, final long value) {
		DecimalFormat formatter = new DecimalFormat(pattern);
		return formatter.format(value);
	}
	
	
	public static final String decimalFormat(final String pattern, final Date date){
		if(date == null)
			return "";
		
		if(pattern != null){
			java.text.SimpleDateFormat format = new java.text.SimpleDateFormat(pattern);
			return format.format(date);
		} else {
			return new Date().toString();
		}
	}

	/**
	 * Exception String
	 *
	 * @param t Throwable
	 * @return Stack Trace
	 */
	@SuppressWarnings("all")
	public static final String printStack(final Throwable t) {
		StringWriter sw = new StringWriter();
		t.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}
	
    /**
     * 문자열 마스킹
     *
     * @param src      원본
     * @param startIdx 시작위치
     * @return 마스킹 적용된 문자열
     */
    public static String masking(String src, int startIdx) {
        return masking(src, DEFAULT_REPLACE, null, startIdx, src.length());
    }

    /**
     * 문자열 마스킹
     *
     * @param src      원본
     * @param startIdx 시작위치
     * @param length   길이
     * @return 마스킹 적용된 문자열
     */
    public static String masking(String src, int startIdx, int length) {
        return masking(src, DEFAULT_REPLACE, null, startIdx, length);
    }

    /**
     * 문자열 마스킹
     *
     * @param src      원본
     * @param replace  대치문자
     * @param startIdx 시작위치
     * @return 마스킹 적용된 문자열
     */
    public static String masking(String src, char replace, int startIdx) {
        return masking(src, replace, null, startIdx, src.length());
    }

    /**
     * 문자열 마스킹
     *
     * @param src      원본
     * @param replace  대치문자
     * @param startIdx 시작위치
     * @param length   길이
     * @return 마스킹 적용된 문자열
     */
    public static String masking(String src, char replace, int startIdx,
                                 int length) {
        return masking(src, replace, null, startIdx, length);
    }

    /**
     * 문자열 마스킹
     *
     * @param src      원본
     * @param replace  대치문자
     * @param exclude  제외문자
     * @param startIdx 시작위치
     * @param length   길이
     * @return 마스킹 적용된 문자열
     */
    public static String masking(String src, char replace, char[] exclude,
                                 int startIdx, int length) {
        StringBuffer sb = new StringBuffer(src);

        // 종료 인덱스
        int endIdx = startIdx + length;
        if (sb.length() < endIdx)
            endIdx = sb.length();

        // 치환
        for (int i = startIdx; i < endIdx; i++) {
            boolean isExclude = false;
            // 제외 문자처리
            if (exclude != null && 0 < exclude.length) {
                char currentChar = sb.charAt(i);

                for (char excludeChar : exclude) {
                    if (currentChar == excludeChar)
                        isExclude = true;
                }
            }

            if (!isExclude)
                sb.setCharAt(i, replace);
            // sb.replace(i, i + 1, replace);
        }

        return sb.toString();
	}
    
	/**
	 * 콤마
	 *
	 * @param amt
	 * @return String
	*/
	public static String setComma(String amt) {
		try {
			return NumberFormat.getInstance().format(Integer.parseInt(amt));
		}catch(Exception e) {
			return "0";
		}
	}

	/**
	 * 객체가 null인지 확인하고 null인 경우 "" 로 바꾸는 메서드
	 *
	 * @param object 원본 객체
	 * @return String 문자열
	*/
	public static String isNullToString(Object object){
		String string = "";
		if(object != null){
			string = object.toString().trim();
		}
		return string;
	}
	
	/**
	 * This method convert "string_util" to "stringUtil"
	 *
	 * @param String targetString
	 * @param char posChar
	 * @return String result
	*/
	public static String convertToCamelCase(String targetString, char posChar){
		StringBuffer result = new StringBuffer();
		boolean nextUpper = false;
		String allLower = targetString.toLowerCase();
		
		for(int i=0; i<allLower.length(); i++){
			char currentChar = allLower.charAt(i);
			if(currentChar == posChar){
				nextUpper = true;
			}else{
				if(nextUpper){
					currentChar = Character.toUpperCase(currentChar);
					nextUpper = false;
				}
				result.append(currentChar);
			}
		}
		return result.toString();
	}
}