package com.mih.bo.common.util;

import org.apache.commons.collections.map.ListOrderedMap;

/**
 * Camel Case 표기법 변환 처리를 포함하는 Map 확장 클래스
 * commons Collections 의 ListOrderedMap을 extends하고 있으며 Map의 key를 입력시 Camel Case 표기법으로 변경하여 처리하는 Map의 구현체
 * (iBatis의 경우 egovMap으로 결과 조회시 별도의 alias 없이 DB 칼럼명 그대로 조회하는 것만으로도 일반적인 VO의 attribute(camel case) 에 대한 resultMap과 같은 효과를 낼 수 있도록 추가됨)
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.03.14.
 */
public class EgovMap extends ListOrderedMap{

	private static final long serialVersionUID = 6723434363565852261L;
	
	/**
	 * key에 대하여 Camel Case 변환하여 super.put(ListOrderedMap)을 호출한다.
	 * @param key - '_' 가 포함된 변수명
	 * @param value - 명시된 key 에 대한 값 (변경 없음)
	 * @return previous value associated with specified key, or null if there was no mapping for key
	 */
	@Override
	public Object put(Object key, Object value){
		return super.put(CamelUtil.convert2CamelCase((String) key), value);
	}
}