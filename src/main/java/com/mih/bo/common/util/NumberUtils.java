package com.mih.bo.common.util;

public class NumberUtils extends org.apache.commons.lang3.math.NumberUtils {

	public static Integer[] split(final String str, final String separator) {
		String[] stringArray = org.apache.commons.lang3.StringUtils.split(str, separator);
		Integer[] intArray = new Integer[stringArray.length];
		for (int i = 0; i < stringArray.length; i++) {
			String numberAsString = stringArray[i];
			intArray[i] = Integer.parseInt(numberAsString);
		}
		return intArray;
	}

	public static Integer[] splitByWholeSeparator(final String str, final String separator) {
		String[] stringArray = org.apache.commons.lang3.StringUtils.splitByWholeSeparator(str, separator);
		Integer[] intArray = new Integer[stringArray.length];
		for (int i = 0; i < stringArray.length; i++) {
			String numberAsString = stringArray[i];
			intArray[i] = Integer.parseInt(numberAsString);
		}
		return intArray;
	}

}
