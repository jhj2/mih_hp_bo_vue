package com.mih.bo.common.util;

import java.nio.charset.Charset;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Charsets;
import com.mih.bo.common.vo.JsonResponse;

/**
 * HTTP Response 시 사용되는 유틸리티
 *
 * @author
 * @version 1.0.0
 * @linkplain JDK 6.0
 * @since 2014. 6. 28.
 */
public class ResponseUtils {

	/**
	 * URL Attribute Name
	 */
	public static final String URL_ATTRIBUTE_NAME = "url";

	/**
	 * Exception Attribute Name
	 */
	public static final String MESSAGE_ATTRIBUTE_NAME = "message";

	private ResponseUtils() {
		// ignore..
	}

	/**
	 * Json Rest Response.
	 *
	 * @param request HttpServletRequest
	 * @return ResponseEntity
	 */
	public static final ResponseEntity<Object> resultJson(final HttpServletRequest request) {
		return resultJson(request, new JsonResponse(), HttpStatus.OK, Charsets.UTF_8);

	}

	/**
	 * Json Rest Response.
	 *
	 * @param request HttpServletRequest
	 * @param body Requeset Body
	 * @return ResponseEntity
	 */
	public static final ResponseEntity<Object> resultJson(final HttpServletRequest request, final Object body) {
		return resultJson(request, body, HttpStatus.OK, Charsets.UTF_8);

	}

	/**
	 * Json Rest Response.
	 *
	 * @param request HttpServletRequest
	 * @param body Requeset Body
	 * @param status Http Status
	 * @return ResponseEntity
	 */
	public static final ResponseEntity<Object> resultJson(final HttpServletRequest request, final Object body,
			final HttpStatus status) {
		return resultJson(request, body, status, Charsets.UTF_8);

	}

	/**
	 * Json Rest Response.
	 *
	 * @param request HttpServletRequest
	 * @param body Requeset Body
	 * @param status Http Status
	 * @return ResponseEntity
	 */
	public static final ResponseEntity<Object> resultJson(final HttpServletRequest request, final Throwable ex,
			final HttpStatus status) {
		JsonResponse res = new JsonResponse(status.value(), status.getReasonPhrase());
		if (StringUtils.isNotBlank(ex.getMessage())) {
			res.setMessage(ex.getMessage());
		}
		return resultJson(request, res, status, Charsets.UTF_8);

	}

	/**
	 * Json Rest Response.
	 *
	 * @param request HttpServletRequest
	 * @param body Requeset Body
	 * @param status Http Status
	 * @param charSet Character Set
	 * @return ResponseEntity
	 */
	@SuppressWarnings("all")
	public static final ResponseEntity<Object> resultJson(final HttpServletRequest request, final Object body,
			final HttpStatus status, final Charset charSet) {
		HttpHeaders headers = new HttpHeaders();
		if (RequestUtils.isIE(request)) {
			headers.add(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE);
		} else {
			headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE + ";charset=" + charSet.name());
		}

		if (body == null) {
			return new ResponseEntity<Object>(new JsonResponse(), headers, status);
		}
		return new ResponseEntity<Object>(body, headers, status);
	}

	/**
	 * Model And View Response
	 *
	 * @param request HttpServletRequest
	 * @param viewName View Name
	 * @return ModelAndView
	 */
	public static final ModelAndView resultModelAndView(final HttpServletRequest request, final String viewName) {
		return resultModelAndView(request, viewName, null);
	}

	/**
	 * Model And View Response
	 *
	 * @param request HttpServletRequest
	 * @param viewName View Name
	 * @param body Object
	 * @return ModelAndView
	 */
	public static final ModelAndView resultModelAndView(final HttpServletRequest request, final String viewName,
			final Object body) {
		ModelAndView mav = new ModelAndView();
		mav.addObject(URL_ATTRIBUTE_NAME, request.getRequestURL());
		if (body != null) {
			if (body instanceof Throwable) {
				Throwable ex = (Throwable) body;
				mav.addObject(MESSAGE_ATTRIBUTE_NAME, ex.getMessage());
			} else if (body instanceof String) {
				mav.addObject(MESSAGE_ATTRIBUTE_NAME, body);
			} else {
				mav.addObject(MESSAGE_ATTRIBUTE_NAME, body.toString());
			}
		}
		mav.setViewName(viewName);
		return mav;
	}

	/**
	 * JSON의 ContextType을 설정한다.
	 *
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 */
	public static final void setJsonContentType(final HttpServletRequest request, final HttpServletResponse response) {
		setJsonContentType(request, response, Charsets.UTF_8);
	}

	/**
	 * JSON의 ContextType을 설정한다.
	 *
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @param charSet Charset
	 */
	public static final void setJsonContentType(final HttpServletRequest request, final HttpServletResponse response,
			final Charset charSet) {
		if (RequestUtils.isIE(request)) {
			response.setContentType(MediaType.TEXT_PLAIN_VALUE);
		} else {
			response.setContentType(MediaType.APPLICATION_JSON_VALUE + ";charset=" + charSet.name());
		}
	}

	/**
	 * Redirect URL를 리턴한다.
	 *
	 * @param request
	 * @return Redirect URL
	 */
	public static final String redirectUrl(final HttpServletRequest request) {
		String redirectUrl = RequestUtils.referer(request);
		if (StringUtils.isBlank(redirectUrl)) {
			redirectUrl = request.getContextPath();
		}
		return RequestUtils.requestServer(request) + StringUtils.replace(redirectUrl, "//", "/");
	}

	/**
	 * Redirect URL를 리턴한다.
	 *
	 * @param request
	 * @param url Redirect URL
	 * @return Redirect URL
	 */
	public static final String redirectUrl(final HttpServletRequest request, final String url) {
		String redirectUrl = "/" + url;
		if (StringUtils.notEqualsAny(request.getContextPath(), "/")) {
			redirectUrl = request.getContextPath() + "/" + url;
		}
		return RequestUtils.requestServer(request) + StringUtils.replace(redirectUrl, "//", "/");
	}

}
