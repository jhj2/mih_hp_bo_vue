package com.mih.bo.common.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Object의 Null 조건 체크 유틸리티
 *
 * @author
 * @version 1.0.0
 * @since 7.0
 */
public class AssertUtils {

	private AssertUtils() {
		// ignore..
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(clazz, "The class must not be null");
	 * </code>
	 *
	 * @param obj the object to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final Object obj, final String message) {
		if (obj == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(clazz1, clazz2, "The class must not be null");
	 * </code>
	 *
	 * @param obj1 the object to check
	 * @param obj2 the object to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final Object obj1, final Object obj2, final String message) {
		if (obj1 == null || obj2 == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(clazz1, clazz2, "The class must not be null");
	 * </code>
	 *
	 * @param objects the object array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final Object[] objects, final String message) {
		for (Object obj : objects) {
			if (obj == null) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(str, "The class must not be null");
	 * </code>
	 *
	 * @param str the String to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final String str, final String message) {
		if (str == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(str1, str2, "The class must not be null");
	 * </code>
	 *
	 * @param str1 the String to check
	 * @param str2 the String to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final String str1, String str2, final String message) {
		if (str1 == null || str2 == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(String[] {str1, str2}, "The class must not be null");
	 * </code>
	 *
	 * @param strs the String array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final String[] strs, final String message) {
		if (strs == null) {
			throw new IllegalArgumentException(message);
		}
		for (String str : strs) {
			if (str == null) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(str, "The class must not be null");
	 * </code>
	 *
	 * @param str the String to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final String str, final String message) {
		if (StringUtils.isBlank(str)) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(str1, str2, "The class must not be null");
	 * </code>
	 *
	 * @param str1 the String to check
	 * @param str2 the String to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final String str1, String str2, final String message) {
		if (StringUtils.isBlank(str1) || StringUtils.isBlank(str2)) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(String[] {str1, str2}, "The class must not be null");
	 * </code>
	 *
	 * @param strs the String array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final String[] strs, final String message) {
		if (strs == null || strs.length <= 0) {
			throw new IllegalArgumentException(message);
		}
		for (String str : strs) {
			if (StringUtils.isBlank(str)) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Assert that an object is not {@code false} .
	 * <p>
	 * <code>
	 * AssertUtils.notFalse(false, "The class must not be null");
	 * </code>
	 *
	 * @param exist the Boolean true/false to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notFalse(final boolean exist, final String message) {
		if (!exist) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code false} .
	 * <p>
	 * <code>
	 * AssertUtils.notTrue(true, "The class must not be null");
	 * </code>
	 *
	 * @param exist the Boolean true/false to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notTrue(final boolean exist, final String message) {
		if (exist) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(str, "The class must not be null");
	 * </code>
	 *
	 * @param bytes the Byte to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final byte[] bytes, final String message) {
		if (bytes == null || bytes.length <= 0) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(list, "The class must not be null");
	 * </code>
	 *
	 * @param list the list to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final List<?> list, final String message) {
		if (list == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(list1, list2, "The class must not be null");
	 * </code>
	 *
	 * @param list1 the list array to check
	 * @param list2 the list array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final List<?> list1, final List<?> list2, final String message) {
		if (list1 == null || list2 == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(new List[] {list1, list2}, "The class must not be null");
	 * </code>
	 *
	 * @param lists the list array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final List<?>[] lists, final String message) {
		if (lists == null) {
			throw new IllegalArgumentException(message);
		}
		for (List<?> list : lists) {
			if (list == null) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(list, "The class must not be null");
	 * </code>
	 *
	 * @param list the list to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final List<?> list, final String message) {
		if (list == null || list.isEmpty()) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(list1, list2, "The class must not be null");
	 * </code>
	 *
	 * @param list1 the list array to check
	 * @param list2 the list array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final List<?> list1, final List<?> list2, final String message) {
		if ((list1 == null || list1.isEmpty()) || (list2 == null || list2.isEmpty())) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(new List[] {list1, list2}, "The class must not be null");
	 * </code>
	 *
	 * @param lists the list array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final List<?>[] lists, final String message) {
		if (lists == null || lists.length <= 0) {
			throw new IllegalArgumentException(message);
		}
		for (List<?> list : lists) {
			if (list == null || list.isEmpty()) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(collection, "The class must not be null");
	 * </code>
	 *
	 * @param collection the collection to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final Collection<?> collection, final String message) {
		if (collection == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(collection1, collection2, "The class must not be null");
	 * </code>
	 *
	 * @param collection1 the collection array to check
	 * @param collection2 the collection array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final Collection<?> collection1, final Collection<?> collection2, final String message) {
		if (collection1 == null || collection2 == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(new collection[] {collection1, collection2}, "The class must not be null");
	 * </code>
	 *
	 * @param collections the collection array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final Collection<?>[] collections, final String message) {
		if (collections == null) {
			throw new IllegalArgumentException(message);
		}
		for (Collection<?> collection : collections) {
			if (collection == null) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(collection, "The class must not be null");
	 * </code>
	 *
	 * @param collection the collection to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final Collection<?> collection, final String message) {
		if (collection == null || collection.isEmpty()) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(collection1, collection2, "The class must not be null");
	 * </code>
	 *
	 * @param collection1 the collection array to check
	 * @param collection2 the collection array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final Collection<?> collection1, final Collection<?> collection2,
			final String message) {
		if ((collection1 == null || collection1.isEmpty()) || (collection2 == null || collection2.isEmpty())) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(new collection[] {collection1, collection2}, "The class must not be null");
	 * </code>
	 *
	 * @param collections the collection array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final Collection<?>[] collections, final String message) {
		if (collections == null || collections.length <= 0) {
			throw new IllegalArgumentException(message);
		}
		for (Collection<?> collection : collections) {
			if (collection == null || collection.isEmpty()) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(map, "The class must not be null");
	 * </code>
	 *
	 * @param map the map to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final Map<?, ?> map, final String message) {
		if (map == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(map1, map2, "The class must not be null");
	 * </code>
	 *
	 * @param map1 the map array to check
	 * @param map2 the map array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final Map<?, ?> map1, final Map<?, ?> map2, final String message) {
		if (map1 == null || map2 == null) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notNull(new map[] {map1, map2}, "The class must not be null");
	 * </code>
	 *
	 * @param maps the map array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notNull(final Map<?, ?>[] maps, final String message) {
		if (maps == null) {
			throw new IllegalArgumentException(message);
		}
		for (Map<?, ?> map : maps) {
			if (map == null) {
				throw new IllegalArgumentException(message);
			}
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(map, "The class must not be null");
	 * </code>
	 *
	 * @param map the map to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final Map<?, ?> map, final String message) {
		if (map == null || map.isEmpty()) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(map1, map2, "The class must not be null");
	 * </code>
	 *
	 * @param map1 the map array to check
	 * @param map2 the map array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final Map<?, ?> map1, final Map<?, ?> map2, final String message) {
		if ((map1 == null || map1.isEmpty()) || (map2 == null || map2.isEmpty())) {
			throw new IllegalArgumentException(message);
		}
	}

	/**
	 * Assert that an object is not {@code null} .
	 * <p>
	 * <code>
	 * AssertUtils.notBlank(new map[] {map1, map2}, "The class must not be null");
	 * </code>
	 *
	 * @param maps the map array to check
	 * @param message the exception message to use if the assertion fails
	 * @throws IllegalArgumentException if the object is {@code null}
	 */
	public static void notBlank(final Map<?, ?>[] maps, final String message) {
		if (maps == null || maps.length <= 0) {
			throw new IllegalArgumentException(message);
		}
		for (Map<?, ?> map : maps) {
			if (map == null || map.isEmpty()) {
				throw new IllegalArgumentException(message);
			}
		}
	}

}
