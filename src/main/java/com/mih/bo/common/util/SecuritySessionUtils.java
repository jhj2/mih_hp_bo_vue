package com.mih.bo.common.util;

import org.springframework.security.core.context.SecurityContextHolder;

import com.mih.bo.security.CustomUserDetails;
import com.mih.bo.security.entity.UserEntity;

public class SecuritySessionUtils {

	public static CustomUserDetails getUserDetails() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(principal instanceof String) {
			return new CustomUserDetails();
		} else {
			CustomUserDetails userDetails = (CustomUserDetails) principal;
			return userDetails;
		}
	}
	
	public static UserEntity getUser() {
		if(getUserDetails().isEnabled()) {
			return getUserDetails().getUser();
		} else {
			return null;
		}
	}
}