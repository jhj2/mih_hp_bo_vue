package com.mih.bo.common.util;

import org.springframework.stereotype.Repository;

/**
 * 파일처리 관련 유틸리티
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021.07.23
 */
@Repository
public class FileMngUtil{
	
//	private static final Logger LOG = LoggerFactory.getLogger(FileMngUtil.class);
//	public static final int BUFF_SIZE = 2048;
//	
//	// 업로드 금지파일확장자(210723 추가)
//	@Value("${file.upload.not.ext}")
//	private String fileUploadNotExt;
//	
//	/**
//	 * 응용어플리케이션에서 고유값을 사용하기 위해 시스템에서 17자리의 Timestamp값을 구하는 기능
//	 * @return Timestamp 값
//	 */
//	public static String getTimeStamp() {
//
//		String rtnStr = null;
//	
//		// 문자열로 변환하기 위한 패턴 설정(년도-월-일 시:분:초:초(자정이후 초))
//		String pattern = "yyyyMMddhhmmssSSS";
//	
//		try {
//			SimpleDateFormat sdfCurrent = new SimpleDateFormat(pattern, Locale.KOREA);
//			Timestamp ts = new Timestamp(System.currentTimeMillis());
//			rtnStr = sdfCurrent.format(ts.getTime());
//		}catch (Exception e){
//			//log.error(StringUtils.printStack(e));
//			//throw new RuntimeException(e);	// 보안점검 후속조치
//			LOG.debug("IGNORED=======" + e.getMessage());
//		}
//		return rtnStr;
//	}
//	
//	/**
//	 * 첨부파일을 서버에 저장
//	 */
//	public void writeUploadedFile(MultipartFile file, String newName, String stordFilePath) throws Exception {
//		
//		// 업로드 금지파일인 경우 에러 발생하도록 추가(210406 추가)
//		if(isNotValidFileExt(newName)){
//			throw new NotUploadFileException("안내/확장자 .gif, .exe는 파일업로드가 불가합니다.");
//		}
//		
//		InputStream stream = null;
//		OutputStream bos = null;
//
//		try{
//			stream = file.getInputStream();
//			File cFile = new File(stordFilePath);
//			
//			if(!cFile.isDirectory()){
//				boolean _flag = cFile.mkdir();
//				if (!_flag) {
//					throw new IOException("Directory creation Failed ");
//				}
//			}
//	
//			bos = new FileOutputStream(stordFilePath + File.separator + newName);
//			int bytesRead = 0;
//			byte[] buffer = new byte[BUFF_SIZE];
//		
//			while((bytesRead = stream.read(buffer, 0, BUFF_SIZE)) != -1){
//				bos.write(buffer, 0, bytesRead);
//			}
//		}catch (Exception e){
//			//log.error(StringUtils.printStack(e));
//			LOG.error("IGNORE:", e);	// 2011.10.10 보안점검 후속조치
//		}finally{
//			if (bos != null){
//				try{
//					bos.close();
//				}catch (Exception ignore){
//					LOG.debug("IGNORED=====" + ignore.getMessage());
//				}
//			}
//			
//			if(stream != null){
//				try {
//					stream.close();
//				}catch (Exception ignore){
//					LOG.debug("IGNORED=====" + ignore.getMessage());
//				}
//			}
//		}
//	}
//	
//	/**
//	 * 업로드 금지파일인지 조회
//	 */
//	private boolean isNotValidFileExt(String fileName){
//		fileName = fileName.toLowerCase();
//		
//		String fileExtList = fileUploadNotExt;	// String fileExtList = egovMessageSource.getMessage("not_upload_file_ext");
//		//log.info("fileExtList========="+fileExtList);
//		
//		Pattern pattern = Pattern.compile("^.+\\.("+fileExtList+")$");	// 업로드금지파일확장자
//		Matcher matcher = pattern.matcher(fileName);
//		
//		if(matcher.find()){
//			return true;
//		}
//		return false;
//	}
//	
//	/**
//	 * 메인호출함수
//	 */
//	public static void main(String args[]){
//		
//		FileMngUtil obj = new FileMngUtil();
//		
//		String fileName = "gif";
//		log.info(fileName+"==>"+obj.isNotValidFileExt(fileName));
//		
//		fileName = "asd.exe";
//		log.info(fileName+"==>"+obj.isNotValidFileExt(fileName));
//		
//		fileName = "asd.gif";
//		log.info(fileName+"==>"+obj.isNotValidFileExt(fileName));
//		
//		fileName = "asd.js";
//		log.info(fileName+"==>"+obj.isNotValidFileExt(fileName));
//		
//		fileName = "asd.jsp.asd";
//		log.info(fileName+"==>"+obj.isNotValidFileExt(fileName));
//		
//		fileName = "asd.asp";
//		log.info(fileName+"==>"+obj.isNotValidFileExt(fileName));
//	}
//	
//	public List<FileUpload> convertFileInfo(int fileIdx, MultipartHttpServletRequest request) {
//		if(ObjectUtils.isEmpty(request)) {
//			return null;
//		}
//		List<FileUpload> fileList = new ArrayList<>();
//		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMdd");
//		ZonedDateTime current = ZonedDateTime.now();
//		String path = "image/"+current.format(format);
//		File file = new File(path);
//		
//		if(file.exists() == false) {
//			file.mkdirs();
//		}
//		
//		Iterator<String> iterator = request.getFileNames();
//		
//		String newFileNm;
//		String originalFileExtention;
//		String contentType;
//		
//		while(iterator.hasNext()) {
//			List<MultipartFile> list = request.getFiles(iterator.next());
//			for (MultipartFile multipartFile : list) {
//				if (multipartFile.isEmpty() == false) {
//					contentType = multipartFile.getContentType();
//					if (ObjectUtils.isEmpty(contentType)) {
//						break;
//					} else {
//						if (contentType.contains("image/jpeg")) {
//							originalFileExtention = ".jpg";
//						} else if(contentType.contains("image/png")) {
//							originalFileExtention = ".png";
//						} else if(contentType.contains("image/gif")) {
//							originalFileExtention = ".gif";
//						} else {
//							break;
//						}
//					}
//					
//					newFileNm = Long.toString(System.nanoTime())+originalFileExtention;
//					FileUpload fileVO = new FileUpload();
//					fileVO.setFileOrgNm(multipartFile.getOriginalFilename());
//					fileVO.setFileSysNm(multipartFile.getOriginalFilename());
//					fileList.add(fileVO);
//					
//					file = new File(path+"/"+newFileNm);
//					try {
//						multipartFile.transferTo(file);
//					} catch (IllegalStateException e) {
//						// TODO Auto-generated catch block
//						log.error(StringUtils.printStack(e));
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						log.error(StringUtils.printStack(e));
//					}
//				}
//			}
//		}
//		return fileList;
//	}

}