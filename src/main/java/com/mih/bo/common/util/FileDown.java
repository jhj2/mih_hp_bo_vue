package com.mih.bo.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mih.bo.common.vo.FileUpload;

/**
 * 첨부파일 다운로드를 위한 컨트롤러 클래스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.02.07
 */
@Controller
public class FileDown {

	private static final Logger log = Logger.getLogger(FileDown.class.getName());
	
	@Value("${file.upload.path}")
	private String fileUploadPath;

	/**
	 * 첨부파일로 등록된 파일에 대하여 다운로드를 제공
	 *
	 * @param request
	 * @param response
	 * @param commandMap
	 * @param strBbsType
	 * @throws Exception void
	*/
	@GetMapping(value="/com/fileDown")
	public void fileDown(HttpServletRequest request, HttpServletResponse response // Map<String, Object> commandMap
			, @RequestParam(value="sysPath", required=false) String sysPath
			, @RequestParam(value="attPath", required=false) String attPath
			, @RequestParam(value="orgFileNm", required=false) String orgFileNm
			, @RequestParam(value="saveFileNm", required=false) String saveFileNm
		) throws Exception {
	
		String downPath = sysPath + attPath;
		FileUpload fileVO = new FileUpload();
//		downPath = fileUploadPath;
//		downPath = downPath+"/"+strBbsType.toLowerCase();
		
		fileVO.setFileOrgNm(orgFileNm);
		fileVO.setFileSysNm(saveFileNm);
		
		File uFile = new File(downPath, fileVO.getFileSysNm());
		int fSize = (int) uFile.length();

		String mimetype = "application/x-msdownload";

		response.setBufferSize(fSize);
		response.setContentType(mimetype);
		setDisposition(fileVO.getFileOrgNm(), request, response);
		response.setContentLength(fSize);

		BufferedInputStream in = null;
		BufferedOutputStream out = null;

		try {
			in = new BufferedInputStream(new FileInputStream(uFile));
			out = new BufferedOutputStream(response.getOutputStream());
	
			FileCopyUtils.copy(in, out);
			out.flush();
		}catch (Exception ex){
			// ex.printStackTrace();
			// 다음 Exception 무시 처리
			// Connection reset by peer: socket write error
			log.debug("IGNORED: " + ex.getMessage());
		}finally{
			
			if(in != null){
				try {
					in.close();
				}catch (Exception ignore){
					// no-op
					log.debug("IGNORED: " + ignore.getMessage());
				}
			}
			
			if(out != null){
				try {
					out.close();
				} catch (Exception ignore) {
					// no-op
					log.debug("IGNORED: " + ignore.getMessage());
				}
			}
		}
	}
	
	/**
	 * Disposition 지정
	 *
	 * @param filename
	 * @param request
	 * @param response
	 * @throws Exception void
	*/
	private void setDisposition(String filename, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String browser = getBrowser(request);
	
		String dispositionPrefix = "attachment; filename=";
		String encodedFilename = null;
	
		if(browser.equals("MSIE")) {
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
		}else if (browser.equals("Firefox")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		}else if (browser.equals("Opera")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		}else if (browser.equals("Chrome")) {
			StringBuffer sb = new StringBuffer();
			for(int i=0; i<filename.length(); i++) {
				char c = filename.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			encodedFilename = sb.toString();
		}else{
			//throw new RuntimeException("Not supported browser");
			throw new IOException("Not supported browser");
		}
		response.setHeader("Content-Disposition", dispositionPrefix + encodedFilename);
	
		if("Opera".equals(browser)){
			response.setContentType("application/octet-stream;charset=UTF-8");
		}
	}
	
	/**
	 * 브라우저 구분 얻기
	 *
	 * @param request
	 * @return String
	*/
	private String getBrowser(HttpServletRequest request){
		String header = request.getHeader("User-Agent");
		if (header.indexOf("MSIE") > -1 || header.indexOf("Trident") > -1){
			return "MSIE";
		}else if (header.indexOf("Chrome") > -1){
			return "Chrome";
		}else if (header.indexOf("Opera") > -1){
			return "Opera";
		}
		return "Firefox";
	}
}