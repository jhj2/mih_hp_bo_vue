package com.mih.bo.common.util;

import java.util.Locale;

import org.springframework.context.support.MessageSourceAccessor;

public class MessageUtils {

	private static MessageSourceAccessor messageSourceAccessor = null;

	public void setMessageSourceAccessor(MessageSourceAccessor messageSourceAccessor) {
		MessageUtils.messageSourceAccessor = messageSourceAccessor;
	}

	/**
	 * KEY에 해당하는 메세지 반환
	 *
	 * @param key
	 * @return
	 */
	private static String getMessage(String key, Object[] objs, Locale locale) {
		try {
			if(objs == null) {
				return messageSourceAccessor.getMessage(key, locale);
			}else {
				return messageSourceAccessor.getMessage(key, objs, locale);
			}
		} catch (Exception e) {
			return messageSourceAccessor.getMessage("nomatch.msg", locale);
		}
	}

	/**
	 * KEY에 해당하는 메세지 반환
	 *
	 * @param key
	 * @return
	 */
	public static String getMessage(String key) {
		return getMessage(key, null, Locale.getDefault());
	}

	/**
	 * KEY에 해당하는 메세지 반환
	 *
	 * @param key
	 * @return
	 */
	public static String getMessage(String key, Object[] objs) {
		return getMessage(key, objs, Locale.getDefault());
	}

}