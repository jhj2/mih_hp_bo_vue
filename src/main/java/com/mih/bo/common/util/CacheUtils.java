package com.mih.bo.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

public class CacheUtils {

	private static final Logger log = LoggerFactory.getLogger(CacheUtils.class);

	private static final String SEPARATOR = ".";

	private CacheUtils() {
		// ignore...
	}

	public static final String generate(Object clazz, Object method, Object... params) {
		int length = params.length;
		StringBuilder sb = new StringBuilder();
		sb.append(clazz).append(SEPARATOR).append(method);
		for (int i = 0; i < length; i++) {
			sb.append(SEPARATOR).append(params[i].toString());
		}
		String key = UUIDUtils.sha256(sb.toString());
		if (log.isDebugEnabled()) {
			log.debug("Original Cache Key: {}, SHA256 Cache Key: {}", sb.toString(), key);
		}
		return key;

	}

	/**
	 * Cache All Clear
	 *
	 * @param cacheManager CacheManager
	 * @param cacheName Cache Name.
	 */
	public static final void cacheAllClear(final CacheManager cacheManager, final String cacheName) {
		if (log.isInfoEnabled()) {
			log.info("Cache All Clear... [CacheName: {}]", cacheName);
		}
		Cache cache = cacheManager.getCache(cacheName);
		cache.clear();
	}

	/**
	 * Cache Clear
	 *
	 * @param cacheManager CacheManager
	 * @param cacheName Cache Name.
	 * @param className Class Name.
	 * @param methodName Method Name.
	 */
	@SuppressWarnings("all")
	public static final void cacheClear(final CacheManager cacheManager, final String cacheName, final String className,
			final String methodName) {
		cacheClear(cacheManager, cacheName, className, methodName, null);
	}

	/**
	 * Cache Clear
	 *
	 * @param cacheManager CacheManager
	 * @param cacheName Cache Name.
	 * @param className Class Name.
	 * @param methodName Method Name.
	 * @param params Parameters.
	 */
	@SuppressWarnings("all")
	public static final void cacheClear(final CacheManager cacheManager, final String cacheName, final String className,
			final String methodName, final String... params) {
		String key = generate(className, methodName, params);
		if (log.isInfoEnabled()) {
			log.info("Cache Clear... [CacheName: {}, CacheKey: {}]", cacheName, key);
		}
		Cache cache = cacheManager.getCache(cacheName);
		cache.evict(key);
	}

}
