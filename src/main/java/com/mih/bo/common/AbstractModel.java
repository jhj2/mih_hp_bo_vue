package com.mih.bo.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 공통 상속 모델
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@ToString @Getter @Setter
public class AbstractModel extends BaseObject {

	private static final long serialVersionUID = 1891216015058327208L;

	/**
	 * 등록일자
	 */
	private String regDt;
	private String registDt;
	
	/**
	 * 등록아이피
	 */
	private String regIp;
	private String registIp;
	
	/**
	 * 등록아이디
	 */
	private String regId;
	private String registId;
	
	/**
	 * 등록자명
	 */
	private String regNm;
	private String registNm;
	
	/**
	 * 수정일자
	 */
	private String updDt;
	private String updtDt;
	
	/**
	 * 수정아이피
	 */
	private String updIp;
	private String updtIp;
	
	/**
	 * 수정아이디
	 */
	private String updId;
	private String updtId;
	
	/**
	 * 수정자명
	 */
	private String updNm;
	private String updtNm;
	
	/**
	 * limit
	 */
	private int limit;
	
	/**
	 * offset
	 */
	private int offset;
}