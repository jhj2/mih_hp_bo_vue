package com.mih.bo.common.enums;

import lombok.Getter;

public enum ExcelTypes {
	SAMPLE("sample.xlsx", "샘플 엑셀 템플릿", ""),
	SAMPLE2("sample2.xlsx", "샘플 엑셀 템플릿", "Sheet1!A10");
	
	@Getter
	private String fileName;
	
	@Getter
	private String desc;
	
	@Getter
	private String targetCell;
	
	ExcelTypes(String fileName, String desc, String targetCell) {
		this.fileName = fileName;
		this.desc = desc;
		this.targetCell = targetCell;
	}
}