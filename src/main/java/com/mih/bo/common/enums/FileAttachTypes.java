package com.mih.bo.common.enums;

import com.mih.bo.common.config.Property;
import com.mih.bo.common.util.StringUtils;

public enum FileAttachTypes {

	/** 공지사항 */
	NOTICE("NOTICE", Property.get("file.attach.notice.path"), "TB_CO_NOTICE"),

	/** 1:!문의 */
	INQUIRY("INQUIRY", Property.get("file.attach.inquiry.path"), "TB_CO_INQRY"), 

	/** 지부 */
	BRANCH("BRANCH", Property.get("file.attach.branch.path"), "TB_BR_BHF"),

	/** 가격 */
	PRICE("PRICE", Property.get("file.attach.price.path"), "TB_GOODS_PRC_HST_MST"),

	/** 배너 */
	BANNER("BANNER", Property.get("file.attach.banner.path"), "TB_DM_BANNER"),

	/** 팝업 */
	POPUP("POPUP", Property.get("file.attach.popup.path"), "TB_DM_POPUP"),
	
	/** 종료신청 */
	END_REQUEST("END_REQUEST", Property.get("file.attach.endrequest.path"), "TB_END_REQST"),
	
	/** 종료신청 */
	EDITOR("EDITOR", Property.get("file.attach.editor.path"), null),
	
	/** 통합게시판 경로 */
	BBS("BBS", Property.get("file.attach.bbs.path"), "BBS");

	private String value;
	
	private String path;
	
	private String table;
	

	private FileAttachTypes(final String value, final String path, final String table) {
		this.path = path;
		this.value = value;
		this.table = table;
	}
	
	/**
	 * Getter Value
	 *
	 * @return the value
	 */
	public String value() {
		return value;
	}
	
	/**
	 * Getter Value
	 *
	 * @return the path
	 */
	public String path() {
		return path;
	}

	/**
	 * Getter Value
	 *
	 * @return the table
	 */
	public String table() {
		return table;
	}

	/**
	 * Getter EnvTypes
	 *
	 * @param value the value
	 * @return EnvTypes
	 */
	public static FileAttachTypes getObject(final String value) {
		for (FileAttachTypes type : values()) {
			if (StringUtils.equalsAnyIgnoreCase(value, type.value())) {
				return type;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.value;
	}
}