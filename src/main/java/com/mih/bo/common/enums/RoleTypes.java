package com.mih.bo.common.enums;

import com.mih.bo.common.util.StringUtils;

/**
 * 권한 유형.
 *
 * @author
 * @version 1.0.0
 * @since 7.0
 */
public enum RoleTypes {

	/** 어드민. */
	ROLE_ADMIN("ROLE_ADMIN"),

	/** 일반 사용자. */
	ROLE_USER("ROLE_USER"),

	/** 권한이 없는 사용자. */
	ROLE_ANONYMOUS("ROLE_ANONYMOUS");

	private String value;

	private RoleTypes(String value) {
		this.value = value;
	}

	/**
	 * Getter Value
	 *
	 * @return the value
	 */
	public String value() {
		return value;
	}

	/**
	 * Value를 이용해서 Enum Object를 리턴한다.
	 *
	 * @param value the value
	 * @return EnvTypes
	 */
	public static RoleTypes getObject(String value) {
		for (RoleTypes type : values()) {
			if (StringUtils.equalsAnyIgnoreCase(value, type.value())) {
				return type;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.value;
	}

}
