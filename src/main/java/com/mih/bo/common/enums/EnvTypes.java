package com.mih.bo.common.enums;

import com.mih.bo.common.util.StringUtils;

public enum EnvTypes {

	/** 로컬 */
	LOCAL("local"),

	/** 개발 */
	DEVELOP("dev"),

	/** 통합 */
	INTEGRATION("integration"),

	/** 테스트 */
	QA("qa"),

	/** 스테이징 */
	STAGING("stag"),

	/** 상용 */
	PRODUCTION("prod");

	private String value;

	private EnvTypes(final String value) {
		this.value = value;
	}

	/**
	 * Getter Value
	 *
	 * @return the value
	 */
	public String value() {
		return value;
	}

	/**
	 * Getter EnvTypes
	 *
	 * @param value the value
	 * @return EnvTypes
	 */
	public static EnvTypes getObject(final String value) {
		for (EnvTypes type : values()) {
			if (StringUtils.equalsAnyIgnoreCase(value, type.value())) {
				return type;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.value;
	}

}
