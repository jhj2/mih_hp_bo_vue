package com.mih.bo.common.yaml;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mih.bo.common.util.StringUtils;


/**
 * Datasource Property
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataSourceProperty extends BaseProperty {

	private static final long serialVersionUID = -388260484798055119L;

	/**
	 * Database Driver
	 */
	private String driver;

	/**
	 * Database JDBC URL
	 */
	private String url;

	/**
	 * Database User Name
	 */
	private String username;

	/**
	 * Database Password
	 */
	private String password;

	/**
	 * Connection
	 * DB
	 */
	private String validationQuery;

	/**
	 * DataSource
	 */
	private int initSize;

	/**
	 * Database IvParameterSpec
	 *
	 * @return the driver
	 */
	public String getDriver() {
		return driver;
	}

	/**
	 * Database Driver
	 *
	 * @param driver the driver to set
	 */
	public void setDriver(final String driver) {
		this.driver = StringUtils.trim(driver);
	}

	/**
	 * Database Driver
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Database JDBC URL
	 *
	 * @param url the url to set
	 */
	public void setUrl(final String url) {
		this.url = StringUtils.trim(url);
	}

	/**
	 * Database User Name
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Database User Name
	 *
	 * @param username the username to set
	 */
	public void setUsername(final String username) {
		this.username = StringUtils.trim(username);
	}

	/**
	 * Database Password
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Database Password
	 *
	 * @param password the password to set
	 */
	public void setPassword(final String password) {
		this.password = StringUtils.trim(password);
	}

	/**
	 * Connection
	 * DB
	 *
	 * @return the validationQuery
	 */
	public String getValidationQuery() {
		return validationQuery;
	}

	/**
	 * Connection
	 * DB
	 *
	 * @param validationQuery the validationQuery to set
	 */
	public void setValidationQuery(final String validationQuery) {
		this.validationQuery = validationQuery;
	}

	/**
	 * DataSource
	 *
	 * @return the initSize
	 */
	public int getInitSize() {
		return initSize;
	}

	/**
	 * DataSource
	 *
	 * @param initSize the initSize to set
	 */
	public void setInitSize(final int initSize) {
		this.initSize = initSize;
	}

}
