package com.mih.bo.common.yaml;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 어플리케이션 정보
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Component
@ConfigurationProperties(prefix = "info")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppInfoProperty {

	/**
	 * Environment
	 */
	private String env;

	/**
	 * App Version
	 */
	private String version;

	/**
	 * App Name
	 */
	private String name;

	/**
	 * App Description
	 */
	private String description;

	/**
	 * Environment
	 *
	 * @return the env
	 */
	public String getEnv() {
		return env;
	}

	/**
	 * Environment
	 *
	 * @param env the env to set
	 */
	public void setEnv(final String env) {
		this.env = env;
	}

	/**
	 * App Version
	 *
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * App Version
	 *
	 * @param version the version to set
	 */
	public void setVersion(final String version) {
		this.version = version;
	}

	/**
	 * App Name
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * App Name
	 *
	 * @param name the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * App Description
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * App Description
	 *
	 * @param description the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

}
