package com.mih.bo.common.yaml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mih.bo.common.BaseObject;


/**
 * Base Property
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseProperty extends BaseObject {

	private static final long serialVersionUID = 5750206927664978128L;

	/**
	 * Comment
	 */
	private String comment;

	/**
	 * minIdle
	 */
	private Integer minIdle;

	/**
	 * maxIdle
	 */
	private Integer maxIdle;

	/**
	 * maxTotal
	 */
	private Integer maxTotal;

	/**
	 * maxWait
	 */
	private Long maxWait;

	/**
	 * Comment
	 *
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Comment
	 *
	 * @param comment the comment to set
	 */
	public void setComment(final String comment) {
		this.comment = comment;
	}

	/**
	 * getter minIdle
	 *
	 * @return the minIdle
	 */
	public Integer getMinIdle() {
		return minIdle;
	}

	/**
	 * setter minIdle
	 *
	 * @param minIdle the minIdle to set
	 */
	public void setMinIdle(final Integer minIdle) {
		this.minIdle = minIdle;
	}

	/**
	 * getter maxIdle
	 *
	 * @return the maxIdle
	 */
	public Integer getMaxIdle() {
		return maxIdle;
	}

	/**
	 * setter maxIdle
	 *
	 * @param maxIdle the maxIdle to set
	 */
	public void setMaxIdle(final Integer maxIdle) {
		this.maxIdle = maxIdle;
	}

	/**
	 * getter maxTotal
	 *
	 * @return the maxTotal
	 */
	public Integer getMaxTotal() {
		return maxTotal;
	}

	/**
	 * setter maxTotal
	 *
	 * @param maxTotal the maxTotal to set
	 */
	public void setMaxTotal(final Integer maxTotal) {
		this.maxTotal = maxTotal;
	}

	/**
	 * getter maxWait
	 *
	 * @return the maxWait
	 */
	public Long getMaxWait() {
		return maxWait;
	}

	/**
	 * setter maxWait
	 *
	 * @param maxWait the maxWait to set
	 */
	public void setMaxWait(final Long maxWait) {
		this.maxWait = maxWait;
	}

}
