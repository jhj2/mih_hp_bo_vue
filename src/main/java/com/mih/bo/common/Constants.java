package com.mih.bo.common;

/**
 * 상수정의
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public class Constants {

	public static final String LOG_LINE = "======================================================";
	public static final String SYSTEM_ID = "SYSTEM";

	public static final String YES = "Y";
	public static final String NO = "N";

	public static final String DOT = ".";
	public static final String COMMA = ",";
	public static final String BAR = "|";

	/**
	 * Default Cache Name
	 */
	public static final String DEFAULT_CACHE = "defaultCache";
	public static final String PAGING_NAME = "paging";
	
	/**
	 * Device Type
	 */
	public static final String MOBILE = "M";
	public static final String PC = "P";

	public static final String YYYYMMDDHHMM = "yyyyMMddHHmm";
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	public static final String YYYYMMDD = "yyyyMMdd";
	
	/** 요청 종류 */
	public static final String PAY_REQUEST_TYPE = "pay";
	public static final String PAY_RES_CD = "0000";

	/** 배치 작업 관련 코드 */
	public static final String BATCH_USER_SCHEDULED = "SYSTEM-BATCH";

	public static final String BATCH_RESULT_START = "S";
	public static final String BATCH_RESULT_END = "E";
	public static final String BATCH_RESULT_FAIL = "F";
	
	/** 배치 실행 이력 */
	public static final String BATCH_ID_CABINET_AUTO_PAYMENT = "100";
	public static final String BATCH_NM_CABINET_AUTO_PAYMENT = "정기결제";
	public static final String BATCH_NM_CABINET_AUTO_PAYMENT_GUIDE = "정기결제 및 계약만료 안내";

	public static final String BATCH_ID_NOTICE_DORMANT = "130";
	public static final String BATCH_NM_NOITCE_DORMANT = "휴면회원 전환 안내";

	public static final String BATCH_ID_SAMEDAY_SERVICE_INFORMATION = "160";
	public static final String BATCH_NM_SAMEDAY_SERVICE_INFORMATION = "이용 당일 서비스 안내";

	/** 채널타입 */
	public static final String CHENNEL_TYPE = "P";

	/**
	 * 등록자 유형
	 */
	public static final String REG_TY_MEMBER = "M";
	public static final String REG_TY_ADMIN = "A";
	public static final String REG_TY_SYSTEM = "S";
	
	/**
	 * 사용여부
	 */
	public static final String USE_YN = "A001";
	
	/**
	 * 댓글 정렬방식
	 */
	public static final String CMT_ORD_WAY = "A002";
	
	/**
	 * 읽기 권한
	 */
	public static final String RD_AUTH = "A003";
	
	/**
	 * 쓰기 권한
	 */
	public static final String WT_AUTH = "A004";
	
	/**
	 * 마케팅 정보 수신 동의
	 */
	public static final String STPLST_CD_AT = "A0303"; // 알림톡
	public static final String STPLST_CD_EMAIL = "A0304"; // 이메일
	public static final String STPLST_CD_TM = "A0305"; // TM(전화)
	
	/** 이용상태 */
	public static final String CABINET_USE_CD = "A06";
	public static final String USE_WAIT = "A0601";
	public static final String USE = "A0602";
	public static final String USE_EXTEND = "A0603";
	public static final String USE_WAIT_END = "A0604";
	public static final String USE_END = "A0605";

	/** 주문상태 */
	public static final String ORDER_WAIT = "A0701";
	public static final String ORDER_SUCCESS = "A0702";
	public static final String ORDER_REFUND = "A0703";
	public static final String ORDER_CANCEL = "A0704";
	public static final String ORDER_FAIL = "A0705";

	/** 결제상태 */
	public static final String PAY_WAIT = "A0801";
	public static final String PAY_SUCCESS = "A0802";
	public static final String PAY_FAIL = "A0803";
	public static final String PAY_END = "A0804";
	
	/**
	 * 캐비닛보관 상품 공통코드
	 */
	public static final String CABINET_GOODS_CD = "A1301";
	
	/** 개인정보조회사유 */
	public static final String PRIVATE_DATA_SEARCH_REASON = "A14";

	/** 정보조회대상유형코드 */
	public static final String PRIVATE_DATA_MEMBER = "A1501";
	public static final String PRIVATE_DATA_TOUR_REQUESTER = "A1502";
	
	/**
	 * 지부투어방문에약시간
	 */
	public static final String TOUR_HOPE_TIME_TY_CD = "A16";
	
	/**
	 * 변경이력유형코드
	 */
	public static final String CHANGE_HIST_TY_CD = "A18"; // 변경이력유형
	public static final String CHANGE_HIST_TY_BGNDE = "A1801"; // 이용희망일
	public static final String DATA_CHANGE_TYPE_A1803 = "A1803";
	
	/** 약관유형 **/
	public static final String STPLAT_TYPE_CD_A1901 = "A1901";
	public static final String STPLAT_TYPE_CD_A1902 = "A1902";
	
	/** 정기결제 유형 */
	public static final String FIXD_TERM_PAY_TY = "A20";
	public static final String FIXED_TERM_CODE = "A2002";
	
	/**
	 * 엑셀다운로드 파일명
	 */
	public static final String BO_TITLE_NM_CABINET_DORMANT = "캐비닛자산관리";
	public static final String BO_TITLE_NM_CABINET_ORDER_DORMANT = "캐비닛서비스관리";
	public static final String BO_TITLE_NM_INQUIRY_DORMANT = "1:1문의관리";
}