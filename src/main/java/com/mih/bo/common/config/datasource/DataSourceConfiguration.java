package com.mih.bo.common.config.datasource;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mih.bo.common.yaml.DataSourceProperty;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
public class DataSourceConfiguration extends HikariDataSourceConfiguration {

	/**
	 *  DataSource Configuration 생성.
	 */
	public DataSourceConfiguration() {
		mybatisConfigFile = "com/config/mybatis-config.xml";
		mybatisMapperPath = "classpath*:com/mapper/*.xml";

		alias = new StringBuilder();
		alias.append("com.mih.bo.task.message.vo").append(",");
		alias.append("com.mih.bo.task.code.vo").append(",");
		alias.append("com.mih.bo.task.branch.vo").append(",");
		alias.append("com.mih.bo.common.vo").append(",");
		alias.append("com.mih.bo.security.entity").append(",");
		alias.append("com.mih.bo.task.user.vo").append(",");
		alias.append("com.mih.bo.task.branch.vo").append(",");
		alias.append("com.mih.bo.task.member.vo").append(",");
		alias.append("com.mih.bo.task.complaint.vo").append(",");
		alias.append("com.mih.bo.task.bbs.vo").append(",");
		alias.append("com.mih.bo.task.org.vo").append(",");
	}

	/**
	 * DataSource Property 설정.
	 */
	@Override
	@Bean(name = "dataSourceProperty")
	@ConfigurationProperties(prefix = "datasource")
	public DataSourceProperty dataSourceProperty() {
		return new DataSourceProperty();
	}

	/**
	 * Data Source 설정.
	 *
	 * @return BasicDataSource
	 */
	@Override
	@Bean(name = "dataSource")
	public DataSource dataSource() {
		this.property = dataSourceProperty();
		return super.dataSource();
	}

	/**
	 * Sql Session Factory 설정/
	 *
	 * @return SqlSessionFactory
	 * @throws Exception
	 */
	@Override
	@Bean(name = "sqlSessionFactory")
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		return super.sqlSessionFactory();
	}

	/**
	 * 마이바티스 {@link org.apache.ibatis.session.SqlSession} 빈을 등록한다.
	 *
	 * SqlSessionTemplate은 SqlSession을 구현하고 코드에서 SqlSession를 대체하는 역할을 한다.
	 * 쓰레드에 안전하게 작성되었기 때문에 여러 DAO나 매퍼에서 공유 할 수 있다.
	 *
	 * @return SqlSessionTemplate
	 * @throws Exception
	 */
	@Bean(name = "sqlSessionTemplate", destroyMethod = "clearCache")
	public SqlSessionTemplate sqlSessionTemplate() throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory());
	}

	@Bean(name = "transactionManager")
	public PlatformTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource());
	}
}