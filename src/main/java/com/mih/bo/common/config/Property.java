package com.mih.bo.common.config;

import java.io.Serializable;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mih.bo.common.util.NumberUtils;
import com.mih.bo.common.util.StringUtils;

public class Property implements Serializable {

	private static final long serialVersionUID = -2648173524002251113L;

	private static final Logger log = LoggerFactory.getLogger(Property.class);

	private static Properties properties;

	private Property() {
		// ignore.
	}

	/**
	 * Property를 설정한다.
	 *
	 * @param prop Set property.
	 */
	public static void set(final Properties prop) {
		if (Property.properties == null) {
			log.info("Setting Properties:: {}", prop);
			Property.properties = prop;
		} else {
			log.info("Properties is not null...");
		}
	}

	/**
	 * Property Key의 String Value 리턴.
	 *
	 * @param key Property Key.
	 * @return Property string value.
	 */
	public static String get(final String key) {
		return StringUtils.trim(properties.getProperty(key));
	}

	/**
	 * Property Key의 String Value 리턴.
	 *
	 * @param key Property Key.
	 * @param searchString the String to search for, may be null
	 * @param replacement the String to replace it with, may be null
	 * @return Property string value.
	 */
	public static String get(final String key, final String searchString, final String replacement) {
		return StringUtils.replaceIgnoreCase(StringUtils.trim(properties.getProperty(key)), searchString, replacement);
	}

	/**
	 * Property Key의 String Value 리턴.
	 *
	 * @param key Property Key.
	 * @param searchList the Strings to search for, no-op if null
	 * @param replacementList the Strings to replace them with, no-op if null
	 * @return Property string value.
	 */
	public static String get(final String key, final String[] searchList, final String[] replacementList) {
		return StringUtils.replaceEach(StringUtils.trim(properties.getProperty(key)), searchList, replacementList);
	}

	/**
	 * Property Key의 Integer Value 리턴.
	 *
	 * @param key Property Key.
	 * @return the int represented by the string, or <code>0</code> if conversion fails
	 */
	public static int getInt(final String key) {
		return getInt(key, 0);
	}

	/**
	 * Property Key의 Integer Value 리턴.
	 *
	 * @param key
	 * @param defaultValue the default value
	 * @return the int represented by the string, or the default if conversion fails
	 */
	public static int getInt(String key, final int defaultValue) {
		String str = StringUtils.trim(properties.getProperty(key));
		return NumberUtils.toInt(str, defaultValue);
	}

	/**
	 * Property Key의 Long Value 리턴.
	 *
	 * @param key Property Key.
	 * @return the long represented by the string, or <code>0</code> if conversion fails
	 */
	public static long getLong(final String key) {
		return getLong(key, 0);
	}

	/**
	 * Property Key의 Long Value 리턴.
	 *
	 * @param key
	 * @param defaultValue the default value
	 * @return the long represented by the string, or the default if conversion fails
	 */
	public static long getLong(String key, final int defaultValue) {
		String str = StringUtils.trim(properties.getProperty(key));
		return NumberUtils.toLong(str, defaultValue);
	}

	/**
	 * Property Key의 Float Value 리턴.
	 *
	 * @param key Property Key.
	 * @return the float represented by the string, or <code>0.0f</code> if conversion fails
	 */
	public static float getFloat(final String key) {
		return getFloat(key, 0);
	}

	/**
	 * Property Key의 Float Value 리턴.
	 *
	 * @param key
	 * @param defaultValue the default value
	 * @return the float represented by the string, or defaultValue if conversion fails
	 */
	public static float getFloat(String key, final int defaultValue) {
		String str = StringUtils.trim(properties.getProperty(key));
		return NumberUtils.toFloat(str, defaultValue);
	}

	/**
	 * Property Key의 Float Value 리턴.
	 *
	 * @param key Property Key.
	 * @return the double represented by the string, or <code>0.0f</code> if conversion fails
	 */
	public static double getDouble(final String key) {
		return getDouble(key, 0);
	}

	/**
	 * Property Key의 Float Value 리턴.
	 *
	 * @param key
	 * @param defaultValue the default value
	 * @return the double represented by the string, or defaultValue if conversion fails
	 */
	public static double getDouble(String key, final int defaultValue) {
		String str = StringUtils.trim(properties.getProperty(key));
		return NumberUtils.toDouble(str, defaultValue);
	}

}
