package com.mih.bo.common.config;

import java.util.List;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.google.common.collect.Lists;
import com.mih.bo.common.enums.EnvTypes;
import com.mih.bo.common.util.StringUtils;

@Configuration
public class PropertyConfiguration {
	
	/**
	 * YAML Property Resource Read.
	 */
	@Bean
	public PropertySourcesPlaceholderConfigurer properties() {
		
		String env = System.getProperty("spring.profiles.active");
		
		List<Resource> resources = Lists.newArrayList();
		if(StringUtils.isEmpty(env) || env.equals(EnvTypes.LOCAL.value())) {
			resources.add(new ClassPathResource("com/property/config.yml"));
		} else {
			resources.add(new ClassPathResource("com/property/config-" + env + ".yml"));
		}

		PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
		YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
		factory.setResources(resources.toArray(new Resource[resources.size()]));

		// Property에 등록한다.
		Property.set(factory.getObject());

		configurer.setProperties(factory.getObject());
		return configurer;
	}
}