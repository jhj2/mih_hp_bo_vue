package com.mih.bo.common.config.datasource;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.mih.bo.common.enums.EnvTypes;
import com.mih.bo.common.util.StringUtils;
import com.mih.bo.common.yaml.DataSourceProperty;

import net.sf.log4jdbc.sql.jdbcapi.DataSourceSpy;

public abstract class HikariDataSourceConfiguration extends AbstractHikariDataSourceConfiguration {

	private static final Logger log = LoggerFactory.getLogger(HikariDataSourceConfiguration.class);

	/**
	 * MyBatis Config
	 */
	protected String mybatisConfigFile;

	/**
	 * MyBatis Mapper
	 */
	protected String mybatisMapperPath;

	protected StringBuilder alias;

	protected DataSourceProperty property;

	/**
	 * DataSource Property
	 */
	public abstract DataSourceProperty dataSourceProperty();

	/**
	 * Data Source
	 *
	 * @return BasicDataSource
	 */
	public DataSource dataSource() {
		if (StringUtils.equalsAnyIgnoreCase(appInfoProperty.getEnv(), EnvTypes.LOCAL.toString())) {
			log.info(">>> Data Source Spy...");
			return new DataSourceSpy(getDataSource(property));
		} else {
			log.info(">>> Data Source...");
			return getDataSource(property);
		}
	}

	/**
	 * Sql Session Factory
	 *
	 * @return SqlSessionFactory
	 * @throws Exception
	 */
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		bean.setDataSource(this.dataSource());

		// MyBatis
		bean.setConfigLocation(new ClassPathResource(mybatisConfigFile));

		// XML
		bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(mybatisMapperPath));
		bean.setTypeAliasesPackage(alias.toString());

		return bean.getObject();
	}
}