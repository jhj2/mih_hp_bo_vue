package com.mih.bo.common.config;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mih.bo.common.Constants;
import com.mih.bo.common.enums.EnvTypes;
import com.mih.bo.common.util.CacheUtils;
import com.mih.bo.common.util.StringUtils;
import com.mih.bo.common.yaml.AppInfoProperty;

import net.sf.ehcache.config.Configuration.Monitoring;

@Configuration
@EnableCaching
@SuppressWarnings("all")
public class CacheConfiguration extends CachingConfigurerSupport {

	private static final Logger log = LoggerFactory.getLogger(CacheConfiguration.class);

	/**
	 * 어플리케이션 정보 Property.
	 */
	@Autowired
	private AppInfoProperty appInfoProperty;

	@Bean
	@Override
	public CacheManager cacheManager() {
		return new EhCacheCacheManager(ehCacheManager());
	}

	@Bean
	@Override
	public KeyGenerator keyGenerator() {
		return new KeyGenerator() {
			@Override
			public Object generate(Object target, Method method, Object... params) {
				return CacheUtils.generate(target.getClass().getName(), method.getName(), params);
			}
		};
	}

	@Bean(destroyMethod = "shutdown")
	public net.sf.ehcache.CacheManager ehCacheManager() {
		net.sf.ehcache.config.Configuration config = new net.sf.ehcache.config.Configuration();
		config.addCache(getDefaultCache());

		if (StringUtils.notEqualsIgnoreCase(appInfoProperty.getEnv(), EnvTypes.PRODUCTION.name())) {
			// EHCache를 MBeas에서 모니터링할 수 있도록 설정.
			config.name("net.sf.ehcache");
			config.setMonitoring(Monitoring.ON.name());
		}
		return net.sf.ehcache.CacheManager.newInstance(config);
	}

	/**
	 * @return
	 */
	private net.sf.ehcache.config.CacheConfiguration getDefaultCache() {
		net.sf.ehcache.config.CacheConfiguration cache = new net.sf.ehcache.config.CacheConfiguration();
		cache.setName(Constants.DEFAULT_CACHE);

		// 캐싱 데이터를 영원히 유지할 것인지 여부
		// true이면 timeout 관련 설정은 무시되고, Element가 캐시에서 삭제되지 않는다.
		cache.setEternal(false);

		// 메모리에 생성될 객체의 최대 수를 설정 (0 = no limit)
		cache.setMaxEntriesLocalHeap(1000);

		// Element가 지정한 시간 동안 사용(조회)되지 않으면 캐시에서 제거된다.
		// 이 값이 0인 경우 조회 관련 만료 시간을 지정하지 않는다. 기본값은 0이다.
		cache.setTimeToIdleSeconds(60);

		// Element가 존재하는 시간. 이 시간이 지나면 캐시에서 제거된다.
		// 이 시간이 0이면 만료 시간을 지정하지 않는다. 기본값은 0이다.
		cache.setTimeToLiveSeconds(300);

		// 저장 공간이 꽉 찰 경우 데이터를 제거하는 규칙 지정
		// LRU - 데이터의 접근 시점을 기준으로 최근 접근 시점이 오래된 데이터부터 삭제
		// LFU - 데이터의 이용 빈도 수를 기준으로 이용 빈도가 가장 낮은 것부터 삭제
		// FIFO - 먼저 저장된 데이터를 우선 삭제
		cache.setMemoryStoreEvictionPolicy("LRU");
		return cache;
	}

}
