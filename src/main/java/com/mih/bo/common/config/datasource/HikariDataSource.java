package com.mih.bo.common.config.datasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mih.bo.common.util.StringUtils;

@SuppressWarnings("all")
public class HikariDataSource extends com.zaxxer.hikari.HikariDataSource {

	private static final Logger log = LoggerFactory.getLogger(HikariDataSource.class);

	/**
	 * JDBC Data Source Comment.
	 *
	 * @param comment Comment
	 */
	public void setComment(String comment) {
		log.info("DB Comment:: {}", StringUtils.trim(comment));
	}

	/**
	 * JDBC connection url
	 *
	 * @param url the new value for the JDBC connection url
	 */
	@Override
	public void setJdbcUrl(final String orgUrl) {
		synchronized (HikariDataSource.class) {
			String url = StringUtils.trim(orgUrl);
			log.debug("URL - {}", url);
			super.setJdbcUrl(url);
		}
	}

	/**
	 * JDBC connection username
	 *
	 * @param username the new value for the JDBC connection username
	 */
	@Override
	public void setUsername(final String orgUsername) {
		String username = StringUtils.trim(orgUsername);
		log.debug("USERNAME - {}", username);
		super.setUsername(username);
	}

	/**
	 * JDBC connection password
	 *
	 * @param password new value for the password
	 */
	@Override
	public void setPassword(final String orgPassword) {
		String password = StringUtils.trim(orgPassword);
		super.setPassword(password);
	}

}