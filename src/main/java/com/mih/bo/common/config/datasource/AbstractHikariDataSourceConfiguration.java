package com.mih.bo.common.config.datasource;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mih.bo.common.enums.EnvTypes;
import com.mih.bo.common.util.StringUtils;
import com.mih.bo.common.yaml.AppInfoProperty;
import com.mih.bo.common.yaml.DataSourceProperty;

public abstract class AbstractHikariDataSourceConfiguration {

	private static final Logger log = LoggerFactory.getLogger(AbstractHikariDataSourceConfiguration.class);

	@Autowired
	protected AppInfoProperty appInfoProperty;

	/**
	 * Data Source
	 *
	 * @return DataSource
	 */
	protected DataSource getDataSource(final DataSourceProperty property) {
		StringBuilder logstack = new StringBuilder();
		logstack.append(property.getComment()).append(" Data Source Initial Info...").append("\n");
		logstack.append("------------------------------------------------------------------------").append("\n");
		logstack.append("| ").append(property.getComment()).append(" Data Source Initial Info...").append("\n");
		logstack.append("------------------------------------------------------------------------").append("\n");

		logstack.append("| SERVER_ENV         | ").append(appInfoProperty.getEnv()).append("\n");
		logstack.append("| DATABASE_ENV       | ").append(property.getComment()).append("\n");

		HikariDataSource dataSource = new HikariDataSource();
		if (StringUtils.equalsIgnoreCase(EnvTypes.DEVELOP.value(), appInfoProperty.getEnv())) {
			dataSource.setComment("개발 DB");
		} else if (StringUtils.equalsIgnoreCase(EnvTypes.PRODUCTION.value(), appInfoProperty.getEnv())) {
			dataSource.setComment("상용 DB");
		} else {
			dataSource.setComment("로컬 DB");
		}
		dataSource.setDriverClassName(property.getDriver());
		dataSource.setJdbcUrl(property.getUrl());
		dataSource.setUsername(property.getUsername());
		dataSource.setPassword(property.getPassword());
		dataSource.setConnectionTestQuery(property.getValidationQuery());

		logstack.append("| DRIVER_CLASS_NAME  | ").append(dataSource.getDriverClassName()).append("\n");
		logstack.append("| JDBC_URL           | ").append(dataSource.getJdbcUrl()).append("\n");
		logstack.append("| USER_NAME          | ").append(dataSource.getUsername()).append("\n");

		int minIdle = property.getMinIdle();
		if (minIdle > 0) {
			dataSource.setMinimumIdle(minIdle);
		}
		logstack.append("| MIN_IDLE           | ").append(dataSource.getMinimumIdle()).append("\n");

		int maxTotal = property.getMaxTotal();
		if (maxTotal > 0) {
			dataSource.setMaximumPoolSize(maxTotal);
		}
		logstack.append("| MAX_SIZE           | ").append(dataSource.getMaximumPoolSize()).append("\n");

		long maxWait = property.getMaxWait();
		if (maxWait > 0) {
			dataSource.setConnectionTimeout(maxWait);
		}
		logstack.append("| MAC_WAIT           | ").append(dataSource.getConnectionTimeout()).append("\n");
		logstack.append("------------------------------------------------------------------------").append("\n");

		if (log.isDebugEnabled()) {
			log.debug(logstack.toString());
		}
		return dataSource;
	}
}