package com.mih.bo.common.exception;

/**
 * 보관복지부 서비스 예외 처리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public class BgbgbServiceException extends RuntimeException {

	private static final long serialVersionUID = -4615176109021218676L;

	/**
	 * Code
	 */
	private int code;

	/**
	 * BgbgbServiceException Exception
	 *
	 * @param code Result Code
	 */
	public BgbgbServiceException(final int code) {
		super();
		this.code = code;
	}

	/**
	 * BgbgbServiceException Exception
	 *
	 * @param message Result Message
	 */
	public BgbgbServiceException(final String message) {
		super(message);
	}

	/**
	 * BgbgbServiceException Exception
	 *
	 * @param code Result Code
	 * @param message Result Message
	 */
	public BgbgbServiceException(final int code, final String message) {
		super(message);
		this.code = code;
	}

	/**
	 * BgbgbServiceException Exception
	 *
	 * @param message Result Message
	 * @param cause Throwable
	 */
	public BgbgbServiceException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * BgbgbServiceException Exception
	 *
	 * @param code Result Code
	 * @param message Result Message
	 * @param cause Throwable
	 */
	public BgbgbServiceException(final int code, final String message, final Throwable cause) {
		super(message, cause);
		this.code = code;

	}

	/**
	 * BgbgbServiceException Exception
	 *
	 * @param cause Throwable
	 */
	public BgbgbServiceException(final Throwable cause) {
		super(cause);
	}

	/**
	 * BgbgbServiceException Exception
	 *
	 * @param code Result Code
	 * @param cause Throwable
	 */
	public BgbgbServiceException(final int code, final Throwable cause) {
		super(cause);
		this.code = code;

	}

	/**
	 * Result Code
	 *
	 * @return the code
	 */
	public int getCode() {
		return code;
	}
}