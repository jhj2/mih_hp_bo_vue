package com.mih.bo.common.vo;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 변경이력
 *
 * @author kis1005
 * @version 1.0.0
 * @since 2021. 12. 03.
 */
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataChange {
	
	private static final long serialVersionUID = -1L;

	private String changeHistIdx;
	private String mberIdx;
	private String ordGoodsIdx;
	private String fixdTermPayIdx;
	private String changeHistTyCd;
	private String changeBeforeValue;
	private String changeAfterValue;
	private String changeResn;
	private Date regDt;
	private String regIp;
	private String regId;
	private String regNm;
}
