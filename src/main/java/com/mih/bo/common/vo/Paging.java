package com.mih.bo.common.vo;

import com.mih.bo.common.AbstractModel;

import lombok.Getter;
import lombok.Setter;

/**
 * 페이징
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Getter @Setter
public class Paging extends AbstractModel {
	
	private static final long serialVersionUID = -1110834526269696814L;
	
	/**
	 * 호출 함수명
	 */
	private String functionName;
	
	/**
	 * Submit Form Name
	 */
	private String formName;
	
	/**
	 * 현재 페이지
	 */
	private int currentPageNo = 1;
    
    /**
	 * 페이지 목록에 게시되는 페이지 건 수
	 */
	private int pageSize = 10;
    
    /**
	 * 한 페이지당 게시되는 게시물 건 수
	 */
	private int recordCountPerPage = 10;
    
    /**
	 * 전체 게시물 건 수
	 */
	private int totalRecordCount;
	
	/**
	 * 전체 페이지 수
	 */
	private int totalPageCount;
	
    /**
	 * 페이지 네이게이션
	 */
	private String pagination;
	
	/**
	 * 페이지 최대 순번
	 */
	private int pageMaxNo;

}
