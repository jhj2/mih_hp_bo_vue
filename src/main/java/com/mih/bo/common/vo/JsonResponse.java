package com.mih.bo.common.vo;

import com.mih.bo.common.BaseObject;

import lombok.Getter;
import lombok.Setter;

/**
 * Json 응답
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Getter @Setter
public class JsonResponse extends BaseObject {

	private static final long serialVersionUID = -674824714191186786L;

	/**
	 * Result Code
	 */
	protected int code = 200;

	/**
	 * Result Message
	 */
	protected String message = "SUCCESS";

	public JsonResponse() {
		// ignore...
	}

	public JsonResponse(final int code) {
		this.code = code;
	}

	public JsonResponse(final int code, final String message) {
		this.code = code;
		this.message = message;
	}
	
}
