package com.mih.bo.common.vo;

import com.mih.bo.common.AbstractModel;

import lombok.Getter;
import lombok.Setter;

/**
 * 첨부 파일
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Getter @Setter
public class AttachFile extends AbstractModel{

	private static final long serialVersionUID = 3610296762970434973L;

	/**
	 * 파일IDX
	 */
	private long fileIdx;
	
	/**
	 * 파일테이블명
	 */
	private String fileTableNm;
	
	/**
	 * 파일테이블IDX
	 */
	private long fileTableIdx;
	
	/**
	 * 파일시스템경로
	 */
	private String sysPath;
	
	/**
	 * 파일첨부경로
	 */
	private String attPath;
	
	/**
	 * 파일원본명
	 */
	private String fileOrgNm;
	
	/**
	 * 파일시스템명
	 */
	private String fileSysNm;
	
	/**
	 * 파일크기(KB)
	 */
	private long fileSize;
	
	/**
	 * 파일확장자
	 */
	private String fileExt;
	
	/**
	 * 파일정렬순서
	 */
	private int fileSort;
	
	/**
	 * 사용여부
	 */
	private String useYn;
	
	/**
	 * 파일대체문구
	 */
	private String fileReplcText;
	
	/**
	 * 파일다운로드건수
	 */
	private int fileDwldCnt;
}