package com.mih.bo.common.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * 파일 업로드
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Getter
@Setter
public class FileUpload extends AttachFile {

	private static final long serialVersionUID = 5104456424422502071L;
	
	/**
	 * 업로드 성공여부
	 */
	private boolean uploadSuccess;
}