package com.mih.bo.common.vo;

import com.mih.bo.common.AbstractModel;

import lombok.Getter;
import lombok.Setter;

/**
 * 공통 조회
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Getter @Setter
public class Search extends AbstractModel{
	
	private static final long serialVersionUID = -2321674944133682208L;

	/**
	 * 검색 유형
	 */
	private String searchKeywordType;
	
	/**
	 * 검색 키워드
	 */
	private String searchKeyword;
	
	/**
	 * 검색 시작 일자
	 */
	private String searchPeriod;
	
	/**
	 * 검색 시작 일자
	 */
	private String searchStartDate;
	
	/**
	 * 검색 종료 일자
	 */
	private String searchEndDate;
}