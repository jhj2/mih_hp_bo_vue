package com.mih.bo.common;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

/**
 * BaseObject
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@SuppressWarnings("all")
public class BaseObject implements Serializable {

	private static final long serialVersionUID = -4670974220660120440L;

	/**
	 * Object Json
	 *
	 * @return Json String
	 * @throws JsonProcessingException
	 */
	public String toJson() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setFilterProvider(new SimpleFilterProvider().setFailOnUnknownId(false));
			return mapper.writeValueAsString(this);
		} catch (Exception e) {
			throw new IllegalStateException(e.getMessage(), e);
		}
	}

	/**
	 * Object
	 *
	 * @return Json String
	 */
	public String toJsonLog() {
		return toJsonLog(null);
	}

	/**
	 * Object Json
	 *
	 * <pre>
	 * SerializationFeature.WRAP_ROOT_VALUE
	 * SerializationFeature.INDENT_OUTPUT
	 * </pre>
	 *
	 * @param feature SerializationFeature
	 * @return Json String
	 */
	public String toJsonLog(final SerializationFeature... feature) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setFilterProvider(new SimpleFilterProvider().setFailOnUnknownId(false));
			if (feature != null) {
				for (SerializationFeature f : feature) {
					mapper.configure(f, true);
				}
			}
			return mapper.writeValueAsString(this);
		} catch (Exception e) {
			return "Json Processing Exception - " + this.toString();
		}
	}

	/**
	 * Object "SerializationFeature.INDENT_OUTPUT" Config Json
	 *
	 * @return Json String
	 */
	public String toJsonPrettify() {
		return toJsonLog(SerializationFeature.INDENT_OUTPUT);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

}

