package com.mih.bo.common.excel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.mih.bo.common.exception.BgbgbServiceException;
import com.mih.bo.common.util.StringUtils;

public class ExcelView extends AbstractXlsView {

	private static final String EXCEL_SAMPLE_PATH = "/excel/";
	
	@Override
	protected void buildExcelDocument(Map<String, Object> modelMap, Workbook workbook, HttpServletRequest request,
		HttpServletResponse response) throws Exception {
		OutputStream os = null;
		InputStream is = null;
		
		try {
		
			/* 만건이상일경우 out of memory 발생 */
			if(modelMap.get("list") instanceof List ) {
				if((((List)modelMap.get("list"))).size() > 10000) {
					throw new BgbgbServiceException("엑셀 다운로드 중 에러가 발생 하였습니다.");
				}
			}
			
			String fileName = StringUtils.defaultString(modelMap.get("exFileName"), "excelFile");
			String filePath = EXCEL_SAMPLE_PATH + fileName;
			
			String outfileName =  StringUtils.defaultString(modelMap.get("fileName"), fileName);
			outfileName = new String(outfileName.getBytes("UTF-8"), "ISO-8859-1");
			
			is = new ClassPathResource(filePath).getInputStream();
			response.setHeader("Content-Type", "application/octet-stream");
			response.setHeader("Content-Disposition", "attachment; filename=" + outfileName);
			os = response.getOutputStream();
			
			Context context = new Context();
			
			if (StringUtils.isEmpty(modelMap.get("headers"))) {
				context.putVar("list", modelMap.get("list"));
				JxlsHelper.getInstance().processTemplate(is, os, context);
			} else {
				context.putVar("headers", modelMap.get("headers"));
				context.putVar("data", modelMap.get("data"));
				Map<String, Object> map = ((List<Map<String, Object>>)modelMap.get("data")).get(0);
				JxlsHelper.getInstance().processGridTemplateAtCell(is, os, context, String.join(",", map.keySet()), modelMap.get("exTargetCell").toString());
			}
		
		
		} catch (Exception e) {
			e.printStackTrace();
			throw new BgbgbServiceException("엑셀 다운로드 중 에러가 발생 하였습니다.");
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					throw new BgbgbServiceException("엑셀 다운로드 중 에러가 발생 하였습니다.");
				}
			}
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					throw new BgbgbServiceException("엑셀 다운로드 중 에러가 발생 하였습니다.");
				}
			}
		}
	}
}