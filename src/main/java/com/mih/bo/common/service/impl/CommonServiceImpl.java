package com.mih.bo.common.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.common.service.CommonService;
import com.mih.bo.common.vo.FileUpload;
import com.mih.bo.dao.CommonDao;
import com.mih.bo.task.branch.vo.Branch;
import com.mih.bo.task.code.vo.CommonCode;
import com.mih.bo.task.member.vo.Member;

/**
 * 공통 서비스 구현체
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Service
public class CommonServiceImpl implements CommonService {

	private static final Logger log = LoggerFactory.getLogger(CommonServiceImpl.class);

	@Autowired
	private CommonDao commonDao;
	
	/*
	 * @see com.bgbgb.bo.commons.service.CommonService#selectBranchComboList()
	*/
	public List<Branch> selectBranchComboList(){
		return commonDao.selectBranchComboList();
	}
	
	/*
	 * @see com.bgbgb.bo.commons.service.CommonService#insertMrInfoInqireHst(com.bgbgb.bo.biz.member.domain.Member)
	*/
	@Override
	public void insertMrInfoInqireHst(Member vo) {
		commonDao.insertMrInfoInqireHst(vo);
	}

	/*
	 * @see com.bgbgb.bo.commons.service.CommonService#selectCodeComboList(java.lang.String)
	*/
	public List<CommonCode> selectCodeComboList(String upperCommonCode){
		return commonDao.selectCodeComboList(upperCommonCode);
	}

	/*
	 * @see com.bgbgb.bo.commons.service.CommonService#commonImg(com.bgbgb.bo.commons.domain.FileUpload)
	*/
	@Override
	public FileUpload commonImg(FileUpload vo) {
		return commonDao.commonImg(vo);
	}

	/*
	 * @see com.bgbgb.bo.commons.service.CommonService#commonImgList(com.bgbgb.bo.commons.domain.FileUpload)
	*/
	@Override
	public List<?> commonImgList(FileUpload vo) {
		return commonDao.commonImgList(vo);
	}

	@Override
	public boolean isStplatAgreYn(Map<String, Object> paramMap) {
		return commonDao.selectStplatAgreCnt(paramMap) > 0;
	}
}
