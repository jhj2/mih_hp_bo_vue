package com.mih.bo.common.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mih.bo.common.enums.FileAttachTypes;
import com.mih.bo.common.vo.AttachFile;
import com.mih.bo.common.vo.FileUpload;

/**
 * 파일 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public interface FileService {
	
	/**
	 * 첨부 파일 목록 조회
	 *
	 * @param attachFile
	 * @return List<AttachFile>
	*/
	public List<AttachFile> selectFileAttachList(AttachFile attachFile);
	
	/**
	 * 파일 업로드
	 *
	 * @param file
	 * @param type
	 * @return FileUpload
	*/
	public FileUpload uploadFile(MultipartFile file, FileAttachTypes type);
	
	/**
	 * 첨부 파일 등록
	 *
	 * @param fileUpload
	*/
	public void insertFileAttach(FileUpload fileUpload);
	
	/**
	 * 첨부 파일 수정
	 *
	 * @param fileUpload
	*/
	public void updateFileAttach(FileUpload fileUpload);
	
}
