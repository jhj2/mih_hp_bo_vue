package com.mih.bo.common.service;

import java.util.List;
import java.util.Map;

import com.mih.bo.common.vo.FileUpload;
import com.mih.bo.task.branch.vo.Branch;
import com.mih.bo.task.code.vo.CommonCode;
import com.mih.bo.task.member.vo.Member;

/**
 * 공통 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public interface CommonService {
	
	/**
	 * 지부 콥보 목록 조회
	 *
	 * @return List<Branch>
	*/
	public List<Branch> selectBranchComboList();

	/**
	 * 공통코드 콤보 목록 조회
	 *
	 * @param upperCommonCode
	 * @return List<CommonCode>
	*/
	public List<CommonCode> selectCodeComboList(String upperCommonCode);
	
	/**
	 * 회원정보조회이력 등록
	 *
	 * @param vo
	*/
	public void insertMrInfoInqireHst(Member vo);

	/**
	 * [B/O] 공통 이미지 팝업조회
	 *
	 * @param vo
	 * @return FileUpload
	*/
	public FileUpload commonImg(FileUpload vo);

	/**
	 * [B/O] 공통 이미지 팝업 리스트 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> commonImgList(FileUpload vo);

	/**
	 * 개별 약관 동의 여부
	 *
	 * @param paramMap
	 * @return
	 */
	public boolean isStplatAgreYn(Map<String, Object> paramMap);
}