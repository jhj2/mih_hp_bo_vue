package com.mih.bo.common.service.impl;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mih.bo.common.Constants;
import com.mih.bo.common.config.Property;
import com.mih.bo.common.enums.FileAttachTypes;
import com.mih.bo.common.service.FileService;
import com.mih.bo.common.util.DateUtils;
import com.mih.bo.common.util.FileUtils;
import com.mih.bo.common.util.StringUtils;
import com.mih.bo.common.util.UUIDUtils;
import com.mih.bo.common.vo.AttachFile;
import com.mih.bo.common.vo.FileUpload;
import com.mih.bo.dao.FileDao;

/**
 * 파일 서비스 구현체
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Service
public class FileServiceImpl implements FileService {
	
	private static final Logger log = LoggerFactory.getLogger(FileServiceImpl.class);
	
	@Autowired
	private FileDao fileDao;

	/*
	 * @see com.bgbgb.bo.commons.service.FileService#uploadFile(org.springframework.web.multipart.MultipartFile, com.bgbgb.bo.commons.enums.FileAttachTypes)
	*/
	@Override
	public FileUpload uploadFile(MultipartFile file, FileAttachTypes type) {
		FileUpload fileUpload = new FileUpload();
		
		if(file.isEmpty()) {
			return fileUpload;
		}
		
		String separator = System.getProperty("file.separator");
		String uploadPath = Property.get("file.upload.path");
		String attachPath = type.path() + separator + DateUtils.toDateString("yyyy"+separator+"MM"+separator+"dd");
		String orginFileName = file.getOriginalFilename();
		String systemFileName = UUIDUtils.md5();
		String fileExt = FilenameUtils.getExtension(orginFileName);
		
		String filePath = uploadPath + attachPath;
		
		if(!FileUtils.isDirectory(filePath)) {
			FileUtils.mkdirs(filePath);
		}
		
		String uploadFileName = filePath + separator + systemFileName + Constants.DOT + fileExt;
		
		try {
			FileUtils.copy(file.getInputStream(), new FileOutputStream(uploadFileName));
			fileUpload.setUploadSuccess(true);
			fileUpload.setFileTableNm(type.table());
			fileUpload.setSysPath(uploadPath);
			fileUpload.setAttPath(attachPath);
			fileUpload.setFileOrgNm(orginFileName);
			fileUpload.setFileSysNm(systemFileName + Constants.DOT + fileExt);
			fileUpload.setFileSize(file.getSize());
			fileUpload.setFileExt(fileExt);
			
		} catch (FileNotFoundException e) {
			log.error(StringUtils.printStack(e));
			fileUpload.setUploadSuccess(false);
		} catch (IOException e) {
			log.error(StringUtils.printStack(e));
			fileUpload.setUploadSuccess(false);
		}
		
		return fileUpload;
	}
	
	/*
	 * @see com.bgbgb.bo.commons.service.FileService#insertFileAttach(com.bgbgb.bo.commons.domain.FileUpload)
	*/
	@Override
	public void insertFileAttach(FileUpload fileUpload) {
		fileDao.insertFileAttach(fileUpload);
	}
	
	/*
	 * @see com.bgbgb.bo.commons.service.FileService#updateFileAttach(com.bgbgb.bo.commons.domain.FileUpload)
	*/
	@Override
	public void updateFileAttach(FileUpload fileUpload) {
		fileDao.updateFileAttach(fileUpload);
	}

	/*
	 * @see com.bgbgb.bo.commons.service.FileService#selectFileAttachList(com.bgbgb.bo.commons.domain.AttachFile)
	*/
	@Override
	public List<AttachFile> selectFileAttachList(AttachFile attachFile) {
		return fileDao.selectFileAttachList(attachFile);
	}
}