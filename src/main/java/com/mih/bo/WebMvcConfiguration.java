package com.mih.bo;

import javax.servlet.DispatcherType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.ISpringTemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;

import com.google.common.base.Charsets;
import com.mih.bo.common.config.Property;
import com.mih.bo.common.web.interceptor.CommonInterceptor;
import com.mih.bo.common.web.interceptor.MenuInterceptor;

/**
 * WebMvcConfiguration
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Configuration
@EnableWebMvc
public class WebMvcConfiguration implements WebMvcConfigurer  {

	private static final Logger log = LoggerFactory.getLogger(WebMvcConfiguration.class);
	
	private static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "classpath:/META-INF/resources/", "classpath:/resources/", "classpath:/static/", "classpath:/public/" };
	
	@Autowired
	private CommonInterceptor commonInterceptor;
	
	@Autowired
	private MenuInterceptor menuInterceptor;
	
	// Thymeleaf 뷰 리졸버 설정
	@Autowired
	private ApplicationContext applicationContext;

	/**
	 * Add handlers to serve static resources such as images, js, and, css
	 * files from specific locations under web application root, the classpath,
	 * and others.
	 *
	 * @param registry InterceptorRegistry
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(commonInterceptor)
			.addPathPatterns("/**")
			.excludePathPatterns("/resources/**");
		
		registry.addInterceptor(menuInterceptor)
			.addPathPatterns("/**")
			.excludePathPatterns("/resources/**");
	}

	/**
	 * Add handlers to serve static resources such as images, js, and, css
	 * files from specific locations under web application root, the classpath,
	 * and others.
	 *
	 * @param registry ResourceHandlerRegistry
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		log.debug("Add resource handlers...");

		// Static Resource 등록.
		registry.addResourceHandler("/resources/**") // Resource Handler
			.addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS); // Resource Locations
		// .setCachePeriod(31556926); // Cache 기간 설정

		registry.addResourceHandler("/webjars/**")
			.addResourceLocations("classpath:/META-INF/resources/webjars/");
		
		// 첨부파일-외부경로 설정
		registry.addResourceHandler("/upload/**") // Resource Handler
			.addResourceLocations("file:///"+Property.get("file.upload.path")+"/");	// Resource Locations
	}
	
	/*
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurer#addViewControllers(org.springframework.web.servlet.config.annotation.ViewControllerRegistry)
	*/
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		// /에 해당하는 url mapping을 /common/test로 forward한다. 
		registry.addViewController( "/" ).setViewName( "redirect:/main" ); 
		// 우선순위를 가장 높게 잡는다. 
		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	}
	
	/**
	 * Configure cross origin requests processing.
	 *
	 * @param registry ViewResolverRegistry
	 */
	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		registry.viewResolver(viewResolver());
	}

	/**
	 * Configure View resolver to provide HTML output This is the default format in absence of any type suffix.
	 *
	 * @return ViewResolver
	 */
//	@Bean
//	public ViewResolver viewResolver() {
//		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//		viewResolver.setViewClass(JstlView.class);
//		viewResolver.setPrefix("/WEB-INF/views/");
//		viewResolver.setSuffix(".jsp");
//		return viewResolver;
//	}
	
	/**
	 * Thymeleaf 뷰 리졸버 설정
	 *
	 * @return ViewResolver
	*/
	@Bean
	public ViewResolver viewResolver() {
		ThymeleafViewResolver resolver = new ThymeleafViewResolver();
		log.debug("viewResolver resolver=========="+resolver);
		resolver.setTemplateEngine((ISpringTemplateEngine) templateEngine());
		resolver.setCharacterEncoding("UTF-8");
		return resolver;
	}
	
	@Bean
	public TemplateEngine templateEngine() {
		SpringTemplateEngine engine = new SpringTemplateEngine();
		engine.setEnableSpringELCompiler(true);
		engine.setTemplateResolver(templateResolver());
		return engine;
	}

	private ITemplateResolver templateResolver() {
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		resolver.setApplicationContext(applicationContext);
		resolver.setPrefix("/templates");
		resolver.setSuffix(".html");
		resolver.setTemplateMode(TemplateMode.HTML);
		return resolver;
	}
	
	/**
	 * BeanNameViewResolver
	 *
	 * @return BeanNameViewResolver
	*/
	@Bean
	public BeanNameViewResolver beanNameViewResolver() {
		BeanNameViewResolver beanNameViewResolver = new BeanNameViewResolver();
		beanNameViewResolver.setOrder(0);
		return beanNameViewResolver;
	}
	
	/**
	 * Json View
	 *
	 * @return MappingJackson2JsonView
	 */
	@Bean
    public MappingJackson2JsonView jsonView(){
        return new MappingJackson2JsonView();
    }

	/**
	 * 404 에러를 ControllerAdvice에서 처리할 수 있도록 설정한다.
	 *
	 * @return DispatcherServlet
	 */
	@Bean
	public DispatcherServlet dispatcherServlet() {
		DispatcherServlet ds = new DispatcherServlet();
		ds.setThrowExceptionIfNoHandlerFound(true);
		return ds;
	}

	/**
	 * 결과를 출력시에 강제로 UTF-8로 설정
	 *
	 * @return HttpMessageConverter
	 */
	@Bean
	public HttpMessageConverter<String> responseBodyConverter() {
		log.debug("Response Body Converter 등록...");
		return new StringHttpMessageConverter(Charsets.UTF_8);
	}

	/**
	 * favicon 404 에러 처리.
	 *
	 * @return WebMvcConfigurer
	 */
	@Bean
	public WebMvcConfigurer faviconWebMvcConfiguration() {
		return new WebMvcConfigurer() {
			@Override
			public void addResourceHandlers(ResourceHandlerRegistry registry) {
				registry.setOrder(Integer.MIN_VALUE);
				registry.addResourceHandler("/favicon.ico") // Resource Handler
						.addResourceLocations("/") // Resource Locations
						.setCachePeriod(0); // Cache 기간 설정
			}
		};
	}

	/**
	 * POST 요청시에 한글이 깨지는 문제 보완
	 *
	 * @return FilterRegistrationBean
	 */
	@Bean
	public FilterRegistrationBean<CharacterEncodingFilter> characterEncodingFilter() {
		log.debug("Character Encoding Filter 등록...");
		final CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding(Charsets.UTF_8.toString());
		characterEncodingFilter.setForceEncoding(true);

		final FilterRegistrationBean<CharacterEncodingFilter> registration = new FilterRegistrationBean<CharacterEncodingFilter>(characterEncodingFilter);
		registration.setDispatcherTypes(DispatcherType.REQUEST);
		registration.setOrder(Integer.MIN_VALUE);
		return registration;
	}

}
