package com.mih.bo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mih.bo.common.vo.AttachFile;
import com.mih.bo.common.vo.FileUpload;
import com.mih.bo.dao.common.MybatisDao;

/**
 * 파일 Dao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Repository
public class FileDao extends MybatisDao {

	private static final String SQL_NAMESPACE = "file";
	
	/**
	 * 첨부 파일 목록 조회
	 *
	 * @param attachFile
	 * @return List<AttachFile>
	*/
	public List<AttachFile> selectFileAttachList(AttachFile attachFile) {
		return selectList(SQL_NAMESPACE, "selectFileAttachList", attachFile);
	}

	/**
	 * 첨부 파일 등록
	 *
	 * @param fileUpload void
	*/
	public void insertFileAttach(FileUpload fileUpload) {
		insert(SQL_NAMESPACE, "insertFileAttach", fileUpload);
	}
	
	/**
	 * 첨부 파일 수정
	 *
	 * @param attachFile void
	*/
	public void updateFileAttach(AttachFile attachFile) {
		insert(SQL_NAMESPACE, "updateFileAttach", attachFile);
	}

	
	/**
	 * 파일 USE_YN값 전체 N 처리
	 *
	 * @param result void
	*/
	public void updateAllUseYn(FileUpload attachFile) {
		update(SQL_NAMESPACE, "updateAllUseYn", attachFile);
	}
}