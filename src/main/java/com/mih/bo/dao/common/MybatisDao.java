package com.mih.bo.dao.common;

import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Component;

import com.mih.bo.common.Constants;

/**
 * MybatisDao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Component
public class MybatisDao {

	/**
	 * Database SQL Session Template
	 */
	@Resource(name = "sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;

	/**
	 * Getter Sql session Template
	 *
	 * @return the sqlSessionTemplate
	 */
	protected SqlSessionTemplate getSqlSession() {
		return this.sqlSessionTemplate;
	}

	/**
	 * 설명
	 *
	 * @param sqlNameSpace
	 * @param sqlId
	 * @return int
	*/
	protected int insert(String sqlNameSpace, String sqlId) {
		return this.sqlSessionTemplate.insert(sqlNameSpace + Constants.DOT + sqlId);
	}

	/**
	 * 설명
	 *
	 * @param sqlNameSpace
	 * @param sqlId
	 * @param parameter
	 * @return int
	*/
	protected int insert(String sqlNameSpace, String sqlId, Object parameter) {
		return this.sqlSessionTemplate.insert(sqlNameSpace + Constants.DOT + sqlId, parameter);
	}

	/**
	 * 설명
	 *
	 * @param <T>
	 * @param sqlNameSpace
	 * @param sqlId
	 * @return T
	*/
	protected <T> T selectOne(String sqlNameSpace, String sqlId) {
	    return this.sqlSessionTemplate.selectOne(sqlNameSpace + Constants.DOT + sqlId);
	}

	/**
	 * 설명
	 *
	 * @param <T>
	 * @param sqlNameSpace
	 * @param sqlId
	 * @param parameter
	 * @return T
	*/
	protected <T> T selectOne(String sqlNameSpace, String sqlId, Object parameter) {
	    return this.sqlSessionTemplate.selectOne(sqlNameSpace + Constants.DOT + sqlId, parameter);
	}

	/**
	 * 설명
	 *
	 * @param <E>
	 * @param sqlNameSpace
	 * @param sqlId
	 * @return List<E>
	*/
	protected <E> List<E> selectList(String sqlNameSpace, String sqlId) {
		return this.sqlSessionTemplate.selectList(sqlNameSpace + Constants.DOT + sqlId);
	}

	/**
	 * 설명
	 *
	 * @param <E>
	 * @param sqlNameSpace
	 * @param sqlId
	 * @param parameter
	 * @return List<E>
	*/
	protected <E> List<E> selectList(String sqlNameSpace, String sqlId, Object parameter) {
		return this.sqlSessionTemplate.selectList(sqlNameSpace + Constants.DOT + sqlId, parameter);
	}

	/**
	 * 설명
	 *
	 * @param sqlNameSpace
	 * @param sqlId
	 * @return int
	*/
	protected int update(String sqlNameSpace, String sqlId) {
		return this.sqlSessionTemplate.update(sqlNameSpace + Constants.DOT + sqlId);
	}

	/**
	 * 설명
	 *
	 * @param sqlNameSpace
	 * @param sqlId
	 * @param parameter
	 * @return int
	*/
	protected int update(String sqlNameSpace, String sqlId, Object parameter) {
		return this.sqlSessionTemplate.update(sqlNameSpace + Constants.DOT + sqlId, parameter);
	}

	/**
	 * 설명
	 *
	 * @param sqlNameSpace
	 * @param sqlId
	 * @return int
	*/
	protected int delete(String sqlNameSpace, String sqlId) {
		return this.sqlSessionTemplate.delete(sqlNameSpace + Constants.DOT + sqlId);
	}

	/**
	 * 설명
	 *
	 * @param sqlNameSpace
	 * @param sqlId
	 * @param parameter
	 * @return int
	*/
	protected int delete(String sqlNameSpace, String sqlId, Object parameter) {
		return this.sqlSessionTemplate.delete(sqlNameSpace + Constants.DOT + sqlId, parameter);
	}

}
