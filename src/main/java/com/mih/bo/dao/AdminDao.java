package com.mih.bo.dao;

import java.util.List;
import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.task.admin.vo.Admin;

/**
 * 관리자관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.02.15.
 */
@Repository
public class AdminDao extends MybatisDao{

	private static final String SQL_NAMESPACE = "admin";

	/**
	 * 회원관리 카운트 조회
	 *
	 * @param vo
	 * @return int
	*/
	public int selectAdminListTotCnt(Admin vo) {
		return (int) selectOne(SQL_NAMESPACE, "selectAdminListTotCnt", vo);
	}

	/**
	 * 회원관리 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> selectAdminList(Admin vo) {
		return selectList(SQL_NAMESPACE, "selectAdminList", vo);
	}

	/**
	 * 관리자관리 단건조회
	 *
	 * @param mngrId
	 * @return Admin
	*/
	public Admin selectAdmin(String mngrId) {
		return selectOne(SQL_NAMESPACE, "selectAdmin", mngrId);
	}
	
	/**
	 * 관리자관리 등록
	 *
	 * @param vo void
	*/
	public void insertAdmin(Admin vo) {
		insert(SQL_NAMESPACE, "insertAdmin", vo);
	}

	/**
	 * 관리자관리 수정
	 *
	 * @param vo void
	*/
	public void updateAdmin(Admin vo) {
		update(SQL_NAMESPACE, "updateAdmin", vo);
	}
}