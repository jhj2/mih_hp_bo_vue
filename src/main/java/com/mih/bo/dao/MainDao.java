package com.mih.bo.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;

/**
 * 메인 Dao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Repository
public class MainDao extends MybatisDao{
	
	private static final String SQL_NAMESPACE = "main";
	
	/**
	 * 주문 현황 조회
	 *
	 * @param param
	 * @return Map<String,Object>
	*/
	public Map<String, Object> selectOrderStat(Map<String, Object> param){
		return selectOne(SQL_NAMESPACE, "selectOrderStat", param);
	}
	
	/**
	 * 결제 현황 조회
	 *
	 * @param batchDate
	 * @return Map<String,Object>
	*/
	public Map<String, Object> selectPaymentStat(String batchDate){
		return selectOne(SQL_NAMESPACE, "selectPaymentStat", batchDate);
	}
	
	/**
	 * 지부 캐비닛 현황 조회
	 *
	 * @return List<Map<String,Object>>
	*/
	public List<Map<String, Object>> selectBranchCabinetStat(){
		return selectList(SQL_NAMESPACE, "selectBranchCabinetStat");
	}
	
	/**
	 * 종료신청 목록 조회
	 *
	 * @return List<Map<String,Object>>
	*/
	public List<Map<String, Object>> selectEndRequestList() {
		return selectList(SQL_NAMESPACE, "selectEndRequestList");
	}
	
	/**
	 * 지부 투어신청 목록 조회
	 *
	 * @return List<Map<String,Object>>
	*/
	public List<Map<String, Object>> selectBranchTourList() {
		return selectList(SQL_NAMESPACE, "selectBranchTourList");
	}
	
	/**
	 * 1:1 문의 목록 조회
	 *
	 * @return List<Map<String,Object>>
	*/
	public List<Map<String, Object>> selectInquiryList() {
		return selectList(SQL_NAMESPACE, "selectInquiryList");
	}

	/**
	 * Home 화면에 1:1 문의 수 / 답변완료 수 / 답변대기 표기
	 *
	 * @return Map<String,Object>
	*/
	public Map<String, Object> selectInqryCnt() {
		return selectOne(SQL_NAMESPACE, "selectInqryCnt");
	}
}