package com.mih.bo.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mih.bo.common.vo.FileUpload;
import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.task.branch.vo.Branch;
import com.mih.bo.task.code.vo.CommonCode;
import com.mih.bo.task.member.vo.Member;

/**
 * 공통 Dao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Repository
public class CommonDao extends MybatisDao {

	private static final String SQL_NAMESPACE = "common";

	/**
	 * 지부 콤보 목록 조회
	 *
	 * @return List<Branch>
	*/
	public List<Branch> selectBranchComboList() {
		return selectList(SQL_NAMESPACE, "selectBranchComboList");
	}

	/**
	 * 공통코드 콤보 목록 조회
	 *
	 * @param upperCommonCode
	 * @return List<CommonCode>
	*/
	public List<CommonCode> selectCodeComboList(String upperCommonCode) {
		return selectList(SQL_NAMESPACE, "selectCodeComboList", upperCommonCode);
	}

	/**
	 * 회원정보조회이력 등록
	 *
	 * @param vo
	*/
	public void insertMrInfoInqireHst(Member vo) {
		insert(SQL_NAMESPACE, "insertMrInfoInqireHst", vo);
	}

	/**
	 * [B/O] 공통 이미지 팝업조회
	 *
	 * @param vo
	 * @return FileUpload
	*/
	public FileUpload commonImg(FileUpload vo) {
		return selectOne(SQL_NAMESPACE, "commonImg", vo);
	}

	/**
	 * [B/O] 공통 이미지 팝업 리스트 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> commonImgList(FileUpload vo) {
		return selectList(SQL_NAMESPACE, "commonImg", vo);
	}

	/**
	 * 개별 약관 동의 카운트
	 *
	 * @param paramMap
	 * @return
	 */
	public int selectStplatAgreCnt(Map<String, Object> paramMap) {
		return selectOne(SQL_NAMESPACE, "selectStplatAgreCnt", paramMap);
	}
}