package com.mih.bo.dao;

import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.security.entity.UserEntity;
import com.mih.bo.task.user.vo.LoginHistory;

/**
 * 로그인 Dao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Repository
public class LoginDao extends MybatisDao {

	private static final String SQL_NAMESPACE = "login";

	/**
	 * 로그인 사용자 조회
	 *
	 * @param userId
	 * @return UserEntity
	*/
	public UserEntity selectLoginUser(String userId) {
		return selectOne(SQL_NAMESPACE, "selectLoginUser", userId);
	}
	
	/**
	 * 로그인 이력 등록
	 *
	 * @param loginHistory
	*/
	public void insertLoginHistory(LoginHistory loginHistory) {
		insert(SQL_NAMESPACE, "insertLoginHistory", loginHistory);
	}

}
