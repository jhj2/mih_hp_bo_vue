package com.mih.bo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.task.code.vo.CommonCode;

/**
 * 공통코드 Dao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Repository
public class CodeDao extends MybatisDao {

	private static final String SQL_NAMESPACE = "code";

	/**
	 * 공통코드 조회
	 *
	 * @param commonCode
	 * @return CommonCode
	*/
	public CommonCode selectCode(String commonCode) {
		return selectOne(SQL_NAMESPACE, "selectCode", commonCode);
	}

	/**
	 * 사용공통코드 목록 조회
	 *
	 * @param upperCommonCode
	 * @return List<CommonCode>
	*/
	public List<CommonCode> selectUseCodeList(String upperCommonCode) {
		return selectList(SQL_NAMESPACE, "selectUseCodeList", upperCommonCode);
	}
	
	/**
	 * 공통코드 목록 조회
	 *
	 * @param commonCode
	 * @return List<CommonCode>
	*/
	public List<CommonCode> selectCodeList(CommonCode commonCode) {
		return selectList(SQL_NAMESPACE, "selectCodeList", commonCode);
	}

}
