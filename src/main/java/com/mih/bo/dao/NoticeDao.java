package com.mih.bo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.task.complaint.vo.Notice;

/**
 * [종합민원실] 공지사항 관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Repository
public class NoticeDao extends MybatisDao {

	private static final String SQL_NAMESPACE = "notice";
	
	/**
	 * 공지사항 목록 조회
	 *
	 * @param notice
	 * @return List<Notice>
	*/
	public List<Notice> selectNoticeList(Notice notice) {
		return selectList(SQL_NAMESPACE, "selectNoticeList", notice);
	}
	
	/**
	 * 공지사항 상세보기
	 *
	 * @param notice
	 * @return Notice
	*/
	public Notice selectNoticeEdit(Notice notice) {
		return selectOne(SQL_NAMESPACE, "selectNoticeEdit", notice);
	}
	
	/**
	 * 공지사항 조회 총 건수
	 *
	 * @param notice
	 * @return int
	*/
	public int selectNoticeListTotalCnt(Notice notice) {
		return (int) selectOne(SQL_NAMESPACE, "selectNoticeListTotalCnt", notice);
	}

	/**
	 * 공지사항 내용 수정
	 *
	 * @param notice
	 * @return int
	*/
	public int updateNotice(Notice notice) {
		return update(SQL_NAMESPACE, "updateNotice", notice);
	}
	
	/**
	 * 공지사항 등록
	 *
	 * @param notice
	 * @return int
	*/
	public int insertNotice(Notice notice) {
		return insert(SQL_NAMESPACE, "insertNotice", notice);
	}
	
	/**
	 * 파일 테이블에 있는 인덱스 조회
	 *
	 * @return int
	*/
	public int selectFileTableIdx() {
		return (int) selectOne(SQL_NAMESPACE, "selectFileTableIdx");
	}
	
	/**
	 * 정렬된 index 조회
	 *
	 * @param notice
	 * @return int
	*/
	public int selectFileSortIdx(Notice notice) {
		return (int) selectOne(SQL_NAMESPACE, "selectFileSortIdx", notice);
	}
	
	/**
	 * 이미지 파일 저장
	 *
	 * @param notice
	*/
	public void insertNoticeAttachFile(Notice notice) {
		insert(SQL_NAMESPACE, "insertNoticeAttachFile", notice);
	}
}
