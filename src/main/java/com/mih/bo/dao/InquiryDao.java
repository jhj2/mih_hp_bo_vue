package com.mih.bo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.task.complaint.vo.Inquiry;

/**
 * [종합민원실] 1:1 문의 관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Repository
public class InquiryDao extends MybatisDao {

	private static final String SQL_NAMESPACE = "inquiry";
	
	/**
	 * 1:1 문의 목록 조회
	 *
	 * @param inqry
	 * @return List<Inquiry>
	*/
	public List<Inquiry> selectInqryList(Inquiry inqry) {
		return selectList(SQL_NAMESPACE, "selectInqryList", inqry);
	}
	
	/**
	 * 1:1 문의 목록 전체 조회 건수
	 *
	 * @param inqry
	 * @return int
	*/
	public int selectInqryListTotalCnt(Inquiry inqry) {
		return (int) selectOne(SQL_NAMESPACE, "selectInqryListTotalCnt", inqry);
	}
	
	/**
	 * 1:1문의 상세 보기
	 *
	 * @param inqry
	 * @return Inquiry
	*/
	public Inquiry selectInqryEdit(Inquiry inqry) {
		return selectOne(SQL_NAMESPACE, "selectInqryEdit", inqry);
	}
	
	/**
	 * 1:1문의 답변 등록
	 *
	 * @param inqry void
	*/
	public void updateInqryAnswer(Inquiry inqry) {
		update(SQL_NAMESPACE, "updateInqryAnswer", inqry);
	}
	
	/**
	 * 1:1문의 이미지 url 가져오기
	 *
	 * @param inqry
	 * @return List<Inquiry>
	*/
	public List<Inquiry> selectImgFileUrl(Inquiry inqry) {
		return selectList(SQL_NAMESPACE, "selectImgFileUrl", inqry);
	}
	
	/**
	 * 고객 연락처 가져오기
	 *
	 * @param inqry
	 * @return Inquiry
	*/
	public Inquiry selectMemberPhoneInfo(Inquiry inqry) {
		return selectOne(SQL_NAMESPACE, "selectMemberPhoneInfo", inqry);
	}
}
