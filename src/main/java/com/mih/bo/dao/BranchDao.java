package com.mih.bo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.task.branch.vo.Branch;

/**
 * 지부관리 Dao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Repository
public class BranchDao extends MybatisDao{
	
	private static final String SQL_NAMESPACE = "branch";
	
	/**
	 * 지부관리 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> selectBranchInfoList(Branch vo) {
		return selectList(SQL_NAMESPACE, "selectBranchInfoList", vo);
	}
	
	/**
	 * 지부관리 카운트조회
	 *
	 * @param vo
	 * @return int
	*/
	public int selectBranchInfoListTotCnt(Branch vo) {
		return (int) selectOne(SQL_NAMESPACE, "selectBranchInfoListTotCnt", vo);
	}

	/**
	 * 지부관리 상세페이지
	 *
	 * @param vo
	 * @return Branch
	*/
	public Branch selectBranchInfo(Branch vo) {
		return (Branch) selectOne(SQL_NAMESPACE, "selectBranchInfo", vo);
	}

	/**
	 * 지부관리 등록
	 *
	 * @param vo 
	*/
	public void insertBranchInfo(Branch vo) {
		insert(SQL_NAMESPACE, "insertBranchInfo", vo);
	}

	/**
	 * 지부관리 수정
	 *
	 * @param vo 
	*/
	public void updateBranchInfo(Branch vo) {
		update(SQL_NAMESPACE, "updateBranchInfo", vo);
	}

	/**
	 * 공통파일 등록
	 *
	 * @param vo 
	*/
	public void insertCmFile(Branch vo) {
		insert(SQL_NAMESPACE, "insertCmFile", vo);
	}

	/**
	 * 지부관리 - 이미지 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> imgList(Branch vo) {
		return selectList(SQL_NAMESPACE, "imgList", vo);
	}

	/**
	 * TB_BR_BHF 테이블 데이터 use_yn = 'N'으로 수정
	 *
	 * @param vo 
	*/
	public void updateBrBhfUseYn(Branch vo) {
		update(SQL_NAMESPACE, "updateBrBhfUseYn", vo);
	}

	/**
	 * 지부투어 카운트 조회
	 *
	 * @param vo
	 * @return int
	*/
	public int selectBranchTourListTotCnt(Branch vo) {
		return (int) selectOne(SQL_NAMESPACE, "selectBranchTourListTotCnt", vo);
	}

	/**
	 * 지부투어 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> selectBranchTourList(Branch vo) {
		return selectList(SQL_NAMESPACE, "selectBranchTourList", vo);
	}

	/**
	 * 지부 리스트 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> brBhfList(Branch vo) {
		return selectList(SQL_NAMESPACE, "brBhfList", vo);
	}

	/**
	 * 지부투어 수정(종료하기)
	 *
	 * @param vo 
	*/
	public void updateBrTour(Branch vo) {
		update(SQL_NAMESPACE, "updateBrTour", vo);
	}
	
	/**
	 * 지부투어 신청자 개인정보 조회
	 *
	 * @param tourReqstIdx
	 * @return Branch
	*/
	public Branch selectBranchTourRequester(String tourReqstIdx) {
		return selectOne(SQL_NAMESPACE, "selectBranchTourRequester", tourReqstIdx);
	}
}