package com.mih.bo.dao;

import java.util.List;
import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.task.bbs.vo.Bbs;

/**
 * 게시판 카테고리 관리 Dao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022. 1. 26.
 */
@Repository
public class BbsCtgryDao extends MybatisDao{
	
	private static final String SQL_NAMESPACE = "bbsCtgry";
	
	/**
	 * 게시판 카테고리 관리 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> selectBbsCtgryList(Bbs vo) {
		return selectList(SQL_NAMESPACE, "selectBbsCtgryList", vo);
	}
	
	/**
	 * 게시판 카테고리 관리 카운트조회
	 *
	 * @param vo
	 * @return int
	*/
	public int selectBbsCtgryListTotCnt(Bbs vo) {
		return (int) selectOne(SQL_NAMESPACE, "selectBbsCtgryListTotCnt", vo);
	}

	/**
	 * 게시판 카테고리 관리 상세페이지
	 *
	 * @param vo
	 * @return Bbs
	*/
	public Bbs selectBbsCtgry(Bbs vo) {
		return (Bbs) selectOne(SQL_NAMESPACE, "selectBbsCtgry", vo);
	}

	/**
	 * 게시판 카테고리 관리 등록
	 *
	 * @param vo void
	*/
	public void insertBbsCtgry(Bbs vo) {
		insert(SQL_NAMESPACE, "insertBbsCtgry", vo);
	}

	/**
	 * 게시판 카테고리 관리 수정
	 *
	 * @param vo void
	*/
	public void updateBbsCtgry(Bbs vo) {
		update(SQL_NAMESPACE, "updateBbsCtgry", vo);
	}

	/**
	 * 게시판 카테고리 권한 조회
	 *
	 * @param vo
	 * @return Bbs
	*/
	public Bbs selectCtgryGrant(Bbs vo) {
		return (Bbs) selectOne(SQL_NAMESPACE, "selectCtgryGrant", vo);
	}
}