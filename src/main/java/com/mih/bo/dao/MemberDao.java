package com.mih.bo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.task.member.vo.Member;

/**
 * 회원관리 Dao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Repository
public class MemberDao extends MybatisDao{

	private static final String SQL_NAMESPACE = "member";

	/**
	 * 회원정보 단건조회
	 *
	 * @param vo
	 * @return Member
	*/
	public Member selectMrMber(Member vo) {
		return (Member) selectOne(SQL_NAMESPACE, "selectMrMber", vo);
	}

	/**
	 * 마케팅 수신 동의 정보 가져오기
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> selectMrUseStplat(Member vo) {
		return selectList(SQL_NAMESPACE, "selectMrUseStplat", vo);
	}

	/**
	 * 페이징 테스트 -> 메시지 발송 내역 카운트 조회
	 *
	 * @param vo
	 * @return int
	*/
	public int selectMsgSendListTotCnt(Member vo) {
		return (int) selectOne(SQL_NAMESPACE, "selectMsgSendListTotCnt", vo);
	}

	/**
	 * 메시지 발송 내역
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> selectMsgSendList(Member vo) {
		return selectList(SQL_NAMESPACE, "selectMsgSendList", vo);
	}

	/**
	 * 회원관리 카운트 조회
	 *
	 * @param vo
	 * @return int
	*/
	public int selectMemberListTotCnt(Member vo) {
		return (int) selectOne(SQL_NAMESPACE, "selectMemberListTotCnt", vo);
	}

	/**
	 * 회원관리 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> selectMemberList(Member vo) {
		return selectList(SQL_NAMESPACE, "selectMemberList", vo);
	}

	/**
	 * 휴면 전환 대상 추출
	 *
	 * @param vo
	 * @return List<Member>
	*/
	public List<Member> selectDormantTransitionTargetList(Member vo) {
		return selectList(SQL_NAMESPACE, "selectDormantTransitionTargetList", vo);
	}

	/**
	 * 탈퇴한 회원인지 체크
	 *
	 * @param vo
	 * @return int
	*/
	public int secsnYnCnt(Member vo) {
		return (int) selectOne(SQL_NAMESPACE, "secsnYnCnt", vo);
	}
}