package com.mih.bo.dao;

import java.util.List;
import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.task.code.vo.ComCd;

/**
 * 공통코드관리 Dao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.03.07.
 */
@Repository
public class ComCdDao extends MybatisDao{
	
	private static final String SQL_NAMESPACE = "comCd";
	
	/**
	 * 공통코드관리 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> selectComCdList(ComCd vo) {
		return selectList(SQL_NAMESPACE, "selectComCdList", vo);
	}
	
	/**
	 * 공통코드관리 카운트조회
	 *
	 * @param vo
	 * @return int
	*/
	public int selectComCdListTotCnt(ComCd vo) {
		return (int) selectOne(SQL_NAMESPACE, "selectComCdListTotCnt", vo);
	}

	/**
	 * 공통코드관리 상세페이지
	 *
	 * @param vo
	 * @return ComCd
	*/
	public ComCd selectComCd(ComCd vo) {
		return (ComCd) selectOne(SQL_NAMESPACE, "selectComCd", vo);
	}

	/**
	 * 공통코드관리 등록
	 *
	 * @param vo void
	*/
	public void insertComCd(ComCd vo) {
		insert(SQL_NAMESPACE, "insertComCd", vo);
	}

	/**
	 * 공통코드관리 수정
	 *
	 * @param vo void
	*/
	public void updateComCd(ComCd vo) {
		update(SQL_NAMESPACE, "updateComCd", vo);
	}
	
	/**
	 * 공통코드관리 삭제
	 *
	 * @param vo void
	 */
	public void deleteComCd(ComCd vo) {
		delete(SQL_NAMESPACE, "deleteComCd", vo);
	}

	/**
	 * 공통코드 리스트 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> cdList(ComCd vo) {
		return selectList(SQL_NAMESPACE, "cdList", vo);
	}

	/**
	 * 공통코드관리 엑셀다운로드 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> excelDownList(ComCd vo) {
		return selectList(SQL_NAMESPACE, "excelDownList", vo);
	}

	/**
	 * 공통코드 리스트 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> comCdList(ComCd vo) {
		return selectList(SQL_NAMESPACE, "comCdList", vo);
	}
}