package com.mih.bo.dao;

import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.task.message.vo.BizMsg;
import com.mih.bo.task.message.vo.MsgSndngHst;

@Repository
public class BizMsgDao extends MybatisDao {
	private static final String SQL_NAMESPACE = "bizMsg";

	/**
	 * 알림톡 등록
	 *
	 * @param bizMsg
	 */
	public void insertBizMsg(BizMsg bizMsg) {
		insert(SQL_NAMESPACE, "insertBizMsg", bizMsg);
	}

	/**
	 * 메시지 전송 이력 등록
	 *
	 * @param msgSndngHst
	 */
	public void insertMsgSndngHst(MsgSndngHst msgSndngHst) {
		insert(SQL_NAMESPACE, "insertMsgSndngHst", msgSndngHst);
	}
}
