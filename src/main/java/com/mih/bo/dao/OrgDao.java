package com.mih.bo.dao;

import org.springframework.stereotype.Repository;
import com.mih.bo.dao.common.MybatisDao;

import java.util.List;
import java.util.Map;

/**
 * 게시판 카테고리 관리 Dao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022. 1. 26.
 */
@Repository
public class OrgDao extends MybatisDao{
	
	private static final String SQL_NAMESPACE = "org";
	
	public List<?> orgList(){
		return selectList(SQL_NAMESPACE, "orgList");
	};
	public void updateOrgList(Map<String,Object> paramMap){
		selectList(SQL_NAMESPACE, "updateOrgList", paramMap);
	}
}