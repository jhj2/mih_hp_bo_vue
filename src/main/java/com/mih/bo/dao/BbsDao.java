package com.mih.bo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mih.bo.dao.common.MybatisDao;
import com.mih.bo.task.bbs.vo.Bbs;

/**
 * 게시판 카테고리 관리 Dao
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022. 1. 26.
 */
@Repository
public class BbsDao extends MybatisDao{
	
	private static final String SQL_NAMESPACE = "bbs";
	
	/**
	 * 카테고리 콤보박스 목록 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> ctgryList(Bbs vo) {
		return selectList(SQL_NAMESPACE, "ctgryList", vo);
	}
	
	/**
	 * 카테고리 내용 상세 보기
	 *
	 * @param vo
	 * @return vo
	*/
	public Bbs selectBbsView(Bbs bbs) {
		return selectOne(SQL_NAMESPACE, "selectBbsView", bbs);
	}
	/**
	 * 게시판 내용 상세 보기 댓글 종류
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> selectBbsCmtList(Bbs vo) {
		return selectList(SQL_NAMESPACE, "selectBbsCmtList", vo);
	}
	/**
	 * 게시글 상세보기 댓글의 리플 리스트 조회
	 *
	 * @param vo
	 * @return List<?>
	 */
	public List<Bbs> selectBbsReCmtList(Bbs vo) {
		return selectList(SQL_NAMESPACE, "selectBbsReCmtList", vo);
	}
	/**
	 * 게시글 상세보기 댓글의 리플 등록
	 *
	 * @param vo
	 * @return int
	 */
	public int insertReCmt(Bbs vo) {
		return insert(SQL_NAMESPACE, "insertReCmt", vo);
	}
	/**
	 * 게시판 카테고리 관리 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> selectBbsList(Bbs vo) {
		return selectList(SQL_NAMESPACE, "selectBbsList", vo);
	}
	
	/**
	 * 게시판 카테고리 관리 조회
	 *
	 * @param vo
	 * @return List<?>
	 */
	public List<Bbs> selectReBbsList(Bbs vo) {
		return selectList(SQL_NAMESPACE, "selectReBbsList", vo);
	}

	/**
	 * 게시판 카테고리 관리 카운트조회
	 *
	 * @param vo
	 * @return int
	*/
	public int selectBbsListTotCnt(Bbs vo) {
		return (int) selectOne(SQL_NAMESPACE, "selectBbsListTotCnt", vo);
	}
	
	/**
	 * 게시판 관리 상세페이지
	 *
	 * @param vo
	 * @return Bbs
	*/
	public Bbs selectBbs(Bbs vo) {
		return (Bbs) selectOne(SQL_NAMESPACE, "selectBbs", vo);
	}

	/**
	 * 게시판 관리 등록
	 *
	 * @param vo void
	*/
	public void insertBbs(Bbs vo) {
		insert(SQL_NAMESPACE, "insertBbs", vo);
	}

	/**
	 * 게시판 관리 수정
	 *
	 * @param vo void
	*/
	public void updateBbs(Bbs vo) {
		update(SQL_NAMESPACE, "updateBbs", vo);
	}
	
	/**
	 * 게시판 댓글 삭제
	 *
	 * @param vo void
	*/
	public void updateBbsCmt(Bbs vo) {
		update(SQL_NAMESPACE, "updateBbsCmt", vo);
	}

}