package com.mih.bo;

import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * BgbgbBoApplication
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@SpringBootApplication(scanBasePackages={"com.mih.bo"})
@EnableScheduling
public class BoApplication {
	
	private static final Logger log = LoggerFactory.getLogger(BoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(BoApplication.class, args);
	}
	
	/**
	 * ThreadPoolTaskExecutor
	 *
	 * @return ThreadPoolTaskExecutor
	*/
	@Bean
	public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
		final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(1);
		executor.setMaxPoolSize(1);
		return executor;
	}
	
	/**
	 * Scheduler Executor
	 *
	 * @return Executor
	 */
	@Bean(destroyMethod = "shutdown")
	public Executor schedulerExecutor() {
		log.debug("Scheduled Thread Pool Executor...");
		return new ScheduledThreadPoolExecutor(1);
	}

	/**
	 * RestTemplate
	 *
	 * @param builder
	 * @return RestTemplate
	*/
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	/**
	 * ObjectMapper
	 *
	 * @return ObjectMapper
	*/
	@Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
		mapper.setFilterProvider(new SimpleFilterProvider().setFailOnUnknownId(false));
		mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

}
