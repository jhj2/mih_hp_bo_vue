package com.mih.bo.security.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import com.mih.bo.common.Constants;
import com.mih.bo.common.util.RequestUtils;
import com.mih.bo.task.user.service.UserService;
import com.mih.bo.task.user.vo.LoginHistory;

/**
 * LoginSuccessHandler
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
	
	@Autowired
	private UserService userService;

	/*
	 * @see org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler#onAuthenticationSuccess(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.Authentication)
	*/
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		LoginHistory loginHistory = new LoginHistory();
		loginHistory.setMngrId(request.getParameter("username"));
		loginHistory.setRegIp(RequestUtils.remoteAddr(request));
		loginHistory.setLoginSuccesYn(Constants.YES);
		userService.insertLoginHistory(loginHistory);
		
		response.sendRedirect("/main");
	}
}