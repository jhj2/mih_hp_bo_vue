package com.mih.bo.security.handler;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * LogoutHandler
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public class LogoutHandler implements org.springframework.security.web.authentication.logout.LogoutHandler {

    /*
     * @see org.springframework.security.web.authentication.logout.LogoutHandler#logout(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.Authentication)
    */
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication)  {
    	HttpSession session = request.getSession();
    	session.invalidate();
    }

}
