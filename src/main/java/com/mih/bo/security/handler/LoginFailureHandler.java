package com.mih.bo.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.mih.bo.common.Constants;
import com.mih.bo.common.util.RequestUtils;
import com.mih.bo.task.user.service.UserService;
import com.mih.bo.task.user.vo.LoginHistory;

/**
 * LoginFailureHandler
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	
	@Autowired
	private UserService userService;

	/*
	 * @see org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler#onAuthenticationFailure(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)
	*/
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		String errorCode = "101";
		if (exception instanceof UsernameNotFoundException) {
			errorCode = "102";
		
		} else if(exception instanceof BadCredentialsException) {
			LoginHistory loginHistory = new LoginHistory();
			loginHistory.setMngrId(request.getParameter("username"));
			loginHistory.setRegIp(RequestUtils.remoteAddr(request));
			loginHistory.setLoginSuccesYn(Constants.NO);
			userService.insertLoginHistory(loginHistory);
			
			errorCode = "103";
		}
		
		setDefaultFailureUrl("/user/login?errorCode=" + errorCode);
		super.onAuthenticationFailure(request, response, exception);
	}
}