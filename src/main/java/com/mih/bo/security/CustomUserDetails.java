package com.mih.bo.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.mih.bo.security.entity.UserEntity;

import lombok.Data;

/**
 * CustomUserDetails
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Data
public class CustomUserDetails implements UserDetails {
	
	private static final long serialVersionUID = 3675403727018830243L;
	
	/**
	 * 사용자 VO
	 */
	private UserEntity user;
	
	/**
	 * 사용자아이디
	 */
	private String username;
	
	/**
	 * 비밀번호
	 */
	private String password;
	
	/**
	 * 활성화 여부
	 */
	private boolean isEnabled;
	
	/**
	 * 계정만료 여부
	 */
	private boolean isAccountNonExpired;
	
	/**
	 * 계정 잠김 여부
	 */
	private boolean isAccountNonLocked;
	
	/**
	 * 인증 만료 여부
	 */
	private boolean isCredentialsNonExpired;
	
	/**
	 * 권한 목록
	 */
	private Collection<? extends GrantedAuthority>authorities;
}