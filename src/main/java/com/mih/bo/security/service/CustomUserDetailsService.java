package com.mih.bo.security.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mih.bo.security.CustomUserDetails;
import com.mih.bo.security.entity.UserEntity;
import com.mih.bo.security.entity.UserRoleEntity;

/**
 * CustomUserDetailsService
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
    private UserSecurityService userSecurityService;
	
    /*
     * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
    */
    @Override
    public UserDetails loadUserByUsername(String loginId) throws UsernameNotFoundException {
        UserEntity userEntity = userSecurityService.getUser(loginId);
        if(userEntity == null) {
        	throw new UsernameNotFoundException("존재하지 않는 아이디입니다.");
        }

        CustomUserDetails userDetails = new CustomUserDetails();
        userDetails.setUsername(userEntity.getUserId());
        userDetails.setPassword(userEntity.getPassword());
        userDetails.setUser(userEntity);
        
        // ROLE 설정
        List<UserRoleEntity> customRoles = userSecurityService.getUserRoles(loginId);
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        if(customRoles != null) {
            for (UserRoleEntity customRole : customRoles) {
                authorities.add(new SimpleGrantedAuthority(customRole.getRoleName()));
            }
        }

        // 권한 목록 설정
        userDetails.setAuthorities(authorities);
        userDetails.setEnabled(true);
        userDetails.setAccountNonExpired(true);
        userDetails.setAccountNonLocked(true);
        userDetails.setCredentialsNonExpired(true);
        
        return userDetails;
    }
}
