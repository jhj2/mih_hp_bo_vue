package com.mih.bo.security.service;

import java.util.List;

import com.mih.bo.security.entity.UserEntity;
import com.mih.bo.security.entity.UserRoleEntity;

/**
 * 사용자 시큐리티 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public interface UserSecurityService {
	
	/**
	 * 사용자 조회
	 *
	 * @param loginUserId
	 * @return UserEntity
	*/
	public UserEntity getUser(String loginUserId);
	
    /**
     * 사용자 권한 조회
     *
     * @param loginUserId
     * @return List<UserRoleEntity>
    */
    public List<UserRoleEntity> getUserRoles(String loginUserId);

}
