package com.mih.bo.security.entity;

import com.mih.bo.common.BaseObject;

/**
 * UserRoleEntity
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public class UserRoleEntity extends BaseObject {
	
	private static final long serialVersionUID = -826555556290280201L;
	
	/**
	 * 로그인아이디
	 */
	private String userLoginId;
	
    /**
     * 권한명
     */
    private String roleName;

    public UserRoleEntity(String userLoginId, String roleName) {
        this.userLoginId = userLoginId;
        this.roleName = roleName;
    }

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLoginId) {
        this.userLoginId = userLoginId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
