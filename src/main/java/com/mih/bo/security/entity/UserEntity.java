package com.mih.bo.security.entity;

import com.mih.bo.common.AbstractModel;

import lombok.Getter;
import lombok.Setter;

/**
 * UserEntity
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Getter @Setter
public class UserEntity extends AbstractModel{
	
	private static final long serialVersionUID = -465865548146565116L;
	
	/**
	 * 사용자아이디
	 */
	private String userId;
	
    /**
     * 비밀번호
     */
    private String password;
    
    /**
     * 사용자명
     */
    private String userName;
    
    /**
     * 이메일
     */
    private String email;
    
    /**
     * 핸드폰번호 1
     */
    private String hpNo1;
    
    /**
     * 핸드폰번호 2
     */
    private String hpNo2;
    
    /**
     * 핸드폰번호 3
     */
    private String hpNo3;

}
