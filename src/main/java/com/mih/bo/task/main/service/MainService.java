package com.mih.bo.task.main.service;

import java.util.List;
import java.util.Map;

/**
 * 메인 세비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public interface MainService {
	
	/**
	 * 주문 현황 조회
	 *
	 * @return Map<String,Object>
	*/
	public Map<String, Object> selectOrderStat();
	
	/**
	 * 결제 현황 조회
	 *
	 * @return Map<String,Object>
	*/
	public Map<String, Object> selectPaymentStat();
	
	/**
	 * 지부별 캐비닛 현황 조회
	 *
	 * @return List<Map<String,Object>>
	*/
	public List<Map<String, Object>> selectBranchCabinetStat();

	/**
	 * 종료신청 목록 조회
	 *
	 * @return List<Map<String,Object>>
	*/
	public List<Map<String, Object>> selectEndRequestList();
	
	/**
	 * 지부 투어 신청 목록 조회
	 *
	 * @return List<Map<String,Object>>
	*/
	public List<Map<String, Object>> selectBranchTourList();
	
	/**
	 * 1:1 문의 목록 조회
	 *
	 * @return List<Map<String,Object>>
	*/
	public List<Map<String, Object>> selectInquiryList();

	/**
	 * Home 화면에 1:1 문의 수 / 답변완료 수 / 답변대기 표기
	 *
	 * @return Object
	*/
	public Map<String, Object> selectInqryCnt();
}