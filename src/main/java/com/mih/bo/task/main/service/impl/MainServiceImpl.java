package com.mih.bo.task.main.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.common.util.DateUtils;
import com.mih.bo.dao.MainDao;
import com.mih.bo.task.main.service.MainService;

/**
 * 메인 서비스 구현체
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Service
public class MainServiceImpl implements MainService {
	
	private static final Logger log = LoggerFactory.getLogger(MainServiceImpl.class);

	@Autowired
	private MainDao mainDao;
	
	/*
	 * @see com.bgbgb.bo.biz.main.service.MainService#selectOrderStat()
	*/
	@Override
	public Map<String, Object> selectOrderStat() {
		Map<String, Object> param = new HashMap<>();
		param.put("yesterday", DateUtils.toDateString(DateUtils.yesterday(), DateUtils.YYYYMMDD));
		param.put("today", DateUtils.toDateString(DateUtils.today(), DateUtils.YYYYMMDD));
		param.put("tomorrow", DateUtils.toDateString(DateUtils.tomorrow(), DateUtils.YYYYMMDD));
		return mainDao.selectOrderStat(param);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.main.service.MainService#selectPaymentStat()
	*/
	@Override
	public Map<String, Object> selectPaymentStat() {
		String batchDate = DateUtils.toDateString(DateUtils.yesterday(), DateUtils.YYYYMMDD);
		if(DateUtils.hour() > 11) {
			batchDate = DateUtils.toDateString(DateUtils.today(), DateUtils.YYYYMMDD);
		}
		return mainDao.selectPaymentStat(batchDate);
	}

	/*
	 * @see com.bgbgb.bo.biz.main.service.MainService#selectBranchCabinetStat()
	*/
	@Override
	public List<Map<String, Object>> selectBranchCabinetStat() {
		return mainDao.selectBranchCabinetStat();
	}
	
	/*
	 * @see com.bgbgb.bo.biz.main.service.MainService#selectEndRequestList()
	*/
	@Override
	public List<Map<String, Object>> selectEndRequestList() {
		return mainDao.selectEndRequestList();
	}

	/*
	 * @see com.bgbgb.bo.biz.main.service.MainService#selectBranchTourList()
	*/
	@Override
	public List<Map<String, Object>> selectBranchTourList() {
		return mainDao.selectBranchTourList();
	}

	/*
	 * @see com.bgbgb.bo.biz.main.service.MainService#selectInquiryList()
	*/
	@Override
	public List<Map<String, Object>> selectInquiryList() {
		return mainDao.selectInquiryList();
	}

	/*
	 * @see com.bgbgb.bo.biz.main.service.MainService#selectInqryCnt()
	*/
	@Override
	public Map<String, Object> selectInqryCnt(){
		return mainDao.selectInqryCnt();
	}
}