package com.mih.bo.task.main.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.util.DateUtils;
import com.mih.bo.common.web.controller.AbstractController;
import com.mih.bo.task.main.service.MainService;

/**
 * 메인 컨트롤러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Controller
public class MainController extends AbstractController{
	
	private static final Logger log = LoggerFactory.getLogger(MainController.class);

	@Autowired
	private MainService mainService;
	
	/**
	 * 메인 회면
	 *
	 * @param request
	 * @return ModelAndView
	*/
	@GetMapping(value="/main")
	public ModelAndView main(HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("today", DateUtils.toDateString(DateUtils.today(), DateUtils.YYYYMMDD_DOT));
		mav.addObject("yesterday", DateUtils.toDateString(DateUtils.yesterday(), DateUtils.YYYYMMDD_DOT));
		
//		mav.addObject("orderStat", mainService.selectOrderStat());
//		mav.addObject("paymentStat", mainService.selectPaymentStat());
//		mav.addObject("branchStat", mainService.selectBranchCabinetStat());
//		mav.addObject("endList", mainService.selectEndRequestList());
//		mav.addObject("tourList", mainService.selectBranchTourList());
//		mav.addObject("inquiryList", mainService.selectInquiryList());
		
		// Home 화면에 1:1 문의 수 / 답변완료 수 / 답변대기 표기
//		mav.addObject("inqryCnt", mainService.selectInqryCnt());
		
		mav.setViewName("main");
		return mav;
	}
}