package com.mih.bo.task.complaint.service;

import java.util.List;

import com.mih.bo.common.vo.Paging;
import com.mih.bo.task.complaint.vo.Notice;

/**
 * [종합민원실] 공지사항 관리 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public interface NoticeService {

	/**
	 * 공지사항 목록 조회
	 *
	 * @param notice
	 * @param paging
	 * @return List<Notice>
	*/
	public List<Notice> selectNoticeList(Notice notice, Paging paging);
	
	/**
	 * 공지사항 상세 보기
	 *
	 * @param notice
	 * @return Notice
	*/
	public Notice selectNoticeEdit(Notice notice);
	
	/**
	 * 총 조회 건수
	 *
	 * @param notice
	 * @return int
	*/
	public int selectNoticeListTotalCnt(Notice notice);

	/**
	 * 공지사항 게시글 수정
	 *
	 * @param notice
	 * @return int
	*/
	public int updateNotice(Notice notice);
	
	/**
	 * 공지사항 게시글 등록
	 *
	 * @param notice
	 * @return int
	*/
	public int insertNotice(Notice notice);
	
	/**
	 * 파일테이블에 저장할 index
	 *
	 * @return int
	*/
	public int selectFileTableIdx();
	
	/**
	 * 첨부파일 정렬 Index
	 *
	 * @param notice
	 * @return int
	*/
	public int selectFileSortIdx(Notice notice);
	
	/**
	 * 첨부파일 저장
	 *
	 * @param notice
	 */
	public void insertNoticeAttachFile(Notice notice);
}
