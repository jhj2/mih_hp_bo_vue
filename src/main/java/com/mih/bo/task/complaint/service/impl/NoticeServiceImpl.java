package com.mih.bo.task.complaint.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.common.Constants;
import com.mih.bo.common.util.PagingUtil;
import com.mih.bo.common.util.SecuritySessionUtils;
import com.mih.bo.common.util.UUIDUtils;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.NoticeDao;
import com.mih.bo.task.complaint.service.NoticeService;
import com.mih.bo.task.complaint.vo.Notice;

/**
 * [종합민원실] 공지사항 관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Service
public class NoticeServiceImpl implements NoticeService {

	@Autowired
	private NoticeDao noticeDao;
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.NoticeService#selectNoticeList(com.bgbgb.bo.biz.complaint.domain.Notice, com.bgbgb.bo.commons.domain.Paging)
	*/
	public List<Notice> selectNoticeList(Notice notice, Paging paging) {
		int iCnt = noticeDao.selectNoticeListTotalCnt(notice);
		if (iCnt > 0) {
			paging.setFormName("frmNotice");
			paging.setTotalRecordCount(iCnt);
			PagingUtil.setPagination(paging);
			
			notice.setLimit(paging.getLimit());
			notice.setOffset(paging.getOffset());
		}
		
		return noticeDao.selectNoticeList(notice);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.NoticeService#selectNoticeEdit(com.bgbgb.bo.biz.complaint.domain.Notice)
	*/
	public Notice selectNoticeEdit(Notice notice) {
		return noticeDao.selectNoticeEdit(notice);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.NoticeService#selectNoticeListTotalCnt(com.bgbgb.bo.biz.complaint.domain.Notice)
	*/
	public int selectNoticeListTotalCnt(Notice notice) {
		return noticeDao.selectNoticeListTotalCnt(notice);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.NoticeService#updateNotice(com.bgbgb.bo.biz.complaint.domain.Notice)
	*/
	public int updateNotice(Notice notice) {
		notice.setUpdtId(SecuritySessionUtils.getUser().getUserId());
		return noticeDao.updateNotice(notice);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.NoticeService#insertNotice(com.bgbgb.bo.biz.complaint.domain.Notice)
	*/
	public int insertNotice(Notice notice) {
		notice.setRegistId(SecuritySessionUtils.getUser().getUserId());
		notice.setUpdtId(SecuritySessionUtils.getUser().getUserId());
		return noticeDao.insertNotice(notice);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.NoticeService#selectFileTableIdx()
	*/
	public int selectFileTableIdx() {
		return noticeDao.selectFileTableIdx();
	}
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.NoticeService#selectFileSortIdx(com.bgbgb.bo.biz.complaint.domain.Notice)
	*/
	public int selectFileSortIdx(Notice notice) {
		return noticeDao.selectFileSortIdx(notice);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.NoticeService#insertNoticeAttachFile(com.bgbgb.bo.biz.complaint.domain.Notice)
	*/
	public void insertNoticeAttachFile(Notice notice) {
		String systemFileName = UUIDUtils.md5();
		notice.setFileTableNm("TB_CO_NOTICE");
		notice.setFileSysNm(systemFileName + Constants.DOT + notice.getFileExtsn());
    	notice.setUseYn("Y");
		noticeDao.insertNoticeAttachFile(notice);
	}
}
