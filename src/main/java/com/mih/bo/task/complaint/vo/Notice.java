package com.mih.bo.task.complaint.vo;

import com.mih.bo.common.vo.Search;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * [종합민원실] 공지사항 관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@ToString
@Getter
@Setter
public class Notice extends Search {

	private static final long serialVersionUID = 4340603006696358738L;
	
	/**
	 * 총건수
	 */
	private int cnt;
	/**
	 * 구분
	 */
	private String se;
	/**
	 * 검색어
	 */
	private String keyword;
	/**
	 * 공지 IDX
	 */
	private long noticeIdx;
	/**
	 * 공지 제목
	 */
	private String noticeSj;
	/**
	 * 공지 내용
	 */
	private String noticeCn;
	/**
	 * 상단공지여부
	 */
	private String upendNoticeYn;
	/**
	 * 조회건수
	 */
	private int inqireCnt;
	/**
	 * 공지지부 (코드)
	 */
	private long noticeBhfIdx;
	/**
	 * 공지지부명
	 */
	private String noticeBhfNm;
	/**
	 * 사용여부
	 */
	private String useYn;
	/**
	 * 삭제여부
	 */
	private String delYn;
	/**
	 * 파일 INDEX
	 */
	private int fileIdx;
	/**
	 * 파일테이블명
	 */
	private String fileTableNm;
	/**
	 * 파일테이블INDEX
	 */
	private int fileTableIdx;
	/**
	 * 시스템경로
	 */
	private String sysPath;
	/**
	 * 첨부파일경로
	 */
	private String attPath;
	/**
	 * 파일원본명
	 */
	private String fileOrgNm;
	/**
	 * 파일시스템명
	 */
	private String fileSysNm;
	/**
	 * 파일용량
	 */
	private int fileSize;
	/**
	 * 파일확장자
	 */
	private String fileExtsn;
	/**
	 * 파일정렬순서
	 */
	private int fileSort;
	/**
	 * 파일대체문구
	 */
	private String fileReplcText;
	/**
	 * 파일다운로드횟수
	 */
	private int fileDwldCnt;
	/**
	 * 파일정렬순서
	 */
	private int fileSortIdx;
	/**
	 * 지부Index
	 */
	private int bhfIdx;
	/**
	 * 지부명
	 */
	private String bhfNm;
}
