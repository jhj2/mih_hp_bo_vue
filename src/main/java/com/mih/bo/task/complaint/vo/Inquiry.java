package com.mih.bo.task.complaint.vo;

import com.mih.bo.common.AbstractModel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * [종합민원실] 1:1 문의 관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@ToString
@Getter
@Setter
public class Inquiry extends AbstractModel {

	private static final long serialVersionUID = 6781437816501748616L;

	/**
	 * 총건수 
	 */
	private int cnt;
	/**
	 * 문의Index
	 */
	private long inqryIdx;
	/**
	 * 문의제목
	 */
	private String inqrySj;
	/**
	 * 문의내용
	 */
	private String inqryCn;
	/**
	 * 답변여부
	 */
	private String answerYn;
	/**
	 * 답변내용
	 */
	private String answerCn;
	/**
	 * 회원Index
	 */
	private long mberIdx;
	/**
	 * 회원명
	 */
	private String mberNm;
	/**
	 * PC/Mobile/App
	 */
	private String chnnlTy;
	/**
	 * 답변자ID
	 */
	private String answrrId;
	/**
	 * [조회조건] 회원 조회 구분
	 */
	private String selMber;
	/**
	 * [조회조건] 회원명
	 */
	private String txtMberNm;
	/**
	 * [조회조건] 답변상태
	 */
	private String status;
	/**
	 * [조회조건] 키워드
	 */
	private String keyword;
	/**
	 * [조회조건] 키워드 조회명
	 */
	private String txtKeyword;
	/**
	 * 이미지 파일 URL
	 */
	private String fileUrl;
	/**
	 * 파일첨부경로
	 */
	private String attPath;
	/**
	 * 파일시스템경로
	 */
	private String fileSysNm;
	/**
	 * 핸드폰번호1
	 */
	private String hpNo1;
	/**
	 * 핸드폰번호2
	 */
	private String hpNo2;
	/**
	 * 핸드폰번호3
	 */
	private String hpNo3;
	/**
	 * 엑셀다운여부
	 */
	private String excelDownYn;
}
