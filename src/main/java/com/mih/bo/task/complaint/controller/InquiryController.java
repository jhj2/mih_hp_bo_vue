package com.mih.bo.task.complaint.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.Constants;
import com.mih.bo.common.util.RequestUtils;
import com.mih.bo.common.util.SecuritySessionUtils;
import com.mih.bo.common.util.StringUtils;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.common.web.controller.AbstractController;
import com.mih.bo.task.complaint.service.InquiryService;
import com.mih.bo.task.complaint.vo.Inquiry;
import com.mih.bo.task.message.service.BizMsgService;

/**
 * [종합민원실] 1:1 문의 관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Controller
public class InquiryController extends AbstractController {

	@Autowired
	private InquiryService inquiryService;
	
	@Autowired
	private BizMsgService bizMsgService;
	
	/**
	 * 전체 조회
	 *
	 * @param inquiry
	 * @param paging
	 * @return ModelAndView
	*/
	@GetMapping("/complaint/inquiry/list")
	public ModelAndView inqryList(@ModelAttribute Inquiry inquiry, @ModelAttribute Paging paging) {
		ModelAndView mav = new ModelAndView("complaint/inquiry/inquiryList");
		// 총건수
		int inquiryListCnt = inquiryService.selectInqryListTotalCnt(inquiry);
		// 조회
		paging.setRecordCountPerPage(20);
		List<Inquiry> inquiryList = inquiryService.selectInqryList(inquiry, paging);
		mav.addObject("list", inquiryList);
		mav.addObject("total", inquiryListCnt);
		// 페이징
		mav.addObject(paging);
		return mav;
	}
	
	/**
	 * 1:1문의 관리 목록 엑셀 다운로드
	 *
	 * @param inquiry
	 * @param paging
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception ModelAndView
	*/
	@PostMapping("/complaint/inquiry/excel")
	public ModelAndView inqryExcelList(@ModelAttribute Inquiry inquiry, @ModelAttribute Paging paging, HttpServletRequest request,  HttpServletResponse response) throws Exception  {

		ModelAndView mav = new ModelAndView("complaint/inquiry/inquiryExcelList");
		// 총건수
		int inquiryListCnt = inquiryService.selectInqryListTotalCnt(inquiry);
		// 조회
		paging.setRecordCountPerPage(20);
		inquiry.setExcelDownYn(Constants.YES);
		List<Inquiry> inquiryList = inquiryService.selectInqryList(inquiry, paging);
	
		
		String setdate = StringUtils.decimalFormat("yyyyMMddHHmmss",new Date());
		String titleName = Constants.BO_TITLE_NM_INQUIRY_DORMANT;

		titleName = new String(titleName.getBytes("KSC5601"), "8859_1");

		String clientBrowser = request.getHeader("User-Agent");

		if(clientBrowser.indexOf("MSIE 5.5")>-1 || clientBrowser.indexOf("MSIE 6.0") > -1 ){
		
		  response.setHeader("Content-Type", "doesn/matter;");
		  response.setHeader("Content-Disposition", "filename="+titleName+"_"+setdate+".xls");
		}else{
		  response.setHeader("Content-Type", "application/vnd.ms-excel;charset=UTF-8");
		  response.setHeader("Content-Disposition", "attachment; filename="+titleName+"_"+setdate+".xls");
		}

		response.setHeader("Content-Transfer-Encoding", "binary;");
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");
		
		mav.addObject("list", inquiryList);
		mav.addObject("total", inquiryListCnt);
		// 페이징
		mav.addObject(paging);
		
		return mav;
	}
	
	/**
	 * 게시글 상세 보기
	 *
	 * @param inquiry
	 * @param model
	 * @return String
	*/
	@GetMapping(value="/complaint/inquiry/view")
	public String inqryView(Inquiry inquiry, Model model) {
		Inquiry inqryEdit = inquiryService.selectInqryEdit(inquiry);
		List<Inquiry> inqryImg = inquiryService.selectImgFileUrl(inquiry);
		model.addAttribute("edit", inqryEdit);
		model.addAttribute("url", inqryImg);
		return "complaint/inquiry/inquiryView";
	}
	
	/**
	 * 답변하기 화면 이동
	 *
	 * @param inquiry
	 * @param model
	 * @return String
	*/
	@GetMapping(value="/complaint/inquiry/form")
	public String inqryAnswer(Inquiry inquiry, Model model) {
		Inquiry inqryAdd = inquiryService.selectInqryEdit(inquiry);
		List<Inquiry> inqryImg = inquiryService.selectImgFileUrl(inquiry);
		model.addAttribute("add", inqryAdd);
		model.addAttribute("url", inqryImg);
		return "complaint/inquiry/inquiryForm";
	}
	
	/**
	 * 답변 등록
	 *
	 * @param request
	 * @param inquiry
	 * @return
	 * @throws Exception String
	*/
	@PostMapping(value="/complaint/inquiry/edit")
	public String updateAnswer(HttpServletRequest request, @ModelAttribute Inquiry inquiry) throws Exception {
		// 세션값 불러오기
		inquiry.setUpdIp(RequestUtils.remoteAddr(request));
		inquiry.setAnswrrId(SecuritySessionUtils.getUser().getUserId());
		inquiry.setUpdId(SecuritySessionUtils.getUser().getUserId());
		inquiry.setAnswerYn(Constants.YES);
		inquiry.setChnnlTy(Constants.CHENNEL_TYPE);
		// escape 처리
		//inquiry.setAnswerCn(StringUtils.escapeString2(inquiry.getAnswerCn()));
        // 내용저장
		inquiryService.updateInqryAnswer(inquiry);
		// 고객 핸드폰 번호 조회
		Inquiry phoneInfo = inquiryService.selectMemberPhoneInfo(inquiry);
		// 알림톡
		Map<String, Object> mapParam = new HashMap<>();
		mapParam.put("mberNm",inquiry.getMberNm());	
		mapParam.put("destPhone", phoneInfo.getHpNo1()+phoneInfo.getHpNo2()+phoneInfo.getHpNo3());
		mapParam.put("mberIdx", inquiry.getMberIdx());
		bizMsgService.sendInquiryAnswer(mapParam);
		
		return "redirect:/complaint/inquiry/list";
	}
}
