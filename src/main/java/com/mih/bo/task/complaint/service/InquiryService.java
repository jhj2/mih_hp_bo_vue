package com.mih.bo.task.complaint.service;

import java.util.List;

import com.mih.bo.common.vo.Paging;
import com.mih.bo.task.complaint.vo.Inquiry;

/**
 * [종합민원실] 1:1 문의 관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public interface InquiryService {

	/**
	 * 목록 조회
	 *
	 * @param inquiry
	 * @param paging
	 * @return List<Inquiry>
	*/
	public List<Inquiry> selectInqryList(Inquiry inquiry, Paging paging);
	
	/**
	 * 목록 조회 전체 조회 건수
	 *
	 * @param inquiry
	 * @return int
	*/
	public int selectInqryListTotalCnt(Inquiry inquiry);
	
	/**
	 * 상세보기
	 *
	 * @param inquiry
	 * @return Inquiry
	*/
	public Inquiry selectInqryEdit(Inquiry inquiry);
	
	/**
	 * 답변등록
	 *
	 * @param inquiry void
	*/
	public void updateInqryAnswer(Inquiry inquiry);
	
	/**
	 * 첨부파일(이미지) URL
	 *
	 * @param inquiry
	 * @return List<Inquiry>
	*/
	public List<Inquiry> selectImgFileUrl(Inquiry inquiry);
	
	/**
	 * 고객 핸드폰 번호 조회
	 *
	 * @param inquiry
	 * @throws Exception void
	*/
	public Inquiry selectMemberPhoneInfo(Inquiry inquiry) throws Exception;
}
