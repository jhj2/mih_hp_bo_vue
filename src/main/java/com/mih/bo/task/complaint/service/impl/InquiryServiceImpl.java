package com.mih.bo.task.complaint.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.common.util.PagingUtil;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.InquiryDao;
import com.mih.bo.task.complaint.service.InquiryService;
import com.mih.bo.task.complaint.vo.Inquiry;

/**
 * [종합민원실] 1:1 문의 관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Service
public class InquiryServiceImpl implements InquiryService {

	@Autowired
	private InquiryDao inquiryDao;
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.InquiryService#selectInqryList(com.bgbgb.bo.biz.complaint.domain.Inquiry, com.bgbgb.bo.commons.domain.Paging)
	*/
	public List<Inquiry> selectInqryList(Inquiry inquiry, Paging paging) {
		int iCnt = inquiryDao.selectInqryListTotalCnt(inquiry);
		if (iCnt > 0) {
			paging.setFormName("frmInqry");
			paging.setTotalRecordCount(iCnt);
			PagingUtil.setPagination(paging);
			
			if(inquiry.getExcelDownYn() == null){
				inquiry.setLimit(paging.getLimit());
				inquiry.setOffset(paging.getOffset());	
			}
		}
		
		return inquiryDao.selectInqryList(inquiry);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.InquiryService#selectInqryListTotalCnt(com.bgbgb.bo.biz.complaint.domain.Inquiry)
	*/
	public int selectInqryListTotalCnt(Inquiry inquiry) {
		return inquiryDao.selectInqryListTotalCnt(inquiry);
	}
	
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.InquiryService#selectInqryEdit(com.bgbgb.bo.biz.complaint.domain.Inquiry)
	*/
	public Inquiry selectInqryEdit(Inquiry inquiry) {
		return inquiryDao.selectInqryEdit(inquiry);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.InquiryService#updateInqryAnswer(com.bgbgb.bo.biz.complaint.domain.Inquiry)
	*/
	public void updateInqryAnswer(Inquiry inquiry) {
		inquiryDao.updateInqryAnswer(inquiry);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.InquiryService#selectImgFileUrl(com.bgbgb.bo.biz.complaint.domain.Inquiry)
	*/
	public List<Inquiry> selectImgFileUrl(Inquiry inquiry) {
		return inquiryDao.selectImgFileUrl(inquiry);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.complaint.service.InquiryService#selectMemberPhoneInfo(com.bgbgb.bo.biz.complaint.domain.Inquiry)
	*/
	public Inquiry selectMemberPhoneInfo(Inquiry inquiry) {
		return inquiryDao.selectMemberPhoneInfo(inquiry);
	}
	
}
