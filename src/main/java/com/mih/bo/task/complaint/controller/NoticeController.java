package com.mih.bo.task.complaint.controller;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.Constants;
import com.mih.bo.common.service.CommonService;
import com.mih.bo.common.util.RequestUtils;
import com.mih.bo.common.util.SecuritySessionUtils;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.common.web.controller.AbstractController;
import com.mih.bo.task.complaint.service.NoticeService;
import com.mih.bo.task.complaint.vo.Notice;


/**
 * [종합민원실] 공지사항 관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Controller
public class NoticeController extends AbstractController {

	@Autowired
	private NoticeService noticeService;

	@Autowired
	private CommonService commonService;
	
	@Value("${file.upload.path}")
	private String fileUploadPath;
	
	/**
	 * 공지사항 목록 조회
	 *
	 * @param notice
	 * @param paging
	 * @return ModelAndView
	*/
	@GetMapping("/complaint/notice/list")
	public ModelAndView noticeList(@ModelAttribute Notice notice, @ModelAttribute Paging paging) {
		ModelAndView mav = new ModelAndView("complaint/notice/noticeList");
		// 총건수
		int noticeListCnt = noticeService.selectNoticeListTotalCnt(notice);
		// 화면에 보여지는 게시글 갯수
		paging.setRecordCountPerPage(20);
		// 조회
		List<Notice> noticeList = noticeService.selectNoticeList(notice, paging);
		
		// 지부 조회
		mav.addObject("noticeBranchList", commonService.selectBranchComboList());
		mav.addObject("list", noticeList);
		mav.addObject("total", noticeListCnt);
		mav.addObject(paging);
		return mav;
	}

	/**
	 * 공지사항 상세보기
	 *
	 * @param request
	 * @param notice
	 * @param model
	 * @return String
	*/
	@GetMapping("/complaint/notice/view")
	public String noticeView(HttpServletRequest request, Notice notice, Model model) {
		// 조회
		Notice noticeEdit = noticeService.selectNoticeEdit(notice);
		// 지부 조회
	    model.addAttribute("noticeBranchList", commonService.selectBranchComboList());
		model.addAttribute("edit", noticeEdit);
		
		return "complaint/notice/noticeView";
	}
	
	/**
	 * 게시글 수정
	 *
	 * @param request
	 * @param notice
	 * @return String
	*/
	@PostMapping(value="/complaint/notice/upload")
	public String updateNotice(HttpServletRequest request, @ModelAttribute Notice notice) {
		String useYn = notice.getUseYn();
		String strNoticeCn = notice.getNoticeCn();
		Pattern pattern = Pattern.compile("<img[^>]*src=[\"']?([^>\"']+)[\"']?[^>]*>");
		Matcher matcher = pattern.matcher(strNoticeCn);
        // 게시판 글번호 가져오기
        int fileTbIdx = noticeService.selectFileTableIdx();
        
        while (matcher.find()){
        	notice.setSysPath(matcher.group(1));
        	notice.setFileTableIdx(fileTbIdx);
        	
        	// 파일 확장자
        	File file = new File(matcher.group());
        	String fileName = file.getName();
        	String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
        	String ext2 = ext.substring(0,3);
        	notice.setFileExtsn(ext2);
        	
        	int fileSortIdx = noticeService.selectFileSortIdx(notice);
        	notice.setFileSort(fileSortIdx);
        	noticeService.insertNoticeAttachFile(notice);
        }
		notice.setUpdIp(RequestUtils.remoteAddr(request));
		notice.setUpdId(SecuritySessionUtils.getUser().getUserId());
		notice.setUseYn(useYn);
		
		// 내용저장
		noticeService.updateNotice(notice);
        
		return "redirect:/complaint/notice/list";
	}
	
	/**
	 * 공지사항 게시글 등록 이동
	 *
	 * @param model
	 * @return String
	*/
	@GetMapping(value="/complaint/notice/form")
	public String noticeAdd(Model model) {
		// 지부 조회
	    model.addAttribute("noticeBranchList", commonService.selectBranchComboList());
	    model.addAttribute("userId", SecuritySessionUtils.getUser().getUserId());
	    
		return "complaint/notice/noticeForm";
	}
	
	/**
	 * 공지사항 게시글 등록
	 *
	 * @param request
	 * @param notice
	 * @return String
	*/
	@PostMapping(value="/complaint/notice/add")
	public String insertNotice(HttpServletRequest request, Notice notice) {
		notice.setRegIp(RequestUtils.remoteAddr(request));
		notice.setUpdIp(RequestUtils.remoteAddr(request));
		notice.setRegId(SecuritySessionUtils.getUser().getUserId());
		notice.setUpdId(SecuritySessionUtils.getUser().getUserId());
		//notice.setUseYn(Constants.NO);
		notice.setUpendNoticeYn(Constants.NO);
		notice.setDelYn(Constants.NO);
		
		// 내용저장
		noticeService.insertNotice(notice);
        
		return "redirect:/complaint/notice/list";
	}
}
