package com.mih.bo.task.admin.vo;

import com.mih.bo.common.vo.Search;
import lombok.Data;

/**
 * 관리자관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.02.15.
 */
@Data
public class Admin extends Search{

	private static final long serialVersionUID = -612490258380999959L;
	
	/**
	 * 
	 */
	private int totCnt;
	
	/**
	 * 관리자아이디
	 */
	private String mngrId;
	/**
	 * 관리자명
	 */
	private String mngrNm;
	/**
	 * 관리자패스워드
	 */
	private String mngrPw;
	/**
	 * 이메일
	 */
	private String email;
	/**
	 * 핸드폰번호
	 */
	private String hpNo;
	/**
	 * 핸드폰번호1
	 */
	private String hpNo1;
	/**
	 * 핸드폰번호2
	 */
	private String hpNo2;
	/**
	 * 핸드폰번호3
	 */
	private String hpNo3;
	/**
	 * 정렬날짜
	 */
	private String sortDt;
}