package com.mih.bo.task.admin.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.common.util.PagingUtil;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.AdminDao;
import com.mih.bo.task.admin.service.AdminService;
import com.mih.bo.task.admin.vo.Admin;

/**
 * 관리자관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.02.15.
 */
@Service
public class AdminServiceImpl implements AdminService{
	
	private static final Logger log = LoggerFactory.getLogger(AdminServiceImpl.class);
	
	@Autowired
	private AdminDao adminDao;

	/*
	 * @see com.mih.bo.task.admin.service.AdminService#selectAdminList(com.mih.bo.task.admin.vo.Admin, com.mih.bo.common.vo.Paging)
	*/
	@Override
	public List<?> selectAdminList(Admin vo, Paging paging){
		int cnt = vo.getTotCnt();	// 총 count 개수조회
		if(cnt > 0){
			paging.setTotalRecordCount(cnt);
			paging.setFormName("adminFrm");	// formName값 세팅
			PagingUtil.setPagination(paging);
			
			vo.setLimit(paging.getLimit());
			vo.setOffset(paging.getOffset());
		}
		return adminDao.selectAdminList(vo);
	}

	/*
	 * @see com.mih.bo.task.admin.service.AdminService#selectAdmin(java.lang.String)
	*/
	@Override
	public Admin selectAdmin(String mngrId) {
		return adminDao.selectAdmin(mngrId);
	}
	
	/*
	 * @see com.mih.bo.task.admin.service.AdminService#insertAdmin(com.mih.bo.task.admin.vo.Admin)
	*/
	@Override
	public void insertAdmin(Admin vo) {
		
		// 핸드폰번호 자리수별 분리
		if(null != vo.getHpNo()){
			String[] hpNo = vo.getHpNo().split("-");
			vo.setHpNo1(hpNo[0]);
			vo.setHpNo2(hpNo[1]);
			vo.setHpNo3(hpNo[2]);
		}
		adminDao.insertAdmin(vo);
	}

	/*
	 * @see com.mih.bo.task.admin.service.AdminService#updateAdmin(com.mih.bo.task.admin.vo.Admin)
	*/
	@Override
	public void updateAdmin(Admin vo) {
		
		// 핸드폰번호 자리수별 분리
		if(null != vo.getHpNo()){
			String[] hpNo = vo.getHpNo().split("-");
			vo.setHpNo1(hpNo[0]);
			vo.setHpNo2(hpNo[1]);
			vo.setHpNo3(hpNo[2]);
		}
		adminDao.updateAdmin(vo);
	}
}