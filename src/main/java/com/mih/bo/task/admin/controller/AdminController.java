package com.mih.bo.task.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.service.CommonService;
import com.mih.bo.common.util.RequestUtils;
import com.mih.bo.common.util.SecuritySessionUtils;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.AdminDao;
import com.mih.bo.task.admin.service.AdminService;
import com.mih.bo.task.admin.vo.Admin;

/**
 * 관리자관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.02.15.
 */
@Controller
public class AdminController{
	private static final Logger log = LoggerFactory.getLogger(AdminController.class);
		
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private AdminDao adminDao;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	
	/**
	 * 관리자관리
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @param paging
	 * @return String
	*/
	@GetMapping(value="/admin/list")
	public String selectList(Model model, HttpServletRequest request,
			Admin vo, @ModelAttribute Paging paging){
		
		// 총 카운트 조회
		int totCnt = adminDao.selectAdminListTotCnt(vo);
		vo.setTotCnt(totCnt);
		
		paging.setRecordCountPerPage(20);	// 1페이지당 게시글 개수
		model.addAttribute("resultList", adminService.selectAdminList(vo, paging));
		model.addAttribute("totCnt", totCnt);
		model.addAttribute("vo", vo);	// 검색조건 유지하기 위한 VO 객체
		
		return "/admin/adminList";
	}
	
	/**
	 * 관리자관리 등록화면
	 *
	 * @param mngrId
	 * @return ModelAndView
	*/
	@GetMapping(path="/admin/add/popup")
	public ModelAndView adminAddPopup(@RequestParam(name="mngrId", required=false) String mngrId){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("admin/adminAddPopup");
		return mav;
	}
	
	/**
	 * 관리자관리 등록
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/admin/add"})
	public String insertAdmin(Model model, HttpServletRequest request, Admin vo){
		
		// 등록자ID 가져오기
		String userId = SecuritySessionUtils.getUser().getUserId();
		vo.setRegId(userId);
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setRegIp(ip);
		
		// 비밀번호 암호화
		vo.setMngrPw(passwordEncoder.encode(vo.getMngrPw()));
		
		adminService.insertAdmin(vo);
		
		return "redirect:/admin/list";
	}
	
	/**
	 * 관리자관리 단건조회 팝업
	 *
	 * @param mngrId
	 * @return ModelAndView
	*/
	@GetMapping(path="/admin/popup")
	public ModelAndView adminPopup(@RequestParam String mngrId){
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("admin", adminService.selectAdmin(mngrId));
		mav.setViewName("admin/adminPopup");
		return mav;
	}
	
	/**
	 * 관리자관리 수정
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/admin/upd"})
	public String updateAdmin(Model model, HttpServletRequest request, Admin vo){
		
		// 수정자ID 가져오기
		String userId = SecuritySessionUtils.getUser().getUserId();
		vo.setUpdtId(userId);
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setUpdtIp(ip);
		
		adminService.updateAdmin(vo);
		
		return "redirect:/admin/list";
	}
	
	/**
	 * 관리자관리 비밀번호 변경 팝업창
	 *
	 * @param mngrId
	 * @return ModelAndView
	*/
	@GetMapping(path="/admin/pw/popup")
	public ModelAndView adminPwPopup(@RequestParam String mngrId){
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("admin", adminService.selectAdmin(mngrId));
		mav.setViewName("admin/adminPwPopup");
		return mav;
	}
	
	/**
	 * 관리자관리 비밀번호 변경
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/admin/pw/upd"})
	public String updateAdminPw(Model model, HttpServletRequest request, Admin vo){
		
		// 수정자ID 가져오기
		String userId = SecuritySessionUtils.getUser().getUserId();
		vo.setUpdId(userId);
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setUpdIp(ip);
		
		// 비밀번호 암호화
		vo.setMngrPw(passwordEncoder.encode(vo.getMngrPw()));
		
		// 관리자관리 비밀번호 변경
		adminService.updateAdmin(vo);
		
		return "redirect:/admin/list";
	}
}