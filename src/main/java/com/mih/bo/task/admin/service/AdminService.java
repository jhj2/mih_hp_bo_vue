package com.mih.bo.task.admin.service;

import java.util.List;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.task.admin.vo.Admin;

/**
 * 관리자관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.02.15.
 */
public interface AdminService{

	/**
	 * 관리자관리 리스트 조회
	 *
	 * @param vo
	 * @param paging
	 * @return List<?>
	*/
	public List<?> selectAdminList(Admin vo, Paging paging);

	/**
	 * 관리자관리 단건조회
	 *
	 * @param mngrId
	 * @return Object
	*/
	public Admin selectAdmin(String mngrId);
	
	/**
	 * 관리자관리 등록
	 *
	 * @param vo void
	*/
	public void insertAdmin(Admin vo);

	/**
	 * 관리자관리 수정
	 *
	 * @param vo void
	*/
	public void updateAdmin(Admin vo);
}