package com.mih.bo.task.message.service;

import java.util.Map;

/**
 * 비즈메시지 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public interface BizMsgService {
	/**
	 * 메시지 전송 샘플
	 *
	 * @throws Exception
	 */
	void sendExample() throws Exception;

	/**
	 * 회원가입 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void sendJoinTheMembership(Map<String, Object> paramMap) throws Exception;

	/**
	 * 인증번호 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void sendCertificationNo(Map<String, Object> paramMap) throws Exception;

	/**
	 * 지부투어예약 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void sendBookBranchTour(Map<String, Object> paramMap) throws Exception;

	/**
	 * 주문완료 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void sendOrderCompleted(Map<String, Object> paramMap) throws Exception;

	/**
	 * 이용 당일 서비스 안내 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void sendServiceUsageGuide(Map<String, Object> paramMap) throws Exception;

	/**
	 * 정기 결제 및 계약 만료 안내 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void sendSubscriptionAndContractExpiration(Map<String, Object> paramMap) throws Exception;

	/**
	 * 휴면 계정 전환 안내 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void sendSwitchingDormantAccounts(Map<String, Object> paramMap) throws Exception;

	/**
	 * 1:1 문의 답변 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void sendInquiryAnswer(Map<String, Object> paramMap) throws Exception;

	/**
	 * 서비스 해지 신청 안내 전송
	 */
	void sendRequestToCancelService(Map<String, Object> paramMap) throws Exception;

	/**
	 * 서비스 해지 신청 안내 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void sendServiceCancellationCompleted(Map<String, Object> paramMap) throws Exception;

	/**
	 * 미결제 안내 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void sendUnpaidNotice(Map<String, Object> paramMap) throws Exception;
}