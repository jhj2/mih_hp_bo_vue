package com.mih.bo.task.message.vo;

import lombok.Builder;

/**
 * 비즈 메시지 VO
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Builder
public class BizMsg {

	private static final long serialVersionUID = -3171614421676966581L;

	/**
	 * 데이터 타입 (SMS 0/FAX 2/PHONE 3/MMS 5/AT 6/FT 7/RCS 8/BI 11/BW 12)
	 */
	private String msgType;
	/**
	 * 데이터 발송 상태 (대기 0/발송중 1/발송완료 2)
	 */
	private String status;
	/**
	 * 데이터 ID
	 */
	private String cmid;
	/**
	 * 데이터 등록 시간
	 */
	private String requestTime;
	/**
	 * 발송 기준 시간
	 */
	private String sendTime;
	/**
	 * 수신번호
	 */
	private String destPhone;
	/**
	 * 수신자명
	 */
	private String sendPhone;
	/**
	 * 메시지 내용
	 */
	private String msgBody;
	/**
	 * 템플릿 코드
	 */
	private String templateCode;
	/**
	 * 발신프로필 키
	 */
	private String senderKey;
	/**
	 * 국가코드일
	 */
	private String nationCode;
	/**
	 * 대체발송 메시지 타입
	 */
	private String reType;
	/**
	 * 대체발송 메시지 내용
	 */
	private String reBody;
	/**
	 * 첨부파일
	 */
	private String attachedFile;
}
