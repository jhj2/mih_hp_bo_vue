package com.mih.bo.task.message.vo;

import com.mih.bo.common.AbstractModel;

import lombok.Builder;

/**
 * 메시지 전송 이력 VO
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Builder
public class MsgSndngHst extends AbstractModel {
	private static final long serialVersionUID = -6217827293574248959L;

	/**
	 * 메시지발송이력IDX
	 */
	private String msgSndngHstIdx;
	/**
	 * E(이메일) A(알림톡)
	 */
	private String msgCoursTy;
	/**
	 * 회원IDX
	 */
	private String mberIdx;
	/**
	 * 메시지발송일시
	 */
	private String msgSndngDt;
	/**
	 * 메시지내용
	 */
	private String msgCn;

}
