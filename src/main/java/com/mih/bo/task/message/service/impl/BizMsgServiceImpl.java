package com.mih.bo.task.message.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.common.Constants;
import com.mih.bo.common.config.Property;
import com.mih.bo.common.service.CommonService;
import com.mih.bo.common.util.BizMsgFactory;
import com.mih.bo.common.util.StringUtils;
import com.mih.bo.dao.BizMsgDao;
import com.mih.bo.task.message.service.BizMsgService;
import com.mih.bo.task.message.vo.BizMsg;
import com.mih.bo.task.message.vo.MsgSndngHst;

/**
 * 비즈메시지 서비스 구현체
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Service
public class BizMsgServiceImpl implements BizMsgService {

	private static final Logger log = LoggerFactory.getLogger(BizMsgServiceImpl.class);

	@Autowired
	private BizMsgDao bizMsgDao;
	@Autowired
	private CommonService commonService;

	/**
	 * 알림톡 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void sendBizMsg(Map<String, Object> paramMap) throws Exception {

		String msgType = Property.get("bizMsg.msgType");
		String status = "0";
		String destPhone = StringUtils.defaultString(paramMap.get("destPhone"));
		String sendPhone = StringUtils.defaultString(paramMap.get("sendPhone"), Property.get("bizMsg.sendPhone"));
		String msgBody = StringUtils.defaultString(paramMap.get("msgBody"));
		String templateCode = StringUtils.defaultString(paramMap.get("templateCode"));
		String senderKey = Property.get("bizMsg.senderKey");
		String nationCode = Property.get("bizMsg.nationCode");
		String reType = Property.get("bizMsg.reType");
		String reBody = "";
		String mberIdx = StringUtils.defaultString(paramMap.get("mberIdx"));
		String buttonYn = StringUtils.defaultString(paramMap.get("buttonYn"));
		String attachedFile = buttonYn.equals("Y") ? "button.json" : "";

		BizMsg bizMsg = BizMsg.builder()
				.msgType(msgType)
				.status(status)
				.destPhone(destPhone)
				.sendPhone(sendPhone)
				.msgBody(msgBody)
				.templateCode(templateCode)
				.senderKey(senderKey)
				.nationCode(nationCode)
				.reType(reType)
				.reBody(reBody)
				.attachedFile(attachedFile)
				.build();
		bizMsgDao.insertBizMsg(bizMsg);

		// 메시지 전송 이력 등록
		if (StringUtils.isNotEmpty(mberIdx)) {
			MsgSndngHst msgSndngHst = MsgSndngHst.builder()
					.mberIdx(mberIdx)
					.msgCoursTy("A")
					.msgCn(msgBody)
					.build();
			insertMsgSndngHst(msgSndngHst);
		}
	}

	/**
	 * 메시지 전송 이력 등록
	 *
	 * @param msgSndngHst
	 * @throws Exception
	 */
	void insertMsgSndngHst(MsgSndngHst msgSndngHst) throws Exception {
		bizMsgDao.insertMsgSndngHst(msgSndngHst);
	}

	/**
	 * 메시지 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	void send(Map<String, Object> paramMap) throws Exception {
		// 마케팅 정보 수신 동의(알림톡) 확인이 필요한 경우
		String marktInfoRecptnCnfirmYn = StringUtils.defaultString(paramMap.get("marktInfoRecptnCnfirmYn"));
		if (marktInfoRecptnCnfirmYn.equals("Y")) {
			paramMap.put("stplstCd", Constants.STPLST_CD_AT);
			if (commonService.isStplatAgreYn(paramMap) == false) {
				log.info("마케팅 정보 수신(알림톡) 미동의로 알림톡 전송 불가 [회원 idx : {}] [템플릿코드 : {}]",
						StringUtils.defaultString(paramMap.get("mberIdx")), StringUtils.defaultString(paramMap.get("templateCode")));
				return;
			}
		}
		String msg = BizMsgFactory.getMessageInstance(StringUtils.defaultString(paramMap.get("templateCode")), paramMap);
		paramMap.put("msgBody", msg);
		sendBizMsg(paramMap);
	}

	@Override
	public void sendExample() throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_000";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		String name = "예제";
		bizMsgMap.put("name", name);
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		bizMsgMap.put("mberIdx", "1");

		send(bizMsgMap);
	}

	/**
	 * 회원가입 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	@Override
	public void sendJoinTheMembership(Map<String, Object> paramMap) throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_2021092817170105367208007";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		// #{고객명}
		bizMsgMap.put("mberNm", StringUtils.defaultString(paramMap.get("mberNm")));
		// 전화번호
		bizMsgMap.put("destPhone", StringUtils.defaultString(paramMap.get("destPhone")));
		// 회원 IDX
		bizMsgMap.put("mberIdx", StringUtils.defaultString(paramMap.get("mberIdx")));
		// 템플릿 코드
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		// 버튼 여부
		bizMsgMap.put("buttonYn", "Y");
		// 마케팅 정보 수신 동의(알림톡) 확인
		bizMsgMap.put("marktInfoRecptnCnfirmYn", "N");

		send(bizMsgMap);
	}

	/**
	 * 인증번호 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	@Override
	public void sendCertificationNo(Map<String, Object> paramMap) throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_2021091714125926143498192";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		// #{인증번호}
		bizMsgMap.put("certificationNo", StringUtils.defaultString(paramMap.get("certificationNo")));
		// 전화번호
		bizMsgMap.put("destPhone", StringUtils.defaultString(paramMap.get("destPhone")));
		// 회원 IDX
		bizMsgMap.put("mberIdx", StringUtils.defaultString(paramMap.get("mberIdx")));
		// 템플릿 코드
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		// 버튼 여부
		bizMsgMap.put("buttonYn", "N");
		// 마케팅 정보 수신 동의(알림톡) 확인
		bizMsgMap.put("marktInfoRecptnCnfirmYn", "N");

		send(bizMsgMap);
	}

	/**
	 * 지부투어예약 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	@Override
	public void sendBookBranchTour(Map<String, Object> paramMap) throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_2021110815390426514574276";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		// #{고객명}
		bizMsgMap.put("mberNm", StringUtils.defaultString(paramMap.get("mberNm")));
		// #{방문일}
		bizMsgMap.put("tourHopeDe", StringUtils.defaultString(paramMap.get("tourHopeDe")));
		// #{지부명}
		bizMsgMap.put("bhfNm", StringUtils.defaultString(paramMap.get("bhfNm")));
		// 전화번호
		bizMsgMap.put("destPhone", StringUtils.defaultString(paramMap.get("destPhone")));
		// 회원 IDX
		bizMsgMap.put("mberIdx", StringUtils.defaultString(paramMap.get("mberIdx")));
		// #{link Url}
		bizMsgMap.put("linkUrl", StringUtils.defaultString(paramMap.get("linkUrl")));
		// 템플릿 코드
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		// 버튼 여부
		bizMsgMap.put("buttonYn", "Y");
		// 마케팅 정보 수신 동의(알림톡) 확인
		bizMsgMap.put("marktInfoRecptnCnfirmYn", "N");

		send(bizMsgMap);
	}

	/**
	 * 주문완료 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	@Override
	public void sendOrderCompleted(Map<String, Object> paramMap) throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_2021091714502726143559222";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		// #{고객명}
		bizMsgMap.put("mberNm", StringUtils.defaultString(paramMap.get("mberNm")));
		// #{주문일}
		bizMsgMap.put("ordDt", StringUtils.defaultString(paramMap.get("ordDt")));
		// #{시작일}
		bizMsgMap.put("useBgnde", StringUtils.defaultString(paramMap.get("useBgnde")));
		// #{지부명}
		bizMsgMap.put("bhfNm", StringUtils.defaultString(paramMap.get("bhfNm")));
		// #{상품명}
		bizMsgMap.put("goodsNm", StringUtils.defaultString(paramMap.get("goodsNm")));
		// #{청구금액1} - 정기결제 예정 금액
		bizMsgMap.put("fixdPayAmt", StringUtils.defaultString(paramMap.get("fixdPayAmt")));
		// #{청구금액2} - 예치금
		bizMsgMap.put("deposit", StringUtils.defaultString(paramMap.get("deposit")));
		// 전화번호
		bizMsgMap.put("destPhone", StringUtils.defaultString(paramMap.get("destPhone")));
		// 회원 IDX
		bizMsgMap.put("mberIdx", StringUtils.defaultString(paramMap.get("mberIdx")));
		// 템플릿 코드
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		// 버튼 여부
		bizMsgMap.put("buttonYn", "Y");
		// 마케팅 정보 수신 동의(알림톡) 확인
		bizMsgMap.put("marktInfoRecptnCnfirmYn", "N");

		send(bizMsgMap);
	}

	/**
	 * 이용 당일 서비스 안내 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	@Override
	public void sendServiceUsageGuide(Map<String, Object> paramMap) throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_2021113015590805367196461";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		// #{고객명}
		bizMsgMap.put("mberNm", StringUtils.defaultString(paramMap.get("mberNm")));
		// #{주문일}
		bizMsgMap.put("ordDt", StringUtils.defaultString(paramMap.get("ordDt")));
		// #{시작일}
		bizMsgMap.put("useBgnde", StringUtils.defaultString(paramMap.get("useBgnde")));
		// #{지부명}
		bizMsgMap.put("bhfNm", StringUtils.defaultString(paramMap.get("bhfNm")));
		// #{상품명}
		bizMsgMap.put("goodsNm", StringUtils.defaultString(paramMap.get("goodsNm")));
		// #{상품관리번호}
		bizMsgMap.put("goodsLbl", StringUtils.defaultString(paramMap.get("goodsLbl")));
		// #{청구금액1} - 정기결제 예정 금액
		bizMsgMap.put("fixdPayAmt", StringUtils.defaultString(paramMap.get("fixdPayAmt")));
		// #{청구금액2} - 예치금
		bizMsgMap.put("deposit", StringUtils.defaultString(paramMap.get("deposit")));
		// 전화번호
		bizMsgMap.put("destPhone", StringUtils.defaultString(paramMap.get("destPhone")));
		// 회원 IDX
		bizMsgMap.put("mberIdx", StringUtils.defaultString(paramMap.get("mberIdx")));
		// #{link Url}
		bizMsgMap.put("linkUrl", StringUtils.defaultString(paramMap.get("linkUrl")));
		// 템플릿 코드
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		// 버튼 여부
		bizMsgMap.put("buttonYn", "Y");
		// 마케팅 정보 수신 동의(알림톡) 확인
		bizMsgMap.put("marktInfoRecptnCnfirmYn", "N");

		send(bizMsgMap);
	}

	/**
	 * 정기 결제 및 계약 만료 안내 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	@Override
	public void sendSubscriptionAndContractExpiration(Map<String, Object> paramMap) throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_2021110811451005367058175";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		// #{고객명}
		bizMsgMap.put("mberNm", StringUtils.defaultString(paramMap.get("mberNm")));
		// #{시작일}
		bizMsgMap.put("useBgnde", StringUtils.defaultString(paramMap.get("useBgnde")));
		// #{종료일}
		bizMsgMap.put("useEndde", StringUtils.defaultString(paramMap.get("useEndde")));
		// #{지부명}
		bizMsgMap.put("bhfNm", StringUtils.defaultString(paramMap.get("bhfNm")));
		// #{상품명}
		bizMsgMap.put("goodsNm", StringUtils.defaultString(paramMap.get("goodsNm")));
		// #{상품관리번호}
		bizMsgMap.put("goodsLbl", StringUtils.defaultString(paramMap.get("goodsLbl")));
		// #{결제일}
		bizMsgMap.put("fixdPayPrarnde", StringUtils.defaultString(paramMap.get("fixdPayPrarnde")));
		// #{청구금액1} - 정기결제 예정 금액
		bizMsgMap.put("fixdPayAmt", StringUtils.defaultString(paramMap.get("fixdPayAmt")));
		// 전화번호
		bizMsgMap.put("destPhone", StringUtils.defaultString(paramMap.get("destPhone")));
		// 회원 IDX
		bizMsgMap.put("mberIdx", StringUtils.defaultString(paramMap.get("mberIdx")));
		// 템플릿 코드
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		// 버튼 여부
		bizMsgMap.put("buttonYn", "Y");
		// 마케팅 정보 수신 동의(알림톡) 확인
		bizMsgMap.put("marktInfoRecptnCnfirmYn", "N");

		send(bizMsgMap);
	}

	/**
	 * 휴면 계정 전환 안내 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	@Override
	public void sendSwitchingDormantAccounts(Map<String, Object> paramMap) throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_2021091714552610216387208";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		// #{날짜}
		bizMsgMap.put("drmncyCnvrsDe", StringUtils.defaultString(paramMap.get("drmncyCnvrsDe")));
		// 전화번호
		bizMsgMap.put("destPhone", StringUtils.defaultString(paramMap.get("destPhone")));
		// 회원 IDX
		bizMsgMap.put("mberIdx", StringUtils.defaultString(paramMap.get("mberIdx")));
		// 템플릿 코드
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		// 버튼 여부
		bizMsgMap.put("buttonYn", "Y");
		// 마케팅 정보 수신 동의(알림톡) 확인
		bizMsgMap.put("marktInfoRecptnCnfirmYn", "N");

		send(bizMsgMap);
	}

	/**
	 * 1:1 문의 답변 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	@Override
	public void sendInquiryAnswer(Map<String, Object> paramMap) throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_2021091714583826143377226";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		// #{고객명}
		bizMsgMap.put("mberNm", StringUtils.defaultString(paramMap.get("mberNm")));
		// 전화번호
		bizMsgMap.put("destPhone", StringUtils.defaultString(paramMap.get("destPhone")));
		// 회원 IDX
		bizMsgMap.put("mberIdx", StringUtils.defaultString(paramMap.get("mberIdx")));
		// 템플릿 코드
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		// 버튼 여부
		bizMsgMap.put("buttonYn", "Y");
		// 마케팅 정보 수신 동의(알림톡) 확인
		bizMsgMap.put("marktInfoRecptnCnfirmYn", "N");

		send(bizMsgMap);
	}

	/**
	 * 서비스 해지 신청 안내 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	@Override
	public void sendRequestToCancelService(Map<String, Object> paramMap) throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_2021091715003210216678209";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		// #{고객명}
		bizMsgMap.put("mberNm", StringUtils.defaultString(paramMap.get("mberNm")));
		// #{시작일}
		bizMsgMap.put("useBgnde", StringUtils.defaultString(paramMap.get("useBgnde")));
		// #{종료일}
		bizMsgMap.put("useEndde", StringUtils.defaultString(paramMap.get("useEndde")));
		// #{지부명}
		bizMsgMap.put("bhfNm", StringUtils.defaultString(paramMap.get("bhfNm")));
		// #{상품명}
		bizMsgMap.put("goodsNm", StringUtils.defaultString(paramMap.get("goodsNm")));
		// #{상품관리번호}
		bizMsgMap.put("goodsLbl", StringUtils.defaultString(paramMap.get("goodsLbl")));
		// #{해지일} - 종료희망일
		bizMsgMap.put("endHopeDe", StringUtils.defaultString(paramMap.get("endHopeDe")));
		// 전화번호
		bizMsgMap.put("destPhone", StringUtils.defaultString(paramMap.get("destPhone")));
		// 회원 IDX
		bizMsgMap.put("mberIdx", StringUtils.defaultString(paramMap.get("mberIdx")));
		// 템플릿 코드
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		// 버튼 여부
		bizMsgMap.put("buttonYn", "Y");
		// 마케팅 정보 수신 동의(알림톡) 확인
		bizMsgMap.put("marktInfoRecptnCnfirmYn", "N");

		send(bizMsgMap);
	}

	/**
	 * 서비스 해지 절차 완료 안내 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	@Override
	public void sendServiceCancellationCompleted(Map<String, Object> paramMap) throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_2021091715015710216752210";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		// #{고객명}
		bizMsgMap.put("mberNm", StringUtils.defaultString(paramMap.get("mberNm")));
		// #{시작일}
		bizMsgMap.put("useBgnde", StringUtils.defaultString(paramMap.get("useBgnde")));
		// #{종료일}
		bizMsgMap.put("useEndde", StringUtils.defaultString(paramMap.get("useEndde")));
		// #{지부명}
		bizMsgMap.put("bhfNm", StringUtils.defaultString(paramMap.get("bhfNm")));
		// #{상품명}
		bizMsgMap.put("goodsNm", StringUtils.defaultString(paramMap.get("goodsNm")));
		// #{상품관리번호}
		bizMsgMap.put("goodsLbl", StringUtils.defaultString(paramMap.get("goodsLbl")));
		// #{해지일} - 종료완료일
		bizMsgMap.put("endComptDt", StringUtils.defaultString(paramMap.get("endComptDt")));
		// 전화번호
		bizMsgMap.put("destPhone", StringUtils.defaultString(paramMap.get("destPhone")));
		// 회원 IDX
		bizMsgMap.put("mberIdx", StringUtils.defaultString(paramMap.get("mberIdx")));
		// 템플릿 코드
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		// 버튼 여부
		bizMsgMap.put("buttonYn", "Y");
		// 마케팅 정보 수신 동의(알림톡) 확인
		bizMsgMap.put("marktInfoRecptnCnfirmYn", "N");

		send(bizMsgMap);
	}

	/**
	 * 미결제 안내 전송
	 *
	 * @param paramMap
	 * @throws Exception
	 */
	@Override
	public void sendUnpaidNotice(Map<String, Object> paramMap) throws Exception {
		String CONST_TEMPLATE_CODE = "bizp_2021091715031510216962211";
		Map<String, Object> bizMsgMap = new HashMap<String, Object>();

		// #{고객명}
		bizMsgMap.put("mberNm", StringUtils.defaultString(paramMap.get("mberNm")));
		// #{미납금액}
		bizMsgMap.put("npyAmt", StringUtils.defaultString(paramMap.get("npyAmt")));
		// #{미납일수}
		bizMsgMap.put("npyDayCnt", StringUtils.defaultString(paramMap.get("npyDayCnt")));
		// 전화번호
		bizMsgMap.put("destPhone", StringUtils.defaultString(paramMap.get("destPhone")));
		// 회원 IDX
		bizMsgMap.put("mberIdx", StringUtils.defaultString(paramMap.get("mberIdx")));
		// 템플릿 코드
		bizMsgMap.put("templateCode", CONST_TEMPLATE_CODE);
		// 버튼 여부
		bizMsgMap.put("buttonYn", "Y");
		// 마케팅 정보 수신 동의(알림톡) 확인
		bizMsgMap.put("marktInfoRecptnCnfirmYn", "N");

		send(bizMsgMap);
	}
}
