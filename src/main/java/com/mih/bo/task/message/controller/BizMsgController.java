package com.mih.bo.task.message.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.mih.bo.task.message.service.BizMsgService;

/**
 * 비즈메시지 컨트롤러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Controller
public class BizMsgController {
	private static final Logger log = LoggerFactory.getLogger(BizMsgController.class);

	@Autowired
	private BizMsgService bizMsgService;

	/**
	 * 메시지 전송 샘플
	 *
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@GetMapping(value = "/message/sendExample")
	public String sendExample(Model model, HttpServletRequest request) throws Exception {
//		bizMsgService.sendExample();

		// 인증번호 전송 예제
		/*
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("certificationNo", "123456");
		paramMap.put("destPhone", "01000000000");
		paramMap.put("mberIdx", "1");
		bizMsgService.sendCertificationNo(paramMap);
		*/
		// 알림톡(이용 당일 서비스 안내) 테스트
		// usageHistoryManagementService.guideTheServiceOnTheDayOfUse();

		return "main";
	}
}