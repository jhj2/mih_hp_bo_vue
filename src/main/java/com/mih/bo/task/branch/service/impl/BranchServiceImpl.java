package com.mih.bo.task.branch.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mih.bo.common.enums.FileAttachTypes;
import com.mih.bo.common.service.FileService;
import com.mih.bo.common.util.PagingUtil;
import com.mih.bo.common.util.SecuritySessionUtils;
import com.mih.bo.common.vo.FileUpload;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.BranchDao;
import com.mih.bo.dao.FileDao;
import com.mih.bo.task.branch.service.BranchService;
import com.mih.bo.task.branch.vo.Branch;
/**
 * 지부관리 서비스 구현체
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Service
public class BranchServiceImpl implements BranchService{
	
	private static final Logger log = LoggerFactory.getLogger(BranchServiceImpl.class);
	
	@Autowired
	private BranchDao branchDao;
	
	// 파일처리 관련 유틸리티 import
	@Autowired
	private FileService fileService;
	
	@Autowired
	private FileDao fileDao;

	/*
	 * @see com.bgbgb.bo.biz.branch.service.BranchService#selectBranchInfoList(com.bgbgb.bo.biz.branch.domain.Branch, com.bgbgb.bo.commons.domain.Paging)
	*/
	@Override
	public List<?> selectBranchInfoList(Branch vo, Paging paging){
		int cnt = vo.getTotCnt();	// 총 count 개수조회
		if(cnt > 0){
			paging.setTotalRecordCount(cnt);
			paging.setFormName("branchFrm");	// formName값 세팅
			PagingUtil.setPagination(paging);
			
			vo.setLimit(paging.getLimit());
			vo.setOffset(paging.getOffset());
		}
		return branchDao.selectBranchInfoList(vo);
	}
	
	/*
	 * @see com.bgbgb.bo.biz.branch.service.BranchService#selectBranchInfo(com.bgbgb.bo.biz.branch.domain.Branch)
	*/
	@Override
	public Branch selectBranchInfo(Branch vo){
		return branchDao.selectBranchInfo(vo);
	}

	/*
	 * @see com.bgbgb.bo.biz.branch.service.BranchService#insertBranchInfo(com.bgbgb.bo.biz.branch.domain.Branch, java.util.List)
	*/
	@Override
	public void insertBranchInfo(Branch vo, List<MultipartFile> files){
		String userId = SecuritySessionUtils.getUser().getUserId();
		vo.setRegId(userId);
		branchDao.insertBranchInfo(vo);
		
		// 파일 업로드(여러개) -> 첨부파일 저장
		int idx = 0;
		for(MultipartFile file : files){
			
			++idx;
			// 파일명 null값 체크
			if(!"".equals(file.getOriginalFilename())){
				FileUpload result = fileService.uploadFile(file, FileAttachTypes.BRANCH);
				log.info(result.toJsonPrettify());
				
				result.setFileTableIdx(vo.getBhfIdx());
				result.setUseYn("Y");
				result.setFileSort((idx));
				fileDao.insertFileAttach(result);
			}
		}
	}

	/*
	 * @see com.bgbgb.bo.biz.branch.service.BranchService#updateBranchInfo(com.bgbgb.bo.biz.branch.domain.Branch)
	*/
	@Override
	public void updateBranchInfo(Branch vo){
		String userId = SecuritySessionUtils.getUser().getUserId();
		ArrayList<String> arr1 = new ArrayList<String>();
		
		// 파일 삭제하는 경우 로직
		if(null != vo.getDelFileIdx()){
			if(!"".equals(vo.getDelFileIdx())) {
				String[] delFileIdx = vo.getDelFileIdx().split("\\|");
				for(int i=0; i<delFileIdx.length; i++) {
					arr1.add(delFileIdx[i]);
				}
				log.info("arr1====="+arr1);
				
				// TB_BR_BHF 테이블 데이터 use_yn = 'N'으로 수정
				if(arr1.size() > 0){
					vo.setArrDelFileIdx(arr1);
					vo.setUseYn("N");
					branchDao.updateBrBhfUseYn(vo);
				}
			}
		}
		
		// 파일 업로드(여러개) -> 첨부파일 저장
		int idx = 0;
		if(null != vo.getFiles()){
			for(MultipartFile file : vo.getFiles()){
				log.info("vo.getFSort().get(idx)====="+vo.getFSort().get(idx));
				
				// 파일명 null값 체크
				if(!"".equals(file.getOriginalFilename())){
					FileUpload result = fileService.uploadFile(file, FileAttachTypes.BRANCH);
					log.info(result.toJsonPrettify());
					
					result.setFileTableIdx(vo.getBhfIdx());
					result.setUseYn("Y");
					result.setFileSort(Integer.parseInt(vo.getFSort().get(idx).toString()));
					fileDao.insertFileAttach(result);
				}
				idx++;
			}
		}
		
		int idx2 = 0;
		if(null != vo.getFiles2()){
			for(MultipartFile file : vo.getFiles2()){
				log.info("vo.getFSort2().get(idx)====="+vo.getFSort2().get(idx2));
				
				// 파일명 null값 체크
				if(!"".equals(file.getOriginalFilename())){
					FileUpload result = fileService.uploadFile(file, FileAttachTypes.BRANCH);
					log.info(result.toJsonPrettify());
					
					result.setFileTableIdx(vo.getBhfIdx());
					result.setUseYn("Y");
					result.setFileSort(Integer.parseInt(vo.getFSort2().get(idx2).toString()));
					fileDao.insertFileAttach(result);
				}
				idx2++;
			}
		}
		vo.setUpdId(userId);
		branchDao.updateBranchInfo(vo);
	}

	/*
	 * @see com.bgbgb.bo.biz.branch.service.BranchService#imgList(com.bgbgb.bo.biz.branch.domain.Branch)
	*/
	@Override
	public List<?> imgList(Branch vo) {
		return branchDao.imgList(vo);
	}

	/*
	 * @see com.bgbgb.bo.biz.branch.service.BranchService#selectBranchTourList(com.bgbgb.bo.biz.branch.domain.Branch, com.bgbgb.bo.commons.domain.Paging)
	*/
	@Override
	public List<?> selectBranchTourList(Branch vo, Paging paging) {
		
		int cnt = vo.getTotCnt();	// 총 count 개수조회
		if(cnt > 0){
			paging.setTotalRecordCount(cnt);
			paging.setFormName("branchFrm");	// formName값 세팅
			PagingUtil.setPagination(paging);
			
			vo.setLimit(paging.getLimit());
			vo.setOffset(paging.getOffset());
		}
		return branchDao.selectBranchTourList(vo);
	}

	/*
	 * @see com.bgbgb.bo.biz.branch.service.BranchService#brBhfList(com.bgbgb.bo.biz.branch.domain.Branch)
	*/
	@Override
	public List<?> brBhfList(Branch vo) {
		return branchDao.brBhfList(vo);
	}

	/*
	 * @see com.bgbgb.bo.biz.branch.service.BranchService#updateBrTour(com.bgbgb.bo.biz.branch.domain.Branch)
	*/
	@Override
	public void updateBrTour(Branch vo) {
		branchDao.updateBrTour(vo);
	}

	/*
	 * @see com.bgbgb.bo.biz.branch.service.BranchService#selectBranchTourRequesterInfo(com.bgbgb.bo.biz.branch.domain.Branch)
	*/
	@Override
	public Branch selectBranchTourRequester(String tourReqstIdx) {
		return branchDao.selectBranchTourRequester(tourReqstIdx);
	}
	
	/**
	 * 첨부파일 저장
	 */
//	private void insertCmFile(Branch vo, HttpServletRequest request) throws Exception{
//		
//		// 파일 저장시작
//		final MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
//		final Map<String, MultipartFile> files = multiRequest.getFileMap();
//		
//		// Globals.properties에 지정한 서버에 저장될 경로
//		String sysPath = Property.get("file.upload.path");
//		String attPath = "branch/" + fileMngUtil.getTimeStamp().substring(0, 8) + "/";	// 저장파일경로
//		String newName = "";		// 파일이름
//		String orginFileName = "";	// 진짜파일이름
//		MultipartFile file;
//		
//		// 폴더없을시 폴더 생성하도록 처리
//		File saveFolder1 = new File(EgovWebUtil.filePathBlackList(sysPath + attPath));
//		if(!saveFolder1.exists() || saveFolder1.isFile()) {
//			saveFolder1.mkdirs();
//		}
//		
//		int idx = 0;
//		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
//		
//		while (itr.hasNext()) {
//			Entry<String, MultipartFile> entry = itr.next();
//			file = entry.getValue();	// 위에서 만든 파일객체에다가 값을 넣는다.
//	
//			if(!"".equals(file.getOriginalFilename())){	// 파일이름이 ""이 아니면
//				orginFileName = file.getOriginalFilename();
//				int index = orginFileName.lastIndexOf(".");	// 파일이름에서 . 을 뒤에서 부터찾는다.
//				String fileExt = orginFileName.substring(index+1);	// 확장자명 구하기
//				
//				//파일이름을  시분초로 저장한다	
//				newName = fileMngUtil.getTimeStamp() + "." + fileExt;	// 파일이름을 시분초+확장자로 저장한다.
//				fileMngUtil.writeUploadedFile(file, newName, attPath);	// 파일과 만든 파일으름 경로를 가지고 업로드한다.
//				// 위가 서버에저장
//				// 아래가 DB에저장
//				
//				// 실제문서명, 저장된 파일명, 저장경로 파일용량]
//				vo.setFileTableNm("tb_br_bhf");	// 테이블명
//				vo.setFileTableIdx(vo.getSelectKey());
//				vo.setSysPath(sysPath);	// 파일시스템경로
//				vo.setAttPath(attPath);	// 파일첨부경로
//				
//				vo.setFileOrgNm(orginFileName);	// 원래파일명
//				vo.setFileSysNm(newName);	// 저장파일명
//				vo.setFileSize(file.getSize());	// 파일사이즈
//				vo.setFileExtsn(fileExt);	// 파일확장자
//				
//				idx = idx + 1;
//				vo.setFileSort(idx);
//				
//				log.info("idx============"+idx);
//				log.info("vo.getFileTableNm============"+vo.getFileTableNm());
//				log.info("vo.getSysPath============"+vo.getSysPath());
//				log.info("vo.getAttPath============"+vo.getAttPath());
//				log.info("vo.getFileOrgNm============"+vo.getFileOrgNm());
//				log.info("vo.getFileSysNm============"+vo.getFileSysNm());
//				log.info("vo.getFileSize============"+vo.getFileSize());
//				log.info("vo.getFileExtsn============"+vo.getFileExtsn()+"\n");
//				
//				branchDao.insertCmFile(vo);
//			}
//		}
//	}
}