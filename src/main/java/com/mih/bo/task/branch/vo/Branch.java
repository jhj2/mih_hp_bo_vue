package com.mih.bo.task.branch.vo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mih.bo.common.AbstractModel;

import lombok.Data;

/**
 * 지부관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021.08.17
 */
@Data
public class Branch extends AbstractModel{
	private static final long serialVersionUID = -6062343006993705364L;
	
	private int totCnt;
	
	// tb_br_bhf(지부)
	private long bhfIdx;
	private String bhfNm;
	private String bhfLa;
	private String bhfLo;
	private String telNo1;
	private String telNo2;
	private String telNo3;
	private String zip;
	private String rdnmadrBass;
	private String rdnmadrDetail;
	private String addr;
	private String useTime;
	private String useGuidance;
	private String visitStret;
	private String bizrno;
	private String useYn;
	private String slogan;
	private String siteCd;
	private String siteKey;
	private String siteNm;
	private String groupId;
	
	// tb_bhf_goods(지부상품)
	private String bhfGoodsIdx;
	private String goodsIdx;
	private String delYn;
	
	// tb_bhf_goods_detail(지부상품상세)
	private String bhfGoodsDetailIdx;
	private String goodsLbl;
	private String useImprtyResnCd;
	private String useImprtyResn;

	// tb_br_tour(지부투어)
	private String tourReqstIdx;
	private String mberIdx;
	private String tourHopeDe;	// 투어 희망일
	private String tourHopeTimeTyCd;
	private String tourHopeTimeTyNm;	// 시간대
	private String visitrNm;
	private String hp;
	
	/**
	 * 핸드폰번호1
	 */
	private String hpNo1;
	/**
	 * 핸드폰번호2
	 */
	private String hpNo2;
	/**
	 * 핸드폰번호3
	 */
	private String hpNo3;
	private String indvdlinfoColctUseAgreYn;
	private String tourSttus;
	private String tourSttusNm;	// 투어상태
	private String tourRqstdt;
	private String tourEndDe;
	private String tourEndId;
	
	// TB_CM_FILE(첨부파일)
	private String fileIdx;	// 파일
	private String fileTableNm;	// 파일테이블명
	private String fileTableIdx;	// 파일테이블IDX
	private String sysPath;	// 파일시스템경로
	private String attPath;	// 파일첨부경로
	private String fileOrgNm;	// 파일원본명
	private String fileSysNm;	// 파일시스템명
	private long fileSize;	// 파일크기(KB)
	private String fileExtsn;	// 파일확장자
	private int fileSort;	// 파일정렬순서
	private String fileReplcText;	// 파일대체문구
	private String fileDwldCnt;	// 파일다운로드건수
	
	private String selectKey;
	private String path;
	
	// 이미지파일 삭제 idx
	private String delFileIdx;
	private ArrayList<String> arrDelFileIdx;
	
	private String[] arrFileIdx;
	private String[] arrFileTableNm;
	private String[] arrFileTableIdx;
	private String[] arrFileSort;
	
	
	private List<MultipartFile> files;
	private List<MultipartFile> files2;
	
	private List<?> fSort;
	private List<?> fSort2;
	
	private String mberGbn;	// 회원 필터 항목(전체, 회원(01), 비회원(02))
	private String itemGbn;	// 회원 필터 항목(이름(01), 휴대폰(02))
	private String keyword;	// 회원 필터 항목(키워드)
	
	private String authKey;	// 암호화키
	private String mberFlag;
	
	private String commCd;
	private String commCdNm;
	
	private String snsId;
	private String tourSts;
	
	// 메시지발송이력IDX
	private String msgSndngHstIdx;
	
	// E(이메일) A(알림톡)
	private String msgCoursTy;
	private String msgCoursTyNm;
	
	// 메시지발송일시
	private String msgSndngDt;
	
	// 메시지내용
	private String msgCn;
	
	// 탈퇴여부
	private String secsnYn;
	
	// 지부오픈일
	private String bhfOpnDt;
	private String opnDt;
	private String opnSi;
	private String opnMin;
	private String opnSec;
	
	// 알림톡링크 URL
	private String linkUrl;
	
	// 오픈일 disable 여부(Y/N)
	private String disableYn;
}