package com.mih.bo.task.branch.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mih.bo.common.vo.Paging;
import com.mih.bo.task.branch.vo.Branch;
/**
 * 지부관리 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public interface BranchService {

	/**
	 * 지부관리 조회
	 *
	 * @param vo
	 * @param paging
	 * @return List<?>
	*/
	public List<?> selectBranchInfoList(Branch vo, Paging paging);
	
	/**
	 * 지부관리 상세페이지
	 *
	 * @param vo
	 * @return Branch
	*/
	public Branch selectBranchInfo(Branch vo);

	/**
	 * 지부관리 등록 
	 *
	 * @param vo
	 * @param files 
	*/
	public void insertBranchInfo(Branch vo, List<MultipartFile> files);

	/**
	 * 지부관리 수정
	 *
	 * @param vo 
	*/
	public void updateBranchInfo(Branch vo);

	/**
	 * 지부관리 - 이미지 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> imgList(Branch vo);

	/**
	 * 지부투어 조회
	 *
	 * @param vo
	 * @param paging
	 * @return List<?>
	*/
	public List<?> selectBranchTourList(Branch vo, Paging paging);

	/**
	 * 지부 리스트 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> brBhfList(Branch vo);

	/**
	 * 지부투어 수정(종료하기)
	 *
	 * @param vo 
	*/
	public void updateBrTour(Branch vo);
	
	/**
	 * 지부투어 신청자 개인정보 조회
	 *
	 * @param tourReqstIdx
	 * @return Branch
	*/
	public Branch selectBranchTourRequester(String tourReqstIdx);
}