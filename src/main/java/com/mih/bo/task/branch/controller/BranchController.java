package com.mih.bo.task.branch.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.Constants;
import com.mih.bo.common.service.CommonService;
import com.mih.bo.common.util.RequestUtils;
import com.mih.bo.common.util.SecuritySessionUtils;
import com.mih.bo.common.util.SessionCookieUtil;
import com.mih.bo.common.vo.FileUpload;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.BranchDao;
import com.mih.bo.task.branch.service.BranchService;
import com.mih.bo.task.branch.vo.Branch;
import com.mih.bo.task.code.service.CodeService;
import com.mih.bo.task.code.vo.CommonCode;
import com.mih.bo.task.member.vo.Member;
/**
 * 지부관리 컨트롤러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Controller
public class BranchController{
	private static final Logger log = LoggerFactory.getLogger(BranchController.class);
	
	@Autowired
	private BranchService branchService;
	
	@Autowired
	private BranchDao branchDao;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private CodeService codeService;
	
	// 카카오 지도 및 주소 API Key
	@Value("${code.map.kakao.api.key}")
	private String kakaoMapKey;
	
	// 지부정보-결재정보 : 사이트명
	@Value("${pay.siteNm}")
	private String siteNm;
	
	// 지부정보-결재정보 : 그룹아이디
	@Value("${pay.groupId}")
	private String groupId;
	
	/**
	 * 지부관리 페이지 이동
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @param paging
	 * @return String
	*/
	@GetMapping(value="/branch/info/list")
	public String selectList(Model model, HttpServletRequest request,
			Branch vo, @ModelAttribute Paging paging){
		
		// 총 카운트 조회
		int totCnt = branchDao.selectBranchInfoListTotCnt(vo);
		vo.setTotCnt(totCnt);
		
		paging.setRecordCountPerPage(20);	// 1페이지당 게시글 개수
		model.addAttribute("resultList", branchService.selectBranchInfoList(vo, paging));
		model.addAttribute("totCnt", totCnt);
		
		return "/branch/branchInfoList";
	}
	
	/**
	 * 지부관리 상세페이지(등록/수정)
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @param fileUpload
	 * @return
	 * @throws Exception String
	*/
	@GetMapping(value={"/branch/info/addView", "/branch/info/updView"})
	public String editView(Model model, HttpServletRequest request, Branch vo, FileUpload fileUpload) throws Exception{
		
		if(request.getRequestURI().indexOf("/branch/info/addView")>-1){
			SessionCookieUtil.setSessionAttribute(request, "STS_FLAG", "add");
			
		}else{
			SessionCookieUtil.setSessionAttribute(request, "STS_FLAG", "upd");
			model.addAttribute("resultVO", branchService.selectBranchInfo(vo));
			
			fileUpload.setFileTableNm("TB_BR_BHF");
			fileUpload.setFileTableIdx(vo.getBhfIdx());
			model.addAttribute("imgList", commonService.commonImgList(fileUpload));
		}
		
		// 카카오 지도 및 주소 API Key
		model.addAttribute("kakaoMapKey", kakaoMapKey);
		
		return "/branch/branchInfoForm";
	}
	
	/**
	 * 지부관리 등록
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @param files
	 * @return String
	*/
	@PostMapping(value={"/branch/info/add"})
	public String insertBranchInfo(Model model, HttpServletRequest request, Branch vo,
			@RequestParam("attachFile") List<MultipartFile> files){
		
		vo.setBizrno(vo.getBizrno().replaceAll("-", ""));
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setRegIp(ip);
		
		// 사이트명, 그룹아이디 값 세팅
		vo.setSiteNm(siteNm);
		vo.setGroupId(groupId);
		
		// 지부오픈일 값 세팅
		String bhfOpnDt = vo.getOpnDt() + " " + vo.getOpnSi() + ":" + vo.getOpnMin() + ":00";
		vo.setBhfOpnDt(bhfOpnDt);
		branchService.insertBranchInfo(vo, files);
		
		return "redirect:/branch/info/list";
	}
	
	/**
	 * 지부관리 수정
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @param files
	 * @param fileSort
	 * @param files2
	 * @param fileSort2
	 * @return String
	*/
	@PostMapping(value={"/branch/info/upd"})
	public String updateBranchInfo(Model model, HttpServletRequest request, Branch vo,
			@RequestParam(name="attachFile", required=false) List<MultipartFile> files, @RequestParam(name="fileSort", required=false) List<?> fileSort,
			@RequestParam(name="newAttachFile", required=false) List<MultipartFile> files2, @RequestParam(name="newFileSort", required=false) List<?> fileSort2){
		
		vo.setBizrno(vo.getBizrno().replaceAll("-", ""));
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setUpdIp(ip);
		vo.setFiles(files);
		vo.setFiles2(files2);
		vo.setFSort(fileSort);
		vo.setFSort2(fileSort2);
		
		// 사이트명, 그룹아이디 값 세팅
		vo.setSiteNm(siteNm);
		vo.setGroupId(groupId);
		
		// 지부오픈일 값 세팅
		if(null != vo.getOpnDt()){
			String bhfOpnDt = vo.getOpnDt() + " " + vo.getOpnSi() + ":" + vo.getOpnMin() + ":00";
			vo.setBhfOpnDt(bhfOpnDt);
		}
		branchService.updateBranchInfo(vo);
		
		return "redirect:/branch/info/list";
	}
	
	/**
	 * 지부투어 관리
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @param paging
	 * @return String
	*/
	@GetMapping(value="/branch/tour/list")
	public String selectList2(Model model, HttpServletRequest request,
			Branch vo, @ModelAttribute Paging paging){
		
		// 총 카운트 조회
		int totCnt = branchDao.selectBranchTourListTotCnt(vo);
		vo.setTotCnt(totCnt);
		
		paging.setRecordCountPerPage(20);	// 1페이지당 게시글 개수
		model.addAttribute("resultList", branchService.selectBranchTourList(vo, paging));
		model.addAttribute("totCnt", totCnt);
		model.addAttribute("vo", vo);	// 검색조건 유지하기 위한 VO 객체
		
		// 지부 리스트 조회
		model.addAttribute("brBhfList", branchService.brBhfList(vo));
		
		// 지부투어방문예약시간
		CommonCode code = new CommonCode();
		code.setUpperCmmnCd(Constants.TOUR_HOPE_TIME_TY_CD);
		List<?> resultList = codeService.selectCodeList(code);
		model.addAttribute("codeList", resultList);
		
		return "/branch/branchTourList";
	}
	
	/**
	 * 지부투어 수정(종료하기)
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@GetMapping(value={"/branch/tour/upd"})
	public String updateBrTour(Model model, HttpServletRequest request, Branch vo){
		
		// ID가져오기
		String userId = SecuritySessionUtils.getUser().getUserId();
		vo.setUpdId(userId);
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setUpdIp(ip);
		vo.setTourSttus("E");
		branchService.updateBrTour(vo);
		
		return "redirect:/branch/tour/list";
	}
	
	/**
	 * 지부투어 신청자 개인정보 조회
	 *
	 * @param tourReqstIdx
	 * @return ModelAndView
	*/
	@GetMapping(path="/branch/tour/requester")
	public ModelAndView selectTourRequester(HttpServletRequest request, @RequestParam String tourReqstIdx){
		ModelAndView mav = new ModelAndView();
		
		// 투어 신청자 조회 이력 등록
		Member member = new Member();
		String userId = SecuritySessionUtils.getUser().getUserId();

		member.setMngrId(userId);
		member.setRegId(userId);
		member.setInfoInqireTrgtTyCd(Constants.PRIVATE_DATA_TOUR_REQUESTER);
		member.setInfoInqireTrgt(tourReqstIdx);
		member.setRegIp(RequestUtils.remoteAddr(request));
		member.setMberNmInqireYn(Constants.YES);
		member.setHpNoInqireYn(Constants.YES);
		member.setEmailInqireYn(Constants.NO);
		member.setRdnmadrInqireYn(Constants.NO);
		member.setCardNoInqireYn(Constants.NO);
		member.setRefndAcnutnoInqireYn(Constants.NO);
		
		commonService.insertMrInfoInqireHst(member);
		
		mav.addObject("requester", branchService.selectBranchTourRequester(tourReqstIdx));
		mav.setViewName("jsonView");
		
		return mav;
	}
}