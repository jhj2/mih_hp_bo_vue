package com.mih.bo.task.code.service;

import java.util.List;

import com.mih.bo.task.code.vo.CommonCode;

/**
 * 코드 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public interface CodeService {
	
	/**
	 * 코드 조회
	 *
	 * @param commonCode
	 * @return CommonCode
	*/
	public CommonCode selectCode(String commonCode);
	
	/**
	 * 사용코드 목록 조회
	 *
	 * @param upperCommonCode
	 * @return List<CommonCode>
	*/
	public List<CommonCode> selectUseCodeList(String upperCommonCode);
	
	/**
	 * 코드 목록 조회
	 *
	 * @param commonCode
	 * @return List<CommonCode>
	*/
	public List<CommonCode> selectCodeList(CommonCode commonCode);

}
