package com.mih.bo.task.code.vo;

import com.mih.bo.common.AbstractModel;

import lombok.Getter;
import lombok.Setter;

/**
 * 공통 코드
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Getter @Setter
public class CommonCode extends AbstractModel {
	
	private static final long serialVersionUID = -5846817905668863786L;

	/**
	 * 공통코드
	 */
	private String cmmnCd;
	
	/**
	 * 공통코드명
	 */
	private String cdKorNm;
	
	/**
	 * 코드영문명
	 */
	private String cdEngNm;
	
	/**
	 * 코드설명
	 */
	private String cdDc;
	
	/**
	 * 사용유무
	 */
	private String useYn;
	
	/**
	 * 상위공통코드
	 */
	private String upperCmmnCd;
	
	/**
	 * 코드레벨
	 */
	private int cdLevel;
	
	/**
	 * 기타1
	 */
	private String etc1;
	
	/**
	 * 기타2
	 */
	private String etc2;
	
	/**
	 * 기타3
	 */
	private String etc3;
	
	/**
	 * 코드 정렬
	 */
	private int cdSort;
}
