package com.mih.bo.task.code.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.dao.CodeDao;
import com.mih.bo.task.code.service.CodeService;
import com.mih.bo.task.code.vo.CommonCode;

/**
 * 코드 서비스 구현체
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Service
public class CodeServiceImpl implements CodeService {
	
	private static final Logger log = LoggerFactory.getLogger(CodeServiceImpl.class);
	
	@Autowired
	private CodeDao codeDao;
	
	/*
	 * @see com.bgbgb.bo.biz.code.service.CodeService#selectCode(java.lang.String)
	*/
	@Override
	public CommonCode selectCode(String commonCode) {
        return codeDao.selectCode(commonCode);
 
	}
	
	/*
	 * @see com.bgbgb.bo.biz.code.service.CodeService#selectUseCodeList(java.lang.String)
	*/
	@Override
	public List<CommonCode> selectUseCodeList(String upperCommonCode) {
        return codeDao.selectUseCodeList(upperCommonCode);
 
	}
	
	/*
	 * @see com.bgbgb.bo.biz.code.service.CodeService#selectCodeList(com.bgbgb.bo.biz.code.domain.CommonCode)
	*/
	@Override
	public List<CommonCode> selectCodeList(CommonCode commonCode) {
        return codeDao.selectCodeList(commonCode);
 
	}
	
	
}
