package com.mih.bo.task.code.service;

import java.util.List;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.task.code.vo.ComCd;

/**
 * 공통코드관리 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.03.07
 */
public interface ComCdService{

	/**
	 * 공통코드관리 조회
	 *
	 * @param vo
	 * @param paging
	 * @return List<?>
	*/
	public List<?> selectComCdList(ComCd vo, Paging paging);
	
	/**
	 * 공통코드관리 상세페이지
	 *
	 * @param vo
	 * @return ComCd
	*/
	public ComCd selectComCd(ComCd vo);

	/**
	 * 공통코드관리 등록
	 *
	 * @param vo void
	*/
	public void insertComCd(ComCd vo);

	/**
	 * 공통코드관리 수정
	 *
	 * @param vo void
	*/
	public void updateComCd(ComCd vo);
	
	/**
	 * 공통코드관리 삭제
	 *
	 * @param vo void
	 */
	public void deleteComCd(ComCd vo);

	/**
	 * 공통코드 리스트 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> cdList(ComCd vo);

	/**
	 * 공통코드관리 엑셀다운로드 조회
	 *
	 * @param vo
	 * @return Object
	*/
	public List<?> excelDownList(ComCd vo);
	
	/**
	 * 공통코드 리스트 조회
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> comCdList(ComCd vo);
}