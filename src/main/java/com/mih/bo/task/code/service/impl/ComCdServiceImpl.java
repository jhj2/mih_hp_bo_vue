package com.mih.bo.task.code.service.impl;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.common.util.PagingUtil;
import com.mih.bo.common.util.SecuritySessionUtils;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.ComCdDao;
import com.mih.bo.task.code.service.ComCdService;
import com.mih.bo.task.code.vo.ComCd;

/**
 * 공통코드관리 서비스 구현체
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.03.07.
 */
@Service
public class ComCdServiceImpl implements ComCdService{
	
	private static final Logger log = LoggerFactory.getLogger(ComCdServiceImpl.class);
	
	@Autowired
	private ComCdDao comCdDao;

	/*
	 * @see com.mih.bo.task.code.service.ComCdService#selectComCdList(com.mih.bo.task.code.vo.ComCd, com.mih.bo.common.vo.Paging)
	*/
	@Override
	public List<?> selectComCdList(ComCd vo, Paging paging){
		int cnt = vo.getTotCnt();	// 총 count 개수조회
		if(cnt > 0){
			paging.setTotalRecordCount(cnt);
			paging.setFormName("comCdFrm");	// formName값 세팅
			PagingUtil.setPagination(paging);
			
			vo.setLimit(paging.getLimit());
			vo.setOffset(paging.getOffset());
		}
		return comCdDao.selectComCdList(vo);
	}
	
	/*
	 * @see com.mih.bo.task.code.service.ComCdService#selectComCd(com.mih.bo.task.code.vo.ComCd)
	*/
	@Override
	public ComCd selectComCd(ComCd vo){
		return comCdDao.selectComCd(vo);
	}

	/*
	 * @see com.mih.bo.task.code.service.ComCdService#insertComCd(com.mih.bo.task.code.vo.ComCd)
	*/
	@Override
	public void insertComCd(ComCd vo){
		String userId = SecuritySessionUtils.getUser().getUserId();
		vo.setRegId(userId);
		comCdDao.insertComCd(vo);
	}

	/*
	 * @see com.mih.bo.task.code.service.ComCdService#updateComCd(com.mih.bo.task.code.vo.ComCd)
	*/
	@Override
	public void updateComCd(ComCd vo){
		String userId = SecuritySessionUtils.getUser().getUserId();
		vo.setUpdId(userId);
		comCdDao.updateComCd(vo);
	}

	/*
	 * @see com.mih.bo.task.code.service.ComCdService#deleteComCd(com.mih.bo.task.code.vo.ComCd)
	*/
	@Override
	public void deleteComCd(ComCd vo) {
		comCdDao.deleteComCd(vo);
	}

	/*
	 * @see com.mih.bo.task.code.service.ComCdService#cdList(com.mih.bo.task.code.vo.ComCd)
	*/
	@Override
	public List<?> cdList(ComCd vo) {
		return comCdDao.cdList(vo);
	}

	/*
	 * @see com.mih.bo.task.code.service.ComCdService#excelDownList(com.mih.bo.task.code.vo.ComCd)
	*/
	@Override
	public List<?> excelDownList(ComCd vo) {
		return comCdDao.excelDownList(vo);
	}

	/*
	 * @see com.mih.bo.task.code.service.ComCdService#comCdList(com.mih.bo.task.code.vo.ComCd)
	*/
	@Override
	public List<?> comCdList(ComCd vo) {
		return comCdDao.comCdList(vo);
	}
}