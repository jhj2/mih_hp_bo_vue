package com.mih.bo.task.code.controller;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.Constants;
import com.mih.bo.common.util.RequestUtils;
import com.mih.bo.common.util.SessionCookieUtil;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.ComCdDao;
import com.mih.bo.task.code.service.ComCdService;
import com.mih.bo.task.code.vo.ComCd;

/**
 * 공통코드관리 컨트롤러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.03.07.
 */
@Controller
public class ComCdController{
	private static final Logger log = LoggerFactory.getLogger(ComCdController.class);
	
	@Autowired
	private ComCdService comCdService;
	
	@Autowired
	private ComCdDao comCdDao;
	
	/**
	 * 공통코드관리 페이지 이동
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @param paging
	 * @return String
	*/
	@GetMapping(value="/com/cd/list")
	public String selectList(Model model, HttpServletRequest request,
			ComCd vo, @ModelAttribute Paging paging){
		
		// 사용여부
		vo.setSelCode("S");
		vo.setUseYn(Constants.YES);
		vo.setLvl(2);
		vo.setComCd(Constants.USE_YN);
		model.addAttribute("useYnSelect", comCdService.comCdList(vo));
		
		// 총 카운트 조회
		int totCnt = comCdDao.selectComCdListTotCnt(vo);
		vo.setTotCnt(totCnt);
		
		paging.setRecordCountPerPage(20);	// 1페이지당 게시글 개수
		model.addAttribute("resultList", comCdService.selectComCdList(vo, paging));
		model.addAttribute("totCnt", totCnt);
		model.addAttribute("vo", vo);
		
		return "/com/comCdList";
	}
	
	/**
	 * 공통코드관리 상세페이지(등록/수정)
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return
	 * @throws Exception String
	*/
	@GetMapping(value={"/com/cd/addView", "/com/cd/updView"})
	public String editView(Model model, HttpServletRequest request, ComCd vo) throws Exception{
		
		// 공통코드 리스트 조회
		List<?> cdList = comCdService.cdList(vo);
		model.addAttribute("cdList", cdList);
		
		if(request.getRequestURI().indexOf("/com/cd/addView")>-1){
			SessionCookieUtil.setSessionAttribute(request, "STS_FLAG", "add");
			
		}else{
			SessionCookieUtil.setSessionAttribute(request, "STS_FLAG", "upd");
			model.addAttribute("resultVO", comCdService.selectComCd(vo));
		}
		
		// 사용여부
		vo.setSelCode("S");
		vo.setUseYn(Constants.YES);
		vo.setLvl(2);
		vo.setComCd(Constants.USE_YN);
		model.addAttribute("useYnSelect", comCdService.comCdList(vo));
		
		return "/com/comCdForm";
	}
	
	/**
	 * 공통코드관리 등록
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/com/cd/add"})
	public String insertComCd(Model model, HttpServletRequest request, ComCd vo){
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setRegIp(ip);
		comCdService.insertComCd(vo);
		
		return "redirect:/com/cd/list";
	}
	
	/**
	 * 공통코드관리 수정
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/com/cd/upd"})
	public String updateComCd(Model model, HttpServletRequest request, ComCd vo){
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setUpdIp(ip);
		comCdService.updateComCd(vo);
		
		return "redirect:/com/cd/list";
	}
	
	/**
	 * 공통코드관리 삭제
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/com/cd/del"})
	public String deleteComCd(Model model, HttpServletRequest request, ComCd vo){
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setUpdIp(ip);
		comCdService.deleteComCd(vo);
		
		return "redirect:/com/cd/list";
	}
	
	/**
	 * 공통코드관리 엑셀다운로드
	 *
	 * @param vo
	 * @param exFileNm
	 * @param exHeaderId
	 * @param exHeaderNm
	 * @return ModelAndView
	*/
	@PostMapping("/com/cd/excel")
	public ModelAndView comCdExcel(ComCd vo, @RequestParam(value="exFileNm", required=false) String exFileNm,
			@RequestParam(value="exHeaderId", required=false) String exHeaderId,
			@RequestParam(value="exHeaderNm", required=false) String exHeaderNm){
		
		HashMap<String, Object> excelMap = new HashMap<String, Object>();
		excelMap.put("exFileNm", exFileNm);	// 현재화면명
		excelMap.put("exHeaderId", exHeaderId);	// 헤더 컬럼ID
		excelMap.put("exHeaderNm", exHeaderNm);	// 헤더 컬럼명
		excelMap.put("resultList", comCdService.excelDownList(vo));

		return new ModelAndView("excelDown", "excelMap", excelMap);
	}
}