package com.mih.bo.task.code.vo;

import com.mih.bo.common.AbstractModel;
import lombok.Data;

/**
 * 공통코드관리
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.03.07.
 */
@Data
public class ComCd extends AbstractModel{
	private static final long serialVersionUID = -245039751910508867L;
	
	private int totCnt;
	
	/**
	 * 공통코드
	 */
	private String comCd;
	
	/**
	 * 상위공통코드
	 */
	private String upperComCd;
	
	/**
	 * 공통코드명
	 */
	private String cdNm;
	
	/**
	 * 코드영문명
	 */
	private String cdEngNm;
	
	/**
	 * 사용여부
	 */
	private String useYn;
	private String useYnNm;

	/**
	 * 코드레벨
	 */
	private int lvl;

	/**
	 * 정렬순서
	 */
	private int cdSort;

	/**
	 * 코드설명
	 */
	private String cdDc;

	/**
	 * 값1
	 */
	private String val1;

	/**
	 * 값2
	 */
	private String val2;

	/**
	 * 값3
	 */
	private String val3;
	
	/**
	 * 검색조건(공통코드, 사용여부)
	 */
	private String searchComCd;
	private String searchUseYn;
	
	/**
	 * 선택값 활성화 Flag
	 */
	private String selCode;
}