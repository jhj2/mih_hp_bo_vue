package com.mih.bo.task.org.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.dao.OrgDao;
import com.mih.bo.task.org.service.OrgService;

import java.util.List;
import java.util.Map;

/**
 * 조직도
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.02.08.
 */
@Service
public class OrgServiceImpl implements OrgService{
	
	private static final Logger log = LoggerFactory.getLogger(OrgServiceImpl.class);
	
	@Autowired
	private OrgDao orgDao;

	@Override
	public List<?> orgList() {
		return orgDao.orgList();
	}

	@Override
	public void updateOrgList(Map<String,Object> paramMap){
		orgDao.updateOrgList(paramMap);
	}
}