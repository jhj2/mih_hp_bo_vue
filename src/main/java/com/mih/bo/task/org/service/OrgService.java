package com.mih.bo.task.org.service;

import java.util.List;
import java.util.Map;

/**
 * 조직도
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.02.08.
 */
public interface OrgService{

    /**
     * 조직도 목록 조회
     *
     * @return Object
     */
    public List<?> orgList();
    /**
     * 조직도 목록 조회
     *
     * @param paramMap
     * @return void
     */
    public void updateOrgList(Map<String,Object> paramMap);
}