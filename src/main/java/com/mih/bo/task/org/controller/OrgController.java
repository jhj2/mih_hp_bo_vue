package com.mih.bo.task.org.controller;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.util.StringUtils;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.OrgDao;
import com.mih.bo.task.org.service.OrgService;
import com.mih.bo.task.org.vo.Org;

/**
 * 조직도
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.02.08.
 */
@Controller
public class OrgController{
	private static final Logger log = LoggerFactory.getLogger(OrgController.class);
	
	@Autowired
	private OrgService orgService;
	
	@Autowired
	private OrgDao orgDao;
	
	// JSON 저장경로
	@Value("${json.save.path}")
	private String jsonSavePath;
	
	/**
	 * 조직도 관리 페이지 이동
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @param paging
	 * @return
	 * @throws IOException String
	*/
	@GetMapping(value="/org/list")
	public String selectList(Model model, HttpServletRequest request,
			Org vo, @ModelAttribute Paging paging) throws IOException{
		
		// 총 카운트 조회
//		int totCnt = orgDao.selectOrgListTotCnt(vo);
//		vo.setTotCnt(totCnt);
//		
//		paging.setRecordCountPerPage(20);	// 1페이지당 게시글 개수
//		model.addAttribute("resultList", orgService.selectOrgList(vo, paging));
//		model.addAttribute("totCnt", totCnt);
//		model.addAttribute("vo", vo);
		
/*		// JSON 파일 읽기
		Reader rd = null;
		String data = "";
		try {
			rd = new FileReader(jsonSavePath + "org.json");
			if(rd != null){
				int read;
				char[] buffer = new char[1000];
				while((read = rd.read(buffer)) != -1){
					data = new String(buffer, 0, read);
				}
				//System.out.println(data);
				
				JSONArray jsonArray = new JSONArray(data);	// JSONObject jsonObject = new JSONObject(data);
				System.out.println("jsonArray===="+jsonArray);
				model.addAttribute("orgList", jsonArray);
				model.addAttribute("orgListLength", jsonArray.length());
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("aaa====");
			model.addAttribute("orgListLength", 0);
			log.error(StringUtils.printStack(e));
		}
*/
		model.addAttribute("orgList", orgService.orgList());
		return "/org/orgList";
	}
	
	/**
	 * json 파일 저장
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return
	 * @throws IOException ModelAndView
	*/
	@PostMapping(value={"/org/json/save"})
	public ModelAndView orgJsonSave(Model model, HttpServletRequest request, Org vo
	, @RequestParam String jsonData) throws IOException{
		
//		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		ModelAndView mav = new ModelAndView("jsonView");
		JSONArray jArray = new JSONArray(jsonData);
		for (Object jsonObject : jArray) {
			Map<String, Object> paramMap = null;
			paramMap = new ObjectMapper().readValue(jsonObject.toString(), Map.class);

			orgService.updateOrgList(paramMap);
		}
		mav.addObject("orgList", orgService.orgList()); //재갱신 해야 함
		return mav;
		
/*		// vo 객체를 파일에 쓰기
		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			fw = new FileWriter(jsonSavePath + "org.json");
			bw = new BufferedWriter(fw);
			bw.write(vo.getResultJson());
			
			//gson.toJson(vo, fw);
			bw.flush();
			fw.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					e.printStackTrace();
				}
			}
			
			if(fw != null) {
				try {
					fw.close();
				}catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("success====");
		
		// JSON 파일 읽기
		Reader rd = null;
		String data = "";
		try {
			rd = new FileReader(jsonSavePath + "org.json");
			if(rd != null){
				int read;
				char[] buffer = new char[1000];
				while((read = rd.read(buffer)) != -1){
					data = new String(buffer, 0, read);
				}
//				System.out.println(data);
				
				JSONArray jsonArray = new JSONArray(data);	// JSONObject jsonObject = new JSONObject(data);
				System.out.println("jsonArray===="+jsonArray);
				mav.addObject("resultList", jsonArray);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			log.error(StringUtils.printStack(e));
		}
		return mav;
//		return "redirect:/org/list";
*/
	}
}