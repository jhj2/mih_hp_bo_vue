package com.mih.bo.task.org.vo;

import com.mih.bo.common.AbstractModel;
import lombok.Data;

/**
 * 조직도
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.02.08.
 */
@Data
public class Org extends AbstractModel{
	private static final long serialVersionUID = -6062343006993705364L;
	
	//private int totCnt;
	private String team;
	private String name;
	
	private String resultJson;

	private String orgCd;
	private String orgNm;
	private String orgUserCd;
	private String orgUserNm;
	private String sort;
	private String lvl;
	private String porgCd;
}