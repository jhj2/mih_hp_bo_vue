package com.mih.bo.task.user.service;

import com.mih.bo.security.service.UserSecurityService;
import com.mih.bo.task.user.vo.LoginHistory;

/**
 * 사용자 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public interface UserService extends UserSecurityService {
	
	/**
	 * 로그인 이력 등록
	 *
	 * @param loginHistory void
	*/
	public void insertLoginHistory(LoginHistory loginHistory);

}
