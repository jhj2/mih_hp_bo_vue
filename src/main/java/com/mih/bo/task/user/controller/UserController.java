package com.mih.bo.task.user.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.web.controller.AbstractController;
import com.mih.bo.common.yaml.AppInfoProperty;

/**
 * 사용자 컨트롤러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Controller
public class UserController extends AbstractController {
	
	private static final Logger log = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	protected AppInfoProperty appInfoProperty;
	
	/**
	 * 로그인
	 *
	 * @param errorCode
	 * @return ModelAndView
	*/
	@GetMapping(path = "/user/login")
	public ModelAndView login(@RequestParam(value = "errorCode", required = false) String errorCode) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("errorCode", errorCode);
		mav.setViewName("/user/login");
		return mav;
	}
	
	/**
	 * 로그아웃
	 *
	 * @return ModelAndView
	*/
	@PostMapping(path = "/user/logout")
	public ModelAndView logout() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/user/login");
		return mav;
	}
}