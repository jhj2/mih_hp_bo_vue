package com.mih.bo.task.user.vo;

import com.mih.bo.common.AbstractModel;

import lombok.Getter;
import lombok.Setter;

/**
 * 로그인 이력
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Getter @Setter
public class LoginHistory extends AbstractModel{
	
	private static final long serialVersionUID = 8482669572289017529L;

	/**
	 * 관리자로그인이력IDX
	 */
	private int loginMstIdx;
	
	/**
	 * 관리자아이디
	 */
	private String mngrId;
	
	/**
	 * 로그인성공여부
	 */
	private String loginSuccesYn;
}