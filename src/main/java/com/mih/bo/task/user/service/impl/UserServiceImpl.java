package com.mih.bo.task.user.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.common.enums.RoleTypes;
import com.mih.bo.dao.LoginDao;
import com.mih.bo.security.entity.UserEntity;
import com.mih.bo.security.entity.UserRoleEntity;
import com.mih.bo.task.user.service.UserService;
import com.mih.bo.task.user.vo.LoginHistory;

/**
 * 사용자 서비스 구현체
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private LoginDao loginDao;

	/*
	 * @see com.bgbgb.bo.security.service.UserSecurityService#getUser(java.lang.String)
	*/
	@Override
	public UserEntity getUser(String userId) {
	    return loginDao.selectLoginUser(userId);
	}
	
	/*
	 * @see com.bgbgb.bo.security.service.UserSecurityService#getUserRoles(java.lang.String)
	*/
	@Override
	public List<UserRoleEntity> getUserRoles(String loginUserId) {
		List<UserRoleEntity> list = new ArrayList<UserRoleEntity>();
		list.add(new UserRoleEntity(loginUserId, RoleTypes.ROLE_ADMIN.value()));
		return list;
	}

	/*
	 * @see com.bgbgb.bo.biz.user.service.UserService#insertLoginHistory(com.bgbgb.bo.biz.user.domain.LoginHistory)
	*/
	@Override
	public void insertLoginHistory(LoginHistory loginHistory) {
		loginDao.insertLoginHistory(loginHistory);
	}
}