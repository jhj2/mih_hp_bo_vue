package com.mih.bo.task.member.vo;

import com.mih.bo.common.api.sns.Auth;

import lombok.Data;

/**
 * 회원정보
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Data
public class Member extends Auth{

	private static final long serialVersionUID = -612490258380999959L;

	/**
	 *  idx값
	 */
	private int idx;

	/**
	 *  동일인 식별 정보
	 */
	private String id;
	/**
	 * 이름 or 닉네임
	 */
	private String name;
	/**
	 * accessToken값
	 */
	private String accessToken;
	/**
	 * refreshToken값
	 */
	private String refreshToken;
	/**
	 * 생년월일
	 */
	private String birthday;
	/**
	 * 휴대폰번호
	 */
	private String mobile;
	/**
	 * ci값
	 */
	private String ci;
	/**
	 * 세션아이디
	 */
	private String sessionId;
	/**
	 * 쿠키키
	 */
	private String cookieKey;
	/**
	 * browser
	 */
	private String browser;
	/**
	 * os
	 */
	private String os;
	/**
	 * day
	 */
	private int day;
	/**
	 * 아이피
	 */
	private String ip;
	/**
	 * 우편번호
	 */
	private String zipCode;
	/**
	 * 기본주소
	 */
	private String addr;
	/**
	 * 상세주소
	 */
	private String addrDtl;
	/**
	 * 닉네임
	 */
	private String nickname;
	/**
	 * 출생년도
	 */
	private String birthyear;
	/**
	 * 회원정보 수정페이지 정보수정버튼 Flag값(01: 기본정보 정보수정 // 02 : 주소정보 정보수정)
	 */
	private String updInfoFlag;

	/**
	 * 로그인 후 호출할 URL
	 */
	private String redirectUri;	
	/**
	 *로그인 여부 
	 */
	private String loginYn;	
	/**
	 * 개인정보 수집 및 이용동의
	 */
	private String[] arrAgreePrivacy;
	/**
	 * 마케팅 수신 동의
	 */
	private String[] arrAgreeChoice;

	// 회원마스터
	/**
	 * 회원IDX
	 */
	private long mberIdx;
	/**
	 * 회원이름
	 */
	private String mberNm;
	/**
	 * 핸드폰번호1
	 */
	private String hpNo1;
	/**
	 * 핸드폰번호2
	 */
	private String hpNo2;
	/**
	 * 핸드폰번호3
	 */
	private String hpNo3;
	/**
	 * 이메일
	 */
	private String email;
	/**
	 * 생년월일
	 */
	private String brthdy;
	/**
	 * 탈퇴여부
	 */
	private String secsnYn;
	/**
	 * 탈퇴일시
	 */
	private String secsnDt;
	/**
	 * 회원구분코드
	 */
	private String mberTyCd;
	/**
	 * 우편번호
	 */
	private String zip;
	/**
	 * 도로명주소기본
	 */
	private String rdnmadrBass;
	/**
	 * 도로명주소상세
	 */
	private String rdnmadrDetail;
	/**
	 * 탈퇴사유코드
	 */
	private String secsnResnCd;
	/**
	 * 탈퇴사유비고
	 */
	private String secsnResnRm;
	/**
	 * 환불은행코드
	 */
	private String refndBankCd;
	/**
	 * 환불은행명
	 */
	private String refndBankNm;
	/**
	 * 환불은행계좌
	 */
	private String refndAcnutno;
	/**
	 * 환불계좌 Full-Name
	 */
	private String refndFullNm;
	/**
	 * 환불예금주
	 */
	private String refndDpstr;
	/**
	 * 최종접속일시
	 */
	private String lastConectDt;
	/**
	 * 휴면전환일
	 */
	private String drmncyCnvrsDe;

	// 회원이용약관
	/**
	 * 회원이용약관IDX
	 */
	private long useStplatIdx;
	/**
	 * 약관코드
	 */
	private String stplstCd;
	/**
	 * 약관동의여부
	 */
	private String stplatAgreYn;
	/**
	 * 약관동의일시
	 */
	private String stplatAgreDt;

	// 회원로그인 이력
	/**
	 * 로그인이력IDX
	 */
	private long loginHstIdx;
	/**
	 * 로그인성공여부
	 */
	private String loginSuccesYn;

	// tb_mr_info_inqire_hst(회원정보조회 이력)
	/**
	 * 회원정보조회이력IDX
	 */
	private String mberInfoInqireIdx;
	/**
	 * 정보조회대상유형코드
	 */
	private String infoInqireTrgtTyCd;
	/**
	 * 정보조회대상
	 */
	private String infoInqireTrgt;
	/**
	 * 관리자ID
	 */
	private String mngrId;
	/**
	 * 조회일시
	 */
	private String inqireDt;
	/**
	 * 개인정보조회사유코드
	 */
	private String indvdlInfoInqireResnCd;
	/**
	 * 개인정보조회사유비고
	 */
	private String indvdlInfoInqireResnRm;
	/**
	 * 회원이름조회여부
	 */
	private String mberNmInqireYn;
	/**
	 * 휴대폰번호조회여부
	 */
	private String hpNoInqireYn;
	/**
	 * 이메일조회여부
	 */
	private String emailInqireYn;
	/**
	 * 주소조회여부
	 */
	private String rdnmadrInqireYn;
	/**
	 * 카드번호조회여부
	 */
	private String cardNoInqireYn;
	/**
	 * 환불계좌조회여부
	 */
	private String refndAcnutnoInqireYn;

	// 회원본인인증 이력
	/**
	 * 본인인증이력IDX
	 */
	private long selfCrtfcHstIdx;
	/**
	 * 발급인증번호
	 */
	private String issuCrtfcNo;
	/**
	 * 발급일시
	 */
	private String issuDt;
	/**
	 * 인증성공여부
	 */
	private String crtfcSuccesYn;
	/**
	 * 인증번호입력일시
	 */
	private String crtfcNoInputDt;

	// SNS 회원
	/**
	 * 회원번호
	 */
	private String snsId;
	/**
	 * SNS 구분
	 */
	private String snsSeCd;
	/**
	 * SNS 이름
	 */
	private String snsSeNm;
	/**
	 * 정보 보호 로그인 Flag
	 */
	private String infoLoginFlag;
	/**
	 * 회원탈퇴 Flag
	 */
	private String secsnFlag;

	/**
	 * 총 갯수
	 */
	private int totCnt;

	// tb_batch_key(배치키 정보), tb_batch_key_issu(배치키 발급)
	/**
	 * 배치키
	 */
	private String batchKey;
	/**
	 * 배치키발급IDX
	 */
	private String batchKeyIssuIdx;
	/**
	 * 카드명
	 */
	private String cardName;
	/**
	 * 카드마스킹번호
	 */
	private String cardMaskNo;
	/**
	 *  메시지발송이력IDX
	 */
	private String msgSndngHstIdx;
	/**
	 * E(이메일) A(알림톡)
	 */
	private String msgCoursTy;
	/**
	 * 메시지타입명
	 */
	private String msgCoursTyNm;
	/**
	 * 메시지발송일시
	 */
	private String msgSndngDt;
	/**
	 * 메시지내용
	 */
	private String msgCn;
	/**
	 * 회원 필터 항목(전체, 회원(01), 비회원(02)
	 */
	private String mberGbn;
	/**
	 * 회원 필터 항목(이름(01), 휴대폰(02))
	 */
	private String itemGbn;
	/**
	 * 회원 필터 항목(키워드)
	 */
	private String keyword;
	/**
	 * 암호화키
	 */
	private String authKey;
	/**
	 * 회원 비회원 구분
	 */
	private String mberFlag;

	/**
	 * 공통코드
	 */
	private String commCd;
	/**
	 * 공통코드명
	 */
	private String commCdNm;
	/**
	 * 투어상태
	 */
	private String tourSts;
	
	private String strCd;
	private String endCd;
	
	/**
	 * 추천인코드
	 */
	private String recCd;
	private String searchRecCd;
	
	/**
	 * 가입기간 조회조건
	 */
	private String searchPeriod;
	private String searchStartDt;
	private String searchEndDt;
	private String orgRegistDt;	// 등록일(가입일)
}