package com.mih.bo.task.member.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.common.util.PagingUtil;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.MemberDao;
import com.mih.bo.task.member.service.MemberService;
import com.mih.bo.task.member.vo.Member;

/**
 * 회원관리 서비스 구현체
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Service
public class MemberServiceImpl implements MemberService{
	
	private static final Logger log = LoggerFactory.getLogger(MemberServiceImpl.class);
	
	@Autowired
	private MemberDao memberDao;
	
	/*
	 * @see com.bgbgb.bo.biz.member.service.MemberService#selectMrMber(com.bgbgb.bo.biz.member.domain.Member)
	*/
	@Override
	public Member selectMrMber(Member vo){
		return memberDao.selectMrMber(vo);
	}

	/*
	 * @see com.bgbgb.bo.biz.member.service.MemberService#selectMrUseStplat(com.bgbgb.bo.biz.member.domain.Member)
	*/
	@Override
	public List<?> selectMrUseStplat(Member vo) {
		return memberDao.selectMrUseStplat(vo);
	}

	/*
	 * @see com.bgbgb.bo.biz.member.service.MemberService#selectMsgSendList(com.bgbgb.bo.biz.member.domain.Member, com.bgbgb.bo.commons.domain.Paging)
	*/
	@Override
	public List<?> selectMsgSendList(Member vo, Paging paging) {
		
		int cnt = vo.getTotCnt();	// 총 count 개수조회
		if(cnt > 0){
			paging.setTotalRecordCount(cnt);
			paging.setFormName("memberPopupFrm");	// formName값 세팅
			paging.setFunctionName("msgSendGoPage");
			PagingUtil.setPagination(paging);
			
			vo.setLimit(paging.getLimit());
			vo.setOffset(paging.getOffset());
		}
		return memberDao.selectMsgSendList(vo);
	}

	/*
	 * @see com.bgbgb.bo.biz.member.service.MemberService#selectMemberList(com.bgbgb.bo.biz.member.domain.Member, com.bgbgb.bo.commons.domain.Paging)
	*/
	@Override
	public List<?> selectMemberList(Member vo, Paging paging){
		int cnt = vo.getTotCnt();	// 총 count 개수조회
		if(cnt > 0){
			paging.setTotalRecordCount(cnt);
			paging.setFormName("memberFrm");	// formName값 세팅
			PagingUtil.setPagination(paging);
			
			vo.setLimit(paging.getLimit());
			vo.setOffset(paging.getOffset());
		}
		return memberDao.selectMemberList(vo);
	}

	/*
	 * @see com.bgbgb.bo.biz.member.service.MemberService#secsnYnCnt(com.bgbgb.bo.biz.member.domain.Member)
	*/
	@Override
	public int secsnYnCnt(Member vo) {
		return memberDao.secsnYnCnt(vo);
	}
}