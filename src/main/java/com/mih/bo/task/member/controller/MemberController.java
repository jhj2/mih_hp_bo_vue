package com.mih.bo.task.member.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.Constants;
import com.mih.bo.common.service.CommonService;
import com.mih.bo.common.util.RequestUtils;
import com.mih.bo.common.util.SecuritySessionUtils;
import com.mih.bo.common.util.StringUtils;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.MemberDao;
import com.mih.bo.task.code.service.CodeService;
import com.mih.bo.task.member.service.MemberService;
import com.mih.bo.task.member.vo.Member;
/**
 * 회원관리 컨트롤러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Controller
public class MemberController{
	private static final Logger log = LoggerFactory.getLogger(MemberController.class);
		
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private CodeService codeService;
	
	/**
	 * 회원정보 공통팝업
	 *
	 * @param vo
	 * @param request
	 * @param response
	 * @param model
	 * @param paging
	 * @return String
	*/
	@GetMapping(value="/member/com/info")
	public String memberComInfo(Member vo, HttpServletRequest request, HttpServletResponse response, ModelMap model
			, @ModelAttribute Paging paging){
		
		// 회원정보 가져오기
		vo = memberService.selectMrMber(vo);
		model.addAttribute("resultVO", vo);
		
		// 마케팅 수신 동의 정보 가져오기
//		vo.setMberIdx(userInfo.getMberIdx());
//		vo.setStplstCd("agreeChoice");
		vo.setStrCd(Constants.STPLST_CD_AT);
		vo.setEndCd(Constants.STPLST_CD_TM);
		List<?> mrUseStplat = memberService.selectMrUseStplat(vo);
		model.addAttribute("mrUseStplat", mrUseStplat);
		
		// 총 카운트 조회(테스트)
		int totCnt = memberDao.selectMsgSendListTotCnt(vo);
		vo.setTotCnt(totCnt);
		
		paging.setRecordCountPerPage(20);	// 1페이지당 게시글 개수
		model.addAttribute("resultList", memberService.selectMsgSendList(vo, paging));
		model.addAttribute("reasonCodeList", codeService.selectUseCodeList(Constants.PRIVATE_DATA_SEARCH_REASON));
		model.addAttribute("totCnt", totCnt);
		
		return "/common/memberPopup";
	}
	
	/**
	 * 메시지 발송내역 ajax 페이징 처리(데이터 조회)
	 *
	 * @param vo
	 * @param request
	 * @param response
	 * @param model
	 * @param paging
	 * @return ModelAndView
	*/
	@PostMapping(value="/member/com/msgInfo")
	public ModelAndView memberComMsgInfo(Member vo, HttpServletRequest request, HttpServletResponse response, ModelMap model
			, @ModelAttribute Paging paging){
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		// 총 카운트 조회(테스트)
		int totCnt = memberDao.selectMsgSendListTotCnt(vo);
		vo.setTotCnt(totCnt);
		
		paging.setRecordCountPerPage(20);	// 1페이지당 게시글 개수
		mav.addObject("resultList", memberService.selectMsgSendList(vo, paging));
		mav.addObject("totCnt", totCnt);
		
		return mav;
	}
	
	/**
	 * 개인정보 조회(식별정보 조회)
	 *
	 * @param vo
	 * @param request
	 * @param response
	 * @param model
	 * @param paging
	 * @return ModelAndView
	*/
	@PostMapping(value="/member/com/infoDetail")
	public ModelAndView memberComInfoDetail(Member vo, HttpServletRequest request, HttpServletResponse response, ModelMap model
			, @ModelAttribute Paging paging){
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		// 회원정보 가져오기
		Member resultVO = memberService.selectMrMber(vo);
		mav.addObject("resultVO", resultVO);
		
		// Flag값 분기
		if(null != resultVO.getMberNm()){
			resultVO.setMberNmInqireYn(Constants.YES);
		}else{
			resultVO.setMberNmInqireYn(Constants.NO);
		}
		
		if(null != resultVO.getHpNo1()){
			resultVO.setHpNoInqireYn(Constants.YES);
		}else{
			resultVO.setHpNoInqireYn(Constants.NO);
		}
		
		if(null != resultVO.getEmail()){
			resultVO.setEmailInqireYn(Constants.YES);
		}else{
			resultVO.setEmailInqireYn(Constants.NO);
		}
		
		if(null != resultVO.getRdnmadrBass()){
			resultVO.setRdnmadrInqireYn(Constants.YES);
		}else{
			resultVO.setRdnmadrInqireYn(Constants.NO);
		}
		
		if(null != resultVO.getCardMaskNo()){
			resultVO.setCardNoInqireYn(Constants.YES);
		}else{
			resultVO.setCardNoInqireYn(Constants.NO);
		}
		
		if(null != resultVO.getRefndBankNm()){
			resultVO.setRefndAcnutnoInqireYn(Constants.YES);
		}else{
			resultVO.setRefndAcnutnoInqireYn(Constants.NO);
		}
		
		// 관리자ID값 가져오기
		String userId = SecuritySessionUtils.getUser().getUserId();
		resultVO.setMngrId(userId);
		resultVO.setRegId(userId);
		
		// IP 가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setRegIp(ip);
		
		// 회원정보조회이력 등록
		resultVO.setIndvdlInfoInqireResnCd(vo.getIndvdlInfoInqireResnCd());
		resultVO.setIndvdlInfoInqireResnRm(vo.getIndvdlInfoInqireResnRm());
		resultVO.setInfoInqireTrgtTyCd(Constants.PRIVATE_DATA_MEMBER);
		commonService.insertMrInfoInqireHst(resultVO);
		
		return mav;
	}
	
	/**
	 * 회원관리
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @param paging
	 * @return String
	*/
	@GetMapping(value="/member/list")
	public String selectList(Model model, HttpServletRequest request,
			Member vo, @ModelAttribute Paging paging){
		
		// [B/O] > [회원관리] 메뉴 접속 시 Default로 "기간:전체"로 나오게 하는 코드
		if(StringUtils.isEmpty(vo.getSearchPeriod())){
			if(StringUtils.isEmpty(vo.getSearchStartDt())){
				vo.setSearchPeriod("0");
			}
		}
		
		// 총 카운트 조회
		int totCnt = memberDao.selectMemberListTotCnt(vo);
		vo.setTotCnt(totCnt);
		
		paging.setRecordCountPerPage(20);	// 1페이지당 게시글 개수
		model.addAttribute("resultList", memberService.selectMemberList(vo, paging));
		model.addAttribute("totCnt", totCnt);
		model.addAttribute("vo", vo);	// 검색조건 유지하기 위한 VO 객체
		
		return "/member/memberList";
	}
	
	@GetMapping(value="/member/private/info")
	public String memberPrivateInfo(Model model, HttpServletRequest request,
			Member vo, @ModelAttribute Paging paging){
		
		// 총 카운트 조회
		int totCnt = memberDao.selectMemberListTotCnt(vo);
		vo.setTotCnt(totCnt);
		
		paging.setRecordCountPerPage(20);	// 1페이지당 게시글 개수
		model.addAttribute("resultList", memberService.selectMemberList(vo, paging));
		model.addAttribute("totCnt", totCnt);
		model.addAttribute("vo", vo);	// 검색조건 유지하기 위한 VO 객체
		
		return "/member/memberList";
	}
}