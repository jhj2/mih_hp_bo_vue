package com.mih.bo.task.member.service;

import java.util.List;

import com.mih.bo.common.vo.Paging;
import com.mih.bo.task.member.vo.Member;

/**
 * 회원관리 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
public interface MemberService {
	
	/**
	 * 회원정보 조회
	 *
	 * @param vo
	 * @return Member
	*/
	public Member selectMrMber(Member vo);
	/**
	 * 마케팅 수신 동의 정보 가져오기
	 *
	 * @param vo
	 * @return List<?>
	*/
	public List<?> selectMrUseStplat(Member vo);

	/**
	 * 메시지 발송 내역
	 *
	 * @param vo
	 * @param paging
	 * @return List<?>
	*/
	public List<?> selectMsgSendList(Member vo, Paging paging);

	/**
	 * 회원관리 리스트 조회
	 *
	 * @param vo
	 * @param paging
	 * @return List<?>
	*/
	public List<?> selectMemberList(Member vo, Paging paging);
	
	/**
	 * 탈퇴한 회원인지 체크
	 *
	 * @param vo
	 * @return int
	*/
	public int secsnYnCnt(Member vo);
}