package com.mih.bo.task.bbs.service;

import java.util.List;

import com.mih.bo.common.vo.Paging;
import com.mih.bo.task.bbs.vo.Bbs;
import com.mih.bo.task.member.vo.Member;

/**
 * 게시판 카테고리 관리 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.01.26
 */
public interface BbsCtgryService{

	/**
	 * 게시판 카테고리 관리 조회
	 *
	 * @param vo
	 * @param paging
	 * @return List<?>
	*/
	public List<?> selectBbsCtgryList(Bbs vo, Paging paging);
	
	/**
	 * 게시판 카테고리 관리 상세페이지
	 *
	 * @param vo
	 * @return Bbs
	*/
	public Bbs selectBbsCtgry(Bbs vo);

	/**
	 * 게시판 카테고리 관리 등록
	 *
	 * @param vo void
	*/
	public void insertBbsCtgry(Bbs vo);

	/**
	 * 게시판 카테고리 관리 수정
	 *
	 * @param vo void
	*/
	public void updateBbsCtgry(Bbs vo);

	/**
	 * 게시판 카테고리 권한 조회
	 *
	 * @param vo
	 * @return Bbs
	*/
	public Bbs selectCtgryGrant(Bbs vo);
}