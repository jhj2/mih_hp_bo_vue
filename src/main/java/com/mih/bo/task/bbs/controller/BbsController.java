package com.mih.bo.task.bbs.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.Constants;
import com.mih.bo.common.service.CommonService;
import com.mih.bo.common.service.FileService;
import com.mih.bo.common.util.RequestUtils;
import com.mih.bo.common.util.SessionCookieUtil;
import com.mih.bo.common.vo.FileUpload;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.BbsDao;
import com.mih.bo.task.bbs.service.BbsService;
import com.mih.bo.task.bbs.service.BbsCtgryService;
import com.mih.bo.task.bbs.vo.Bbs;
import com.mih.bo.task.code.service.ComCdService;
import com.mih.bo.task.code.vo.ComCd;

/**
 * 게시판 카테고리 관리 컨트롤러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.01.26.
 */
@Controller
public class BbsController{
	private static final Logger log = LoggerFactory.getLogger(BbsController.class);
	
	@Autowired
	private BbsService bbsService;
	
	@Autowired
	private BbsCtgryService bbsCtgryService;

	@Autowired
	private BbsDao bbsDao;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private FileService fileService;
	
	@Autowired
	private ComCdService comCdService;
	
	/**
	 * 게시판 관리 페이지 이동
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @param paging
	 * @return String
	*/
	@GetMapping(value="/bbs/list")
	public String selectList(Model model, HttpServletRequest request,
			Bbs vo, @ModelAttribute Paging paging){
		if(vo.getSearchData() != null){	// 검색결과가 있으면
			model.addAttribute("search", vo);
		} else { // 검색결과가 없으면
			//searchCtgryIdx 넣어주기
			List<?> ctgryList = bbsService.ctgryList(vo);
			if (ctgryList.size() > 0) {
				vo.setSearchCtgryIdx(((Bbs) ctgryList.get(0)).getCommCd());
			}
		}
		// 총 카운트 조회
		int totCnt = bbsDao.selectBbsListTotCnt(vo);
		vo.setTotCnt(totCnt);
		
		paging.setRecordCountPerPage(20);	// 1페이지당 게시글 개수
		List<?> bbsList = bbsService.selectBbsList(vo, paging);
		bbsList = bbsService.selectReBbsList((List<Bbs>)bbsList);
		model.addAttribute("resultList", bbsList);
		model.addAttribute("totCnt", totCnt);
		model.addAttribute("vo", vo);
		
		// 게시판 카테고리 리스트 조회
		model.addAttribute("ctgryList", bbsService.ctgryList(vo));
				
		return "/bbs/BbsList";
	}
	
	/**
	 * 게시글 상세보기
	 *
	 * @param model
	 * @return String
	*/
	@GetMapping(value="/bbs/detail")
	public String noticeView(Bbs vo, Model model, HttpServletRequest req, FileUpload fileUpload) {
		
		// 조회
		int bbsIdx = vo.getBbsIdx();
		Bbs bbsDetail = bbsService.selectBbsView(vo);
		
		// 게시판 카테고리 권한 조회
		vo.setCtgryIdx(bbsDetail.getCtgryIdx());
		Bbs bbsGrant = bbsCtgryService.selectCtgryGrant(vo);
		model.addAttribute("resultGrant", bbsGrant);
		
		// 게시글 댓글 내용 조회
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -7);	// 7일간 보이도록 하기위해서.
		String nowday = format.format(cal.getTime());
		
		model.addAttribute("bbsResult", bbsDetail);
		model.addAttribute("nowday", nowday);
		
		// 게시글 댓글 내용
		bbsGrant.setBbsIdx(bbsIdx);
		List<?> cmtList = bbsService.selectBbsCmtList(bbsGrant);
		cmtList = bbsService.selectBbsReCmtList((List<Bbs>)cmtList);
		model.addAttribute("bbsCmtList", cmtList);
		
		// 첨부파일 리스트 조회
		fileUpload.setFileTableNm("BBS");
		fileUpload.setFileTableIdx(vo.getBbsIdx());
		model.addAttribute("fileList", commonService.commonImgList(fileUpload));
		
		return "bbs/BbsListDetail";
	}

	/**
	 * 게시글 댓글 리플 등록
	 *
	 * @param request
	 * @param Bbs
	 * @return ModelAndView
	 */
	@GetMapping(value="/bbs/detail/addReCmt")
	public ModelAndView insertCommunityCmt(Model model,HttpServletRequest request, Bbs bbs
			, @RequestParam(name="bbsIdx", required=false) int bbsIdx
			, @RequestParam(name="bbsCmtIdx", required=false) int bbsCmtIdx
			, @RequestParam(name="depth", required=false) int depth
			, @RequestParam(name="bbsCmtCn", required=false) String bbsCmtCn) {
		bbs.setRegIp(RequestUtils.remoteAddr(request));
		bbs.setUpdIp(RequestUtils.remoteAddr(request));
		bbs.setDelYn(Constants.NO);

		// 아이디 연동시 지울것
		bbs.setRegId("김광수");

		bbs.setBbsIdx(bbsIdx);
		bbs.setBbsCmtIdx(bbsCmtIdx);
		bbs.setDepth(depth);
		bbs.setBbsCmtCn(bbsCmtCn);

		// 내용저장
		List<?> reCmtList = bbsService.insertReCmt(bbs);

		ModelAndView mav = new ModelAndView("jsonView");

		mav.addObject("flag", "success");
		mav.addObject("reCmtList", reCmtList);
		return mav;
	}
	
	/**
	 * 게시판 관리 상세페이지(등록/수정)
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return
	 * @throws Exception String
	*/
	@GetMapping(value={"/bbs/addView", "/bbs/updView"})
	public String editView(Model model, HttpServletRequest request, Bbs vo, FileUpload fileUpload, ComCd comCd) throws Exception{
		
		// 사용여부
		comCd.setUseYn(Constants.YES);
		comCd.setLvl(2);
		comCd.setComCd(Constants.USE_YN);
		model.addAttribute("useYnRadio", comCdService.comCdList(comCd));
		
		// 게시판 카테고리 리스트 조회
		List<?> ctgryList = bbsService.ctgryList(vo);
		model.addAttribute("ctgryList", ctgryList);
		if(request.getRequestURI().indexOf("/bbs/addView")>-1){
			SessionCookieUtil.setSessionAttribute(request, "STS_FLAG", "add");
			
			if (ctgryList.size() > 0 && vo.getCtgryIdx() == 0) {
				vo.setCtgryIdx(Integer.parseInt(((Bbs) ctgryList.get(0)).getCommCd()));
			}
			Bbs bbsGrant = bbsCtgryService.selectCtgryGrant(vo);

			int bbsIdx = vo.getBbsIdx();
			bbsGrant.setBbsIdx(bbsIdx);
			long ref = vo.getRef();
			long depth = vo.getDepth();
			if (ref > 0){
				bbsGrant.setRef(ref);
			}
			if (depth > 0){
				bbsGrant.setDepth(depth);
			}
			model.addAttribute("resultGrant", bbsGrant);
		}else{
			SessionCookieUtil.setSessionAttribute(request, "STS_FLAG", "upd");
			
			Bbs selectBbs = bbsService.selectBbs(vo);
			model.addAttribute("resultVO", selectBbs);
			
			vo.setCtgryIdx(selectBbs.getCtgryIdx());
			Bbs bbsGrant = bbsCtgryService.selectCtgryGrant(vo);
			model.addAttribute("resultGrant", bbsGrant);

			// 게시글 댓글 내용
			int bbsIdx = vo.getBbsIdx();
			bbsGrant.setBbsIdx(bbsIdx);
			model.addAttribute("bbsCmtList", bbsService.selectBbsCmtList(bbsGrant));

			// 첨부파일 리스트 조회
			fileUpload.setFileTableNm("BBS");
			fileUpload.setFileTableIdx(vo.getBbsIdx());
			model.addAttribute("fileList", commonService.commonImgList(fileUpload));
		}
		return "/bbs/BbsForm";
	}
	
	/**
	 * 게시판 관리 등록
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/bbs/add"})
	public String insertBbs(Model model, HttpServletRequest request, Bbs vo
		, @RequestParam(name="attachFile", required=false) List<MultipartFile> files){
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setRegIp(ip);
		vo.setFiles(files);
		
		// 내용 값 insert할때 \r\n => <br>로 변경해서 저장
		vo.setBbsCn(vo.getBbsCn().replaceAll("(\r\n|\r|\n|\n\r)", "<br>"));
		
		bbsService.insertBbs(vo);
		
		return "redirect:/bbs/list";
	}
	
	/**
	 * 게시판 관리 수정
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/bbs/upd"})
	public String updateBbs(Model model, HttpServletRequest request, Bbs vo
		, @RequestParam(name="attachFile", required=false) List<MultipartFile> files
		, @RequestParam(name="fileSort", required=false) List<?> fileSort){
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setUpdIp(ip);
		vo.setFiles(files);
		vo.setFSort(fileSort);
		
		// 내용 값 update할때 \r\n => <br>로 변경해서 저장
		vo.setBbsCn(vo.getBbsCn().replaceAll("(\r\n|\r|\n|\n\r)", "<br>"));
		
		bbsService.updateBbs(vo);
		
		return "redirect:/bbs/list";
	}
	
	/**
	 * 게시판 관리 삭제
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/bbs/del"})
	public String deleteBbs(Model model, HttpServletRequest request, Bbs vo){
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setUpdIp(ip);
		vo.setDelYn(Constants.YES);
		bbsService.updateBbs(vo);
		
		return "redirect:/bbs/list";
	}
	
	/**
	 * 게시판 댓글 삭제
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/bbs/bbsCmt/del"})
	public String deleteBbsCmt(Model model, HttpServletRequest request, Bbs vo){
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setUpdIp(ip);
		vo.setDelYn(Constants.YES);
		bbsService.updateBbsCmt(vo);
		
		return "redirect:/bbs/updView?bbsIdx="+vo.getBbsIdx();
	}
	
	/**
	 * 첨부파일 삭제
	 *
	 * @param request
	 * @param vo
	 * @param fileIdx
	 * @return ModelAndView
	*/
	@PostMapping(value="/bbs/file/del")
	public ModelAndView bbsFileDel(HttpServletRequest request, FileUpload vo,
			@RequestParam(value="fileIdx", required=false) int fileIdx){
		
		ModelAndView mav = new ModelAndView("jsonView");
		
		vo.setFileIdx(fileIdx);
		vo.setUseYn(Constants.NO);
		fileService.updateFileAttach(vo);
		
		mav.addObject("flag", "success");
		return mav;
	}
}