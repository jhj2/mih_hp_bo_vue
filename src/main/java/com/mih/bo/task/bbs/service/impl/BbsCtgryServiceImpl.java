package com.mih.bo.task.bbs.service.impl;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mih.bo.common.util.PagingUtil;
import com.mih.bo.common.util.SecuritySessionUtils;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.BbsCtgryDao;
import com.mih.bo.task.bbs.service.BbsCtgryService;
import com.mih.bo.task.bbs.vo.Bbs;

/**
 * 게시판 카테고리 관리 서비스 구현체
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.01.26.
 */
@Service
public class BbsCtgryServiceImpl implements BbsCtgryService{
	
	private static final Logger log = LoggerFactory.getLogger(BbsCtgryServiceImpl.class);
	
	@Autowired
	private BbsCtgryDao bbsCtgryDao;

	/*
	 * @see com.mih.bo.task.bbs.service.BbsCtgryService#selectBbsCtgryList(com.mih.bo.task.bbs.vo.Bbs, com.mih.bo.common.vo.Paging)
	*/
	@Override
	public List<?> selectBbsCtgryList(Bbs vo, Paging paging){
		int cnt = vo.getTotCnt();	// 총 count 개수조회
		if(cnt > 0){
			paging.setTotalRecordCount(cnt);
			paging.setFormName("bbsCtgryFrm");	// formName값 세팅
			PagingUtil.setPagination(paging);
			
			vo.setLimit(paging.getLimit());
			vo.setOffset(paging.getOffset());
		}
		return bbsCtgryDao.selectBbsCtgryList(vo);
	}
	
	/*
	 * @see com.mih.bo.task.bbs.service.BbsCtgryService#selectBbsCtgry(com.mih.bo.task.bbs.vo.Bbs)
	*/
	@Override
	public Bbs selectBbsCtgry(Bbs vo){
		return bbsCtgryDao.selectBbsCtgry(vo);
	}

	/*
	 * @see com.mih.bo.task.bbs.service.BbsCtgryService#insertBbsCtgry(com.mih.bo.task.bbs.vo.Bbs)
	*/
	@Override
	public void insertBbsCtgry(Bbs vo){
		String userId = SecuritySessionUtils.getUser().getUserId();
		vo.setRegId(userId);
		bbsCtgryDao.insertBbsCtgry(vo);
	}

	/*
	 * @see com.mih.bo.task.bbs.service.BbsCtgryService#updateBbsCtgry(com.mih.bo.task.bbs.vo.Bbs)
	*/
	@Override
	public void updateBbsCtgry(Bbs vo){
		String userId = SecuritySessionUtils.getUser().getUserId();
		vo.setUpdId(userId);
		bbsCtgryDao.updateBbsCtgry(vo);
	}

	/*
	 * @see com.mih.bo.task.bbs.service.BbsCtgryService#updateBbsCtgry(com.mih.bo.task.bbs.vo.Bbs)
	*/
	@Override
	public Bbs selectCtgryGrant(Bbs vo) {
		return bbsCtgryDao.selectCtgryGrant(vo);
	}
}