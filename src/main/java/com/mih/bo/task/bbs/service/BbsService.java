package com.mih.bo.task.bbs.service;

import java.util.List;

import com.mih.bo.common.vo.Paging;
import com.mih.bo.task.bbs.vo.Bbs;

/**
 * 게시판 카테고리 관리 서비스
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.01.26
 */
public interface BbsService{
	/**
	 * 카테고리 콤보박스 목록 조회
	 *
	 * @param Org
	 * @return Object
	*/
	public List<?> ctgryList(Bbs vo);
	
	/**
	 * 게시글 상세보기
	 *
	 * @param Org
	 * @return Bbs
	*/
	public Bbs selectBbsView(Bbs vo);
	/**
	 * 게시글 상세보기 댓글
	 *
	 * @param Org
	 * @return Bbs
	*/
	public List<?> selectBbsCmtList(Bbs vo);
	/**
	 * 게시글 상세보기 댓글의 리플 리스트 조회
	 *
	 * @param Org
	 * @return Bbs
	 */
	public List<?> selectBbsReCmtList(List<Bbs> voList);
	/**
	 * 게시글 상세보기 댓글의 리플 등록
	 *
	 * @param Org
	 * @return Bbs
	 */
	public List<?> insertReCmt(Bbs vo);
	/**
	 * 게시판 카테고리 관리 조회
	 *
	 * @param vo
	 * @param paging
	 * @return List<?>
	*/
	public List<?> selectBbsList(Bbs vo, Paging paging);
	/**
	 * 게시판 카테고리 관리 조회
	 *
	 * @param List<Bbs>
	 * @return List<Bbs>
	 */
	public List<Bbs> selectReBbsList(List<Bbs> voList);
	/**
	 * 게시판 카테고리 관리 상세페이지
	 *
	 * @param vo
	 * @return Bbs
	*/
	public Bbs selectBbs(Bbs vo);

	/**
	 * 게시판 관리 등록
	 *
	 * @param vo void
	*/
	public void insertBbs(Bbs vo);

	/**
	 * 게시판 관리 수정
	 *
	 * @param vo void
	*/
	public void updateBbs(Bbs vo);
	
	/**
	 * 게시판 댓글 삭제
	 *
	 * @param vo void
	*/
	public void updateBbsCmt(Bbs vo);

}