package com.mih.bo.task.bbs.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mih.bo.common.Constants;
import com.mih.bo.common.util.RequestUtils;
import com.mih.bo.common.util.SessionCookieUtil;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.BbsCtgryDao;
import com.mih.bo.task.bbs.service.BbsCtgryService;
import com.mih.bo.task.bbs.vo.Bbs;
import com.mih.bo.task.code.service.ComCdService;
import com.mih.bo.task.code.vo.ComCd;

/**
 * 게시판 카테고리 관리 컨트롤러
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.01.26.
 */
@Controller
public class BbsCtgryController{
	private static final Logger log = LoggerFactory.getLogger(BbsCtgryController.class);
	
	@Autowired
	private BbsCtgryService bbsCtgryService;
	
	@Autowired
	private BbsCtgryDao bbsCtgryDao;
	
	@Autowired
	private ComCdService comCdService;
	
	/**
	 * 게시판 카테고리 관리 페이지 이동
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @param paging
	 * @return String
	*/
	@GetMapping(value="/bbs/ctgry/list")
	public String selectList(Model model, HttpServletRequest request,
			Bbs vo, @ModelAttribute Paging paging){
		
		// 총 카운트 조회
		int totCnt = bbsCtgryDao.selectBbsCtgryListTotCnt(vo);
		vo.setTotCnt(totCnt);
		
		paging.setRecordCountPerPage(20);	// 1페이지당 게시글 개수
		model.addAttribute("resultList", bbsCtgryService.selectBbsCtgryList(vo, paging));
		model.addAttribute("totCnt", totCnt);
		model.addAttribute("vo", vo);
		
		return "/bbs/BbsCtgryList";
	}
	
	/**
	 * 게시판 카테고리 관리 상세페이지(등록/수정)
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return
	 * @throws Exception String
	*/
	@GetMapping(value={"/bbs/ctgry/addView", "/bbs/ctgry/updView"})
	public String editView(Model model, HttpServletRequest request, Bbs vo, ComCd comCd) throws Exception{
		
		// 사용여부
		comCd.setUseYn(Constants.YES);
		comCd.setLvl(2);
		comCd.setComCd(Constants.USE_YN);
		model.addAttribute("useYnRadio", comCdService.comCdList(comCd));
		
		// 댓글 정렬방식
		comCd.setComCd(Constants.CMT_ORD_WAY);
		model.addAttribute("cmtOrdWayRadio", comCdService.comCdList(comCd));
		
		// 읽기 권한
		comCd.setComCd(Constants.RD_AUTH);
		model.addAttribute("rdAuthRadio", comCdService.comCdList(comCd));
		
		// 쓰기 권한
		comCd.setComCd(Constants.WT_AUTH);
		model.addAttribute("wtAuthRadio", comCdService.comCdList(comCd));
		
		if(request.getRequestURI().indexOf("/bbs/ctgry/addView")>-1){
			SessionCookieUtil.setSessionAttribute(request, "STS_FLAG", "add");
			
		}else{
			SessionCookieUtil.setSessionAttribute(request, "STS_FLAG", "upd");
			model.addAttribute("resultVO", bbsCtgryService.selectBbsCtgry(vo));
		}
		return "/bbs/BbsCtgryForm";
	}
	
	/**
	 * 게시판 카테고리 관리 등록
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/bbs/ctgry/add"})
	public String insertBbsCtgry(Model model, HttpServletRequest request, Bbs vo){
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setRegIp(ip);
		bbsCtgryService.insertBbsCtgry(vo);
		
		return "redirect:/bbs/ctgry/list";
	}
	
	/**
	 * 게시판 카테고리 관리 수정
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/bbs/ctgry/upd"})
	public String updateBbsCtgry(Model model, HttpServletRequest request, Bbs vo){
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setUpdIp(ip);
		
		bbsCtgryService.updateBbsCtgry(vo);
		
		return "redirect:/bbs/ctgry/list";
	}
	
	/**
	 * 게시판 카테고리 관리 삭제
	 *
	 * @param model
	 * @param request
	 * @param vo
	 * @return String
	*/
	@PostMapping(value={"/bbs/ctgry/del"})
	public String deleteBbsCtgry(Model model, HttpServletRequest request, Bbs vo){
		
		// IP가져오기
		String ip = RequestUtils.remoteAddr(request);
		vo.setUpdIp(ip);
		vo.setDelYn(Constants.YES);
		bbsCtgryService.updateBbsCtgry(vo);
		
		return "redirect:/bbs/ctgry/list";
	}

	/**
	 * 게시판 카테고리 권한 조회
	 *
	 * @param vo
	 * @param ctgryIdx
	 * @return ModelAndView
	*/
	@PostMapping(value="/bbs/ctgry/grant")
	public ModelAndView getBbsCtgryGrant(Bbs vo
			, @RequestParam(name="ctgryIdx", required=false) int ctgryIdx
			, @ModelAttribute Paging paging){
		
		ModelAndView mav = new ModelAndView("jsonView");
		vo.setCtgryIdx(ctgryIdx);
		mav.addObject("resultGrant", bbsCtgryService.selectCtgryGrant(vo));
		return mav;
	}
}