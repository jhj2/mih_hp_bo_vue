package com.mih.bo.task.bbs.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mih.bo.common.Constants;
import com.mih.bo.common.enums.FileAttachTypes;
import com.mih.bo.common.service.FileService;
import com.mih.bo.common.util.PagingUtil;
import com.mih.bo.common.util.SecuritySessionUtils;
import com.mih.bo.common.vo.FileUpload;
import com.mih.bo.common.vo.Paging;
import com.mih.bo.dao.BbsDao;
import com.mih.bo.dao.FileDao;
import com.mih.bo.task.bbs.service.BbsService;
import com.mih.bo.task.bbs.vo.Bbs;

/**
 * 통합게시판 관리 서비스 구현체
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.01.26.
 */
@Service
public class BbsServiceImpl implements BbsService{
	
	private static final Logger log = LoggerFactory.getLogger(BbsServiceImpl.class);
	
	@Autowired
	private BbsDao bbsDao;
	
	// 파일처리 관련 유틸리티 import
	@Autowired
	private FileService fileService;
	
	@Autowired
	private FileDao fileDao;

	
	/*
	 * @see com.mih.bo.task.bbs.service.BbsService#selectBbsView(com.mih.bo.task.bbs.vo.Bbs)
	*/
	public Bbs selectBbsView(Bbs vo) {
		return bbsDao.selectBbsView(vo);
	}
	
	/*
	 * @see com.mih.bo.task.bbs.service.BbsService#selectBbsCmtList(int)
	*/
	public List<?> selectBbsCmtList(Bbs vo) {
		return bbsDao.selectBbsCmtList(vo);
	}
	
	public List<?> selectBbsReCmtList(List<Bbs> voList) {
		for(int idx=0;idx<voList.size();idx++){
			if(voList.get(idx).isReBbsFlag()){
				List<?> bbsReCmtList = selectBbsReCmtList(bbsDao.selectBbsReCmtList(voList.get(idx)));
				voList.addAll(idx+1, (List<Bbs>)bbsReCmtList);
				idx += bbsReCmtList.size();
			}
		}
		return voList;
	}

	public List<?> insertReCmt(Bbs vo){
		bbsDao.insertReCmt(vo);
		return bbsDao.selectBbsCmtList(vo);
	}

	/*
	 * @see com.mih.bo.task.bbs.service.BbsService#ctgryList(com.mih.bo.task.bbs.vo.Bbs)
	*/
	@Override
	public List<?> ctgryList(Bbs vo) {
		return bbsDao.ctgryList(vo);
	}
	
	/*
	 * @see com.mih.bo.task.bbs.service.BbsService#selectBbsList(com.mih.bo.task.bbs.vo.Bbs, com.mih.bo.common.vo.Paging)
	*/
	@Override
	public List<?> selectBbsList(Bbs vo, Paging paging){
		int cnt = vo.getTotCnt();	// 총 count 개수조회
		if(cnt > 0){
			paging.setTotalRecordCount(cnt);
			paging.setFormName("bbsFrm");	// formName값 세팅
			PagingUtil.setPagination(paging);
			
			vo.setLimit(paging.getLimit());
			vo.setOffset(paging.getOffset());
		}
		return bbsDao.selectBbsList(vo);
	}
	
	@Override
	public List<Bbs> selectReBbsList(List<Bbs> voList){
		for(int idx=0;idx<voList.size();idx++){
			if(voList.get(idx).isReBbsFlag()){
				List<Bbs> reBbsList = selectReBbsList(bbsDao.selectReBbsList(voList.get(idx)));
				voList.addAll(idx+1, reBbsList);
				idx += reBbsList.size();
			}
		}
		return voList;
	}

	/*
	 * @see com.mih.bo.task.bbs.service.BbsService#selectBbs(com.mih.bo.task.bbs.vo.Bbs)
	*/
	@Override
	public Bbs selectBbs(Bbs vo){
		return bbsDao.selectBbs(vo);
	}

	/*
	 * @see com.mih.bo.task.bbs.service.BbsService#insertBbs(com.mih.bo.task.bbs.vo.Bbs)
	*/
	@Override
	public void insertBbs(Bbs vo){
		String userId = SecuritySessionUtils.getUser().getUserId();
		vo.setRegId(userId);
		bbsDao.insertBbs(vo);
		
		// 파일 업로드(여러개) -> 첨부파일 저장
		int idx = 0;
		if(null != vo.getFiles()){
			for(MultipartFile file : vo.getFiles()){
				
				++idx;
				// 파일명 null값 체크
				if(!"".equals(file.getOriginalFilename())){
					FileUpload result = fileService.uploadFile(file, FileAttachTypes.BBS);
					log.info(result.toJsonPrettify());
					
					result.setFileTableIdx(vo.getBbsIdx());
					result.setUseYn(Constants.YES);
					result.setFileSort(idx);
					fileDao.insertFileAttach(result);
				}
			}
		}
	}

	/*
	 * @see com.mih.bo.task.bbs.service.BbsService#updateBbs(com.mih.bo.task.bbs.vo.Bbs)
	*/
	@Override
	public void updateBbs(Bbs vo){
		String userId = SecuritySessionUtils.getUser().getUserId();
		
		// 파일 업로드(여러개) -> 첨부파일 저장
		int idx = 0;
		if(null != vo.getFiles()){
			for(MultipartFile file : vo.getFiles()){
				log.info("vo.getFSort().get(idx)====="+vo.getFSort().get(idx));
				
				// 파일명 null값 체크
				if(!"".equals(file.getOriginalFilename())){
					FileUpload result = fileService.uploadFile(file, FileAttachTypes.BBS);
					log.info(result.toJsonPrettify());
					
					result.setFileTableIdx(vo.getBbsIdx());
					result.setUseYn(Constants.YES);
					result.setFileSort(Integer.parseInt(vo.getFSort().get(idx).toString()));
					fileDao.insertFileAttach(result);
				}
				idx++;
			}
		}
		
		vo.setUpdId(userId);
		bbsDao.updateBbs(vo);
	}

	/*
	 * @see com.mih.bo.task.bbs.service.BbsService#updateBbsCmt(com.mih.bo.task.bbs.vo.Bbs)
	*/
	@Override
	public void updateBbsCmt(Bbs vo){
		String userId = SecuritySessionUtils.getUser().getUserId();
		vo.setUpdId(userId);
		bbsDao.updateBbsCmt(vo);
	}
}