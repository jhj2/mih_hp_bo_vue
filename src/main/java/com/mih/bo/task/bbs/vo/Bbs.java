package com.mih.bo.task.bbs.vo;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mih.bo.common.AbstractModel;
import lombok.Data;

/**
 * 게시판 카테고리, 게시판
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2022.01.26.
 */
@Data
public class Bbs extends AbstractModel{
	private static final long serialVersionUID = -6062343006993705364L;
	
	private int totCnt;
	
	/**
	 * 게시판 카테고리
	 */
	private int ctgryIdx;
	private String ctgryNm;
	private String sort;
	private String useYn;
	private String useYnNm;
	private String delYn;
	private String fileUseYn;
	private String filePermCnt;
	private String filePermBt;
	private String viewFilePermBt;
	private String ansUseYn;
	private String cmtUseYn;
	private String ediUseYn;
	private String cmtOrdWay;
	private String cmtOrdWayNm;
	private String rdAuth;
	private String wtAuth;
	private String wtPw;
	
	private String viewDelFlag;
	
	/**
	 * 상위 게시글 IDX
	 */
	private long ref;
	/**
	 * 게시판 DEPTH
	 */
	private long depth;
	/**
	 * 답글 여부
	 */
	private boolean reBbsFlag;
    /**
     * 답글 사용여부
     */
	private String repUseYn;

	/**
	 * 공지설정개수
	 */
	private int notiCnt;
	
	/**
	 * 게시판
	 */
	private int bbsIdx;
	private int bbsUpperIdx;
	private String bbsLvl;
	private String bbsSj;
	private String bbsCn;
	private String viewBbsCn;	// [Front] 게시물 내용
	
	/**
	 * 게시판 댓글
	 */
	private int bbsCmtIdx;
	private String bbsCmtCn;
	
	/**
	 * 검색조건
	 */
	private String searchCtgryNm;
	private String searchData;
	private String searchCondition;
	
	private String commCd;
	private String commCdNm;
	
	private String searchCtgryIdx;
	
	/**
	 * 첨부파일(TB_CM_FILE)
	 */
	private List<MultipartFile> files;
	private List<?> fSort;
	
	private String fileIdx;	// 파일
	private String fileTableNm;	// 파일테이블명
	private String fileTableIdx;	// 파일테이블IDX
	private String sysPath;	// 파일시스템경로
	private String attPath;	// 파일첨부경로
	private String fileOrgNm;	// 파일원본명
	private String fileSysNm;	// 파일시스템명
	private long fileSize;	// 파일크기(KB)
	private String fileExt;	// 파일확장자
	private int fileSort;	// 파일정렬순서
	private String fileReplcText;	// 파일대체문구
	private String fileDwldCnt;	// 파일다운로드건수
}