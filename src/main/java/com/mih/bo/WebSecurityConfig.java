package com.mih.bo;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.mih.bo.security.filter.CustomAuthenticationFilter;
import com.mih.bo.security.handler.LoginFailureHandler;
import com.mih.bo.security.handler.LoginSuccessHandler;
import com.mih.bo.security.handler.LogoutHandler;
import com.mih.bo.security.handler.LogoutSuccessHandler;
import com.mih.bo.security.provider.CustomAuthenticationProvider;

import lombok.RequiredArgsConstructor;

/**
 * WebSecurityConfig
 *
 * @author madeinheaven
 * @version 1.0.0
 * @since 2021. 9. 10.
 */
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    /*
     * @see org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter#configure(org.springframework.security.config.annotation.web.builders.WebSecurity)
    */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().requestMatchers(PathRequest.toStaticResources().atCommonLocations());
    }

    /*
     * @see org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter#configure(org.springframework.security.config.annotation.web.builders.HttpSecurity)
    */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        	http.csrf().disable()
        		.authorizeRequests()
        		.antMatchers("/main").authenticated()
                .antMatchers("/member/**").authenticated()
                .antMatchers("/cabinet/**").authenticated()
                .antMatchers("/payment/**").authenticated()
                .antMatchers("/branch/**").authenticated()
                .antMatchers("/complaint/**").authenticated()
                .antMatchers("/site/**").authenticated()
                .antMatchers("/stplat/**").authenticated()
                .antMatchers("/sample/**").authenticated()
//                .antMatchers("/admin").hasRole("ADMIN")
                .anyRequest().permitAll()
                .and()
                .headers().frameOptions().sameOrigin()
                .and()
            .formLogin()
                .loginPage("/user/login")
                .permitAll()
                .and()
                .addFilterBefore(customAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
//        	http.sessionManagement().maximumSessions(1).expiredUrl("/login?expired=true");
        	http.logout()
        		.logoutUrl("/user/logout")
        		.deleteCookies("JSESSIONID")
        		.addLogoutHandler(logoutHandler())
        		.logoutSuccessHandler(logoutSuccessHandler());
    }

    /**
     * BCryptPasswordEncoder
     *
     * @return BCryptPasswordEncoder
    */
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * CustomAuthenticationFilter
     *
     * @return
     * @throws Exception CustomAuthenticationFilter
    */
    @Bean
    public CustomAuthenticationFilter customAuthenticationFilter() throws Exception {
        CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManager());
        customAuthenticationFilter.setFilterProcessesUrl("/user/authen");
        customAuthenticationFilter.setAuthenticationSuccessHandler(loginSuccessHandler());
        customAuthenticationFilter.setAuthenticationFailureHandler(loginFailureHandler());
        customAuthenticationFilter.afterPropertiesSet();
        return customAuthenticationFilter;
    }

    /**
     * LoginSuccessHandler
     *
     * @return LoginSuccessHandler
    */
    @Bean
    public LoginSuccessHandler loginSuccessHandler() {
        return new LoginSuccessHandler();
    }
    
    /**
     * LoginFailureHandler
     *
     * @return LoginFailureHandler
    */
    @Bean
    public LoginFailureHandler loginFailureHandler() {
        return new LoginFailureHandler();
    }
    
    /**
     * LogoutSuccessHandler
     *
     * @return LogoutSuccessHandler
    */
    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new LogoutSuccessHandler();
    }
    
    /**
     * LogoutHandler
     *
     * @return LogoutHandler
    */
    @Bean
    public LogoutHandler logoutHandler() {
        return new LogoutHandler();
    }

    /**
     * CustomAuthenticationProvider
     *
     * @return CustomAuthenticationProvider
    */
    @Bean
    public CustomAuthenticationProvider customAuthenticationProvider() {
        return new CustomAuthenticationProvider(userDetailsService, bCryptPasswordEncoder());
    }

    /*
     * @see org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter#configure(org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder)
    */
    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) {
        authenticationManagerBuilder.authenticationProvider(customAuthenticationProvider());
    }

}